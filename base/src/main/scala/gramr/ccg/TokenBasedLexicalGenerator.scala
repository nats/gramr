package gramr.ccg

import gramr.parse.SyntaxInformation
import gramr.text._

trait TokenBasedLexicalGenerator[MR] extends LexicalGenerator[MR] {

  override def generate(syntax: SyntaxInformation): Set[GeneratedLexicalItem[MR]] = generate(syntax.tokens)

  def generate(tokens: Vector[Token]): Set[GeneratedLexicalItem[MR]]
}
