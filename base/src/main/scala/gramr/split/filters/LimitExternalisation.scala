package gramr
package split
package filters

import gramr.text.Token
import gramr.graph._

import Graph.ops._

case class LimitExternalisation[MR : Graph](limit: Int) extends SplitFilter[MR] {

  def apply(split: Split[MR], sentence: Vector[Token]): Boolean = {
    pred(split.left) && pred(split.right)
  }

  def pred(partialMR: PartialMR[MR]): Boolean = {
    partialMR.mr.externalLayers.forall(_._2.size <= limit)
  }

}
