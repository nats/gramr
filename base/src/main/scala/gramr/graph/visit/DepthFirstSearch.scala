package gramr.graph
package visit

import scala.collection.mutable
import Graph.ops._

import scala.annotation.tailrec

class DepthFirstSearch[MR: Graph](
  adjacentEdges: Int => Iterable[Indexed[Edge]]
) {
  /** Conducts depth-first search.
    *
    * @param graph the graph to traverse
    * @param visitor the visitor to call for every visited element
    * @param startNode if provided, DFS will start at this node; otherwise, it starts at the root
    * @param restart if true, search will be restarted if necessary to ensure all nodes are visited in unconnected
    *                graphs
    */
  def apply(graph: MR, visitor: Visitor, startNode: Option[Int] = None, restart: Boolean = false, pass: Pass = pass): Unit = {
    if(restart) {
      restarting(graph, visitor, startNode = startNode, pass = pass)
    }
    else {
      pass(graph, visitor, startNode)
    }
  }

  /** Runs DFS until all nodes have been visited.
    *
    * The first DFS pass is started from `startNode`, or the root, if `startNode` has not been supplied.
    * If the first DFS pass did not visit all nodes because the graph is not connected, DFS is restarted from the
    * lowest-indexed unvisited node. This is repeated until all nodes have been visited.
    *
    * @param graph the graph to traverse
    * @param visitor the visitor to call for every visited element
    * @param startNode if provided, DFS will start at this node; otherwise, it starts at the root
    */
  def restarting(graph: MR, visitor: Visitor, startNode: Option[Int] = None, pass: Pass = pass): Unit = {
    val tracker = new UnvisitedNodesVisitor(graph)
    val combined = Visitor.combine(visitor, tracker)

    @tailrec
    def nextPass(startNode: Option[Int]): Unit = {
      pass(graph, combined, startNode)
      tracker.nextNode match {
        case Some(next) => nextPass(Some(next))
        case None => ()
      }
    }
    nextPass(startNode)
  }


  /** Passes implement the core DFS functionality. The arguments are:
    *
    * - Meaning representation to visit
    * - Visitor to call
    * - Optional node to start at (root if not given)
    */
  type Pass = (MR, Visitor, Option[Int]) => Unit

  /** Conducts a single standard DFS pass.
   *
   * @param graph the graph to traverse
   * @param visitor the visitor to call for every visited element
   * @param startNode if provided, DFS will start at this node; otherwise, it starts at the root
   */
  def pass(graph: MR, visitor: Visitor, startNode: Option[Int] = None): Unit = {
    val visited = mutable.Set.empty[Int]

    def continueFrom(nodeIndex: Int): Unit = {
      if(!visited.contains(nodeIndex)) {
        visited.add(nodeIndex)
        visitor.visitNode(nodeIndex)
        adjacentEdges(nodeIndex).foreach {
          case Indexed(edge, edgeIndex) =>
            if(!visited.contains(edgeIndex)) {
              visited.add(edgeIndex)
              visitor.visitEdge(edgeIndex)
              if(edge.target == nodeIndex) {
                continueFrom(edge.source)
              }
              else {
                continueFrom(edge.target)
              }
            }
        }
      }
    }

    if(!graph.isEmpty) {
      val first = startNode.getOrElse(graph.root)
      continueFrom(first)
    }
  }

  /** Implements DFS which continues for a fixed number of steps after reaching a visited node.
    *
    * This algorithm can be used to enumerate all paths of length n.
    *
    * @param extraSteps number of steps to continue after reaching a visited node.
    * @param graph the graph to traverse
    * @param visitor the visitor to call for every visited element
    * @param startNode if provided, DFS will start at this node; otherwise, it starts at the root
    */
  def plusNPass(extraSteps: Int)(graph: MR, visitor: Visitor, startNode: Option[Int] = None): Unit = {
    val visited = mutable.Set.empty[Int]

    def continueFrom(nodeIndex: Int, remainingSteps: Option[Int] = None): Unit = {
      if(remainingSteps.forall(_ > 0)) {
        // Calculate limit for the next step
        // If there is already a limit, reduce it; otherwise, see if we need to introduce one
        val limit = remainingSteps.map(_ - 1).orElse { if(visited.contains(nodeIndex)) Some(extraSteps) else None }
        visited.add(nodeIndex)
        visitor.visitNode(nodeIndex)
        if(limit.forall(_ > 0)) {
          adjacentEdges(nodeIndex).foreach {
            case Indexed(edge, edgeIndex) =>
              visited.add(edgeIndex)
              visitor.visitEdge(edgeIndex)
              if (edge.target == nodeIndex) {
                continueFrom(edge.source, limit)
              }
              else {
                continueFrom(edge.target, limit)
              }
          }
        }
      }
    }

    if(!graph.isEmpty) {
      val first = startNode.getOrElse(graph.root)
      continueFrom(first)
    }
  }
}


/** Implements depth-first search algorithms. */
object DepthFirstSearch {
  /** Runs a depth-first search on the graph.
    *
    * Follows both inbound and outbound edges from the start node in depth-first order.
    *
    * @param graph the graph to search on
    * @param visitor visitor object that is called for every visited node
    * @param startNode if provided, DFS will start at this node; otherwise, it starts at the root
    * @param restart if true, search will be restarted if necessary to ensure all nodes are visited in unconnected
    *                graphs
    */
  def apply[MR: Graph](
    graph: MR,
    visitor: Visitor,
    startNode: Option[Int] = None,
    restart: Boolean = false
  ): Unit = {
    val algo = new DepthFirstSearch[MR]((nodeIndex) => graph.adjacentEdges(nodeIndex))
    algo.apply(graph, visitor, startNode = startNode, restart = restart)
  }

  /** Runs a depth-first search on the graph, visiting edges by lexicographic order of their labels.
    *
    * Follows both inbound and outbound edges from the start node in depth-first order.
    *
    * @param graph the graph to search on
    * @param visitor visitor object that is called for every visited node
    * @param startNode if provided, DFS will start at this node; otherwise, it starts at the root
    * @param restart if true, search will be restarted if necessary to ensure all nodes are visited in unconnected
    *                graphs
    */
  def labelOrdered[MR: Graph](
    graph: MR,
    visitor: Visitor,
    startNode: Option[Int] = None,
    restart: Boolean = false
  ): Unit = {
    val algo = new DepthFirstSearch[MR](nodeIndex => graph.adjacentEdges(nodeIndex).toVector.sortBy(_.element.label))
    algo.apply(graph, visitor, startNode = startNode, restart = restart)
  }

  /** Runs a depth-first search on the graph with extra steps.
    *
    * Follows only outbound edges from the start node in depth-first order.
    *
    * @param graph the graph to search on
    * @param visitor visitor object that is called for every visited node
    * @param extraSteps number of steps to continue after encountering an already visited node
    * @param startNode if provided, DFS will start at this node; otherwise, it starts at the root
    * @param restart if true, search will be restarted if necessary to ensure all nodes are visited in unconnected
    *                graphs
    */
  def forwardEdgesPlusN[MR: Graph](
    graph: MR,
    visitor: Visitor,
    extraSteps: Int,
    startNode: Option[Int] = None,
    restart: Boolean = false
  ): Unit = {
    val algo = new DepthFirstSearch[MR]((nodeIndex) => graph.outEdges(nodeIndex))
    algo.apply(graph, visitor, startNode = startNode, restart = restart, pass = algo.plusNPass(extraSteps))
  }
}
