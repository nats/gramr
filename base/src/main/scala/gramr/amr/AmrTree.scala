package gramr.amr

import scala.collection.immutable.Map
import scala.util.parsing.combinator._
import scala.util.parsing.input.CharSequenceReader
import gramr.graph.Attribute

/**
  * A linearised AMR. The shape of this structure directly mirrors the linearised form.
  */
sealed trait AmrTree {
  def label: String

  def attributes: Map[String, Attribute]

  override def toString: String = AmrTree.format(this)

  def setAttribute(name: String, value: Attribute): AmrTree
}

case class InnerNode(
  label: String,
  children: List[Link],
  attributes: Map[String, Attribute] = Map.empty
) extends AmrTree {

  def setAttribute(name: String, value: Attribute): InnerNode = copy(attributes = attributes + (name -> value))

}

case class Leaf(
  label: String,
  attributes: Map[String, Attribute] = Map.empty
) extends AmrTree {

  def setAttribute(name: String, value: Attribute): Leaf = copy(attributes = attributes + (name -> value))

}

case class EmptyAmrTree(
  attributes: Map[String, Attribute] = Map.empty
) extends AmrTree {

  def label: String = "ε"

  def setAttribute(name: String, value: Attribute): EmptyAmrTree = copy(attributes = attributes + (name -> value))

}

case class Link(
  label: String,
  subTree: AmrTree,
  attributes: Map[String, Attribute] = Map.empty
) {

  def setAttribute(name: String, value: Attribute): Link = copy(attributes = attributes + (name -> value))

}

object AmrTree {

  def format(amr: AmrTree): String = {
    val indentation = "    "

    def formatIndented(amr: AmrTree, depth: Int): String = amr match {
      case InnerNode(label, children, _) =>
        val headLine = children.find(_.label == "instance") match {
          case Some(concept) =>
            s"($label / ${concept.subTree.asInstanceOf[Leaf].label}"
          case None =>
            s"($label"
        }
        val links = children.filter(_.label != "instance").map {
          l => (indentation * (depth + 1)) + s":${l.label} ${formatIndented(l.subTree, depth + 1)}"
        }
        (headLine :: links).mkString("\n") + ")"
      case Leaf(label, _) => label
      case EmptyAmrTree(_) => "ε"
    }

    formatIndented(amr, 0)
  }

}

/**
  * Parses a linearised AMR tree in ARM-Corpus form.
  */
object AmrTreeParser extends RegexParsers {
  def amr: Parser[AmrTree] =
    emptyAmr | nonEmptyAmr | singleNodeAmr

  def emptyAmr: Parser[AmrTree] =
    "ε" ^^ { _ => EmptyAmrTree() }

  def nonEmptyAmr: Parser[AmrTree] =
    "(" ~ (id ~ links | external ~ standardLinks) ~ ")" ^^ { case _ ~ (label ~ links) ~ _ => InnerNode(label, links) }

  def singleNodeAmr: Parser[AmrTree] =
    (id | quoted | source | external) ^^ { label => Leaf(label) }

  def links: Parser[List[Link]] =
    instanceLink ~ standardLinks ^^ { case i ~ links => i :: links }

  def instanceLink: Parser[Link] =
    "/" ~ (id | source) ~ alignment.? ^^ { case _ ~ prop ~ _ => Link("instance", Leaf(prop), Map.empty) }

  def standardLinks: Parser[List[Link]] =
    (standardLink ~ standardLinks).? ^^ { case Some(link ~ links) => link :: links; case None => List() }

  def standardLink: Parser[Link] =
    edge ~ alignment.? ~ (amr | leaf) ^^ { case e ~ _ ~ t => Link(e, t, Map.empty) }

  def leaf: Parser[AmrTree] =
    (id | quoted | source | external) ~ alignment.? ^^ { case id ~ _ => Leaf(id) }

  def id: Parser[String] =
    """[\w-+\.\']+""".r

  def quoted: Parser[String] =
    "\".*?\"".r

  def source: Parser[String] =
    """<[\w-]+>""".r

  def external: Parser[String] =
    """<\d+?>""".r

  def edge: Parser[String] =
    """:([\w-<>]+)""".r ^^ {
      _.drop(1)
    }

  def alignment: Parser[String] =
    """~\w\.\d+(,\d+)*""".r

  def parseString(s: String): AmrTree = {
    phrase(AmrTreeParser.amr)(new CharSequenceReader(s)) match {
      case Success(tree, _) => tree
      case NoSuccess(msg, next) =>
        val context = next.source.toString
        val offset = next.offset
        throw ParseError(msg, context, offset)
    }
  }

  case class ParseError(msg: String, context: String, offset: Int) extends Throwable {
    override def getMessage: String = "Error parsing AMR: " + msg + " at " +
      context.substring(Math.max(0, offset - 20), offset) +
      "<ERROR>" +
      context.substring(offset, Math.min(offset + 20, context.length))
  }

}

