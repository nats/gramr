package gramr.parse.evaluate

import gramr.util.SimpleRetrievalStats

case class BatchParseStats(
  parses: Vector[ParseStats]
) {

  val total: Int = parses.size

  val successful: Int = parses.count(_.successful)

  def successfulParses: Iterable[ParseStats] = parses.filter(_.successful)

  val bestScoredParses: SimpleRetrievalStats = parses.map(_.bestScoredParse).fold(SimpleRetrievalStats.empty)(_ + _)

  val bestFoundParses: SimpleRetrievalStats = parses.map(_.bestFoundParse).fold(SimpleRetrievalStats.empty)(_ + _)

  override def toString: String = {
    s"$successful/$total successful; top f1: ${bestScoredParses.f1}; best found f1: ${bestFoundParses.f1}"
  }

}
