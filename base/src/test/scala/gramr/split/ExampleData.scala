package gramr.split

import gramr.ccg.EasyccgCombinatorSystem._
import gramr.ccg._
import gramr.construction.SyntacticContext
import gramr.construction.sgraph.SGraphRule
import gramr.graph.edit.sgraph.Operators
import gramr.graph.{AdjacencyMeaningGraph, Graph}
import gramr.split.filters.SplitFilterTrue
import gramr.split.labelling.{CompleteNodeAlignments, LabelAlignments}
import gramr.text.{Example, HasMR, Span, Token}
import gramr.util.Parsing

object ExampleData {

  type MR = AdjacencyMeaningGraph

  implicit def meaningGraphLike: Graph[MR] = AdjacencyMeaningGraph.graphInstance

  val mgl: Graph[MR] = AdjacencyMeaningGraph.graphInstance

  val patterns = Vector(
    new SGraphRule[MR](
      "fa",
      (context: SyntacticContext) => context.combinator == FA,
      Operators.Application[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "ba",
      (context: SyntacticContext) => context.combinator == BA,
      Operators.Application[MR](),
      backward = true
    ),
    new SGraphRule[MR](
      "fc",
      (context: SyntacticContext) => context.combinator == FC,
      Operators.Application[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "fc2",
      (context: SyntacticContext) => context.combinator == FC2,
      Operators.Application[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "bxc",
      (context: SyntacticContext) => context.combinator == BXC,
      Operators.Application[MR](),
      backward = true
    ),
    new SGraphRule[MR](
      "bxc2",
      (context: SyntacticContext) => context.combinator == BXC2,
      Operators.Application[MR](),
      backward = true
    ),
    new SGraphRule[MR](
      "conj",
      (context: SyntacticContext) => context.combinator == Conj,
      Operators.Substitution[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "lp",
      (context: SyntacticContext) => context.combinator == LPunct,
      Operators.Identity[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "rp",
      (context: SyntacticContext) => context.combinator == RPunct,
      Operators.Identity[MR](),
      backward = true
    )
  )

  case class SimpleExample(
    id: String,
    sentence: Vector[Token],
    reference: MR
  ) {
    def indexedDerivations = ???
  }

  implicit val exampleInstance: Example[SimpleExample] = new Example[SimpleExample] {
    override def id(a: SimpleExample) = a.id

    override def sentence(a: SimpleExample) = a.sentence

    override def indexedDerivations(a: SimpleExample) = a.indexedDerivations
  }

  implicit val hasMRInstance: HasMR[SimpleExample, MR] = new HasMR[SimpleExample, MR] {
    override def mr(a: SimpleExample) = a.reference
  }

  type E = SimpleExample

  def t(s: String): Token = Token(s, s)
  def syn(s: String): SyntacticCategory = SyntacticCategory(s)
  def amr(s: String): AdjacencyMeaningGraph = Parsing.parseAmr(s)

  val lexicon = StoredLexicon.fromItems[MR](Iterable(
    PlainLexicalItem(Vector(t("anna")), syn("NP"), amr("(a / anna)")),  // 1
    PlainLexicalItem(Vector(t("bettina")), syn("NP"), amr("(b / bettina)")),  // 2
    PlainLexicalItem(Vector(t("manny")), syn("NP"), amr("(m / manny)")),  // 3
    PlainLexicalItem(Vector(t("hubert")), syn("NP"), amr("(h / hubert)")),  // 4
    PlainLexicalItem(Vector(t("married")), syn("""(S\NP)/NP"""), amr("(m / marry :ARG0 <0> :ARG1 <1>)")),  // 5
    PlainLexicalItem(Vector(t("met")), syn("""(S\NP)/NP"""), amr("(m / meet :ARG0 <0> :ARG1 <1>)")),  // 6
    PlainLexicalItem(Vector(t("married"), t("bettina")), syn("""S\NP"""), amr("(m / marry :ARG0 <0> :ARG1 (b / bettina))")),  // 7
    PlainLexicalItem(Vector(t("married"), t("hubert")), syn("""S\NP"""), amr("(m / marry :ARG0 <0> :ARG1 (h / hubert))")),  // 8
    PlainLexicalItem(Vector(t("married"), t("manny")), syn("""S\NP"""), amr("(m / marry :ARG0 <0> :ARG1 (m2 / manny))")),  // 9
    PlainLexicalItem(Vector(t("met"), t("anna")), syn("""S\NP"""), amr("(m / meet :ARG0 <0> :ARG1 (a / anna))")),  // 10
    PlainLexicalItem(Vector(t("met"), t("bettina")), syn("""S\NP"""), amr("(m / meet :ARG0 <0> :ARG1 (b / bettina))")),  // 11
    PlainLexicalItem(Vector(t("met"), t("hubert")), syn("""S\NP"""), amr("(m / meet :ARG0 <0> :ARG1 (h / hubert))")),  // 12
    PlainLexicalItem(Vector(t("met"), t("manny")), syn("""S\NP"""), amr("(m / meet :ARG0 <0> :ARG1 (m2 / manny))")),  // 13
    PlainLexicalItem(Vector(t("anna"), t("married"), t("hubert")), syn("""S"""), amr("(m / marry :ARG0 (a / anna) :ARG1 (h / hubert))")),  // 14
    PlainLexicalItem(Vector(t("anna"), t("married"), t("manny")), syn("""S"""), amr("(m / marry :ARG0 (a / anna) :ARG1 (m2 / manny))")),  // 15
    PlainLexicalItem(Vector(t("hubert"), t("married"), t("manny")), syn("""S"""), amr("(m / marry :ARG0 (h / hubert) :ARG1 (m2 / manny))")),  // 16
    PlainLexicalItem(Vector(t("manny"), t("married"), t("bettina")), syn("""S"""), amr("(m / marry :ARG0 (m2 / manny) :ARG1 (b / bettina))")),  // 17
    PlainLexicalItem(Vector(t("bettina"), t("met"), t("anna")), syn("""S"""), amr("(m / meet :ARG0 (b / bettina) :ARG1 (a / anna))")),  // 18
    PlainLexicalItem(Vector(t("manny"), t("met"), t("hubert")), syn("""S"""), amr("(m / meet :ARG0 (m2 / manny) :ARG1 (h / hubert))"))  // 19
  ))

  val derivations = Map(
    "anna" -> Vector(("0", Derivation(
      LexicalStep(Vector(t("anna")), syn("NP"), Span(0, 0), ())
    ))),
    "anna married manny" -> Vector(("0", Derivation(
      CombinatoryStep(BA, List(
        LexicalStep(Vector(t("anna")), syn("NP"), Span(0, 0), ()),
        CombinatoryStep(FA, List(
          LexicalStep(Vector(t("married")), syn("""(S\NP)/NP"""), Span(1, 1), ()),
          LexicalStep(Vector(t("manny")), syn("NP"), Span(2, 2), ())
        ), syn("""S\NP"""), Span(1, 2), ())
      ), syn("""S"""), Span(0, 2), ())
    ))),
    "anna married hubert" -> Vector(("0", Derivation(
      CombinatoryStep(BA, List(
        LexicalStep(Vector(t("anna")), syn("NP"), Span(0, 0), ()),
        CombinatoryStep(FA, List(
          LexicalStep(Vector(t("married")), syn("""(S\NP)/NP"""), Span(1, 1), ()),
          LexicalStep(Vector(t("hubert")), syn("NP"), Span(2, 2), ())
        ), syn("""S\NP"""), Span(1, 2), ())
      ), syn("""S"""), Span(0, 2), ())
    ))),
    "hubert married manny" -> Vector(("0", Derivation(
      CombinatoryStep(BA, List(
        LexicalStep(Vector(t("hubert")), syn("NP"), Span(0, 0), ()),
        CombinatoryStep(FA, List(
          LexicalStep(Vector(t("married")), syn("""(S\NP)/NP"""), Span(1, 1), ()),
          LexicalStep(Vector(t("manny")), syn("NP"), Span(2, 2), ())
        ), syn("""S\NP"""), Span(1, 2), ())
      ), syn("""S"""), Span(0, 2), ())
    ))),
    "manny married bettina" -> Vector(("0", Derivation(
      CombinatoryStep(BA, List(
        LexicalStep(Vector(t("manny")), syn("NP"), Span(0, 0), ()),
        CombinatoryStep(FA, List(
          LexicalStep(Vector(t("married")), syn("""(S\NP)/NP"""), Span(1, 1), ()),
          LexicalStep(Vector(t("bettina")), syn("NP"), Span(2, 2), ())
        ), syn("""S\NP"""), Span(1, 2), ())
      ), syn("""S"""), Span(0, 2), ())
    ))),
    "bettina met anna" -> Vector(("0", Derivation(
      CombinatoryStep(BA, List(
        LexicalStep(Vector(t("bettina")), syn("NP"), Span(0, 0), ()),
        CombinatoryStep(FA, List(
          LexicalStep(Vector(t("met")), syn("""(S\NP)/NP"""), Span(1, 1), ()),
          LexicalStep(Vector(t("anna")), syn("NP"), Span(2, 2), ())
        ), syn("""S\NP"""), Span(1, 2), ())
      ), syn("""S"""), Span(0, 2), ())
    ))),
    "manny met hubert" -> Vector(("0", Derivation(
      CombinatoryStep(BA, List(
        LexicalStep(Vector(t("manny")), syn("NP"), Span(0, 0), ()),
        CombinatoryStep(FA, List(
          LexicalStep(Vector(t("met")), syn("""(S\NP)/NP"""), Span(1, 1), ()),
          LexicalStep(Vector(t("hubert")), syn("NP"), Span(2, 2), ())
        ), syn("""S\NP"""), Span(1, 2), ())
      ), syn("""S"""), Span(0, 2), ())
    ))),
  )

  val examples = Map(
    "anna" -> SimpleExample("anna", Vector(t("anna")), amr("(a / anna)")),
    "anna married manny" ->
      SimpleExample("anna married manny", Vector(t("anna"), t("married"), t("manny")), amr("(m / marry :ARG0 (a / anna) :ARG1 (m2 / manny))")),
    "anna married hubert" ->
      SimpleExample("anna married hubert", Vector(t("anna"), t("married"), t("hubert")), amr("(m / marry :ARG0 (a / anna) :ARG1 (h / hubert))")),
    "hubert married manny" ->
      SimpleExample("hubert married manny", Vector(t("hubert"), t("married"), t("manny")), amr("(m / marry :ARG0 (h / hubert) :ARG1 (m2 / manny))")),
    "manny married bettina" ->
      SimpleExample("manny married bettina", Vector(t("manny"), t("married"), t("bettina")), amr("(m / marry :ARG0 (m2 / manny) :ARG1 (b / bettina))")),
    "bettina met anna" ->
      SimpleExample("bettina met anna", Vector(t("bettina"), t("met"), t("anna")), amr("(m / meet :ARG0 (b / bettina) :ARG1 (a / anna))")),
    "manny met hubert" ->
      SimpleExample("manny met hubert", Vector(t("manny"), t("met"), t("hubert")), amr("(m / meet :ARG0 (m2 / manny) :ARG1 (h / hubert))"))
  )

  def exampleDerivationPairs(key: String): Iterable[(E, (String, Derivation[Unit]))] = {
    derivations(key).map {
      case (name, derivation) =>
        (examples(key), (name, derivation))
    }
  }

  val splitGeneratorFactory = (example: E) => new PatternBasedSplitGenerator[MR](
    LabelAlignments[MR]() andThen CompleteNodeAlignments[MR](),
    patterns
  )

  val sentenceSplitter = new RecursiveSplitting[MR, E](
    splitGeneratorFactory,
    SplitFilterTrue[MR]()
  )
}
