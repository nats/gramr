package gramr
package split

import ccg._

trait SplitGenerator[MR] {

  def splits(partialMR: PartialMR[MR], derivation: Derivation[Unit]): Stream[Split[MR]]

}
