package gramr.util

import gramr.util.VectorUtils.Bin
import org.scalatest.FlatSpec

class VectorUtilsSpec extends FlatSpec {

  "Binning" should "create all the expected combinations" in {

    val items: Vector[String] = Vector("abc", "bcd", "cde")

    val bins: Vector[Bin[String]] = Vector(
      Bin(_.startsWith("a"), min = Some(1)),
      Bin({s => s.startsWith("a") || s.startsWith("b")}, max = Some(1)),
      Bin(_ => true, max = Some(2))
    )

    val binnings = VectorUtils.binnings(items, bins).toVector

    assert(binnings == Vector(
      Vector(Vector("abc"), Vector("bcd"), Vector("cde")),
      Vector(Vector("abc"), Vector(), Vector("bcd", "cde"))
    ))

  }

}