package gramr.parse
import gramr.parse.chart.Chart
import gramr.text.{Example, HasMR, Token}
import Example.ops._
import com.typesafe.scalalogging.LazyLogging

/** A parser that parses each example only once and memorises the produced parse chart.
  *
  * On subsequent calls for the same example (identified by its ID), the memorised chart is returned.
  *
  * @param underlying parser to use for the first parse
  * @tparam MR type of meaning representation
  * @tparam FV type of feature vector
  * @tparam C type of example
  */
class MemorisingParser[MR, FV, C: Example](
  underlying: Parser[MR, FV, C]
)(implicit hasMR: HasMR[C, MR]) extends Parser[MR, FV, C] with LazyLogging {

  private var parses = Map.empty[String, Chart[MR, FV]]

  override def parse(sentence: Vector[Token], context: C): Chart[MR, FV] = {
    parses.get(context.id) match {
      case Some(chart) =>
        logger.trace(s"Retrieved stored parse chart for ${context.id}")
        chart
      case None =>
        val chart = underlying.parse(sentence, context)
        synchronized {
          parses = parses + (context.id -> chart)
        }
        logger.trace(s"Stored new preparsed chart for ${context.id}")
        chart
    }
  }
}
