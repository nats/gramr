package gramr.util

import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.FileAppender
import ch.qos.logback.core.encoder.Encoder
import com.typesafe.scalalogging.LazyLogging

object Logging extends LazyLogging {

  /** Sets up logging to a log dir given by a pattern.
    *
    * The pattern passed to this file points out a path, but can contain instances of variables that are replaced
    * by this function.
    *
    * This function creates the resulting directory and sets up scalalogging to write its output to 'log.txt' in the
    * directory.
    *
    * @param pattern a path pattern
    * @return File object pointing to the created log directory
    */
  def setupLogging(pattern: String): File = {
    val targetDir = instantiateLogPattern(pattern)
    targetDir.mkdir()
    setupLogger(targetDir)
    logger.info(s"Logging to $targetDir")
    targetDir
  }

  def instantiateLogPattern(pattern: String): File = {
    val now = Calendar.getInstance.getTime
    val dateFormat = new SimpleDateFormat("YYYYMMdd")
    val timeFormat = new SimpleDateFormat("HHmmss")
    val dateString = dateFormat.format(now)
    val timeString = timeFormat.format(now)
    val stringReplaced = pattern.replace("$DATE", dateString).replace("$TIME", timeString)
    val reportPath = new File(stringReplaced)

    reportPath
  }

  def setupLogger(logDir: File): Unit = {
    val l = logger.underlying.asInstanceOf[ch.qos.logback.classic.Logger]
    val loggerContext = l.getLoggerContext
    val appender: FileAppender[ILoggingEvent] = new FileAppender[ILoggingEvent]
    appender.setContext(loggerContext)
    appender.setFile(new File(logDir, "log.txt").toString)
    appender.setAppend(false)
    val encoder: PatternLayoutEncoder = new PatternLayoutEncoder
    encoder.setContext(loggerContext)
    encoder.setPattern("%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n")
    encoder.start()
    appender.setEncoder(encoder.asInstanceOf[Encoder[ILoggingEvent]])
    appender.start()
    val logbackLogger: ch.qos.logback.classic.Logger = loggerContext.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME)
    logbackLogger.addAppender(appender)
  }

}
