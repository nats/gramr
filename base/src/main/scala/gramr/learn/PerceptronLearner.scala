package gramr.learn

import gramr.text.Example
import gramr.util.Cache

import FeatureVector.Syntax._

/** Implements the averaged perceptron algorithm.
  *
  * @param initialModel initial parameter configuration
  * @param learningRate function that determines the learning rate from the update index
  * @param decay factor to decay the parameters by before each update
  * @tparam MR type of meaning representation
  * @tparam FV type of feature vector
  * @tparam E type of example
  */
class PerceptronLearner[MR, FV: FeatureVector, E: Example](
  initialModel: LinearModel,
  val learningRate: Int => Double = 1.0 / _,
  val decay: Double = 0.0
) extends LinearLearner[MR, FV, E] {

  var nUpdates = 1

  var model: LinearModel = initialModel
  var scaledParams: LinearModel = LinearModel.empty(initialModel.featureIndex)

  protected val testModelCache: Cache[LinearModel] = Cache { () =>
    model.subtract(scaledParams.scale(1.0 / nUpdates))
  }

  protected def invalidateModels(): Unit = {
    testModelCache.invalidate()
  }

  /**
    * The (non-averaged) model for use in training.
    */
  def trainModel: LinearModel = model

  /**
    * The (averaged) model for use in testing.
    */
  def testModel: LinearModel = testModelCache.value

  /** Updates the parameters directly by adding a feature delta.
    *
    * The learning rate is taken into account
    *
    * This action is counted as one single update for learning rate / decay / averaging purposes.
    *
    * @param gradients the parameter deltas to apply
    */
  def updateGradients(gradients: Iterable[FV]): Unit = {
    val lr = learningRate(nUpdates)
    val decayFactor = 1.0 - decay * lr
    model = model.scale(decayFactor)
    gradients.foreach { gradient =>
      val delta = gradient * -1.0
      model = model.add(delta)
      scaledParams = scaledParams.add(delta * nUpdates.toDouble)
    }
    invalidateModels()

    nUpdates += 1
  }

}
