package gramr.graph

import org.scalatest._

import SubGraphOps.Implicits._

class SubGraphSpec extends FlatSpec {

  import scala.language.existentials

  val mgl: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance

  import mgl.Building._

  implicit def meaningGraphLike: Graph[AdjacencyMeaningGraph] = mgl

  "The sub-graph algorithm" should "identify true sub-graphs" in {

    val (g, n000) = AdjacencyMeaningGraph.buildReturning {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("conceptC")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
      } yield n000
    }

    // equal

    assert(g.containsGraph(g))


    // true sub-graph

    val subg = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        _ <- mkEdge("labelAB", n0, n00)
      } yield ()
    }

    assert(g.containsGraph(subg))


    // Refocused

    assert(g.containsGraph(g.refocus(n000)))

  }

  it should "identify sub-graphs with externalised nodes" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("conceptC")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
      } yield ()
    }

    val subg1 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("labelAB", n0, n00)
      } yield ()
    }

    assert(g1.containsGraph(subg1))


    // “Pure edge”

    val g2 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("conceptC")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
      } yield ()
    }

    val subg2 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkExternalNode(ExternalReference(0))
        n00 <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("labelAB", n0, n00)
      } yield ()
    }

    assert(g2.containsGraph(subg2))

  }

  it should "not be fooled by similar but different graphs" in {

    // Wrong node label

    val g1 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("conceptC")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
      } yield ()
    }

    val subg1 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("conceptD")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
      } yield ()
    }

    assert(!g1.containsGraph(subg1))


    // Too many external nodes

    val g2 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        _ <- mkEdge("labelAB", n0, n00)
      } yield ()
    }

    val subg2 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkExternalNode(ExternalReference(0))
        n00 <- mkExternalNode(ExternalReference(0))
        n000 <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
      } yield ()
    }

    assert(!g2.containsGraph(subg2))

    // Too many edges

    val g3 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        _ <- mkEdge("labelAB", n0, n00)
      } yield ()
    }

    val subg3 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelAB2", n0, n00)
      } yield ()
    }

    assert(!g3.containsGraph(subg3))

  }

  it should "correctly handle markers" in {
    val g = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("conceptC")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
      } yield ()
    }

    val subg = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("<coref>")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
      } yield ()
    }

    assert(g.containsGraph(subg))
  }

  it should "correctly handle markers that need to be resolved" in {
    val g = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        _ <- mkEdge("e1", n0, n00)
        _ <- mkEdge("e2", n0, n00)
      } yield ()
    }

    val subg = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("<coref>")
        _ <- mkEdge("e1", n0, n00)
        _ <- mkEdge("e2", n0, n000)
      } yield ()
    }

    assert(g.containsGraph(subg))
  }

  "Equivalency checks" should "work for equal graphs" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("conceptC")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
      } yield ()
    }

    assert(g1.isEquivalentTo(g1))

  }

  they should "work for refocused graphs" in {

    val (g1, n00) = AdjacencyMeaningGraph.buildReturning {
      for {
        n0 <- mkReferentNode("conceptA")
        n00 <- mkReferentNode("conceptB")
        n000 <- mkReferentNode("conceptC")
        _ <- mkEdge("labelAB", n0, n00)
        _ <- mkEdge("labelBC", n00, n000)
        _ <- normalise(false)
      } yield n00
    }

    val refocused = g1.refocus(n00)
    assert(refocused.isEquivalentTo(g1))
  }

}
