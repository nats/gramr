package gramr.graph

trait GraphBuilder[T] {

  def build: T

  def addNode(n: Node): Int

  def addEdge(e: Edge): Int

  def addEdge(
    label: String,
    source: Int,
    target: Int,
    attributes: Attributes = Attributes.empty
  ): Int = addEdge(Edge(label, source, target, attributes))

  def addReferentNode(concept: String, attributes: Attributes = Attributes.empty): Int = {
    addNode(ReferentNode(concept, attributes))
  }

  def addConstantNode(concept: String, attributes: Attributes = Attributes.empty): Int = {
    addNode(ConstantNode(concept, attributes))
  }

  def addExternalNode(reference: ExternalReference, attributes: Attributes = Attributes.empty): Int = {
    addNode(ExternalNode(reference, attributes))
  }

  def moveEdge(p: Int, newSource: Int, newTarget: Int)

  def replaceNode(p: Int, n: Node)

  def relabelEdge(p: Int, label: String)

  def replaceAttributes(p: Int, attr: Attributes)

  def remove(pointer: Int): Unit

  def setRoot(pointer: Int): Unit

  /**
    * This specialty method is needed for some unit tests and switches off
    * focusing the graph on the root.
    */
  def setNormalise(value: Boolean): Unit

}
