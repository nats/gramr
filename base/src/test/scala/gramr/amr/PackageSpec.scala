package gramr.amr

import org.scalatest.FlatSpec

class PackageSpec extends FlatSpec {

  "AMR trees with non-unique node names" should "be transformed into unique trees" in {

    val t1Input = AmrTreeParser.parseString(
      """
        |(a / a
        |   :e (a / a)
        |   :e (b / b
        |         :e a))
      """.stripMargin)

    // the reference at the bottom should resolve to the graph's root
    val t1Expected = AmrTreeParser.parseString(
      """
        |(a / a
        |   :e (a2 / a)
        |   :e (b / b
        |         :e a))
      """.stripMargin)

    val t1Output = makeNamesUnique(t1Input)

    assert(t1Expected == t1Output)


    val t2Input = AmrTreeParser.parseString(
      """
        |(a / a
        |   :e (a / x)
        |   :e (a2 / y)
        |   :e (b / b
        |         :e a2))
      """.stripMargin)

    // existing unique labels should remain unique and not be changed
    val t2Expected = AmrTreeParser.parseString(
      """
        |(a / a
        |   :e (a3 / x)
        |   :e (a2 / y)
        |   :e (b / b
        |         :e a2))
      """.stripMargin)

    val t2Output = makeNamesUnique(t2Input)

    assert(t2Expected == t2Output)


    // PROXY_AFP_ENG_20070521_0178.15
    val t3Input = AmrTreeParser.parseString(
      """
        |(b / begin-01
        |      :ARG0 (c / country :name (n / name :op1 "Estonia"))
        |      :ARG1 (b2 / bar-01
        |            :ARG0 c
        |            :ARG1 (a / access-01
        |                  :ARG1 (a6 / and
        |                        :op1 (w / website
        |                              :mod (g / government-organization
        |                                    :ARG0-of (g2 / govern-01))
        |                              :mod (k / key))
        |                        :op2 (w3 / website
        |                              :mod (p / private)
        |                              :quant (s / some)))))
        |      :time (b / before
        |            :op1 (n2 / now)
        |            :quant (t / temporal-quantity :quant 3
        |                  :unit (w2 / week)))
        |      :time (a4 / after
        |            :op1 (o / onset
        |                  :poss (a5 / attack-01
        |                        :ARG1 c
        |                        :mod (c2 / cyber)
        |                        :quant (s2 / series)))))
        |
      """.stripMargin
    )

    val t3Expected = AmrTreeParser.parseString(
      """
        |(b / begin-01
        |      :ARG0 (c / country :name (n / name :op1 "Estonia"))
        |      :ARG1 (b2 / bar-01
        |            :ARG0 c
        |            :ARG1 (a / access-01
        |                  :ARG1 (a6 / and
        |                        :op1 (w / website
        |                              :mod (g / government-organization
        |                                    :ARG0-of (g2 / govern-01))
        |                              :mod (k / key))
        |                        :op2 (w3 / website
        |                              :mod (p / private)
        |                              :quant (s / some)))))
        |      :time (b3 / before
        |            :op1 (n2 / now)
        |            :quant (t / temporal-quantity :quant 3
        |                  :unit (w2 / week)))
        |      :time (a4 / after
        |            :op1 (o / onset
        |                  :poss (a5 / attack-01
        |                        :ARG1 c
        |                        :mod (c2 / cyber)
        |                        :quant (s2 / series)))))
        |
      """.stripMargin
    )

    val t3Output = makeNamesUnique(t3Input)

    assert(t3Expected == t3Output)

  }

}
