package gramr

package object split {

  type SplitOutcome[MR] = Either[String, SplitChart[MR]]

}
