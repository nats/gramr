package gramr.util

import scala.util.matching.Regex

/** Collects functionality concerned with various ways of expressing dates and times.
  */
object DateTime {

  // Data types

  /** A (possibly partially defined) date.
    */
  case class Date(
    year: Option[Int],
    month: Option[Int],
    day: Option[Int]
  )

  object Date {
    def fromStrings(year: Option[String], month: Option[String], day: Option[String]): Date =
      Date(year.map(Integer.parseInt), month.map(Integer.parseInt), day.map(Integer.parseInt))
  }


  // Numeric dates are dates in the form 160513.
  // Month or day can be 00 to leave it out of the date.

  val numericDateRE: Regex = """(\d\d)(\d\d)(\d\d)""".r

  object numericDateMatcher {
    def unapply(s: String): Option[Date] = parseNumericDate(s)
  }

  def parseNumericDate(s: String): Option[Date] = s match {
    case numericDateRE(year, month, day) =>
      val maybeYear = Some("20" + year)
      val maybeMonth = if(month != "00") Some(month) else None
      val maybeDay = if(day != "00") Some(day) else None

      Some(Date.fromStrings(maybeYear, maybeMonth, maybeDay))
    case _ => None
  }



  // Dash dates are dates in the form "2016-05-13".

  val dashDateRE: Regex = """(\d+)-(\d+)-(\d+)""".r

  object dashDateMatcher {
    def unapply(s: String): Option[Date] = parseDashDate(s)
  }

  def parseDashDate(s: String): Option[Date] = s match {
    case dashDateRE(year, month, day) =>
      Some(Date.fromStrings(Some(year), Some(month), Some(day)))
    case _ => None
  }


  // American slash dates are dates in the form "05/13/2016".

  val slashDateRE: Regex = """(\d+)/(\d+)/(\d+)""".r

  object americanSlashDateMatcher {
    def unapply(s: String): Option[Date] = parseAmericanSlashDate(s)
  }

  def parseAmericanSlashDate(s: String): Option[Date] = s match {
    case slashDateRE(month, day, year) =>
      Some(Date.fromStrings(Some(year), Some(month), Some(day)))
    case _ => None
  }


  // Written dates in various forms that occur in English writing.

  object yearMatcher {
    val yearRE: Regex = """(\d\d)?\d\d""".r

    def unapply(x: String): Option[Int] = {
      x match {
        case yearRE(_) => Some(Integer.parseInt(x))
        case _ => None
      }
    }
  }

  object monthMatcher {
    val months = List(
      "january" -> 1,
      "february" -> 2,
      "march" -> 3,
      "april" -> 4,
      "may" -> 5,
      "june" -> 6,
      "july" -> 7,
      "august" -> 8,
      "september" -> 9,
      "october" -> 10,
      "november" -> 11,
      "december" -> 12
    )

    val monthRE: Regex = """(\w+)\.?""".r

    def unapply(x: String): Option[Int] = {
      x match {
        case monthRE(monthPrefix) =>
          val entry = months.find { case (monthName, monthNum) =>
            monthName.startsWith(monthPrefix)
          }
          entry.map(_._2)
        case _ => None
      }
    }
  }

  object dayMatcher {
    val dayRE: Regex = """(\d\d?)(st|nd|rd|th)?""".r

    def unapply(x: String): Option[Int] = {
      x match {
        case dayRE(day, _) =>
          if (Integer.parseInt(day) <= 31) Some(Integer.parseInt(day)) else None
        case _ => None
      }
    }

    object withSuffix {
      def unapply(x: String): Option[Int] = x match {
        case dayRE(day, suf) =>
          if (Integer.parseInt(day) <= 31 && !(suf eq null)) Some(Integer.parseInt(day)) else None
        case _ => None
      }
    }

  }

  def parseTextDate(tokens: Seq[String]): Option[Date] = tokens.map(_.toLowerCase) match {
    // "may 12", "may 12th", "nov. 15"
    case Seq(monthMatcher(month), dayMatcher(day)) =>
      Some(Date(None, Some(month), Some(day)))
    // "may, 12"
    case Seq(monthMatcher(month), ",", dayMatcher(day)) =>
      Some(Date(None, Some(month), Some(day)))
    // "may 12, 2015"
    case Seq(monthMatcher(month), dayMatcher(day), ",", yearMatcher(year)) =>
      Some(Date(Some(year), Some(month), Some(day)))
    // "may 12 2015"
    case Seq(monthMatcher(month), dayMatcher(day), yearMatcher(year)) =>
      Some(Date(Some(year), Some(month), Some(day)))
    // "may, 12, 2015"
    case Seq(monthMatcher(month), ",", dayMatcher(day), ",", yearMatcher(year)) =>
      Some(Date(Some(year), Some(month), Some(day)))
    // "may 2015"
    case Seq(monthMatcher(month), yearMatcher(year)) =>
      Some(Date(Some(year), Some(month), None))
    // "may, 2015"
    case Seq(monthMatcher(month), ",", yearMatcher(year)) =>
      Some(Date(Some(year), Some(month), None))
    // "12 may"
    case Seq(dayMatcher(day), monthMatcher(month)) =>
      Some(Date(None, Some(month), Some(day)))
    // "12 may 2015"
    case Seq(dayMatcher(day), monthMatcher(month), yearMatcher(year)) =>
      Some(Date(Some(year), Some(month), Some(day)))
    // "may of 2015"
    case Seq(monthMatcher(month), "of", yearMatcher(year)) =>
      Some(Date(Some(year), Some(month), None))
    // "2015"
    case Seq(yearMatcher(year)) =>
      Some(Date(Some(year), None, None))
    // "[the] 1st"
    case Seq(dayMatcher.withSuffix(day)) =>
      Some(Date(None, None, Some(day)))
    // "1st of September"
    case Seq(dayMatcher.withSuffix(day), "of", monthMatcher(month)) =>
      Some(Date(None, Some(month), Some(day)))
    case _ => None
  }
}
