package gramr.amr

import java.io.File

import scala.collection.LinearSeq
import scala.io.Source
import scala.util.matching.Regex

/**
  * Provides functionality for parsing AMR corpora.
  */
object AmrCorpusParser {
  def parseString(content: String): Stream[Either[ParseError, AmrCorpusItem]] =
    parseLines(content.split("\n").toList)

  def parseFile(fileName: File): Stream[Either[ParseError, AmrCorpusItem]] =
    parseLines(Source.fromFile(fileName).getLines.toStream)

  def parseLines(lines: LinearSeq[String]): Stream[Either[ParseError, AmrCorpusItem]] =
    chunkRecords(lines).map(parseRecord).collect { case Some(e) => e }

  def chunkRecords(lines: LinearSeq[String]): Stream[List[String]] = {
    def isDividingLine(l: String): Boolean = l.trim.isEmpty

    if (lines.isEmpty)
      Stream.empty
    else {
      val (head, tail) = lines.span {
        !isDividingLine(_)
      }
      head.toList #:: chunkRecords(tail.dropWhile(isDividingLine))
    }
  }

  /**
    * A chunk can contain a record (Some(_)) or be purely comment (None). If there is a
    * record, it can parse correctly (Some(Right(_))) or there can be an error (Some(Left(_))).
    */
  def parseRecord(lines: List[String]): Option[Either[ParseError, AmrCorpusItem]] = {
    val (comment, body) = lines.span {
      _.startsWith("#")
    }
    if (body.isEmpty) // A pure comment that we need to skip
      None
    else {
      val bodyStr = body.mkString("\n")
      try {
        val body = AmrTreeParser.parseString(bodyStr)
        val attributes = comment.flatMap(parseAttributes)

        Some(Right(new AmrCorpusItem(body, attributes)))
      }
      catch {
        case e: AmrTreeParser.ParseError => Some(Left(AmrParseError(e, bodyStr)))
      }
    }
  }

  val attributeRE: Regex = """ ::([^ ]*)""".r

  case class AttributeInfo(name: String, start: Int, valStart: Int)

  def parseAttributes(line: String): List[(String, String)] = {
    val infos = for {
      mtch <- attributeRE.findAllMatchIn(line).toList
      attrName = mtch.group(1)
    } yield AttributeInfo(attrName, mtch.start, mtch.end + 1)

    def toAttributes(infos: List[AttributeInfo]): List[(String, String)] = infos match {
      case info1 :: info2 :: rest => (info1.name, line.substring(info1.valStart, info2.start)) :: toAttributes(
        info2 :: rest
      )
      case info :: Nil =>
        val valString =
          if (info.valStart < line.length)
            line.substring(info.valStart)
          else
            ""
        (info.name, valString) :: Nil
      case Nil => Nil
    }

    toAttributes(infos).map { case (n, v) => (n, v.trim) }
  }

  sealed trait ParseError extends Throwable

  case class AmrParseError(e: AmrTreeParser.ParseError, bodyStr: String) extends ParseError {
    override def getMessage: String = e.getMessage + " in " + bodyStr
  }

}
