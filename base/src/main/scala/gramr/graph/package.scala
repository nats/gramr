package gramr

import scala.util.matching.Regex

package object graph {
  import Graph.ops._

  def allPaths[MR : Graph](mr: MR, length: Int): Iterable[Vector[Indexed[Element]]] =
    mr.nodes.view.flatMap(n => pathsFromNode(mr, n, length))

  def pathsFromNode[MR : Graph](
    mr: MR,
    node: Indexed[Node],
    len: Int
  ): Iterable[Vector[Indexed[Element]]] = {
    if(len > 0) {
      for {
        e <- mr.outEdges(node.pointer)
        p <- pathsFromNode(mr, mr.node(e.element.target), len - 1)
      } yield node +: e +: p
    }
    else {
      Iterable(Vector(node))
    }
  }

  /** Produces a sequence of node sets where the nodes in every set are equidistant from the start node.
    *
    * The node sets are produced in order of increasing distance from the start node. The first level consists of the
    * only start node itself, the second of all its neighbors, and so on. Every node in the graph is contained in
    * exactly one set.
    *
    * @param mr the graph to traverse
    * @param start index of the node to start at
    * @tparam MR type of meaning representation
    * @return stream of levels
    */
  def levelsFrom[MR: Graph](mr: MR, start: Int): Stream[Set[Int]] = {
    def continue(level: Set[Int], seen: Set[Int]): Stream[Set[Int]] = {
      val next = level.flatMap(index => mr.neighbours(index).map(_.pointer)) -- seen
      if(next.nonEmpty) {
        next #:: continue(next, seen ++ next)
      }
      else {
        Stream.empty
      }
    }

    val level0 = Set(start)
    level0 #:: continue(level0, level0)
  }

  /** Produces the levels of the graph, starting from its root.
    *
    * @param mr the graph to traverse
    * @tparam MR type of meaning representation
    * @return stream of levels
    */
  def levels[MR: Graph](mr: MR): Stream[Set[Int]] = {
    mr.rootOption match {
      case Some(root) => levelsFrom(mr, root)
      case _ => Stream.empty
    }
  }

  // SplitResult

  case class SplitResult[T : Graph](
    graph: T,
    externalised: Set[Int]
  )


  // Sanity

  sealed trait SanityError

  case class NotConnectedError() extends SanityError


  sealed trait WalkTree

  case class VisitNode(
    nodePointer: Int,
    firstTime: Boolean,
    subTrees: Vector[VisitEdge]
  ) extends WalkTree

  case class VisitEdge(
    edgePointer: Int,
    subTree: VisitNode
  ) extends WalkTree

  // Exceptions

  case class NotSaneException(errors: List[SanityError]) extends Exception(errors.toString)

  case class NotConnectedException() extends Exception(
    "The graph was not connected, but an operation required it to be"
  )

  case class ExternalReference(index: Int) {

    override def toString = s"<$index>"

  }

  case class SourceNodeMissingException(edge: Edge)
    extends Exception(s"Source node missing for $edge")

  case class TargetNodeMissingException(edge: Edge)
    extends Exception(s"Target node missing for $edge")

  case class NotDisjointException[T](g1: T, g2: T)
    extends Exception("The graphs' indices are not disjoint")

  val opRe: Regex = """op(\d+)(-of)?""".r
}
