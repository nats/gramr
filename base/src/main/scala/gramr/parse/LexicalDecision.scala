package gramr.parse

import gramr.ccg._
import gramr.text._

case class LexicalDecision[MR](
  item: LexicalItem[MR],
  sentence: Vector[Token],
  span: Span
) extends Decision[MR] {

  def result: MR = item.meaning

  lazy val syntax: SyntaxInformation = SyntaxInformation(sentence, span, item.synCat)

}
