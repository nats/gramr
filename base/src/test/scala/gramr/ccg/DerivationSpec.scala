package gramr.ccg

import org.scalatest._

class DerivationSpec extends FlatSpec {

  import gramr.text._, Token.Implicits._
  import gramr.ccg._
  import EasyccgCombinatorSystem._

  "Serialisation to attribute values" should "yield the expected results" in {

    val der = Derivation(
      CombinatoryStep(
        BA, List(
          LexicalStep(Vector("Vincent"), Atom("NP"), Span(0, 0), ()),
          CombinatoryStep(
            FA, List(
              LexicalStep(
                Vector("loves"), ForwardSlash(BackwardSlash(Atom("S"), Atom("NP")), Atom("NP")), Span(1, 1), ()
              ),
              LexicalStep(Vector("Mia"), Atom("NP"), Span(2, 2), ())
            ), BackwardSlash(Atom("S"), Atom("NP")), Span(1, 2), ()
          )
        ), Atom("S"), Span(0, 2), ()
      )
    )

    val expected = """(comb ba S 0-2 ((lex NP 0-0 ("'Vincent'")) (comb fa S\NP 1-2 ((lex (S\NP)/NP 1-1 ("'loves'")) (lex NP 2-2 ("'Mia'"))))))"""

    assert(der.toAttributeString == expected)

    val parsed = Derivation.fromAttributeString(expected, Vector("Vincent", "loves", "Mia"))
    assert(parsed == der)

  }

}
