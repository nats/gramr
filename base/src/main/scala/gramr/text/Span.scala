package gramr.text

/** Denotes a non-empty span in a sentence.
  *
  * Spans are inclusive: start is the index of the first token; end is the index of the last token within the span.
  */
case class Span(start: Int, end: Int) {

  def in[A](sentence: Vector[A]): Vector[A] = sentence.slice(start, end + 1)

  def contains(index: Int): Boolean = index >= start && index <= end

  def size: Int = end - start + 1

  def intersect(other: Span): Option[Span] = {
    val newStart = Math.max(start, other.start)
    val newEnd = Math.min(end, other.end)
    if (newEnd >= newStart)
      Some(Span(newStart, newEnd))
    else
      None
  }

}
