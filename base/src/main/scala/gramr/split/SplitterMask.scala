package gramr.split

trait SplitterMask {
  def apply(id: String, idx: Int): Boolean
}

case class NegativeSplitterMask(
  excluded: Set[(String, Int)]
) extends SplitterMask {
  def apply(id: String, idx: Int): Boolean = !excluded.contains((id, idx))
}

case class PositiveSplitterMask(
  included: Set[(String, Int)]
) extends SplitterMask {
  def apply(id: String, idx: Int): Boolean = included.contains((id, idx))
}
