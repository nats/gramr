package gramr.graph
package mark

import Graph.ops._

case class Marker(id: Int) {

  def resolve[MR : Graph](mr: MR): Int = {
    val matching = mr.nodes.filter { case Indexed(el, p) =>
      el.attributes.contains("markers") &&
        el.attributes.get[DefaultAttribute[Set[Marker]]]("markers").value.contains(this)
    }.toList
    if (matching.size != 1) {
      throw new Exception("No (or more than one) marker match")
    }
    matching.head.pointer
  }

  def insertAt[MR : Graph](pointer: Int, mr: MR): MR = {
    val builder = implicitly[Graph[MR]].builder(mr, normalise = false)
    val newAttributes = insertInto(mr.get(pointer).element.attributes)
    builder.replaceAttributes(pointer, newAttributes)
    builder.build
  }

  def insertInto(attr: Attributes): Attributes = {
    val markers =
      if (attr.contains("markers"))
        attr.get[DefaultAttribute[Set[Marker]]]("markers").value
      else
        Set.empty[Marker]
    attr + ("markers" -> DefaultAttribute(markers + this))
  }

}

object Marker {

  def delete(attr: Attributes): Attributes = {
    attr - "markers"
  }

  def delete[MR : Graph](mr: MR): MR = mr.mapAttributes(indexed => delete(indexed.element.attributes))

}
