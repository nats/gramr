package gramr
package split
package filters

import gramr.ccg.PlainLexicalItem
import gramr.text.Token
import gramr.graph._

case class ExcludeBlacklist[MR : Graph](
  blacklist: Set[PlainLexicalItem[MR]]
) extends SplitFilter[MR] {

  def apply(split: Split[MR], sentence: Vector[Token]): Boolean = {
    filterPartialMR(split.left, sentence) && filterPartialMR(split.right, sentence)
  }

  def filterPartialMR(partialMR: PartialMR[MR], sentence: Vector[Token]): Boolean = {
    PartialMR.lexicalItems(partialMR, sentence).forall { item => !blacklist.contains(item.toPlainLexicalItem) }
  }

}
