package gramr.graph
package mark

trait MarkerInventor[T] {

  def inventMarker(t: T): (Marker, T)

}
