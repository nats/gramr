package gramr.util.progressbar

import com.typesafe.scalalogging.LazyLogging
import org.joda.time.format.{PeriodFormat, PeriodFormatterBuilder}
import org.joda.time.{DateTime, Period}

class ProgressBar(
  private var total: Int,
  val tag: String = "",
  val displayType: ProgressBar.DisplayType = ProgressBar.OneLineUpdatingDisplay()
) extends LazyLogging {
  val startTime: DateTime = DateTime.now
  var period: Option[Period] = None

  private var done: Int = 0

  def increment(amount: Int = 1): Unit = this.synchronized {
    done = done + amount
  }

  def set(done: Int, total: Int = total): Unit = this.synchronized {
    this.done = done
    this.total = total
  }

  def display(): Unit = this.synchronized {
    displayType.display(done, total, tag, startTime)
  }

  def finish(): Unit = this.synchronized {
    done = total
    period = Some(new Period(startTime, DateTime.now))
    display()
    println()
    val seconds = period.get.toStandardSeconds.getSeconds
    val periodFormat = PeriodFormat.getDefault.print(period.get)
    logger.info(s"${tag.trim} took $seconds seconds ($periodFormat)")
  }
}

object ProgressBar {

  trait DisplayType {
    def display(done: Int, total: Int, tag: String, startTime: DateTime): Unit
  }

  case class OneLineUpdatingDisplay() extends DisplayType {
    def display(done: Int, total: Int, tag: String, startTime: DateTime): Unit = {
      val workchars = Vector('|', '/', '-', '\\')

      val percent =
        if (total > 0) (done * 100) / total else 100
      val bar = (1 to percent / 2).map(_ => "#").mkString
      val spaces = (percent / 2 to 50).map(_ => " ").mkString
      val workchar = workchars(done % workchars.length)

      val period = Some(new Period(startTime, DateTime.now))
      val fmt = new PeriodFormatterBuilder().printZeroAlways().minimumPrintedDigits(2).appendHours().appendLiteral(":").appendMinutes().appendLiteral(":").appendSeconds().toFormatter
      val time = fmt.print(period.get)

      print(s"\r$tag$bar$workchar$spaces $percent% ($done/$total) – $time")
    }
  }

  case class MultiLineDisplay(every: Int) extends DisplayType {

    import java.text.SimpleDateFormat
    import java.util.Calendar

    var last = 0

    def display(done: Int, total: Int, tag: String, startTime: DateTime): Unit = {
      if (done != last && done % every == 0) {
        val percent = (done * 100) / total
        val time = Calendar.getInstance.getTime
        val formattedTime = new SimpleDateFormat("HH:mm:ss.SSS").format(time)
        println(s"$tag$formattedTime: $percent% ($done/$total)")
      }
      last = done
    }
  }

}
