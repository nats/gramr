package gramr.graph

trait GraphOps[T] {

  import scala.language.higherKinds

  def self: T
  def typeClassInstance: Graph[T]

  // Basic operators

  def isEmpty: Boolean = typeClassInstance.isEmpty(self)

  def getOption(index: Int): Option[Indexed[Element]] = typeClassInstance.getOption(self, index)

  def elements: Iterable[Indexed[Element]] = typeClassInstance.elements(self)

  def root: Int = typeClassInstance.root(self)

  def rootOption: Option[Int] = typeClassInstance.rootOption(self)

  def outEdges(n: Int): Iterable[Indexed[Edge]] = typeClassInstance.outEdges(self, n)

  def inEdges(n: Int): Iterable[Indexed[Edge]] = typeClassInstance.inEdges(self, n)

  /** Compare two meaning graphs disregarding attributes.
    *
    */
  def ~=(other: T): Boolean = typeClassInstance.~=(self, other)

  def addNode(n: Node): (T, Int) = typeClassInstance.addNode(self, n)

  def addEdge(e: Edge): (T, Int) = typeClassInstance.addEdge(self, e)

  /** Removes an element from the graph.
    *
    * Note: When removing a node, it is up to the user to make sure that no edges are adjacent to this node.
    * The graph may end up in an invalid state otherwise.
    *
    * @param index an element index
    * @return the graph with the element removed
    */
  def remove(index: Int): T = typeClassInstance.remove(self, index)

  def replaceElement(p: Int, replacement: Element): T = typeClassInstance.replaceElement(self, p, replacement)

  /** Construct the union of two graphs.
    *
    * The element indices of the two graphs are required to be disjoint.
    *
    * @param other another graph
    * @return the union of both graphs
    * @throws NotDisjointException if the indices of the two graphs are not disjunct
    */
  def union(other: T): T = typeClassInstance.union(self, other)

  /** Map element contents without changing graph structure.
    *
    * The mapping function may not change edge sources and targets, otherwise the graph's integrity is violated.
    *
    * @param f a function that modifies elements, without changing edge sources and targets
    * @return a graph
    */
  def mapElementContents(f: Indexed[Element] => Element): T = typeClassInstance.mapElementContents(self)(f)

  // TODO switch to cats
  def traverse[M[_]](f: Indexed[Element] => M[Indexed[Element]])(implicit app: scalaz.Applicative[M]): M[T] =
    typeClassInstance.traverse(self)(f)

  def filter(pred: Indexed[Element] => Boolean): T = typeClassInstance.filter(self)(pred)

  /**
    * Splits the graph into its connected components.
    *
    * If the graph is empty, it will be returned in a single-element stream. Otherwise, it will be split
    * into all of its non-empty components, i.e. the resulting stream will not contain an empty graph.
    */
  def components: Iterable[T] = typeClassInstance.components(self)

  def walkTree(source: Int): VisitNode = typeClassInstance.walkTree(self, source)

  /**
    * Rebuilds the graph with the specified node as the new focus, i.e. the node at path 0. All node paths are
    * adjusted accordingly.
    *
    * Throws NotConnectedException if the graph is not connected.
    */
  def refocus(p: Int): T = typeClassInstance.refocus(self, p)

  /**
    * Builds the graph in a “standard” form to facilitate == comparisons. Of course, == will never check
    * complete equivalence, but graphs with the same root as well as edge order should be considered
    * equal after being normalised.
    */
  def normalise: T = typeClassInstance.normalise(self)

  /**
    * Renames the elements in this graph to not collide with those in the “with” graph
    */
  def preventCollisions(wit: T): T = typeClassInstance.preventCollisions(self, wit)


  // Convenience access to elements

  def get(p: Int): Indexed[Element] = typeClassInstance.get(self, p)

  def nodeOption(index: Int): Option[Indexed[Node]] = typeClassInstance.nodeOption(self, index)

  def node(index: Int): Indexed[Node] = typeClassInstance.node(self, index)

  def edgeOption(index: Int): Option[Indexed[Edge]] = typeClassInstance.edgeOption(self, index)

  def edge(index: Int): Indexed[Edge] = typeClassInstance.edge(self, index)

  def nodes: Iterable[Indexed[Node]] = typeClassInstance.nodes(self)

  def edges: Iterable[Indexed[Edge]] = typeClassInstance.edges(self)

  def externalNodes: Iterable[Indexed[ExternalNode]] = typeClassInstance.externalNodes(self)

  def isRooted: Boolean = typeClassInstance.isRooted(self)

  def adjacentEdges(n: Int): Iterable[Indexed[Edge]] = typeClassInstance.adjacentEdges(self, n)

  def neighbours(n: Int): Iterable[Indexed[Node]] = typeClassInstance.neighbours(self, n)

  def outDegree(n: Int): Int = typeClassInstance.outDegree(self, n)

  def inDegree(n: Int): Int = typeClassInstance.inDegree(self, n)

  def degree(n: Int): Int = typeClassInstance.degree(self, n)

  def isEquivalentTo(other: T): Boolean = typeClassInstance.isEquivalentTo(self, other)


  // TODO: unify these

  def elementAttributes(e: Int): Attributes = typeClassInstance.elementAttributes(self, e)

  def nodeAttributes(n: Int): Attributes = typeClassInstance.nodeAttributes(self, n)

  def getNodeAttributes[A](n: Int, attr: String): A = typeClassInstance.getNodeAttributes(self, n, attr)

  // TODO: partly move these outside of MeaningGraph

  def allEdgesValid: Boolean = typeClassInstance.allEdgesValid(self)

  def isSane: Boolean = typeClassInstance.isSane(self)

  /**
    * Checks rooted connectedness: True iff. all nodes and edges are reachable from the root via forward edges.
    */
  def isConnected: Boolean = typeClassInstance.isConnected(self)

  def isLoopFree: Boolean = typeClassInstance.isLoopFree(self)

  def opEdgesUnique: Boolean = typeClassInstance.opEdgesUnique(self)

  def isSaneAMR: Boolean = typeClassInstance.isSaneAMR(self)

  def map(f: (Indexed[Element] => Indexed[Element])): T = typeClassInstance.map(self)(f)

  def mapNodes(f: (Indexed[Node] => Indexed[Node])): T = typeClassInstance.mapNodes(self)(f)

  def mapEdges(f: (Indexed[Edge] => Indexed[Edge])): T = typeClassInstance.mapEdges(self)(f)

  def mapAttributes(f: Indexed[Element] => Attributes): T = typeClassInstance.mapAttributes(self)(f)

  def mapNodeAttributes(f: Indexed[Node] => Attributes): T = typeClassInstance.mapNodeAttributes(self)(f)

  def mapEdgeAttributes(f: Indexed[Edge] => Attributes): T = typeClassInstance.mapEdgeAttributes(self)(f)

  /**
    * Non-deterministic mapping allows providing several possible outcomes per element.
    * The result is the cross-product of all possible per-element outcomes.
    */
  def mapNonDeterministic(f: Indexed[Element] => Stream[Indexed[Element]]): Stream[T] =
    typeClassInstance.mapNonDeterministic(self)(f)

  def mapAttributesNonDeterministic(f: Indexed[Element] => Stream[Attributes]): Stream[T] =
    typeClassInstance.mapAttributesNonDeterministic(self)(f)

  def mapNodeAttributesNonDeterministic(f: Indexed[Node] => Stream[Attributes]): Stream[T] =
    typeClassInstance.mapNodeAttributesNonDeterministic(self)(f)

  def mapEdgeAttributesNonDeterministic(f: Indexed[Edge] => Stream[Attributes]): Stream[T] =
    typeClassInstance.mapEdgeAttributesNonDeterministic(self)(f)

  def clearAttributes: T = typeClassInstance.clearAttributes(self)

  def clearAttribute(name: String): T = typeClassInstance.clearAttribute(self, name)

  def renameAttribute(name: String, newName: String): T = typeClassInstance.renameAttribute(self, name, newName)

  /**
    * A meaning graph’s kernel is the graph with all external nodes and their adjacent edges removed.
    */
  def kernel: T = typeClassInstance.kernel(self)

  /** Groups external nodes by their indices.
    */
  def externalLayers: Map[Int, Vector[Indexed[Node]]] =
    typeClassInstance.externalLayers(self)

  /** Counts the layers of external nodes, i.e., the graph’s semantic arity.
    */
  def externalLayerCount: Int = typeClassInstance.externalLayerCount(self)

  def findExternalNode(depth: Int): Option[Int] = typeClassInstance.findExternalNode(self, depth)
}