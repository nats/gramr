package gramr.learn

import com.typesafe.scalalogging.LazyLogging
import gramr.parse.chart.Chart

trait UpdateStrategy[MR, FV, E] {

  def compute(example: E, charts: Map[String, Chart[MR, FV]]): Iterable[Update[FV]]

}


object UpdateStrategy extends LazyLogging {

  def fallBack[MR, FV, E](
    s1: UpdateStrategy[MR, FV, E],
    s2: UpdateStrategy[MR, FV, E]
  ): UpdateStrategy[MR, FV, E] = { (example: E, charts: Map[String, Chart[MR, FV]]) =>
    val u1 = s1.compute(example, charts)
    if(u1.nonEmpty) {
      logger.trace(s"Updating with $s1")
      u1
    }
    else {
      logger.trace(s"Falling back to $s2")
      s2.compute(example, charts)
    }
  }

  def choose[MR, FV, E](
    decide: (E, Map[String, Chart[MR, FV]]) => UpdateStrategy[MR, FV, E]
  ): UpdateStrategy[MR, FV, E] = {
    (example: E, charts: Map[String, Chart[MR, FV]]) => decide(example, charts).compute(example, charts)
  }

}
