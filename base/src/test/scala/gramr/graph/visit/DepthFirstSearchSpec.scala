package gramr.graph
package visit

import scala.collection.mutable

import org.scalatest.FlatSpec

class DepthFirstSearchSpec extends FlatSpec {

  implicit val graphInstance: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance
  import graphInstance.Building._

  class TestVisitor extends Visitor {
    val visited: mutable.Map[Int, Int] = mutable.Map.empty[Int, Int].withDefault(_ => 0)

    override def visitNode(nodeIndex: Int): Unit = visited.put(nodeIndex, visited(nodeIndex) + 1)

    val visitedEdges: mutable.Map[Int, Int] = mutable.Map.empty[Int, Int].withDefault(_ => 0)

    override def visitEdge(edgeIndex: Int): Unit = visitedEdges.put(edgeIndex, visitedEdges(edgeIndex) + 1)
  }

  "Depth-first search" should "visit every node" in {
    val simpleGraph = graphInstance.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
      } yield ()
    }

    val visitor = new TestVisitor()
    DepthFirstSearch(simpleGraph, visitor)
    assert(visitor.visited.keySet == simpleGraph.nodes.map(_.pointer).toSet)
  }

  "Depth-first search" should "follow both forward and backward edges" in {
    val withBackward = graphInstance.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ca", c, a)
      } yield ()
    }

    val visitor = new TestVisitor()
    DepthFirstSearch(withBackward, visitor)
    assert(visitor.visited.keySet == withBackward.nodes.map(_.pointer).toSet)
  }

  it should "visit every node only once even if there is more than one path to the node" in {
    val doubleDiamond = graphInstance.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        d <- mkReferentNode("d")
        e <- mkReferentNode("e")
        f <- mkReferentNode("f")
        g <- mkReferentNode("g")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
        _ <- mkEdge("bd", b, d)
        _ <- mkEdge("cd", c, d)
        _ <- mkEdge("de", d, e)
        _ <- mkEdge("df", d, f)
        _ <- mkEdge("eg", e, g)
        _ <- mkEdge("eg", f, g)
      } yield ()
    }

    val visitor = new TestVisitor()
    DepthFirstSearch(doubleDiamond, visitor)
    assert(visitor.visited.keySet == doubleDiamond.nodes.map(_.pointer).toSet)
    for(Indexed(_, index) <- doubleDiamond.nodes) {
      assert(visitor.visited(index) == 1)
    }
  }

  it should "not visit nodes which are not reachable from the root" in {
    val (disconnected, (a, b, c)) = graphInstance.buildReturning {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        // The "d, e, f" component is unreachable from a
        d <- mkReferentNode("d")
        e <- mkReferentNode("e")
        f <- mkReferentNode("f")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
        _ <- mkEdge("de", d, e)
        _ <- mkEdge("df", d, f)
        _ <- normalise(false)
      } yield (a, b, c)
    }

    val visitor = new TestVisitor()
    DepthFirstSearch(disconnected, visitor)
    assert(visitor.visited.keySet == Set(a, b, c))
  }

  it should "visit nodes which are not reachable from the root if restart is set to true" in {
    val (disconnected, (a, b, c, d, e, f)) = graphInstance.buildReturning {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        // The "d, e, f" component is unreachable from a
        d <- mkReferentNode("d")
        e <- mkReferentNode("e")
        f <- mkReferentNode("f")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
        _ <- mkEdge("de", d, e)
        _ <- mkEdge("df", d, f)
        _ <- normalise(false)
      } yield (a, b, c, d, e, f)
    }

    val visitor = new TestVisitor()
    DepthFirstSearch(disconnected, visitor, restart = true)
    assert(visitor.visited.keySet == Set(a, b, c, d, e, f))
  }

  it should "not visit nodes which are not reachable from the start node" in {
    val (disconnected, (d, e, f)) = graphInstance.buildReturning {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        // The "d, e, f" component is unreachable from a
        d <- mkReferentNode("d")
        e <- mkReferentNode("e")
        f <- mkReferentNode("f")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
        _ <- mkEdge("de", d, e)
        _ <- mkEdge("df", d, f)
        _ <- normalise(false)
      } yield (d, e, f)
    }

    val visitor = new TestVisitor()
    DepthFirstSearch(disconnected, visitor, startNode = Some(d))
    assert(visitor.visited.keySet == Set(d, e, f))
  }

  it should "accept an empty graph" in {
    val visitor = new TestVisitor()
    DepthFirstSearch(graphInstance.empty, visitor)
    assert(visitor.visited.isEmpty)
  }

  "Plus-n depth first search" should "take appropriate extra steps" in {
    val (doubleDiamond, (ab, ac, bd, cd, de, df, eg, fg)) = graphInstance.buildReturning {
      for {
        _ <- normalise(false)
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        d <- mkReferentNode("d")
        e <- mkReferentNode("e")
        f <- mkReferentNode("f")
        g <- mkReferentNode("g")
        ab <- mkEdge("ab", a, b)
        ac <- mkEdge("ac", a, c)
        bd <- mkEdge("bd", b, d)
        cd <- mkEdge("cd", c, d)
        de <- mkEdge("de", d, e)
        df <- mkEdge("df", d, f)
        eg <- mkEdge("eg", e, g)
        fg <- mkEdge("fg", f, g)
      } yield (ab, ac, bd, cd, de, df, eg, fg)
    }

    val visitor0 = new TestVisitor()
    DepthFirstSearch.forwardEdgesPlusN(doubleDiamond, visitor0, extraSteps = 0)
    // should visit all edges
    assert(visitor0.visitedEdges.keySet == doubleDiamond.edges.map(_.pointer).toSet)
    // All edges should be visited once
    assert(visitor0.visitedEdges(ab) == 1)
    assert(visitor0.visitedEdges(ac) == 1)
    assert(visitor0.visitedEdges(bd) == 1)
    assert(visitor0.visitedEdges(cd) == 1)
    assert(visitor0.visitedEdges(de) == 1)
    assert(visitor0.visitedEdges(df) == 1)
    assert(visitor0.visitedEdges(eg) == 1)
    assert(visitor0.visitedEdges(fg) == 1)

    val visitor1 = new TestVisitor()
    DepthFirstSearch.forwardEdgesPlusN(doubleDiamond, visitor1, extraSteps = 1)
    // should visit all edges
    assert(visitor1.visitedEdges.keySet == doubleDiamond.edges.map(_.pointer).toSet)
    // All edges should be visited once
    assert(visitor1.visitedEdges(ab) == 1)
    assert(visitor1.visitedEdges(ac) == 1)
    assert(visitor1.visitedEdges(bd) == 1)
    assert(visitor1.visitedEdges(cd) == 1)
    assert(visitor1.visitedEdges(de) == 2)
    assert(visitor1.visitedEdges(df) == 2)
    assert(visitor1.visitedEdges(eg) == 1)
    assert(visitor1.visitedEdges(fg) == 1)

    val visitor2 = new TestVisitor()
    DepthFirstSearch.forwardEdgesPlusN(doubleDiamond, visitor2, extraSteps = 2)
    // should visit all edges
    assert(visitor2.visitedEdges.keySet == doubleDiamond.edges.map(_.pointer).toSet)
    // All edges should be visited once
    assert(visitor2.visitedEdges(ab) == 1)
    assert(visitor2.visitedEdges(ac) == 1)
    assert(visitor2.visitedEdges(bd) == 1)
    assert(visitor2.visitedEdges(cd) == 1)
    assert(visitor2.visitedEdges(de) == 2)
    assert(visitor2.visitedEdges(df) == 2)
    assert(visitor2.visitedEdges(eg) == 2)
    assert(visitor2.visitedEdges(fg) == 2)

  }

}
