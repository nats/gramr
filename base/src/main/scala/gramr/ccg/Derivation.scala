package gramr.ccg

import gramr.text._

import scala.util.matching.Regex

case class Derivation[+L](root: DerivationStep[L]) {

  /**
    * Flattens the derivation into a list of steps.
    */
  def steps: List[DerivationStep[L]] = {
    def stepsFrom(step: DerivationStep[L]): List[DerivationStep[L]] =
      step :: step.subSteps.flatMap(stepsFrom)

    stepsFrom(root)
  }

  def sentence: Vector[Token] = root.tokens

  def leaves: List[LexicalStep[L]] = steps collect { case s: LexicalStep[L] => s }

  def map[M](f: L => M): Derivation[M] = mapSteps(step => f(step.label))

  def mapSteps[M](f: DerivationStep[L] => M): Derivation[M] = Derivation(root.mapSteps(f))

  def zip[M](other: Derivation[M]): Derivation[(L, M)] = Derivation(root.zip(other.root))


  override def toString: String = toAttributeString

  /**
    * Converts the derivation to a string suitable for use as an AMR attribute.
    */
  def toAttributeString: String = Derivation.toAttributeString(this)

  /** Converts the derivation to a string in CCGbank’s AUTO format.
    */
  def toAutoString: String = Derivation.toAutoString(this)

  /** Recalculates the spans of each derivation node based on the token counts at the leaves.
    */
  def fixSpans: Derivation[L] = {
    def spannedTokens(step: DerivationStep[L]): Int = step match {
      case CombinatoryStep(_, subSteps, _, _, _) => subSteps.map(spannedTokens).sum
      case LexicalStep(tokens, _, _, _) => tokens.size
    }

    def fixStep(step: DerivationStep[L], startIndex: Int = 0): DerivationStep[L] = step match {
      case CombinatoryStep(combinator, subSteps, synCat, _, label) =>
        val (_, fixedSubSteps) = subSteps.foldLeft((startIndex, Vector.empty[DerivationStep[L]])) { (acc, step) =>
          val (stepStartIndex, fixedSteps) = acc
          val fixedStep = fixStep(step, stepStartIndex)
          (fixedStep.span.end + 1, fixedSteps :+ fixedStep)
        }
        val span = Span(startIndex, fixedSubSteps.last.span.end)
        CombinatoryStep(combinator, fixedSubSteps.toList, synCat, span, label)
      case LexicalStep(tokens, synCat, _, label) =>
        val span = Span(startIndex, startIndex + tokens.size - 1)
        LexicalStep(tokens, synCat, span, label)
    }

    Derivation(fixStep(root))
  }

}

object Derivation {

  import scala.util.parsing.combinator.RegexParsers

  // Conversion to and from attribute strings

  case class AttributeParsingException(attr: String, msg: String)
    extends Exception(s"Unable to parse derivation attribute $attr: $msg")

  def toAttributeString(d: Derivation[_]): String = stepToAttributeString(d.root)

  def stepToAttributeString(step: DerivationStep[_]): String = step match {
    case CombinatoryStep(combinator, subSteps, synCat, span, _) =>
      val subStepsString = subSteps.map(stepToAttributeString).mkString(" ")
      s"(comb $combinator $synCat ${spanToString(span)} ($subStepsString))"
    case LexicalStep(tokens, synCat, span, _) =>
      s"(lex $synCat ${spanToString(span)} (${tokens.map(_.text).mkString("\"'", "'\" \"'", "'\"")}))"
  }

  def spanToString(span: Span): String = s"${span.start}-${span.end}"

  def toAutoString(d: Derivation[_]): String = stepToAutoString(d.root)

  def stepToAutoString(step: DerivationStep[_]): String = step match {
    case CombinatoryStep(combinator, subSteps, synCat, span, _) =>
      val cat = synCat.toString
      val head = combinator.headIndex
      val dtrs = subSteps.map(stepToAutoString).mkString(" ")
      s"(<T $cat $head ${subSteps.length}> $dtrs )"
    case LexicalStep(tokens, synCat, span, _) =>
      val cat = synCat.toString
      val word = tokens.head.text
      s"(<L $cat pos pos $word $cat>)"
  }

  //noinspection UnnecessaryPartialFunction,UnnecessaryPartialFunction,UnnecessaryPartialFunction
  trait AttributeParser extends RegexParsers with SyntacticCategory.Parser {

    import scala.language.postfixOps

    /** The token inventory for the sentence.
      */
    def tokens: Vector[Token]

    /** The combinator system to use for parsing.
      */
    def combinatorSystem: CombinatorSystem

    def derivation: Parser[Derivation[Unit]] = step ^^ { case s => Derivation(s) }

    def step: Parser[DerivationStep[Unit]] = combStep | lexStep

    private def combStep = "(comb" ~ combinator ~ syncat ~ span ~ "(" ~ (step *) ~ "))" ^^ {
      case _ ~ cmb ~ cat ~ span ~ _ ~ steps ~ _ =>
        CombinatoryStep(cmb, steps, cat, span, ())
    }

    private def lexStep = "(lex" ~ syncat ~ span ~ "(" ~ (token *) ~ "))" ^^ {
      case _ ~ cat ~ span ~ _ ~ _ ~ _ =>
        LexicalStep(tokens.slice(span.start, span.end + 1), cat, span, ())
    }

    private def combinator: Parser[Combinator] = """\w+""".r ^^ { case c => combinatorSystem.parse(c).get }

    private def span: Parser[Span] = new Parser[Span] {
      val re: Regex = """(\d+)-(\d+)""".r

      def apply(in: Input): ParseResult[Span] = {
        val source = in.source
        val offset = in.offset
        val start = handleWhiteSpace(source, offset)
        re.findPrefixMatchOf(source.subSequence(start, source.length)) match {
          case Some(matched) =>
            val spanStart = Integer.parseInt(matched.group(1))
            val spanEnd = Integer.parseInt(matched.group(2))
            Success(Span(spanStart, spanEnd), in.drop(start + matched.end - offset))
          case None =>
            Failure(s"expected span, but ${in.first} found", in.drop(start - offset))
        }
      }
    }

    private def token: Parser[String] = """\"'.+?'\"""".r ^^ { case s => s.substring(2, s.length - 2) }

  }

  def fromAttributeString(s: String, sentenceTokens: Vector[Token]): Derivation[Unit] = {

    import scala.util.parsing.input.CharSequenceReader
    import scala.language.reflectiveCalls

    val parser = new AttributeParser {
      override val tokens: Vector[Token] = sentenceTokens

      override val combinatorSystem = new EasyccgCombinatorSystem()

      def run: Derivation[Unit] = phrase(derivation)(new CharSequenceReader(s)) match {
        case Success(der, _) => der
        case NoSuccess(msg, _) => throw AttributeParsingException(s, msg)
      }
    }

    parser.run
  }

}


sealed trait DerivationStep[+L] {

  def synCat: SyntacticCategory

  def span: Span

  def tokens: Vector[Token]

  def label: L

  def subSteps: List[DerivationStep[L]]

  def leaves: Iterable[DerivationStep[L]]

  def height: Int

  def map[M](f: L => M): DerivationStep[M] = mapSteps(step => f(step.label))

  def mapSteps[M](f: DerivationStep[L] => M): DerivationStep[M]

  def zip[M](other: DerivationStep[M]): DerivationStep[(L, M)]

  override def toString: String = Derivation.stepToAttributeString(this)

}

case class CombinatoryStep[+L](
  combinator: Combinator,
  subSteps: List[DerivationStep[L]],
  override val synCat: SyntacticCategory,
  override val span: Span,
  override val label: L
)
  extends DerivationStep[L] {

  override def height: Int =
    if (subSteps.isEmpty)
      0
    else
      subSteps.map(_.height).max + 1

  def tokens: Vector[Token] = subSteps.flatMap(_.tokens).toVector

  override def leaves: Iterable[DerivationStep[L]] = subSteps.flatMap(_.leaves)

  def mapSteps[M](f: DerivationStep[L] => M): DerivationStep[M] =
    copy(label = f(this), subSteps = subSteps.map(_.mapSteps(f)))

  def zip[M](other: DerivationStep[M]): DerivationStep[(L, M)] =
    copy(label = (label, other.label), subSteps = subSteps.zip(other.subSteps).map { case (l, r) => l.zip(r) })

}

case class LexicalStep[+L](
  tokens: Vector[Token],
  override val synCat: SyntacticCategory,
  override val span: Span,
  override val label: L
)
  extends DerivationStep[L] {

  override def subSteps: List[DerivationStep[L]] = List.empty

  override def height: Int = 0

  override def leaves: Iterable[DerivationStep[L]] = Iterable(this)

  def mapSteps[M](f: DerivationStep[L] => M): DerivationStep[M] = copy(label = f(this))

  def zip[M](other: DerivationStep[M]): DerivationStep[(L, M)] = copy(label = (label, other.label))

}
