package gramr.parse.postprocess

import gramr.graph.Graph
import gramr.parse.Parser
import gramr.parse.chart.Chart
import gramr.text.Token

class PostProcessedParser[MR : Graph, F, C](
  baseParser: Parser[MR, F, C],
  postProcessor: (Chart[MR, F], C) => Chart[MR, F]
) extends Parser[MR, F, C] {

  override def parse(sentence: Vector[Token], context: C): Chart[MR, F] = {
    val chart = baseParser.parse(sentence, context)
    postProcessor(chart, context)
  }

}
