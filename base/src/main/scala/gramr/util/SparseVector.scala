package gramr.util

import scala.collection.Searching._
import monocle.Lens
import monocle.macros.GenLens

/**
  * A sparse vector mapping non-negative indices to double values.
  *
  * @param indices the indices represented in the vector
  * @param values vector of the same size as indices containing the corresponding values
  */
case class SparseVector(
  indices: Array[Int],
  values: Array[Double]
) {

  import SparseVector._

  assert(indices.forall(_ > 0))
  assert(values.forall(_ >= 0.0))

  /** Retrieves the element at a given index.
    *
    * @param index the index to retrieve
    * @return the element
    */
  def apply(index: Int): Double = if(index > 0) {
    // Hacky: negative indices are used to denote nonexistent lexemes, therefore they are being hard-zeroed here
    indices.search(index) match {
      case Found(i) => values(i)
      case _ => 0.0
    }
  } else 0.0

  /** Transforms each value of the vector.
    *
    * @param f the function to apply to every element
    * @return the transformed vector
    */
  def map(f: Double => Double): SparseVector = _values.modify(_.map(f))(this)

  /** Scales the vector so its values sum to one.
    *
    * @return the normalized vector
    */
  def normalize: SparseVector = {
    val countsTotal = values.sum
    if(countsTotal == 0.0) {
      this
    }
    else {
      map(_ / countsTotal)
    }
  }

  /** Scales the vector by a given factor.
    *
    * @return the scaled vector
    */
  def scale(factor: Double): SparseVector = {
    map(v => v * factor)
  }

  override def toString: String = (indices zip values).map { case (i, v) => f"$i: $v%1.4e" }.mkString(", ")
}

object SparseVector {

  val _values: Lens[SparseVector, Array[Double]] = GenLens[SparseVector](_.values)

  def apply(map: Map[Int, Double]): SparseVector = {
    val indices = map.keys.filter(_ >= 0).toArray.sorted
    new SparseVector(indices, indices.map(map))
  }

  def empty: SparseVector = SparseVector(Array.empty, Array.empty)

  def add(c1: SparseVector, c2: SparseVector): SparseVector = {
    // Step through both vectors in parallel, by index, adding values along the way.
    val newArrayLength = c1.indices.length + c2.indices.length
    val newIndices = new Array[Int](newArrayLength)
    val newValues = new Array[Double](newArrayLength)

    var i1: Int = 0  // Current position in c1
    var i2: Int = 0  // Current position in c2
    var out: Int = 0  // Current position in output vector

    while(i1 < c1.indices.length || i2 < c2.indices.length) {
      if(i1 >= c1.indices.length) {
        newIndices(out) = c2.indices(i2)
        newValues(out) = c2.values(i2)
        i2 += 1
      }
      else if(i2 >= c2.indices.length) {
        newIndices(out) = c1.indices(i1)
        newValues(out) = c1.values(i1)
        i1 += 1
      }
      else {
        val idx1 = c1.indices(i1)
        val idx2 = c2.indices(i2)
        if(idx1 < idx2) {
          newIndices(out) = idx1
          newValues(out) = c1.values(i1)
          i1 += 1
        }
        else if(idx1 == idx2) {
          newIndices(out) = idx1
          newValues(out) = c1.values(i1) + c2.values(i2)
          i1 += 1
          i2 += 1
        }
        else if(idx1 > idx2) {
          newIndices(out) = idx2
          newValues(out) = c2.values(i2)
          i2 += 1
        }
      }
      out += 1
    }

    SparseVector(newIndices.slice(0, out), newValues.slice(0, out))
  }

}