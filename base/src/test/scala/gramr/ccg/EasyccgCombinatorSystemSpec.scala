package gramr.ccg

import org.scalatest._

class EasyccgCombinatorSystemSpec extends FreeSpec {

  import scala.language.implicitConversions

  import EasyccgCombinatorSystem._

  val system = new EasyccgCombinatorSystem()

  implicit def parseSynCat(string: String): SyntacticCategory =
    SyntacticCategory.parse(string).get

  "Syntactic application should produce the expected result" - {
    "with the FA combinator" in {

      val arg0: SyntacticCategory = """S/NP"""
      val arg1: SyntacticCategory = """NP"""
      val expectedResult: SyntacticCategory = """S"""

      assert(system.synApply(FA, arg0, arg1).contains(expectedResult))

    }

    "with the BA combinator" in {

      val arg0: SyntacticCategory = """NP"""
      val arg1: SyntacticCategory = """S\NP"""
      val expectedResult: SyntacticCategory = """S"""

      assert(system.synApply(BA, arg0, arg1).contains(expectedResult))

    }

    "with the FC combinator" in {

      val arg0: SyntacticCategory = """X/Y"""
      val arg1: SyntacticCategory = """Y/Z"""
      val expectedResult: SyntacticCategory = """X/Z"""

      assert(system.synApply(FC, arg0, arg1).contains(expectedResult))
    }

    "with the BXC combinator" in {

      val arg0: SyntacticCategory = """Y/Z"""
      val arg1: SyntacticCategory = """X\Y"""
      val expectedResult: SyntacticCategory = """X/Z"""

      assert(system.synApply(BXC, arg0, arg1).contains(expectedResult))
    }

    "with the FC2 combinator" in {

      val arg0: SyntacticCategory = """X/Y"""
      val arg1: SyntacticCategory = """(Y/Z)/A"""
      val expectedResult: SyntacticCategory = """(X/Z)/A"""

      assert(system.synApply(FC2, arg0, arg1).contains(expectedResult))
    }

    "with the BXC2 combinator" in {

      val arg0: SyntacticCategory = """(Y/Z)/A"""
      val arg1: SyntacticCategory = """X\Y"""
      val expectedResult: SyntacticCategory = """(X/Z)/A"""

      assert(system.synApply(BXC2, arg0, arg1).contains(expectedResult))
    }

  }

  "Unary conversion" - {
    "of S[dcl]/NP" in {

      val orig = """S[dcl]/NP"""
      val expected: Set[SyntacticCategory] = Set("""NP\NP""", """N\N""")
      val retrieved: Set[SyntacticCategory] = system.unaryConversions(orig).map(_._2).toSet

      assert(retrieved == expected)

    }
  }

  "Type raising" - {
    "should be detected for NP type raised over S" in {
      val tr = """S[X]/(S[X]\NP)"""
      assert(EasyccgCombinatorSystem.isTypeRaised(tr))
    }
    "should be detected for NP type raised over VP" in {
      val tr = """(S[X]\NP)\((S[X]\NP)/NP)"""
      assert(EasyccgCombinatorSystem.isTypeRaised(tr))
    }
    "should be detected for PP type raised over VP" in {
      val tr = """(S[X]\NP)\((S[X]\NP)/PP)"""
      assert(EasyccgCombinatorSystem.isTypeRaised(tr))
    }
    "should not be detected for non-type-raised categories" in {
      assert(!EasyccgCombinatorSystem.isTypeRaised("""S[X]"""))
      assert(!EasyccgCombinatorSystem.isTypeRaised("""S"""))
      assert(!EasyccgCombinatorSystem.isTypeRaised("""NP"""))
      assert(!EasyccgCombinatorSystem.isTypeRaised("""PP"""))
      assert(!EasyccgCombinatorSystem.isTypeRaised("""S\NP"""))
      assert(!EasyccgCombinatorSystem.isTypeRaised("""S/NP"""))
      assert(!EasyccgCombinatorSystem.isTypeRaised("""(S[dcl]\NP)/NP"""))
      assert(!EasyccgCombinatorSystem.isTypeRaised("""."""))
      assert(!EasyccgCombinatorSystem.isTypeRaised(""";"""))
    }
  }

}
