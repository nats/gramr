package gramr.util

import monix.execution.{Ack, Scheduler}
import monix.execution.Ack.Continue
import monix.reactive.{Observable, Observer}
import scala.concurrent.duration._

import scala.concurrent.Future

package object progressbar {

  /** Keeps track of the observable, incrementing the progress bar whenever an element is produced.
    *
    * @param observable an observable
    * @param total total number of expected elements
    * @param tag short string to display at the start of the progress bar
    * @param displayType display configuration for the progress bar
    */
  def trackObservable(
    observable: Observable[_],
    total: Int,
    tag: String = "",
    displayType: ProgressBar.DisplayType = ProgressBar.OneLineUpdatingDisplay()
  )(implicit scheduler: Scheduler): Unit = {
    val bar = new ProgressBar(total, tag, displayType)
    bar.display()

    val tick = Observable.timerRepeated(0.seconds, 1.second, ()).foreach { _ => bar.display() }

    observable.subscribe {
      new Observer[Any]() {
        override def onNext(value: Any): Future[Ack] = {
          bar.increment()
          bar.display()
          Continue
        }

        override def onComplete(): Unit = {
          tick.cancel()
          bar.finish()
        }

        override def onError(ex: Throwable): Unit = {

        }
      }
    }
  }

}
