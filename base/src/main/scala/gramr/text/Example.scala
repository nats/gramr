package gramr
package text

import gramr.ccg.Derivation

import scala.language.implicitConversions

trait Example[A] {
  def id(a: A): String

  def sentence(a: A): Vector[Token]

  def indexedDerivations(a: A): Vector[(String, Derivation[Unit])]
}


object Example {

  object ops {

    implicit def toExampleOps[A: Example](a: A): ExampleOps[A] =
      new ExampleOps(a)

  }

}


trait HasMR[A, MR] {

  def mr(a: A): MR

}


class ExampleOps[A](a: A)(implicit instance: Example[A]) {

  def id: String = instance.id(a)

  def sentence: Vector[Token] = instance.sentence(a)

  def reference[MR](implicit hasMR: HasMR[A, MR]): MR = hasMR.mr(a)

  def indexedDerivations: Vector[(String, Derivation[Unit])] = instance.indexedDerivations(a)

}
