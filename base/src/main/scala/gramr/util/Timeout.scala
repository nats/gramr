package gramr.util

import scala.concurrent.duration.Deadline
import scala.concurrent.duration.Duration
import scala.concurrent.duration.FiniteDuration

trait Timeout {
  def check: Unit
}

case class FiniteTimeout(deadline: Deadline) extends Timeout {

  def check: Unit =
    if (deadline.isOverdue())
      throw TimeoutException()

}

case object InfiniteTimeout extends Timeout {

  def check: Unit = ()

}

object Timeout {

  def fromDuration(duration: Duration): Timeout = {
    if (duration.isInstanceOf[FiniteDuration]) {
      val deadline = Deadline.now + duration.asInstanceOf[FiniteDuration]
      FiniteTimeout(deadline)
    }
    else InfiniteTimeout
  }

}

case class TimeoutException(override val getMessage: String = "Timeout") extends Exception
