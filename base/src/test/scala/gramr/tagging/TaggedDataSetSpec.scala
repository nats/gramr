package gramr.tagging

import gramr.ccg.Derivation
import gramr.split.{SentenceSplittingAlgorithm, SplitOutcome}
import gramr.split.em.DelexicalizedEstimator
import org.scalatest.FreeSpec

class TaggedDataSetSpec extends FreeSpec {
  import gramr.split.ExampleData._

  def extractDerivations(example: E): Seq[(String, Derivation[Unit])] = {
    derivations(example.sentence.map(_.text).mkString(" "))
  }

  val maxLexNodes = 1

  val exampleDerivationPairs = for(e <- examples.values; d <- extractDerivations(e)) yield (e, d)
  val delexedLexicon = gramr.graph.lexemes.delexicalizeLexicon(lexicon, maxLexNodes)
  val model = new DelexicalizedEstimator(exampleDerivationPairs, lexicon, delexedLexicon.lexemes, maxLexNodes, sentenceSplitter, 1).estimate

  "Creating a tagged dataset from example data" - {
    "should produce a TaggedDataSet if all goes well" in {
      val dataSet = TaggedDataSet.fromSplitCharts[MR, E](examples.values, extractDerivations, sentenceSplitter, model, delexedLexicon)
    }

    "should work if the splitting algorithm encounters a timeout" in {
      val sentenceSplitterWithErrors = new SentenceSplittingAlgorithm[MR, E] {
        override def split(example: E, derivation: Derivation[Unit], id: String): SplitOutcome[MR] = {
          if(example.sentence.exists(t => t.lemma == "manny")) {
            Left("This splitter is not a fan of Manny")
          }
          else {
            sentenceSplitter.split(example, derivation, id)
          }
        }
      }

      val dataSet = TaggedDataSet.fromSplitCharts[MR, E](examples.values, extractDerivations, sentenceSplitterWithErrors, model, delexedLexicon)
    }
  }
}
