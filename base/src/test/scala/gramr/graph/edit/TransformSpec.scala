package gramr.graph.edit

import gramr.graph.{AdjacencyMeaningGraph, Graph}
import Transform._
import gramr.util.Parsing
import org.scalatest.FlatSpec

class TransformSpec extends FlatSpec {

  implicit val mgl: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance

  "Flattening nested conjunction nodes" should "combine a single nesting" in {
    val original = Parsing.parseAmr(
      """(a / and
        |  :op1 (x / x)
        |  :op2 (a2 / and
        |    :op1 (y / y)
        |    :op2 (z / z)))""".stripMargin)
    val expected = Parsing.parseAmr(
      """(a / and
        |  :op1 (x / x)
        |  :op2 (y / y)
        |  :op3 (z / z))""".stripMargin)

    val results = (for {
      m <- markNodeNamed("a")
      _ <- flattenNestedConj(m)
    } yield ()).perform(original)

    assert(results.size == 1)
    val result = results.head._1

    assert(result.normalise ~= expected)
  }

  it should "correctly insert sub-operands in the middle" in {
    val original = Parsing.parseAmr(
      """(a / and
        |  :op1 (x / x)
        |  :op2 (a2 / and
        |    :op1 (y / y)
        |    :op2 (z / z))
        |  :op3 (b / b))""".stripMargin)
    val expected = Parsing.parseAmr(
      """(a / and
        |  :op1 (x / x)
        |  :op2 (y / y)
        |  :op3 (z / z)
        |  :op4 (b / b))""".stripMargin)

    val results = (for {
      m <- markNodeNamed("a")
      _ <- flattenNestedConj(m)
    } yield ()).perform(original)

    assert(results.size == 1)
    val result = results.head._1

    assert(result.normalise ~= expected)

  }

  it should "correctly merge a conjunction with two sub-nodes" in {
    val original = Parsing.parseAmr(
      """(a / and
        |  :op1 (x / x)
        |  :op2 (a2 / and
        |    :op1 (y / y)
        |    :op2 (z / z))
        |  :op3 (a3 / and
        |    :op1 (b / b)
        |    :op2 (c / c)))""".stripMargin)
    val expected = Parsing.parseAmr(
      """(a / and
        |  :op1 (x / x)
        |  :op2 (y / y)
        |  :op3 (z / z)
        |  :op4 (b / b)
        |  :op5 (c / c))""".stripMargin)

    val results = (for {
      m <- markNodeNamed("a")
      _ <- flattenNestedConj(m)
    } yield ()).perform(original)

    assert(results.size == 1)
    val result = results.head._1

    assert(result.normalise ~= expected)

  }

  it should "correctly merge a conjunction with depth of 2" in {
    val original = Parsing.parseAmr(
      """(a / and
        |  :op1 (x / x)
        |  :op2 (a2 / and
        |    :op1 (a3 / and
        |      :op1 (b / b)
        |      :op2 (c / c))
        |    :op2 (z / z)))""".stripMargin)
    val expected = Parsing.parseAmr(
      """(a / and
        |  :op1 (x / x)
        |  :op2 (b / b)
        |  :op3 (c / c)
        |  :op4 (z / z))""".stripMargin)

    val results = (for {
      m <- markNodeNamed("a")
      _ <- flattenNestedConj(m)
    } yield ()).perform(original)

    assert(results.size == 1)
    val result = results.head._1

    assert(result.normalise ~= expected)

  }

  "Flattening all nested conjunctions" should "work if several conjunctions are present" in {
    val original = Parsing.parseAmr(
      """(r / root
        |  :ARG0 (a / and
        |    :op1 (b / b)
        |    :op2 (a2 / and
        |      :op1 (c / c)
        |      :op2 (d / d)))
        |  :ARG1 (a3 / and
        |    :op1 (a4 / and
        |      :op1 (e / e)
        |      :op2 (f / f))
        |    :op2 (g / g)))""".stripMargin)
    val expected = Parsing.parseAmr(
      """(r / root
        |  :ARG0 (a / and
        |    :op1 (b / b)
        |    :op2 (c / c)
        |    :op3 (d / d))
        |  :ARG1 (a3 / and
        |    :op1 (e / e)
        |    :op2 (f / f)
        |    :op3 (g / g)))""".stripMargin)

    val results = flattenConjunctions.perform(original)

    assert(results.size == 1)
    val result = results.head._1

    assert(result.normalise ~= expected)
  }


}
