package gramr.util

import scala.collection.immutable.HashMap

/** Implements a multimap where one key can be assigned multiple values.
  *
  * Values for the same key are unordered. Implementation is backed by a `HashMap`.
  */
@SerialVersionUID(1L)
class MultiMap[K, V] private(val impl: HashMap[K, Set[V]]) extends Serializable {

  /** Retrieve the set of stored keys. */
  def keySet: Set[K] = impl.keySet

  /** Iterate over all values stored in the multimap. */
  def values: Iterable[V] = impl.values.flatten

  /** Check whether the multimap is empty.
    *
    * A multimap is empty if it contains no values.
    */
  def isEmpty: Boolean = values.isEmpty

  /** Check whether the multimap is non-empty.
    *
    * This is the inverse of `isEmpty`.
    */
  def nonEmpty: Boolean = !isEmpty

  /** Iterate over all stored key-value pairs.
    *
    * If a key is assigned multiple values, it will appear an according number of times in the iteration.
    */
  def entries: Iterable[(K, V)] = impl.flatMap {
    // toVector conversion to avoid entries getting deleted during Set construction
    case (key, values) => values.toVector.map(value => (key, value))
  }

  /** Iterate over all values for a given key.
    *
    * @param key a key
    */
  def get(key: K): Iterable[V] = impl.get(key) match {
    case None => Iterable.empty
    case Some(entries) => entries
  }

  /** Filter entries based on key and value. */
  def filter(p: (K, V) => Boolean): MultiMap[K, V] = {
    val newImpl = impl.map { case (k, entries) => (k, entries.filter { v => p(k, v) }) }
    new MultiMap[K, V](newImpl)
  }

  /** Add a key-value pair to the map, returning an updated copy.
    *
    * @param kv a key-value pair
    * @return the updated multimap
    */
  def +(kv: (K, V)): MultiMap[K, V] = {
    val (k, v) = kv
    val newImpl = impl.get(k) match {
      case Some(entries) =>
        val newEntries = entries + v
        impl + (k -> newEntries)
      case None => impl + (k -> Set(v))
    }
    new MultiMap[K, V](newImpl)
  }

}


object MultiMap {

  /** Create an empty `MultiMap`.
    *
    * @tparam K key type
    * @tparam V value type
    */
  def empty[K, V]: MultiMap[K, V] = new MultiMap(HashMap.empty)

  /** Create a `MultiMap` from an iterable of key-value pairs.
    *
    * @param entries key-value pairs to put in the multimap
    * @tparam K key type
    * @tparam V value type
    */
  def from[K, V](entries: Iterable[(K, V)]): MultiMap[K, V] =
    entries.foldLeft(empty[K, V]) { case (map, (k, v)) => map + (k -> v) }

}
