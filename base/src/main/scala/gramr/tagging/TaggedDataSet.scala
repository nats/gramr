package gramr.tagging

import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

import gramr.ccg.{DelexedLexicon, Derivation}
import gramr.graph.lexemes.Lexeme
import gramr.split.{SentenceSplittingAlgorithm, SplitChart}
import gramr.split.em.DelexicalizedModel
import gramr.text.{Example, Token}
import Example.ops._
import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable.ArrayBuffer


case class TaggedDataSet(
  examples: Vector[Option[TaggedExample]]
) {

  def addLayer(layer: Iterable[Layer]): TaggedDataSet = {
    TaggedDataSet(
      (examples zip layer).map {
        case (Some(example), l) =>
          Some(example.addLayer(l))
        case (None, _) => None
      }
    )
  }

  def save(file: File): Unit = {
    val writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))
    examples.flatten.foreach { example =>
      writer.println(example.id)
      writer.println(example.sentence.mkString(" "))
      example.layers.foreach { layer =>
        writer.println(layer.name + ": " + layer.tags.mkString(" "))
      }
      writer.println()
    }
    writer.close()
  }

}


object TaggedDataSet extends LazyLogging {

  /** Splits a whole data set and turns the split charts into supertag annotations.
    *
    * Warning: Splitting is performed sequentially and may be slow.
    */
  def fromSplitCharts[MR, E: Example](
    examples: Iterable[E],
    extractDerivations: E => Seq[(String, Derivation[Unit])],
    splitter: SentenceSplittingAlgorithm[MR, E],
    parameters: DelexicalizedModel[MR],
    lexicon: DelexedLexicon[MR]
  ): TaggedDataSet = {
    def tagExample(example: E): Option[TaggedExample] = {
      extractDerivations(example).headOption match {
        case Some((derivationName, derivation)) =>
          splitter.split(example, derivation, derivationName) match {
            case Right(chart) =>
              tagsFromSplitChart(example, chart, parameters, lexicon)
            case Left(error) =>
              logger.debug(s"Not tagging ${example.id}: $error")
              None
          }
        case None =>
          None
      }
    }

    val taggedExamples = examples.map(tagExample)

    TaggedDataSet(taggedExamples.toVector)
  }

  def tagsForSplit[MR](
    sentence: Seq[Token],
    entries: Map[Int, (SplitChart.Entry[MR], Lexeme, MR)],
    parameters: DelexicalizedModel[MR],
    lexicon: DelexedLexicon[MR]
  ): Iterable[(String, String)] = {


    sentence.indices.map { index =>
      entries.get(index) match {
        case None => ("UNK", "UNK")
        case Some((entry, lexeme, templateMr)) =>
          val token = sentence(index)
          val templateIds = parameters.templateIds(entry, templateMr)
          if (templateIds.isEmpty) {
            logger.warn(s"No lexicon entry with syncat ${entry.partialMR.synCats.mkString(", ")} for template:\n$templateMr")
            ("UNK", "UNK")
          }
          else {
            templateIds.filter(templateId => !lexicon.delexed.itemsById.keySet.contains(templateId)).foreach { nonExistent =>
              logger.warn(s"Template $nonExistent is not in the lexicon")
            }
            val templates = templateIds.map(lexicon.delexed.itemsById)
            // There may be more than one template, each with a different syncat. Hack: the shortest syncat is usually
            // the simplest one that should be predicted. Actually, it’s a gap in the EM algorithm that it doesn’t take
            // syncats into account.
            val templateId = templates.minBy(_.synCat.toString.length).index
            if (parameters.parameters.templateParameters(templateId) == Double.NegativeInfinity) {
              logger.warn(s"Template $templateId has probability zero")
            }
            val lexemeId = DelexicalizedModel.lexemeId(Seq(token), lexicon.lexemes)(lexeme) match {
              case Some(id) => id
              case None => logger.warn(s"Lexeme $lexeme is not in the lexicon")
            }
            (templateId.toString, lexemeId.toString)
          }
      }
    }
  }

  def tagsFromSplitChart[MR, E: Example](
    example: E,
    chart: SplitChart[MR],
    parameters: DelexicalizedModel[MR],
    lexicon: DelexedLexicon[MR]
  ): Option[TaggedExample] = {
    val bestSplit = parameters.bestSplit(chart)
    // Create a map with the entries for each token
    val entries: Map[Int, (SplitChart.Entry[MR], Lexeme, MR)] = bestSplit.flatMap {
      case (entry, lexeme, templateMr) if entry.span.size == 1 =>
        Some((entry.span.start, (entry, lexeme, templateMr)))
      case _ => None
    }.toMap

    val (tmpl, lex) = tagsForSplit(example.sentence, entries, parameters, lexicon).unzip
    val lexLabels = extractLexLabels(example.sentence, entries)
    if(lexLabels.size < tmpl.size) {
      logger.warn(s"${example.id}: too few lex labels")
    }
    val tagged = TaggedExample(
      example.id,
      example.sentence.map(_.text),
      Vector(
        Layer("template-id", tmpl.toVector),
        Layer("lexeme-id", lex.toVector),
        Layer("lex-label", lexLabels.toVector)
      ))
    Some(tagged)
  }

  def extractLexLabels[MR](sentence: Seq[Token], entries: Map[Int, (SplitChart.Entry[MR], Lexeme, MR)]): Iterable[String] = {
    sentence.indices.map { index =>
      entries.get(index) match {
        case None => "UNK"
        case Some((_, lexeme, _)) =>
          lexeme.labels.headOption.getOrElse("NIL")
      }
    }
  }

  /** Create a data set containing only example IDs and sentence tokens
    *
    * @param examples the examples to put into the data set
    * @return the tagged data set
    */
  def fromExamples[E: Example](examples: Iterable[E]): TaggedDataSet = {
    val taggedExamples = examples.map { example => Some(TaggedExample(example.id, example.sentence.map(_.text), Vector.empty)) }
    TaggedDataSet(taggedExamples.toVector)
  }

  class Builder {
    val examples: ArrayBuffer[Option[TaggedExample]] = ArrayBuffer.empty

    def add(example: Option[TaggedExample]): Unit = examples.append(example)

    def build: TaggedDataSet = TaggedDataSet(examples.toVector)
  }

}


case class TaggedExample(
  id: String,
  sentence: Vector[String],
  layers: Vector[Layer]
) {

  def addLayer(layer: Layer): TaggedExample = copy(layers = layers :+ layer)

}


case class Layer(
  name: String,
  tags: Vector[String]
)
