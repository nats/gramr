package gramr.jobs

import java.io.{File, PrintWriter}

import com.typesafe.scalalogging.LazyLogging
import gramr.ccg.CombinatorSystem
import gramr.graph.Graph
import gramr.learn.{Update, UpdateStrategy}
import gramr.parse.chart.ExtractDerivation
import gramr.text.Example.ops._
import gramr.text.{Example, HasMR}
import gramr.util.SimpleRetrievalStats

class ChartOps[MR: Graph, FV, E: Example](job: Job[E, (E, NamedParses[MR, FV])])(implicit hasMR: HasMR[E, MR]) extends LazyLogging {

  import job.scheduler

  def evaluate(evaluationFunction: (MR, E) => SimpleRetrievalStats): Job[E, (E, NamedEvals)] = {
    def computation(x: (E, NamedParses[MR, FV])): (E, NamedEvals) = x match {
      case (example, charts) =>
        val evals = charts.map {
          case (name, chart) =>
            (name, gramr.parse.evaluate.evaluateParse(chart, example, evaluationFunction))
        }
        (example, evals)
    }
    job.append(ComputeStage(computation))
  }


  def generateUpdates(
    updateStrategy: UpdateStrategy[MR, FV, E],
    condition: (NamedParses[MR, FV], E) => Boolean
  ): Job[E, (E, Iterable[Update[FV]])] = {

    def computation(item: (E, NamedParses[MR, FV])): (E, Iterable[Update[FV]]) = item match {
      case (example, namedCharts) =>
        if(condition(namedCharts, example)) {
          (example, updateStrategy.compute(example, namedCharts))
        }
        else {
          logger.trace(s"${example.id}: No update (update condition not met)")
          (example, Iterable.empty[Update[FV]])
        }
    }

    job.append(ComputeStage(computation))
  }


  def logAmr(
    writer: PrintWriter,
    evaluationFunction: (MR, E) => SimpleRetrievalStats
  ): Job[E, (E, NamedParses[MR, FV])] = job.listenToItems {
    case (example, namedParses) =>
      writer.synchronized {
        namedParses foreach {
          case (name, chart) =>
            val stats = gramr.parse.evaluate.evaluateParse(chart, example, evaluationFunction)
            writer.println(s"# ::id ${example.id}")
            writer.println(s"# ::tok ${example.sentence.mkString(" ")}")
            writer.println(s"# ::parser $name")
            writer.println(s"# ::precision ${stats.bestScoredParse.precision} ::recall ${stats.bestScoredParse.recall} ::f1 ${stats.bestScoredParse.f1}")
            chart.outputStates.headOption match {
              case Some(topParse) =>
                writer.println(topParse.mr.toString)
              case None =>
                writer.println("(n / noparse)")
            }
            writer.println()
            writer.flush()
        }
      }
  }

  def logTopDerivationAsHtml(
    directory: File
  )(implicit cs: CombinatorSystem): Job[E, (E, NamedParses[MR, FV])] = job.listenToItems {
    case (example, namedParses) =>
      import gramr.ccg.presentation.Implicits._
      import gramr.graph.presentation.ops._

      namedParses foreach {
        case (name, chart) =>
          val file = new File(directory, s"${job.name}-$name-${example.id}.html")
          val extractor = ExtractDerivation[MR, FV](cs)
          extractor.fromChart(chart) foreach { stateDerivation =>
            val mrDerivation = stateDerivation.map(_.mr)
            gramr.util.html.writeToFile(file, mrDerivation)
          }
      }
  }

  def logChartAsHtml(
    directory: File
  )(implicit cs: CombinatorSystem): Job[E, (E, NamedParses[MR, FV])] = job.listenToItems {
    case (example, namedParses) =>
      import gramr.graph.presentation.ops._
      import gramr.parse.chart.Chart.ops._

      namedParses foreach {
        case (name, chart) =>
          val file = new File(directory, s"${job.name}-$name-${example.id}.chart.html")
          gramr.util.html.writeToFile(file, chart)
      }
  }



}
