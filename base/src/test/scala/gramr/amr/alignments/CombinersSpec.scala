package gramr.amr.alignments

import org.scalatest.FlatSpec

class CombinersSpec extends FlatSpec {

  "The vote combiner" should "combine two alignment sets" in {
    val sets = Seq(
      Set(1, 2, 3),
      Set(3, 4, 5)
    )

    assert(Combiners.vote(0)(sets) == Set(1, 2, 3, 4, 5))
    assert(Combiners.vote(1)(sets) == Set(1, 2, 3, 4, 5))
    assert(Combiners.vote(2)(sets) == Set(3))
    assert(Combiners.vote(3)(sets) == Set())
  }


  it should "combine three alignment sets" in {
    val sets = Seq(
      Set(1, 2, 3, 4, 5),
      Set(1, 3, 5),
      Set(1, 5, 9)
    )

    assert(Combiners.vote(0)(sets) == Set(1, 2, 3, 4, 5, 9))
    assert(Combiners.vote(1)(sets) == Set(1, 2, 3, 4, 5, 9))
    assert(Combiners.vote(2)(sets) == Set(1, 3, 5))
    assert(Combiners.vote(3)(sets) == Set(1, 5))
    assert(Combiners.vote(4)(sets) == Set())
  }

}
