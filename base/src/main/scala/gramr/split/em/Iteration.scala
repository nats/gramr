package gramr.split
package em

import breeze.numerics._
import gramr.ccg.{Derivation, StoredLexicon}
import gramr.text.{Example, Span}
import Example.ops._

import scala.collection.mutable.ArrayBuffer
import breeze.linalg.softmax
import com.typesafe.scalalogging.LazyLogging

class Iteration[MR, E: Example, M, P](
  model: M,
  lexicon: StoredLexicon[MR],
  sentenceSplitter: SentenceSplittingAlgorithm[MR, E],
  adjustCounts: P => P = (x: P) => x
)(implicit val emModel: EMModel[M, MR, P]) extends LazyLogging {

  import EMModel._

  def add(c1: P, c2: P): P = {
    emModel.addCounts(c1, c2)
  }

  def estimate(exampleDerivationPairs: Iterable[(E, (String, Derivation[Unit]))]): M = {
    var sentenceIndex = 0
    val (sentenceWiseCounts, logLikelihoods) = exampleDerivationPairs.par.map {
      case (example, (derivationName, derivation)) =>
        sentenceIndex += 1
        sentenceCounts(example, derivationName, derivation)
    }.seq.unzip

    val counts = emModel.sumCounts(sentenceWiseCounts)

    val totalLogLikelihood = logLikelihoods.sum
    logger.info(s"EM total log likelihood: $totalLogLikelihood")

    val countsAdjusted = adjustCounts(counts)

    model.update(countsAdjusted)
  }

  def logAdd(a: Double, b: Double): Double = softmax(a, b)

  def logSum(values: Iterable[Double]): Double = {
    if(values.isEmpty) {
      Double.NegativeInfinity
    }
    else {
      softmax(values)
    }
  }

  def sentenceCounts(example: E, derivationName: String, derivation: Derivation[Unit]): (P, Double) = {
    var counts: P = emModel.emptyCounts
    var logLikelihood: Double = 1.0

    sentenceSplitter.split(example, derivation, derivationName) match {
      case Right(chart) =>

        // inside and outside keep the computed log-scores
        val inside = ArrayBuffer.fill(chart.itemCount)(Double.NegativeInfinity)
        val outside = ArrayBuffer.fill(chart.itemCount)(Double.NegativeInfinity)

        // params are not in log-domain
        val params = chart.entries.map(entry => (entry.id, model.score(entry, chart))).toMap

        // Walk across the entries from the bottom up to calculate inside scores
        for (length <- 1 to chart.sentence.length; start <- 0 to chart.sentence.size - length) {
          val end = start + length - 1
          val span = Span(start, end)
          val entries = chart.get(span)
          entries.foreach { entry =>
            val param = model.score(entry, chart)
            val splits = chart.splits.filter(_.parentId == entry.id)

            // We can *either* choose to pull this entry from the lexicon, *or* choose one of the possible decompositions
            // inside(entry.id) = param + splits.map(split => split.childIds.toVector.map(inside).product).sum
            inside(entry.id) = logAdd(log(param), logSum(splits.map(split => split.childIds.toVector.map(inside).sum)))

            if(inside(entry.id) == 0.0) {
              logger.warn(s"inside==0.0 for entry ${entry.partialMR}")
            }
          }
        }

        val parents = chart.entries.map { entry =>
          (entry.id, chart.splits.filter(split => split.childIds.contains(entry.id)))
        }.toMap

        val roots = chart.entries.filter(entry => parents(entry.id).isEmpty).toVector

        // Initialise outside scores for all root entries to 1 == logprob 0
        roots.foreach { root =>
          outside(root.id) = 0.0
        }

        // Walk across the entries from the top down to calculate outside scores
        for (length <- Range.inclusive(chart.sentence.length, 1, -1); start <- 0 to chart.sentence.length - length) {
          val end = start + length - 1
          val span = Span(start, end)
          val entries = chart.get(span)
          entries.foreach { entry =>
            val ps = parents(entry.id)
            ps.foreach { split =>
              val neighbours = split.childIds.filter(_ != entry.id)
              outside(entry.id) = logAdd(outside(entry.id), outside(split.parentId) + neighbours.map(inside).sum)
            }
          }
        }

        val totalLogPotential = softmax(roots.map(entry => inside(entry.id)))
        if (totalLogPotential == Double.NegativeInfinity) {
          logger.warn(s"Total log potential -inf in ${example.id} derivation $derivationName")
        }
        else {
          logLikelihood = logLikelihood + totalLogPotential

          // For calculating the counts, we deviate from the IO algorithm: we don't use the inside score for the counts
          // since a lex entry isn't selected if any of its children are selected.
          def entryCount(entryId: Int) = exp(log(params(entryId)) + outside(entryId) - totalLogPotential)

          val lexEntryCounts = chart.entries.map(entry => emModel.count(model, chart)(entryCount(entry.id), entry))
          val lexEntryCountsSum = emModel.sumCounts(lexEntryCounts)
          counts = emModel.addCounts(counts, lexEntryCountsSum)

        }

      case Left(error) =>
        logger.debug(s"Skipping example ${example.id}: $error")
    }

    (counts, logLikelihood)
  }
}
