package gramr
package split

import ccg._

trait SentenceSplittingAlgorithm[MR, E] {

  def split(example: E, derivation: Derivation[Unit], id: String): SplitOutcome[MR]

}
