package gramr.graph

import org.scalatest._

import gramr.amr._

import AmrTreeConversion._
import Graph.ops._

class AdjacencyMeaningGraphSpec extends FlatSpec {


  type T = AdjacencyMeaningGraph

  implicit def meaningGraphLike: Graph[T] = AdjacencyMeaningGraph.graphInstance

  val mgl: Graph[T] = AdjacencyMeaningGraph.graphInstance

  import mgl.Building._

  "Amr tree to meaning graph conversion" should "process a simple AMR correctly" in {
    val amrStr =
      """(e / establish-01
        |  :ARG1 (m / model
        |          :mod (i / innovate-01
        |                 :ARG1 (i2 / industry))))""".stripMargin
    val amr = AmrTreeParser.parseString(amrStr)

    val graph = mgl.build {
      for {
        n0 <- mkReferentNode("establish-01")
        n00 <- mkReferentNode("model")
        n000 <- mkReferentNode("innovate-01")
        n0000 <- mkReferentNode("industry")
        _ <- mkEdge("ARG1", n0, n00)
        _ <- mkEdge("mod", n00, n000)
        _ <- mkEdge("ARG1", n000, n0000)
      } yield ()
    }

    val parsed = Graph.fromAmrTree[T](amr)

    assert(parsed ~= graph)
  }

  it should "work for a somewhat more complex example" in {

    val amrStr =
      """(f / face-01
        |      :ARG0 (p2 / person :name (n / name :op1 "Raghad"))
        |      :ARG1 (c2 / charge-05
        |            :ARG1 p2
        |            :ARG2 (t / terrorism)
        |            :location (c3 / country :name (n2 / name :op1 "Iraq"))))""".stripMargin
    val amr = AmrTreeParser.parseString(amrStr)
    val parsed = Graph.fromAmrTree[T](amr)

    val graph = mgl.build {
      for {
        f <- mkReferentNode("face-01")
        p2 <- mkReferentNode("person")
        n <- mkReferentNode("name")
        rag <- mkConstantNode("\"Raghad\"")
        c2 <- mkReferentNode("charge-05")
        t <- mkReferentNode("terrorism")
        c3 <- mkReferentNode("country")
        n2 <- mkReferentNode("name")
        iraq <- mkConstantNode("\"Iraq\"")
        _ <- mkEdge("ARG0", f, p2)
        _ <- mkEdge("name", p2, n)
        _ <- mkEdge("op1", n, rag)
        _ <- mkEdge("ARG1", f, c2)
        _ <- mkEdge("ARG1", c2, p2)
        _ <- mkEdge("ARG2", c2, t)
        _ <- mkEdge("location", c2, c3)
        _ <- mkEdge("name", c3, n2)
        _ <- mkEdge("op1", n2, iraq)
      } yield ()
    }

    assert(parsed ~= graph)

  }

  "Conversion to AMR and back" should "yield back the same graph" in {

    val graph = mgl.build {
      for {
        f <- mkReferentNode("face-01")
        p2 <- mkReferentNode("person")
        n <- mkReferentNode("name")
        rag <- mkConstantNode("\"Raghad\"")
        c2 <- mkReferentNode("charge-05")
        t <- mkReferentNode("terrorism")
        c3 <- mkReferentNode("country")
        n2 <- mkReferentNode("name")
        iraq <- mkConstantNode("\"Iraq\"")
        _ <- mkEdge("ARG0", f, p2)
        _ <- mkEdge("name", p2, n)
        _ <- mkEdge("op1", n, rag)
        _ <- mkEdge("ARG1", f, c2)
        _ <- mkEdge("ARG1", c2, p2)
        _ <- mkEdge("ARG2", c2, t)
        _ <- mkEdge("location", c2, c3)
        _ <- mkEdge("name", c3, n2)
        _ <- mkEdge("op1", n2, iraq)
      } yield ()
    }

    val amr = graph.toAmrTree
    val converted = Graph.fromAmrTree[T](amr)

    assert(converted ~= graph)

  }


  "Edge reversals" should "be detected and manipulated correctly" in {

    val e = Edge("label", 0, 1)
    val r = Edge("label-of", 1, 0)

    assert(e.flip == r)
    assert(r.isReversed)
    assert(r.unflip == e)
    assert(r.flip == e)
    assert(e.unflip == e)
  }


  "Connectedness of meaning graphs" should "be detected" in {

    val g1 = mgl.build {
      for {
        _ <- normalise(false)
        n0 <- mkReferentNode("date-entity")
        n00 <- mkConstantNode("2002")
        n01 <- mkConstantNode("1")
        n02 <- mkConstantNode("5")
        _ <- mkEdge("year", n0, n00)
        _ <- mkEdge("month", n0, n01)
        _ <- mkEdge("day", n0, n02)
      } yield ()
    }

    assert(g1.isConnected)

    val g2 = mgl.build {
      for {
        _ <- normalise(false)
        n0 <- mkReferentNode("date-entity")
        n00 <- mkConstantNode("2002")
        n01 <- mkConstantNode("1")
        n02 <- mkConstantNode("5")
        _ <- mkEdge("year", n0, n00)
        _ <- mkEdge("month", n0, n01)
      } yield ()
    }

    assert(!g2.isConnected)

    val g3 = mgl.build {
      for {
        _ <- normalise(false)
        n0 <- mkReferentNode("a")
        n00 <- mkReferentNode("b")
        n000 <- mkReferentNode("c")
        _ <- mkEdge("ab", n0, n00)
        _ <- mkEdge("bc", n00, n000)
      } yield ()
    }

    assert(g3.isConnected)

    val g3MissingEdge = mgl.build {
      for {
        _ <- normalise(false)
        n0 <- mkReferentNode("a")
        n00 <- mkReferentNode("b")
        n000 <- mkReferentNode("c")
        _ <- mkEdge("bc", n00, n000)
      } yield ()
    }

    assert(!g3MissingEdge.isConnected)

    val g3FlippedEdge = mgl.build {
      for {
        _ <- normalise(false)
        n0 <- mkReferentNode("a")
        n00 <- mkReferentNode("b")
        n000 <- mkReferentNode("c")
        _ <- mkEdge("ab", n0, n00)
        _ <- mkEdge("cb", n000, n00)
        _ <- normalise(false)
      } yield ()
    }

    assert(g3FlippedEdge.isConnected)


  }

  "The graph kernel" should "omit exactly the expected elements" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkExternalNode(ExternalReference(0))
        n00 <- mkConstantNode("5")
        _ <- mkEdge("day", n0, n00)
      } yield ()
    }

    val k1 = AdjacencyMeaningGraph.build {
      for {
        n00 <- mkConstantNode("5")
      } yield ()
    }

    val kernel = g1.kernel.normalise

    assert(k1 == kernel)
  }

  "The kernel" should "not contain any external nodes and floating edges" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkExternalNode(ExternalReference(1))
        n00 <- mkExternalNode(ExternalReference(1))
        n000 <- mkReferentNode("thing")
        _ <- mkEdge("edge1", n0, n00)
        _ <- mkEdge("edge1", n00, n000)
      } yield ()
    }

    val g1k = AdjacencyMeaningGraph.build {
      for {
        _ <- mkReferentNode("thing")
      } yield ()
    }

    assert(g1.kernel.normalise == g1k)

  }

  "External reference layers" should "be counted correctly" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("thing")
        n00 <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("label", n0, n00)
      } yield ()
    }

    assert(g1.externalLayerCount == 1)

    val g2 = AdjacencyMeaningGraph.build {
      for {
        n0 <- mkReferentNode("thing")
        n00 <- mkExternalNode(ExternalReference(0))
        n000 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("label", n0, n00)
        _ <- mkEdge("label", n00, n000)
      } yield ()
    }

    assert(g2.externalLayerCount == 2)
  }

  "Moving edges" should "work" in {

    val (g1, (e, a, b, c)) = AdjacencyMeaningGraph.buildReturning {
      for {
        _ <- normalise(false)
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        e <- mkEdge("e", a, b)
      } yield (e, a, b, c)
    }

    val g2 = AdjacencyMeaningGraph.build {
      for {
        _ <- normalise(false)
        _ <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        _ <- mkEdge("e", b, c)
      } yield ()
    }

    assert(g1.moveEdge(e, a, b) ~= g1)
    assert(g1.moveEdge(e, b, c) ~= g2)

  }

  "Normalising the graph" should "preserve all nodes in an unconnected graph" in {
    val unconnected = AdjacencyMeaningGraph.build {
      for {
        _ <- normalise(false)
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
        d <- mkReferentNode("d")
        e <- mkReferentNode("e")
        f <- mkReferentNode("f")
        _ <- mkEdge("de", d, e)
        _ <- mkEdge("df", d, f)
      } yield ()
    }

    val norm = unconnected.normalise
    assert(norm.elements.size == unconnected.elements.size)
  }

}
