package gramr
package split
package chartFilters

/** Selects split items based on a predicate on their span size (e.g. _ <= 2 to select
  * items spanning a maximum of 2 tokens).
  */
case class SpanSize[MR](f: Int => Boolean) extends SplitChartFilter[MR] {

  def instantiate(splitChart: SplitChart[MR]) = new SplitChartFilter.ItemFilter[MR] {

    def apply(item: PartialMR[MR]): Boolean = f(item.span.size)

  }

}
