package gramr.parse.scorer

import gramr.parse.Decision
import gramr.text.Span

/** DecisionScorers assign a label to a parsing decision.
  *
  * The label is a central element of chart parsing algorithms, and usually combines the extracted features with the
  * assigned score (see ScoredFeatures). However, other types of label are possible. The produced labels will usually
  * implement the ChartLabel typeclass.
  *
  * @tparam MR type of meaning representation
  * @tparam C type of context
  * @tparam L type of label, usually implementing ChartLabel
  */
trait DecisionScorer[MR, C, L] {

  def label(decision: Decision[MR], span: Span, context: C): L
}
