package gramr.graph
package edit
package sgraph

import cats.syntax.traverse._
import cats.instances.vector._
import gramr.graph.mark._
import gramr.util.VectorUtils
import scala.util.Try

import Edit._
import Graph.ops._

object Split {

  /** Determines the split direction of an edge, if it is identifiable. This is the case if both its adjacent nodes share the same direction.
    */
  def edgeDirection[MR : Graph](
    indexedEdge: Indexed[Edge], mr: MR
  ): Option[SplitDirection] = {
    val Indexed(Edge(_, source, target, _), _) = indexedEdge
    val sourceDirection = SplitDirection.of(mr.node(source))
    val targetDirection = SplitDirection.of(mr.node(target))
    if (sourceDirection == targetDirection) {
      Some(sourceDirection)
    }
    else {
      None
    }
  }

  /** Finds the set of nodes that are candidates for splitting.
    *
    * This is defined as all nodes that are labelled right and have neighbours that are labelled left.
    *
    * @return pointers to all frontier nodes
    */
  def frontierNodes[MR : Graph](mr: MR): Iterable[Indexed[Node]] = {
    mr.nodes.filter { in =>
      SplitDirection.of(in) == SplitRight &&
        mr.adjacentEdges(in.pointer).exists { ie =>
          edgeDirection(ie, mr).isEmpty }
    }
  }

  def annotateEdgeDirections[MR](mr: MR)(implicit mgl: Graph[MR]): Option[MR] = {
    (mr mapEdgeAttributesNonDeterministic { iel =>
      for {
        dir <- edgeDirection(iel, mr).toStream
      } yield dir.label(iel.element.attributes)
    }).headOption
  }

  def flipDirections[MR : Graph](mr: MR): MR = {
    mr.mapAttributes { iel: Indexed[Element] =>
      SplitDirection.query(iel) match {
        case Some(dir) => dir.opposite.label(iel.element.attributes)
        case None => iel.element.attributes
      }
    }
  }

  def finalizeSplitGraph[MR : Graph](mr: MR, direction: SplitDirection): MR = {
    val filtered = SplitDirection.erase(mr.filter(SplitDirection.of(_) == direction))

    val rerooted =
      if(filtered.isEmpty) {
        filtered
      }
      else {
        // Does this half of the split contain the original root node?
        filtered.nodes.find(_.element.attributes.contains("root")) match {
          case Some(root) => filtered.refocus(root.pointer)
          case None =>
            if(direction == SplitLeft) {
              val leftRoot = filtered.nodes.find(_.element.attributes.contains("leftRoot"))
              if(leftRoot.isEmpty) {
                throw new Exception("finalizeSplitGraph: leftRoot not found")
              }
              filtered.refocus(leftRoot.get.pointer)
            }
            else {
              val rightRoot = filtered.nodes.find(_.element.attributes.contains("rightRoot"))
              if(rightRoot.isEmpty) {
                throw new Exception("finalizeSplitGraph: rightRoot not found")
              }
              filtered.refocus(rightRoot.get.pointer)
            }
        }
      }

    rerooted.clearAttribute("root").clearAttribute("leftRoot").clearAttribute("rightRoot").normalise
  }

  def performSplit[MR : Graph](splitOperator: SplitOperator[MR])(mr: MR, backward: Boolean): Iterable[(MR, MR)] = {
    // Add a root attribute so we can transfer it to any newly created placeholders
    val rootedMR = mr.mapAttributes { case Indexed(el, p) => if(p == mr.root) el.attributes + ("root", DefaultAttribute(())) else el.attributes }
    // If this is a backward step, flip the assigned directions
    val flippedMR = if(backward) flipDirections(rootedMR) else rootedMR
    for {
      split <- splitOperator(flippedMR)
      annotatedEdgeDirections <- annotateEdgeDirections[MR](split)
      left = finalizeSplitGraph(annotatedEdgeDirections, SplitLeft)
      right = finalizeSplitGraph(annotatedEdgeDirections, SplitRight)
    } yield if(backward) (right, left) else (left, right)
  }

  def splitSemIdentity[MR](mr: MR)(implicit mgl: Graph[MR]): Iterable[MR] = {
    if(mr.nodes.exists(n => SplitDirection.of(n) != SplitRight)) {
      Iterable()
    }
    else {
      Iterable(mr)
    }
  }

  def splitSemApply[MR](maxCorefs: Int = 1)(mr: MR)(implicit mgl: Graph[MR]): Iterable[MR] = {
    if(mr.nodes.exists(n => SplitDirection.of(n) == SplitLeft)) {
      if(mr.nodes.exists(n => SplitDirection.of(n) == SplitRight)) {
        // Find out whether this is a modification – if so, we need to limit the arg graph to the root
        val modifying = SplitDirection.of(mr.node(mr.root)) == SplitRight

        val bins: Vector[VectorUtils.Bin[Indexed[Node]]] = Vector(
          // If we are splitting a modifier, the arg root can only be the root (no deep modification)
          VectorUtils.Bin(n => !modifying || n.pointer == mr.root, min = Some(1), max = Some(1)), // arg graph root
          VectorUtils.Bin(_ => true, max = Some(maxCorefs)), // coref
          VectorUtils.Bin(_.element.isExternalNode) // placeholders to unmerge
        )

        val frontier = frontierNodes(mr).toVector
        val binnings = VectorUtils.binnings[Indexed[Node]](frontier, bins).toVector
        for {
          Vector(Vector(rightRoot), corefs, placeholders) <- binnings
          (result, _) <- Try { splitSemApplyBinning(rightRoot, corefs, placeholders).perform(mr) }.toOption.toIterable.flatten
        } yield result
      }
      else {
        // Cannot split if right-hand side is empty
        Iterable()
      }
    }
    else {
      // All nodes are labelled “right” – extract an identity function
      val builder = mgl.builder(mr)
      builder.addExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty) + ("leftRoot" -> DefaultAttribute(())))
      Iterable(builder.build)
    }
  }

  def splitSemApplyBinning[MR](
    rightRoot: Indexed[Node],
    corefs: Vector[Indexed[Node]],
    placeholders: Vector[Indexed[Node]]
  )(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    for {
      rightRootMarker <- mark(rightRoot.pointer)
      // Set the root attribute for the argument root
      _ <- setAttribute[MR, DefaultAttribute[Unit]](rightRoot.pointer, ("rightRoot", DefaultAttribute(())))
      corefMarkers <- corefs.map(_.pointer).traverse(mark(_))
      _ <- corefMarkers.traverse(unfillCoref(_))
      placeholderMarkers <- placeholders.map(_.pointer).traverse(mark(_))
      _ <- placeholderMarkers.traverse(unmergePlaceholder(_))
      _ <- unfillPlaceholder(rightRootMarker)
    } yield ()
  }

  /** Splits a modify operator.
    *
    * Modification is like application where the function graph’s outermost placeholder is the root. However,
    * the modifier can be attached at a certain depth below the argument’s root. This covers cases like:
    *
    *     “opium farmers”
    *     opium: N/N (<0> :ARG1 (o / opium))
    *     farmers: N (p / person :ARG0-of (f / farm))
    *
    * Here, the modifier “opium” should be attached at depth 1.
    *
    * @param depth the depth of the node where the modifier is attached
    * @param unique if true, only split if there is a unique node at the given depth
    * @param maxCorefs number of coreferences that may be extracted
    * @param mr the meaning representation to split
    * @tparam MR type of meaning representation
    * @return all possible results from splitting
    */
  def splitSemModify[MR](depth: Int, unique: Boolean = true, maxCorefs: Int = 1)(mr: MR)(implicit mgl: Graph[MR]): Iterable[MR] = {
    val level: Set[Int] = levels(mr).applyOrElse(depth, (_: Int) => Set.empty[Int])
    val bins: Vector[VectorUtils.Bin[Indexed[Node]]] = Vector(
      VectorUtils.Bin(n => level.contains(n.pointer), min = Some(1), max = Some(1)), // modified node
      VectorUtils.Bin(_ => true, max = Some(maxCorefs)), // coref
      VectorUtils.Bin(_.element.isExternalNode) // placeholders to unmerge
    )

    if(mr.nodes.exists(n => SplitDirection.of(n) == SplitLeft)  // Cannot split if left-hand side or right-hand side is empty
      && mr.nodes.exists(n => SplitDirection.of(n) == SplitRight)
      && SplitDirection.of(mr.node(mr.root)) == SplitRight  // Root must be on the right
      && (!unique || level.size == 1)  // Observe uniqueness restriction
    ) {
      val frontier = frontierNodes(mr).toVector
      val binnings = VectorUtils.binnings[Indexed[Node]](frontier, bins).toVector
      for {
        Vector(Vector(modifiee), corefs, placeholders) <- binnings
        (result, _) <- Try { splitSemModifyBinning(modifiee, corefs, placeholders).perform(mr) }.toOption.toIterable.flatten
      } yield result
    }
    else {
      Iterable()
    }
  }

  def splitSemModifyBinning[MR](
    modifiee: Indexed[Node],
    corefs: Vector[Indexed[Node]],
    placeholders: Vector[Indexed[Node]]
  )(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    for {
      modifieeMarker <- mark(modifiee.pointer)
      corefMarkers <- corefs.map(_.pointer).traverse(mark(_))
      _ <- corefMarkers.traverse(unfillCoref(_))
      placeholderMarkers <- placeholders.map(_.pointer).traverse(mark(_))
      _ <- placeholderMarkers.traverse(unmergePlaceholder(_))
      unfilled <- unfillPlaceholder(modifieeMarker)
      // Set the leftRoot to refocus the function graph
      _ <- setAttribute[MR, DefaultAttribute[Unit]](unfilled, ("leftRoot", DefaultAttribute(())))
    } yield ()
  }

  def splitSemCompose[MR](maxCorefs: Int = 1)(mr: MR)(implicit mgl: Graph[MR]): Iterable[MR] = {
    val bins: Vector[VectorUtils.Bin[Indexed[Node]]] = Vector(
      VectorUtils.Bin(_ => true, min = Some(1), max = Some(1)), // arg graph root
      VectorUtils.Bin(_ => true, max = Some(maxCorefs)) // coref
    )

    if (mr.nodes.exists(n => SplitDirection.of(n) == SplitLeft)
      && mr.nodes.exists(n => SplitDirection.of(n) == SplitRight)) {
      // Check that all external nodes are left-labelled and that at least one external node exists
      if (mr.externalNodes.nonEmpty && mr.externalNodes.forall(SplitDirection.of(_) == SplitLeft)) {
        val frontier = frontierNodes(mr).toVector
        val binnings = VectorUtils.binnings[Indexed[Node]](frontier, bins).toVector
        for {
          Vector(Vector(rightRoot), corefs) <- binnings
          (result, _) <- Try {
            splitSemComposeBinning(rightRoot, corefs, placeholder(mr, 0)).perform(mr)
          }.toOption.toIterable.flatten
        } yield result
      }
      else {
        Iterable()
      }
    }
    else {
      // Cannot split when left-hand side or right-hand side is empty
      Iterable()
    }
  }

  def splitSemComposeBinning[MR](
    rightRoot: Indexed[Node],
    corefs: Vector[Indexed[Node]],
    outermostPlaceholder: Int
  )(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    for {
      rightRootMarker <- mark(rightRoot.pointer)
      // Set the root attribute for the argument root
      _ <- setAttribute[MR, DefaultAttribute[Unit]](rightRoot.pointer, ("rightRoot", DefaultAttribute(())))
      corefMarkers <- corefs.map(_.pointer).traverse(mark(_))
      _ <- corefMarkers.traverse(unfillCoref(_))
      // keep track of the current outermost placeholder
      outermostPlaceholderMarker <- mark(outermostPlaceholder)
      // unfill the argument placeholder, making it the outermost placeholder
      newPlaceholderIndex <- unfillPlaceholder(rightRootMarker)
      newPlaceholderMarker <- mark(newPlaceholderIndex)
      newPlaceholderReference <- getMR.map { _.node(newPlaceholderIndex).element.asInstanceOf[ExternalNode].reference }
      // swap the indices of the previous outermost placeholder and the added placeholder
      _ <- movePlaceholder(outermostPlaceholderMarker, newPlaceholderReference.index)
      _ <- movePlaceholder(newPlaceholderMarker, newPlaceholderReference.index - 1)
    } yield ()
  }

  def splitSemSubstitute[MR](maxCorefs: Int = 1)(mr: MR)(implicit mgl: Graph[MR]): Iterable[MR] = {
    val bins: Vector[VectorUtils.Bin[Indexed[Node]]] = Vector(
      VectorUtils.Bin(_ => true, min = Some(1), max = Some(1)), // arg graph root
      VectorUtils.Bin(_ => true, max = Some(maxCorefs)) // coref
    )

    if (mr.nodes.exists(n => SplitDirection.of(n) == SplitLeft) && mr.nodes.exists(n => SplitDirection.of(n) == SplitRight)) {
      // Check that all external nodes are right-labelled, except for the outermost external node
      if (mr.externalNodes.forall { in =>
        if(in.element.reference.index < mr.externalLayerCount - 1)
          SplitDirection.of(in) == SplitRight
        else
          SplitDirection.of(in) == SplitLeft
      }) {
        val frontier = frontierNodes(mr).toVector
        val binnings = VectorUtils.binnings[Indexed[Node]](frontier, bins).toVector
        for {
          Vector(Vector(rightRoot), corefs) <- binnings
          (result, _) <- Try {
            splitSemSubstituteBinning(rightRoot, corefs, placeholder(mr, 0)).perform(mr)
          }.toOption.toIterable.flatten
        } yield result
      }
      else {
        Iterable()
      }
    }
    else {
      // Cannot split when left-hand-side or right-hand side is empty
      Iterable()
    }
  }

  def splitSemSubstituteBinning[MR](
    rightRoot: Indexed[Node],
    corefs: Vector[Indexed[Node]],
    outermostPlaceholder: Int
  )(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    for {
      rightRootMarker <- mark(rightRoot.pointer)
      // Set the root attribute for the argument root
      _ <- setAttribute[MR, DefaultAttribute[Unit]](rightRoot.pointer, ("rightRoot", DefaultAttribute(())))
      corefMarkers <- corefs.map(_.pointer).traverse(mark(_))
      _ <- corefMarkers.traverse(unfillCoref(_))
      outermostPlaceholderMarker <- mark(outermostPlaceholder)
      // it should be guaranteed that the outermost placeholder is the only left-labelled placeholder
      // we move it to index zero
      _ <- movePlaceholder(outermostPlaceholderMarker, 0)
      // now unfill the argument placeholder, giving it index 1
      _ <- unfillPlaceholder(rightRootMarker)
    } yield ()
  }

  /** Split a frontier node into a placeholder (left-aligned) and a variable node (right-aligned).
    *
    * @param nodeMarker the frontier node to split
    * @return index of the added placeholder
    */
  def unfillPlaceholder[MR : Graph](nodeMarker: Marker): Edit[MR, Int] = {
    def addPlaceholder(mr: MR, unmerged: Int, isRoot: Boolean): (MR, Int) = {
      val builder = implicitly[Graph[MR]].builder(mr, normalise = false)
      val attributes = if(isRoot) SplitLeft.label(Attributes.empty) + ("leftRoot" -> DefaultAttribute(())) else SplitLeft.label(Attributes.empty)
      val newNode = builder.addExternalNode(
        ExternalReference((Iterable(-1) ++ mr.nodes.filter(SplitDirection.of(_) == SplitLeft).collect({case Indexed(ExternalNode(r, _), _) => r.index})).max + 1),
        attributes
      )
      (builder.build, newNode)
    }

    unmergeNodeImpl[MR](addPlaceholder)(nodeMarker)
  }

  def unfillCoref[MR : Graph](nodeMarker: Marker): Edit[MR, Int] = {
    for {
      rightSide <- choose(List(true, false))
      unmerged <- if(rightSide) unfillCorefRight(nodeMarker) else unfillCorefLeft(nodeMarker)
    } yield unmerged
  }

  /** Split a frontier node, creating a coreference on the left side.
    *
    * @param nodeMarker the frontier node to split
    * @return index of the added placeholder
    */
  def unfillCorefLeft[MR : Graph](nodeMarker: Marker): Edit[MR, Int] = {
    def addPlaceholder(mr: MR, unmerged: Int, isRoot: Boolean): (MR, Int) = {
      val builder = implicitly[Graph[MR]].builder(mr, normalise = false)
      val attributes = if(isRoot) SplitLeft.label(Attributes.empty) + ("leftRoot" -> DefaultAttribute(())) else SplitLeft.label(Attributes.empty)
      val placeholder = builder.addReferentNode("<coref>", attributes)
      (builder.build, placeholder)
    }

    unmergeNodeImpl[MR](addPlaceholder)(nodeMarker)
  }

  /** Split a frontier node, creating a coreference on the right side.
    *
    * @param nodeMarker the frontier node to split
    * @return index of the added placeholder
    */
  def unfillCorefRight[MR : Graph](nodeMarker: Marker): Edit[MR, Int] = {
    def addPlaceholder(mr: MR, unmerged: Int, isRoot: Boolean): (MR, Int) = {
      val builder = implicitly[Graph[MR]].builder(mr, normalise = false)
      // The original node must be labelled left (it was originally right)
      builder.replaceAttributes(unmerged, SplitLeft.label(mr.get(unmerged).element.attributes))
      val attributes = if(isRoot) SplitRight.label(Attributes.empty) + ("rightRoot" -> DefaultAttribute(())) else SplitRight.label(Attributes.empty)
      val placeholder = builder.addReferentNode("<coref>", attributes)
      (builder.build, placeholder)
    }

    unmergeNodeImpl[MR](addPlaceholder, SplitRight)(nodeMarker)
  }

  /** Split a frontier placeholder into one left-aligned and one right-aligned placeholder.
    *
    * @param nodeMarker the frontier node to split
    * @return index of the added placeholder
    */
  def unmergePlaceholder[MR : Graph](nodeMarker: Marker): Edit[MR, Int] = {
    def addPlaceholder(mr: MR, unmerged: Int, isRoot: Boolean): (MR, Int) = {
      val builder = implicitly[Graph[MR]].builder(mr, normalise = false)
      val index = mr.node(unmerged).element.asInstanceOf[ExternalNode].reference
      val attributes = if(isRoot) SplitLeft.label(Attributes.empty) + ("leftRoot" -> DefaultAttribute(())) else SplitLeft.label(Attributes.empty)
      val placeholder = builder.addExternalNode(index, attributes)
      (builder.build, placeholder)
    }

    unmergeNodeImpl[MR](addPlaceholder)(nodeMarker)
  }

  /** A generalized form of all unmerging operations.
    *
    * @param addPlaceholder a function that inserts a placeholder node into a meaning representation, depending on the node to be unmerged
    * @return index of the added placeholder
    */
  private def unmergeNodeImpl[MR : Graph](addPlaceholder: (MR, Int, Boolean) => (MR, Int), placeholderDirection: SplitDirection = SplitLeft)(nodeMarker: Marker): Edit[MR, Int] = {
    for {
      mr <- getMR[MR]
      node <- resolve[MR](nodeMarker)
      // Add the placeholder to the MR
      (withAddedNode, addedNode) = addPlaceholder(mr, node, mr.node(node).element.attributes.contains("rightRoot"))
      _ <- setMR(withAddedNode)
      // Change all the edges as necessary
      _ <- modifyMR { mr: MR =>
        val withMappedEdges = mr.mapEdges {
          case Indexed(e, ep) =>
            val newSource = if(e.source == node && SplitDirection.of(mr.node(e.target)) == placeholderDirection) addedNode else e.source
            val newTarget = if(e.target == node && SplitDirection.of(mr.node(e.source)) == placeholderDirection) addedNode else e.target
            Indexed(e.copy(source = newSource, target = newTarget), ep)
          }
        withMappedEdges
      }
    } yield addedNode
  }

}