package gramr

import gramr.graph.AmrTreeConversion.NodeName

import scala.annotation.tailrec
import scala.collection.mutable

package object amr {

  /** Provides an iterator over an AMR's nodes. */
  def nodeIterator(amr: AmrTree): Iterator[AmrTree] = amr match {
    case InnerNode(_, links, _) =>
      Iterator(amr) ++ links.iterator.flatMap(link => nodeIterator(link.subTree))
    case _ =>
      Iterator(amr)
  }

  /** Transforms an AMR tree so that all node names are unique.
    *
    * The algorithm only changes node labels at the definition site. All references to a node name are left untouched,
    * so they refer to the first definition of the name after the transformation.
    *
    * Only processes node names that are in the canonical form ([a-z]\d*).
    *
    * @param amr an AMR tree
    * @return a transformed AMR tree
    */
  def makeNamesUnique(amr: AmrTree): AmrTree = {
    // Put all existing node labels into a set, so that these labels are never re-invented
    val protectedLabels = mutable.Set.empty[String]
    protectedLabels ++= nodeIterator(amr).collect { case InnerNode(label, _, _) => label }

    // While scanning through the AMR, keep track of which node names we have seen
    val seenNodes = mutable.Set.empty[String]

    /** Invent an unused label for a node that has been found to have a duplicate label.
      *
      * @param label the node label to fix
      * @return a node label
      */
    @tailrec
    def inventLabel(label: String): String = {
      NodeName.parse(label) match {
        case Some(parsedName) if protectedLabels.contains(label) =>
          // This name doesn't work, try the next one
          val label2 = parsedName.copy(index = parsedName.index + 1)
          inventLabel(label2.toString)  // recursively increment the index
        case Some(parsedName) =>
          // This name works, protect and return it
          protectedLabels.add(parsedName.toString)
          label
        case None =>
          label
      }
    }

    def recurse(subTree: AmrTree): AmrTree = subTree match {
      case InnerNode(label, links, attr) =>
        val fixedLabel = if(!seenNodes.contains(label)) label else inventLabel(label)
        seenNodes.add(fixedLabel)
        val fixedLinks = links.map {
          case Link(llabel, lsubTree, lattr) => Link(llabel, recurse(lsubTree), lattr)
        }
        InnerNode(fixedLabel, fixedLinks, attr)
      case n => n
    }

    recurse(amr)
  }

}
