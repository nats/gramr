package gramr.construction

import gramr.ccg.{Combinator, CombinatoryStep, SyntacticCategory}

/** Context that is sufficient to determine whether a split pattern is applicable.
  */
case class SyntacticContext(
  combinator: Combinator,
  synCat: SyntacticCategory,
  childCats: List[SyntacticCategory]
)

object SyntacticContext {

  def fromStep(step: CombinatoryStep[Unit]) = SyntacticContext(
    step.combinator,
    step.synCat,
    step.subSteps.map(_.synCat)
  )

}
