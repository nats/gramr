package gramr.graph

import gramr.graph.mark.{Marker, MarkerInventor}

import Graph.ops._

package object edit {

  import Edit._

  import scala.language.implicitConversions

  class EditException(msg: String) extends Exception(msg)

  def noop[MR : Graph]: Edit[MR, Unit] = pure[MR, Unit](())

  implicit def markerInventor[MR : Graph]: MarkerInventor[EditState[MR]] =
    (s: EditState[MR]) => (Marker(s.nextMarker), s.copy(nextMarker = s.nextMarker + 1))

  def inventMarker[MR : Graph]: Edit[MR, Marker] = for {
    state <- get[MR]
    marker = Marker(state.nextMarker)
    _ <- modify { s: EditState[MR] => s.copy(nextMarker = s.nextMarker + 1) }
  } yield marker

  def mark[MR : Graph](pointer: Int): Edit[MR, Marker] = for {
    marker <- inventMarker
    _ <- modifyMR { mr: MR =>
      marker.insertAt(pointer, mr)
    }
  } yield marker

  def markNodeNamed[MR](name: String)(implicit mgl: Graph[MR]): Edit[MR, Marker] = {
    for {
      mr <- getMR[MR]
      node = mr.nodes.filter({ case Indexed(el, p) => el.attributes.getOption[DefaultAttribute[String]]("node_name").exists(_.value == name) }).head
      m <- mark(node.pointer)
    } yield m
  }

  def resolve[MR : Graph](marker: Marker): Edit[MR, Int] =
    inspect(s => marker.resolve(s.mr))

  def setRoot[MR : Graph](root: Marker): Edit[MR, Unit] = for {
    rootNode <- resolve(root)
    _ <- modifyMR { mr: MR =>
      mr.refocus(rootNode)
    }
  } yield ()

  def setAttribute[MR, A <: Attribute](pointer: Int, attribute: (String, A))(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    for {
      _ <- modifyMR { mr: MR =>
        val builder = mgl.builder(mr, normalise = false)
        val newAttributes = mr.get(pointer).element.attributes + attribute
        builder.replaceAttributes(pointer, newAttributes)
        builder.build
      }
    } yield ()
  }

  def build[MR : Graph, A](action: GraphBuilder[MR] => A): Edit[MR, A] = for {
    mr <- getMR[MR]
    (newMR, result) = {
      val builder = implicitly[Graph[MR]].builder(mr, normalise = false)
      val result = action(builder)
      (builder.build, result)
    }
    _ <- setMR(newMR)
  } yield result

  def relabelEdge[MR](edgePointer: Int, newLabel: String)(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    modifyMR { (mr: MR) =>
      val builder = mgl.builder(mr, normalise = false)
      builder.relabelEdge(edgePointer, newLabel)
      builder.build
    }
  }

  def mergeAttributes[MR : Graph](attr1: Attributes, attr2: Attributes): Attributes = {
    attr1.extend("markers",
      DefaultAttribute(
        attr1.getOption[DefaultAttribute[Set[Marker]]]("markers").getOrElse(DefaultAttribute(Set.empty)).value ++
          attr2.getOption[DefaultAttribute[Set[Marker]]]("markers").getOrElse(DefaultAttribute(Set.empty)).value)
    )
  }

  /** Left-biased merging of two nodes. Keeps the label of the first node.
    *
    * @param marker1 a marker for the first node to merge
    * @param marker2 a marker for the second node to merge
    */
  def mergeNodes[MR](marker1: Marker, marker2: Marker)(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    for {
      np1 <- resolve(marker1)
      np2 <- resolve(marker2)
      _ <- modifyMROrFail { mr: MR =>
        val newNode = mr.node(np1).element.setAttributes(mergeAttributes(mr.node(np1).element.attributes, mr.node(np2).element.attributes))
        val builder = mgl.builder(mr, normalise = false)
        var cycle = false
        builder.replaceNode(np1, newNode)
        mr.adjacentEdges(np2).foreach {
          case Indexed(Edge(_, source, target, _), edgePointer) =>
            val newSource = if (source == np2) np1 else source
            val newTarget = if (target == np2) np1 else target
            if (newSource == newTarget) {
              //throw CycleException(mr, np1, np2)
              cycle = true
            }
            builder.moveEdge(edgePointer, newSource, newTarget)
        }
        builder.remove(np2)
        if(mr.root == np2) {
          builder.setRoot(np1)
        }
        if(!cycle) Some(builder.build) else None
      }
    } yield ()
  }


  case class CycleException[MR : Graph](
    mr: MR,
    n1: Int,
    n2: Int
  ) extends EditException(
    s"Cycle created merging ${mr.node(n1).element.content} and ${mr.node(n2).element.content} in:\n$mr"
  )

}
