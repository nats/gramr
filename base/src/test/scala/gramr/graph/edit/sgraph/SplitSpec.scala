package gramr.graph
package edit
package sgraph

import gramr.graph.edit.sgraph.Split._
import gramr.util.Parsing
import org.scalatest.FlatSpec

class SplitSpec extends FlatSpec {

  import scala.language.existentials

  type MR = AdjacencyMeaningGraph

  implicit val graph: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance
  import graph.Building._

  "Unfilling placeholders" should "work in a simple example" in {
    val (original, c) = graph.buildReturning {
      for {
        a <- mkReferentNode("a", Attributes(Map("splitDirection" -> SplitLeft)))
        b <- mkReferentNode("b", Attributes(Map("splitDirection" -> SplitLeft)))
        c <- mkReferentNode("c", Attributes(Map("splitDirection" -> SplitRight)))
        d <- mkReferentNode("d", Attributes(Map("splitDirection" -> SplitRight)))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        _ <- mkEdge("cd", c, d)
        _ <- normalise(false)
      } yield c
    }

    val expected = graph.build {
      for {
        a <- mkReferentNode("a", Attributes(Map("splitDirection" -> SplitLeft)))
        b <- mkReferentNode("b", Attributes(Map("splitDirection" -> SplitLeft)))
        cp <- mkExternalNode(ExternalReference(0), Attributes(Map("splitDirection" -> DefaultAttribute(SplitLeft))))
        c <- mkReferentNode("c", Attributes(Map("splitDirection" -> SplitRight)))
        d <- mkReferentNode("d", Attributes(Map("splitDirection" -> SplitRight)))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, cp)
        _ <- mkEdge("cd", c, d)
        _ <- normalise(false)
      } yield ()
    }

    val results = (for {
      cMarker <- mark[AdjacencyMeaningGraph](c)
      _ <- unfillPlaceholder(cMarker)
    } yield ()).perform(original)

    assert(results.size == 1)
    val result = results.head._1

    assert(result.normalise ~= expected.normalise)
  }

  "Unfilling corefs" should "work in a simple example" in {
    val (original, b) = graph.buildReturning {
      for {
        a <- mkReferentNode("a", Attributes(Map("splitDirection" -> SplitLeft)))
        b <- mkReferentNode("b", Attributes(Map("splitDirection" -> SplitLeft)))
        c <- mkReferentNode("c", Attributes(Map("splitDirection" -> SplitRight)))
        d <- mkReferentNode("d", Attributes(Map("splitDirection" -> SplitRight)))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        _ <- mkEdge("cd", c, d)
        _ <- normalise(false)
      } yield b
    }

    val expected = graph.build {
      for {
        a <- mkReferentNode("a", Attributes(Map("splitDirection" -> SplitLeft)))
        b <- mkReferentNode("b", Attributes(Map("splitDirection" -> SplitLeft)))
        c <- mkReferentNode("c", Attributes(Map("splitDirection" -> SplitRight)))
        cp <- mkReferentNode("<coref>", Attributes(Map("splitDirection" -> SplitRight)))
        d <- mkReferentNode("d", Attributes(Map("splitDirection" -> SplitRight)))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc-of", c, cp)
        _ <- mkEdge("cd", c, d)
        _ <- normalise(false)
      } yield ()
    }

    val results = (for {
      bMarker <- mark[AdjacencyMeaningGraph](b)
      _ <- unfillCorefRight(bMarker)
    } yield ()).perform(original)

    assert(results.size == 1)
    val result = results.head._1

    assert(result.normalise ~= expected.normalise)
  }

  "Unmerging placeholders" should "work in a simple example" in {
    val (original, p) = graph.buildReturning {
      for {
        a <- mkReferentNode("a", SplitLeft.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        p <- mkExternalNode(ExternalReference(0), SplitRight.label(Attributes.empty))
        _ <- mkEdge("bpb", b, p)
        _ <- mkEdge("cpc", c, p)
        _ <- normalise(false)
      } yield p
    }

    val expected = graph.build {
      for {
        a <- mkReferentNode("a", SplitLeft.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        pb <- mkExternalNode(ExternalReference(0), SplitRight.label(Attributes.empty))
        pc <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("bpb", b, pb)
        _ <- mkEdge("cpc", c, pc)
        _ <- normalise(false)
      } yield ()
    }

    val results = (for {
      pMarker <- mark[AdjacencyMeaningGraph](p)
      _ <- unmergePlaceholder(pMarker)
    } yield ()).perform(original)

    assert(results.size == 1)
    val result = results.head._1

    assert(result.normalise == expected.normalise)
  }

  "Splitting applications" should "work on a minimal example" in {
    val original = graph.build {
      for {
        a <- mkReferentNode("a", SplitLeft.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitRight.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    val expected = graph.build {
      for {
        a <- mkReferentNode("a", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitRight.label(Attributes.empty))
        _ <- mkEdge("ab", a, p)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    val splits = splitSemApply(1)(original).map(_.normalise).toVector
    assert(splits.size == 1)
    assert(splits(0) ~= expected)
  }

  it should "correctly split a first-order application when the function term is binary" in {
    val expected = graph.build {
      for {
        marry <- mkReferentNode("marry", SplitLeft.label(Attributes.empty))
        p0 <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        p1 <- mkExternalNode(ExternalReference(1), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
        _ <- mkReferentNode("manny", SplitRight.label(Attributes.empty))
      } yield ()
    }

    val original = graph.build {
      for {
        marry <- mkReferentNode("marry", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        manny <- mkReferentNode("manny", SplitRight.label(Attributes.empty))
        _ <- mkEdge("ARG0", marry, p)
        _ <- mkEdge("ARG1", marry, manny)
      } yield ()
    }

    val splits = splitSemApply(1)(original).map(_.normalise).toVector

    assert(splits.size == 1)
    assert(splits(0) ~= expected)
  }

  it should "correctly split off an identity function" in {
    val original = graph.build {
      for {
        _ <- mkReferentNode("something", SplitRight.label(Attributes.empty))
      } yield ()
    }

    val expected = graph.build {
      for {
        _ <- mkReferentNode("something", SplitRight.label(Attributes.empty))
        _ <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
      } yield ()
    }

    val splits = splitSemApply(1)(original).map(_.normalise).toVector
    assert(splits.size == 1)
    assert(splits(0) ~= expected)
  }

  it should "not split off an empty argument" in {
    val original = graph.build {
      for {
        _ <- mkReferentNode("something", SplitLeft.label(Attributes.empty))
      } yield ()
    }

    val splits = splitSemApply(1)(original).map(_.normalise).toVector
    assert(splits.isEmpty)
  }

  it should "work in a simple composition" in {
    val expected = graph.build {
      for {
        possible <- mkReferentNode("possible", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("domain", possible, p)

        marry <- mkReferentNode("marry", SplitRight.label(Attributes.empty))
        p0 <- mkExternalNode(ExternalReference(0), SplitRight.label(Attributes.empty))
        p1 <- mkExternalNode(ExternalReference(1), SplitRight.label(Attributes.empty))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val original = graph.build {
      for {
        possible <- mkReferentNode("possible", SplitLeft.label(Attributes.empty))
        marry <- mkReferentNode("marry", SplitRight.label(Attributes.empty))
        _ <- mkEdge("domain", possible, marry)
        p0 <- mkExternalNode(ExternalReference(0), SplitRight.label(Attributes.empty))
        p1 <- mkExternalNode(ExternalReference(1), SplitRight.label(Attributes.empty))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val splits = splitSemApply(1)(original).map(_.normalise).toVector

    assert(splits.size == 1)
    assert(splits(0) ~= expected)
  }

  it should "not split deep modification" in {
    val original = graph.build {
      for {
        a <- mkReferentNode("a", SplitRight.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    val splits = splitSemApply()(original).map(_.normalise).toVector
    assert(splits.isEmpty)
  }

  it should "not extract any coref nodes if there is no coreference" in {
    val original = graph.build {
      for {
        l <- mkReferentNode("left", SplitLeft.label(Attributes.empty))
        r <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        _ <- mkEdge("op1", l, r)
      } yield ()
    }

    val expected = graph.build {
      for {
        l <- mkReferentNode("left", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("op1", l, p)

        r <- mkReferentNode("right", SplitRight.label(Attributes.empty))
      } yield ()
    }

    val splits0 = splitSemApply(0)(original).map(_.normalise).toVector
    assert(splits0.size == 1)
    assert(splits0(0) ~= expected)

    val splits1 = splitSemApply(1)(original).map(_.normalise).toVector
    assert(splits1.size == 1)
    assert(splits1(0) ~= splits0(0))

    val splits2 = splitSemApply(2)(original).map(_.normalise).toVector
    assert(splits2.size == 1)
    assert(splits2(0) ~= splits0(0))
  }

  it should "extract a coreference when a placeholder is referenced multiple times" in {
    // “A said it wants B”
    val original = graph.build {
      for {
        say <- mkReferentNode("say", SplitLeft.label(Attributes.empty))
        a <- mkExternalNode(ExternalReference(0), SplitRight.label(Attributes.empty))
        want <- mkReferentNode("want", SplitRight.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        _ <- mkEdge("ARG0", say, a)
        _ <- mkEdge("ARG1", say, want)
        _ <- mkEdge("ARG0", want, a)
        _ <- mkEdge("ARG1", want, b)
      } yield ()
    }

    val expected = graph.build {
      for {
        say <- mkReferentNode("say", SplitLeft.label(Attributes.empty))
        a <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        wantRef <- mkExternalNode(ExternalReference(1), SplitLeft.label(Attributes.empty))
        want <- mkReferentNode("want", SplitRight.label(Attributes.empty))
        aCoref <- mkReferentNode("<coref>", SplitRight.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        _ <- mkEdge("ARG0", say, a)
        _ <- mkEdge("ARG1", say, wantRef)
        _ <- mkEdge("ARG0", want, aCoref)
        _ <- mkEdge("ARG1", want, b)
      } yield ()
    }

    val splits1 = splitSemApply(1)(original).map(_.normalise).toVector
    assert(splits1.exists(g => g ~= expected))
  }

  it should "extract all options for a single coreference" in {
    val original = graph.build {
      for {
        l <- mkReferentNode("left", SplitLeft.label(Attributes.empty))
        r1 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r2 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        _ <- mkEdge("op1", l, r1)
        _ <- mkEdge("op2", l, r2)
        _ <- mkEdge("e", r1, r2)
      } yield ()
    }

    val expected1 = graph.build {
      for {
        l <- mkReferentNode("left", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        c <- mkReferentNode("<coref>", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("op1", l, p)
        _ <- mkEdge("op2", l, c)

        r1 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r2 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        _ <- mkEdge("e", r1, r2)
      } yield ()
    }

    val expected2 = graph.build {
      for {
        l <- mkReferentNode("left", SplitLeft.label(Attributes.empty))
        c <- mkReferentNode("<coref>", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("op1", l, c)
        _ <- mkEdge("op2", l, p)

        r1 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r2 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        _ <- mkEdge("e", r1, r2)
      } yield ()
    }

    // Representations where the coref is extracted to the right aren’t checked here

    val splits0 = splitSemApply(0)(original).map(_.normalise).toVector
    assert(splits0.isEmpty)

    val splits1 = splitSemApply(1)(original).map(_.normalise).toVector
    assert(splits1.exists(g => g ~= expected1))
    assert(splits1.exists(g => g ~= expected2))

    val splits2 = splitSemApply(2)(original).map(_.normalise).toVector
    assert(splits2.exists(g => g ~= expected1))
    assert(splits2.exists(g => g ~= expected2))
  }

  it should "extract all options for two coreferences" in {
    val original = graph.build {
      for {
        l <- mkReferentNode("left", SplitLeft.label(Attributes.empty))
        r1 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r2 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r3 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        _ <- mkEdge("op1", l, r1)
        _ <- mkEdge("op2", l, r2)
        _ <- mkEdge("op3", l, r3)
        _ <- mkEdge("e", r1, r2)
        _ <- mkEdge("e", r2, r3)
      } yield ()
    }

    val expected1 = graph.build {
      for {
        l <- mkReferentNode("left", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        c1 <- mkReferentNode("<coref>", SplitLeft.label(Attributes.empty))
        c2 <- mkReferentNode("<coref>", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("op1", l, p)
        _ <- mkEdge("op2", l, c1)
        _ <- mkEdge("op3", l, c2)

        r1 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r2 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r3 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        _ <- mkEdge("e", r1, r2)
        _ <- mkEdge("e", r2, r3)
      } yield ()
    }

    val expected2 = graph.build {
      for {
        l <- mkReferentNode("left", SplitLeft.label(Attributes.empty))
        c1 <- mkReferentNode("<coref>", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        c2 <- mkReferentNode("<coref>", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("op1", l, c1)
        _ <- mkEdge("op2", l, p)
        _ <- mkEdge("op3", l, c2)

        r1 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r2 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r3 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        _ <- mkEdge("e", r1, r2)
        _ <- mkEdge("e", r2, r3)
      } yield ()
    }

    val expected3 = graph.build {
      for {
        l <- mkReferentNode("left", SplitLeft.label(Attributes.empty))
        c1 <- mkReferentNode("<coref>", SplitLeft.label(Attributes.empty))
        c2 <- mkReferentNode("<coref>", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("op1", l, c1)
        _ <- mkEdge("op2", l, c2)
        _ <- mkEdge("op3", l, p)

        r1 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r2 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        r3 <- mkReferentNode("right", SplitRight.label(Attributes.empty))
        _ <- mkEdge("e", r1, r2)
        _ <- mkEdge("e", r2, r3)
      } yield ()
    }

    val splits0 = splitSemApply(0)(original).map(_.normalise).toVector
    assert(splits0.isEmpty)

    val splits1 = splitSemApply(1)(original).map(_.normalise).toVector
    assert(splits1.isEmpty)

    val splits2 = splitSemApply(2)(original).map(_.normalise).toVector
    assert(splits2.exists(g => g ~= expected1))
    assert(splits2.exists(g => g ~= expected2))
    assert(splits2.exists(g => g ~= expected3))
  }

  "Splitting modify" should "extract a modifier at depth 0" in {
    val original = graph.build {
      for {
        a <- mkReferentNode("a", SplitRight.label(Attributes.empty))
        b <- mkReferentNode("b", SplitLeft.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    // depth 0 should be equivalent to application
    val expected = graph.build {
      for {
        a <- mkReferentNode("a", SplitRight.label(Attributes.empty))
        b <- mkReferentNode("b", SplitLeft.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", p, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    val splits = splitSemModify(depth = 0)(original).map(_.normalise).toVector
    assert(splits.size == 1)
    assert(splits(0) ~= expected)
  }

  it should "not extract a modifier at depth 1 if performed for depth 0" in {
    val original = graph.build {
      for {
        a <- mkReferentNode("a", SplitRight.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    val splits = splitSemModify(depth = 0)(original).map(_.normalise).toVector
    assert(splits.isEmpty)
  }

  it should "extract a modifier at depth 1" in {
    val original = graph.build {
      for {
        a <- mkReferentNode("a", SplitRight.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    val expected = graph.build {
      for {
        a <- mkReferentNode("a", SplitRight.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", p, c)
      } yield ()
    }

    val splits = splitSemModify(depth = 1)(original).map(_.normalise).toVector
    assert(splits.size == 1)
    assert(splits(0) ~= expected)
  }

  it should "not extract a modifier at depth 0 if performed for depth 1" in {
    val original = graph.build {
      for {
        a <- mkReferentNode("a", SplitRight.label(Attributes.empty))
        b <- mkReferentNode("b", SplitLeft.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    val splits = splitSemModify(depth = 1)(original).map(_.normalise).toVector
    assert(splits.isEmpty)
  }

  it should "observe the uniqueness constraint" in {
    val original = graph.build {
      for {
        a <- mkReferentNode("a", SplitRight.label(Attributes.empty))
        b1 <- mkReferentNode("b1", SplitRight.label(Attributes.empty))
        b2 <- mkReferentNode("b2", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab1", a, b1)
        _ <- mkEdge("ab2", a, b2)
        _ <- mkEdge("b2c", b2, c)
      } yield ()
    }  // Level 1 is not unique, there are both b1 and b2

    val splits = splitSemModify(depth = 1)(original).map(_.normalise).toVector
    assert(splits.isEmpty)

    val expected = graph.build {
      for {
        a <- mkReferentNode("a", SplitRight.label(Attributes.empty))
        b1 <- mkReferentNode("b1", SplitRight.label(Attributes.empty))
        b2 <- mkReferentNode("b2", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        p <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab1", a, b1)
        _ <- mkEdge("ab2", a, b2)
        _ <- mkEdge("b2c", p, c)
      } yield ()
    }

    val splitsNonUnique = splitSemModify(depth = 1, unique = false)(original).map(_.normalise).toVector
    assert(splitsNonUnique.size == 1)
    assert(splitsNonUnique(0) ~= expected)
  }

  it should "not include the root in the modifier" in {
    val original = graph.build {
      for {
        a <- mkReferentNode("a", SplitLeft.label(Attributes.empty))
        b <- mkReferentNode("b", SplitRight.label(Attributes.empty))
        c <- mkReferentNode("c", SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    val splits = splitSemModify(depth = 1)(original).map(_.normalise).toVector
    assert(splits.isEmpty)
  }

  "Splitting compose" should "work in a transitive verb example" in {
    val mannyMarried = graph.build {
      for {
        marry <- mkReferentNode("marry", SplitLeft.label(Attributes.empty))
        arg0 <- mkReferentNode("manny", SplitRight.label(Attributes.empty))
        arg1 <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("ARG0", marry, arg0)
        _ <- mkEdge("ARG1", marry, arg1)
      } yield ()
    }

    val marry = graph.build {
      for {
        marry <- mkReferentNode("marry")
        arg0 <- mkExternalNode(ExternalReference(0))
        arg1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, arg0)
        _ <- mkEdge("ARG1", marry, arg1)
      } yield ()
    }

    val manny = graph.build {
      for {
        _ <- mkReferentNode("manny")
      } yield ()
    }

    val result = performSplit(splitSemCompose[MR]())(mannyMarried, backward = false).toVector
    assert(result.size == 1)
    val Vector((l, r)) = result

    assert(l ~= marry)
    assert(r ~= manny)
  }

  it should "work in the presence of extra placeholders" in {
    val mannyMarried = graph.build {
      for {
        marry <- mkReferentNode("marry", SplitLeft.label(Attributes.empty))
        domain <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        arg0 <- mkReferentNode("manny", SplitRight.label(Attributes.empty))
        arg1 <- mkExternalNode(ExternalReference(1), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("domain", marry, domain)
        _ <- mkEdge("ARG0", marry, arg0)
        _ <- mkEdge("ARG1", marry, arg1)
      } yield ()
    }

    val marry = graph.build {
      for {
        marry <- mkReferentNode("marry")
        domain <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        arg0 <- mkExternalNode(ExternalReference(1))
        arg1 <- mkExternalNode(ExternalReference(2))
        _ <- mkEdge("domain", marry, domain)
        _ <- mkEdge("ARG0", marry, arg0)
        _ <- mkEdge("ARG1", marry, arg1)
      } yield ()
    }

    val manny = graph.build {
      for {
        _ <- mkReferentNode("manny")
      } yield ()
    }

    val result = performSplit(splitSemCompose[MR]())(mannyMarried, backward = false).toVector
    assert(result.size == 1)
    val Vector((l, r)) = result

    assert(l ~= marry)
    assert(r ~= manny)
  }

  it should "fail if there are any external references labelled “right”" in {
    val mannyMarried = graph.build {
      for {
        marry <- mkReferentNode("marry", SplitLeft.label(Attributes.empty))
        domain <- mkExternalNode(ExternalReference(0), SplitRight.label(Attributes.empty))
        arg0 <- mkReferentNode("manny", SplitRight.label(Attributes.empty))
        arg1 <- mkExternalNode(ExternalReference(1), SplitLeft.label(Attributes.empty))
        _ <- mkEdge("domain", marry, domain)
        _ <- mkEdge("ARG0", marry, arg0)
        _ <- mkEdge("ARG1", marry, arg1)
      } yield ()
    }

    val result = performSplit(splitSemCompose[MR]())(mannyMarried, backward = false).toVector
    assert(result.isEmpty)
  }

  it should "fail if there are less than two placeholders" in {
    val mannyMarried = graph.build {
      for {
        marry <- mkReferentNode("marry", SplitLeft.label(Attributes.empty))
        arg0 <- mkReferentNode("manny", SplitRight.label(Attributes.empty))
        _ <- mkEdge("ARG0", marry, arg0)
      } yield ()
    }

    val result = performSplit(splitSemCompose[MR]())(mannyMarried, backward = false).toVector
    assert(result.isEmpty)
  }

  "Splitting substitutions" should "work in a conjunction with 0-ary arguments" in {
    val and = graph.build {
      for {
        and <- mkReferentNode("and")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("op1", and, p0)
        _ <- mkEdge("op2", and, p1)
      } yield ()
    }

    val tea = graph.build {
      for {
        _ <- mkReferentNode("tea")
      } yield ()
    }

    val andtea = graph.build {
      for {
        and <- mkReferentNode("and", SplitLeft.label(Attributes.empty))
        p0 <- mkExternalNode(ExternalReference(0), SplitLeft.label(Attributes.empty))
        tea <- mkReferentNode("tea", SplitRight.label(Attributes.empty))
        _ <- mkEdge("op1", and, p0)
        _ <- mkEdge("op2", and, tea)
      } yield ()
    }

    val result = performSplit(splitSemSubstitute[MR]())(andtea, backward = false).toVector
    assert(result.size == 1)
    val Vector((l, r)) = result

    assert(l ~= and)
    assert(r ~= tea)
  }

  it should "work in a conjunction of functional terms" in {
    val and = graph.build {
      for {
        and <- mkReferentNode("and")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("op1", and, p0)
        _ <- mkEdge("op2", and, p1)
      } yield ()
    }

    val married = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val expAndmarried = graph.build {
      for {
        and <- mkReferentNode("and", SplitLeft.label(Attributes.empty))
        op1 <- mkExternalNode(ExternalReference(2), SplitLeft.label(Attributes.empty))
        marry <- mkReferentNode("marry", SplitRight.label(Attributes.empty))
        p0 <- mkExternalNode(ExternalReference(0), SplitRight.label(Attributes.empty))
        p1 <- mkExternalNode(ExternalReference(1), SplitRight.label(Attributes.empty))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
        _ <- mkEdge("op1", and, op1)
        _ <- mkEdge("op2", and, marry)
      } yield ()
    }

    val result = performSplit(splitSemSubstitute[MR]())(expAndmarried, backward = false).toVector
    assert(result.size == 1)
    val Vector((l, r)) = result

    assert(l ~= and)
    assert(r ~= married)
  }

}