package gramr.parse

import gramr.graph.Graph
import gramr.graph.evaluate.Smatch
import gramr.parse.chart.Chart
import gramr.text.{Example, HasMR}
import Example.ops._
import gramr.util.SimpleRetrievalStats

package object evaluate {

  def evaluateParse[MR: Graph, FV, E: Example](
    chart: Chart[MR, FV],
    example: E,
    evaluate: (MR, E) => SimpleRetrievalStats
  )(implicit hasMR: HasMR[E, MR]): ParseStats = {
    if(chart.outputStates.isEmpty) {
      val failRetrievalStats = SimpleRetrievalStats(new Smatch[MR]().tripleCount(example.reference), 0, 0)
      ParseStats(successful = false, failRetrievalStats, failRetrievalStats)
    }
    else {
      val bestFoundStats = chart.outputStates.map { state => evaluate(state.mr, example) }.maxBy(_.f1)
      val bestScoredStats = evaluate(chart.outputStates.maxBy(_.score).mr, example)
      ParseStats(successful = true, bestFoundParse = bestFoundStats, bestScoredParse = bestScoredStats)
    }
  }

}
