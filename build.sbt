scalaVersion := "2.12.8"

cancelable in Global := true

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)

lazy val commonSettings = Seq(
  organization := "de.uni-hamburg",
  version := "306a",
  scalaVersion := "2.12.8",
  scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation"),
  testOptions in Test += Tests.Argument("-oF"),
  assemblyMergeStrategy in assembly := {
    case "reflect.properties" => MergeStrategy.discard  // this file is present in several libraries
    case x =>
      val oldStrategy = (assemblyMergeStrategy in assembly).value
      oldStrategy(x)
  }
)

lazy val gramr = (project in file(".")).
  aggregate(base, experiments).
  dependsOn(experiments).
  settings(commonSettings: _*).
  settings(
    name := "gramr",
    mainClass in Compile := Some("gramr.experiments.RunScript"),
    fork in run := true,
    javaOptions += "-Xmx8g",
    outputStrategy := Some(StdoutOutput)
  )

lazy val base = (project in file("base")).
  settings(commonSettings: _*).
  settings(
    name := "gramr-base",
    libraryDependencies ++= Seq(
      parserCombinators, parboiled, scalatest, scalatags, logback, scala_logging, commonsCsv,
      nscala_time, monix,
      cats_macros, cats_kernel, cats_core,
      monocleCore, monocleMacro, monocleLaw,
      breeze
    )
  )

lazy val experiments = (project in file("experiments")).
  dependsOn(base).
  settings(commonSettings: _*).
  settings(
    name := "gramr-experiments",
    libraryDependencies ++= Seq(
      logback, scala_logging,
      "org.scala-lang" % "scala-compiler" % scalaVersion.value,
      ammonite
    ),
    fork in run := false
  )

lazy val scalatest = "org.scalatest" %% "scalatest" % "3.0.4" % "test"
lazy val scalatags = "com.lihaoyi" %% "scalatags" % "0.6.7"
lazy val parserCombinators = "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"
lazy val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
lazy val scala_logging = "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0"
lazy val parboiled = "org.parboiled" %% "parboiled" % "2.1.4"
lazy val nscala_time = "com.github.nscala-time" %% "nscala-time" % "2.16.0"
lazy val commonsCsv = "org.apache.commons" % "commons-csv" % "1.5"
lazy val monix = "io.monix" %% "monix" % "3.0.0-RC2"

lazy val cats_macros = "org.typelevel" %% "cats-macros" % "1.0.0-MF"
lazy val cats_kernel = "org.typelevel" %% "cats-kernel" % "1.0.0-MF"
lazy val cats_core = "org.typelevel" %% "cats-core" % "1.0.0-MF"

lazy val monocleVersion = "1.4.0"
lazy val monocleCore = "com.github.julien-truffaut" %%  "monocle-core"  % monocleVersion
lazy val monocleMacro = "com.github.julien-truffaut" %%  "monocle-macro" % monocleVersion
lazy val monocleLaw = "com.github.julien-truffaut" %%  "monocle-law"   % monocleVersion % "test"

lazy val breeze = "org.scalanlp" %% "breeze" % "0.13.2"

lazy val ammonite = "com.lihaoyi" % "ammonite" % "2.3.8" cross CrossVersion.full
