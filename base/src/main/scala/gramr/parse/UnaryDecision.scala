package gramr.parse

import gramr.construction.SyntacticContext

case class UnaryDecision[MR](
  result: MR,
  context: SyntacticContext
) extends CombinatoryDecision[MR]
