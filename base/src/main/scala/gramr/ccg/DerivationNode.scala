package gramr.ccg

import gramr.text.{Span, Token}

case class DerivationNode[L](
  sentence: Vector[Token],
  span: Span,
  synCat: SyntacticCategory,
  label: L
) {

  def tokens: Vector[Token] = span.in(sentence)

}
