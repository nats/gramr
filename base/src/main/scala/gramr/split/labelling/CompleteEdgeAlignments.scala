package gramr
package split
package labelling

import gramr.graph._
import ccg._
import Graph.ops._
import gramr.graph.edit.{SplitDirection, SplitLeft, SplitRight, SplitUnconstrained}

/** Given a graph that already has a complete node labelling, computes the set of possible edge colourings.
  *
  * - If an edge is already constrained, it will keep its label
  * - If an edge is unconstrained and both adjacent nodes have the same label, the edge gets that label
  * - Otherwise, a left or right labelling will be assigned non-deterministically
  *
  * All possible combinations of edge labellings under these rules are returned.
  */
case class CompleteEdgeAlignments[MR : Graph]() extends LabellingFunction[MR] {

  def label(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): Stream[PartialMR[MR]] = {
    val PartialMR(mr, span, synCat) = partialMR
    val newMRs = mr.mapEdgeAttributesNonDeterministic {
      case Indexed(edge, _) =>
        val directions = edge.attributes.get[SplitDirection]("splitDirection") match {
          case SplitLeft => Stream(SplitLeft)
          case SplitRight => Stream(SplitRight)
          case SplitUnconstrained =>
            val sourceDirection = mr.node(edge.source).element.attributes.get[SplitDirection]("splitDirection")
            val targetDirection = mr.node(edge.target).element.attributes.get[SplitDirection]("splitDirection")
            if (sourceDirection == targetDirection)
              Stream(sourceDirection)
            else
              Stream(SplitLeft, SplitRight)
        }
        directions.map { direction => edge.attributes + ("splitDirection" -> direction) }
    }
    newMRs.map { newMR => PartialMR(newMR, span, synCat) }
  }
}
