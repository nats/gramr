package gramr.util

/**
  * Implements information retrieval style statistics.
  */
trait RetrievalStats {

  def total: Int

  // instances to be retrieved
  def correct: Int

  // retrieved relevant instances
  def incorrect: Int // retrieved but irrelevant instances

  def precision: Double =
    if (correct.toDouble + incorrect.toDouble > 0.0)
      correct.toDouble / (correct.toDouble + incorrect.toDouble)
    else
      1.0

  def recall: Double =
    if (total.toDouble > 0.0)
      correct.toDouble / total.toDouble
    else
      1.0

  def f1: Double =
    if (precision + recall > 0.0)
      2.0 * ((precision * recall) / (precision + recall))
    else
      0.0

}


case class SimpleRetrievalStats(total: Int, correct: Int, incorrect: Int) extends RetrievalStats {

  def +(other: RetrievalStats): SimpleRetrievalStats =
    SimpleRetrievalStats(total + other.total, correct + other.correct, incorrect + other.incorrect)

  def display: String = f"p=$precision%.4f r=$recall%.4f f1=$f1%.4f"

}

object SimpleRetrievalStats {

  def empty = SimpleRetrievalStats(0, 0, 0)

}