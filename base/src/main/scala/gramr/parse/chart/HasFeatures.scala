package gramr.parse.chart

trait HasFeatures[FV, L] {

  def features(l: L): FV

}
