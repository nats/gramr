package gramr
package split

import gramr.text.Token

/** Defines a predicate on Split objects to limit the grammar induction search space.
  *
  * SentenceSplitters apply a split filter to every new candidate split to decide whether it should be kept
  * (and processed further). Therefore, SplitFilters can be used to weed out incorrect or implausible splitting results.
  *
  * SplitFilters can be combined using the `and` and `or` combinators.
  */
trait SplitFilter[MR] {
  self =>

  def apply(split: Split[MR], sentence: Vector[Token]): Boolean

  def and(other: SplitFilter[MR]): SplitFilter[MR] =
    (split: Split[MR], sentence: Vector[Token]) => self(split, sentence) && other(split, sentence)

  def or(other: SplitFilter[MR]): SplitFilter[MR] =
    (split: Split[MR], sentence: Vector[Token]) => self(split, sentence) || other(split, sentence)

}
