package gramr.jobs

import com.typesafe.scalalogging.LazyLogging
import gramr.parse.Parser
import gramr.text.Example
import Example.ops._
import gramr.ccg.Derivation
import gramr.split.{SentenceSplittingAlgorithm, SplitChart}

import scala.collection.immutable.ListMap
import scala.util.Try


class ExampleOps[MR, E: Example](job: Job[E, E]) extends LazyLogging {

  import job.scheduler

  /** Parses each sentence in parallel.
    *
    * @param parser the parser to parse with
    * @param parallelism number of concurrent parse jobs
    * @tparam FV type of feature vector
    * @return tuples of examples and charts produced by the parser
    */
  def parse[FV](
    parser: Parser[MR, FV, E],
    parallelism: Int,
    inOrder: Boolean = true,
    name: String = "parse"
  ): Job[E, (E, NamedParses[MR, FV])] = {
    multiParse(ListMap(name -> parser), parallelism, inOrder)
  }

  /** Parses each sentence with two parsers and yields both charts.
    *
    * @param first the first parser
    * @param second the second parser
    * @param parallelism number of concurrent parse jobs
    * @tparam FV type of feature vector
    * @return tuples of examples and charts produced by both parsers
    */
  def dualParse[FV](
    first: Parser[MR, FV, E],
    second: Parser[MR, FV, E],
    parallelism: Int,
    firstName: String = "first",
    secondName: String = "second",
    inOrder: Boolean = true
  ): Job[E, (E, NamedParses[MR, FV])] = {
    val parsers = ListMap(firstName -> first, secondName -> second)
    multiParse(parsers, parallelism, inOrder)
  }

  def multiParse[FV](
    parsers: NamedParsers[MR, FV, E],
    parallelism: Int,
    inOrder: Boolean,
  ): Job[E, (E, NamedParses[MR, FV])] = {
    def computation(example: E): (E, NamedParses[MR, FV]) = {
      val charts = parsers.map {
        case (name, parser) =>
          logger.trace(s"${job.name}: Parsing ${example.id} with $name")
          val chart = parser.parse(example.sentence, example)
          logger.trace(s"${job.name}: Finished parsing ${example.id} with $name")
          (name, chart)
      }

      (example, charts)
    }

    job.append(ParallelComputeStage(computation, parallelism, inOrder))
  }

}
