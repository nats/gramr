package gramr.ccg

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import gramr.graph.Graph
import gramr.graph.lexemes.LexemeDictionary.LexemeItem
import gramr.graph.lexemes.{LexemeDictionary, WildcardFiller}
import gramr.parse.{LexicalChoice, LexicalDecision, SyntaxInformation, TagDistribution}
import gramr.text.{Example, Token}

import Example.ops._

class DelexedLexicon[MR : Graph](
  val delexed: StoredLexicon[MR],
  val lexemes: LexemeDictionary
) extends Lexicon[MR] with LazyLogging {

  /**
    * Retrieves all lexical items for the given key.
    */
  override def get(key: LexiconKey): Iterable[LexicalItem[MR]] = {
    val delexedKey = key.copy(tokens = Vector(Token("*", "*")))
    val templates = delexed.get(delexedKey)
    val matchingLexemes = lexemes.get(key.tokens)
    val lexedTemplates = templates.flatMap(lexicalizeLexItem(matchingLexemes))
    lexedTemplates
  }

  def lexicalizeLexItem(matchingLexemeItems: Iterable[LexemeDictionary.LexemeItem])(item: IndexedLexicalItem[MR]): Iterable[DelexedLexicalItem[MR]] = {
    for {
      lexemeItem <- matchingLexemeItems
      lexedMeaning <- lexemeItem.lexeme.lexicalize(item.meaning)
    } yield DelexedLexicalItem(
      item.tokens, item.synCat, lexedMeaning, lexemeItem, item.meaning, item.index
    )
  }

  def save(templateFile: File, lexemeFile: File): Unit = {
    delexed.saveToTextFile(templateFile)
    lexemes.save(lexemeFile)
  }

  /** Provides a lexical choice that generates only decisions from a given supertag set.
    *
    * The supertag set is extracted from each example individually. Limiting the generation of decisions in this way
    * saves a lot of overhead compared to using an equivalent filter later on.
    *
    * @param extractTemplateTags a function to extract supertag distributions from examples
    * @param tolerateUnannotated whether the choice should be unlimited (instead of empty) if no supertag annotation is available
    * @tparam E arbitrary type for examples
    * @return a lexical choice
    */
  def supertagLimitedLexicalChoice[E: Example](
    extractTemplateTags: E => Seq[TagDistribution],
    tolerateUnannotated: Boolean = false
  ): LexicalChoice[E, MR] = (e: E) => {
    val templateDists = extractTemplateTags(e).map(_.tags.flatMap(gramr.util.strToInt))

    i: SyntaxInformation => {
      // Is this a single-token span?
      if(i.span.size == 1) {
        if(templateDists.size > i.span.start) {
          val tokens = i.span.in(i.sentence)
          val templates = templateDists(i.span.start).map(id => delexed.itemsById(id))

          val matchingLexemes: Iterable[LexemeItem] = lexemes.get(tokens)

          val lexedTemplates = templates.flatMap(lexicalizeLexItem(matchingLexemes))
          for (candidate <- lexedTemplates.iterator) yield {
            LexicalDecision(candidate, i.sentence, i.span)
          }
        }
        else {
          logger.debug(s"Example ${e.id}: No template tag distribution found for token ${i.span.start}. There are tags for ${templateDists.size} tokens.")
          if(tolerateUnannotated) {
            // We probably don't have any supertags for this sentence, so we fall back to allowing any decision.
            lexicalChoice(e)(i)
          }
          else {
            Iterator.empty
          }
        }
      }
      else {
        // For nodes that span more than a single node, we don't produce any lexical decisions.
        Iterator.empty
      }
    }
  }

}

object DelexedLexicon extends LazyLogging {

  def load[MR : Graph](templateFile: File, lexemeFile: File, wildcardFillers: Iterable[WildcardFiller]): DelexedLexicon[MR] = {
    val lexicon = new DelexedLexicon[MR](
      StoredLexicon.loadFromTextFile(templateFile),
      LexemeDictionary.load(lexemeFile, wildcardFillers)
    )
    logger.info(s"Loaded ${lexicon.delexed.size} templates, ${lexicon.lexemes.size} lexemes")
    lexicon
  }

}

case class DelexedLexicalItem[MR](
  tokens: Vector[Token],
  synCat: SyntacticCategory,
  meaning: MR,
  lexeme: LexemeDictionary.LexemeItem,
  template: MR,
  templateId: Int
) extends LexicalItem[MR]
