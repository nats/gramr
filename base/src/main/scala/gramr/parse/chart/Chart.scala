package gramr.parse.chart

import gramr.parse.chart.trimming.TrimmingPredicate
import gramr.text.{Span, Token}
import gramr.util.html.ToHtml

trait Chart[MR, FV] {

  def sentence: Vector[Token]

  def spans: Iterable[Span]

  def copy: Chart[MR, FV]

  def states(span: Span): Iterable[State[MR, FV]]

  def successors(state: State[MR, FV]): Iterable[State[MR, FV]]

  def outputStates: Iterable[State[MR, FV]]

  def trim(predicate: TrimmingPredicate[MR, FV]): Unit

  def trimmed(predicate: TrimmingPredicate[MR, FV]): Chart[MR, FV]

  def deepTrimmed(predicate: TrimmingPredicate[MR, FV]): Chart[MR, FV]

}

object Chart {
  object ops {
    implicit def toHtml[MR: ToHtml, FV]: ToHtml[Chart[MR, FV]] = new ChartToHtml[MR, FV]
  }
}
