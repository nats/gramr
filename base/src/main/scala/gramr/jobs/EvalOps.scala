package gramr.jobs

import com.typesafe.scalalogging.LazyLogging
import gramr.graph.Graph
import gramr.parse.evaluate.BatchParseStats
import gramr.text.{Example, HasMR}
import gramr.util.ParseStatsCsvLogger
import Example.ops._

class EvalOps[MR: Graph, FV, E: Example](job: Job[E, (E, NamedEvals)])(implicit hasMR: HasMR[E, MR]) extends LazyLogging {


  def gatherBatchEvals(items: Seq[(E, NamedEvals)]): Map[String, BatchParseStats] = {
    if(items.nonEmpty) {
      val evals = items.map(_._2)
      val keys = items.head._2.keys
      keys.map { key =>
        val statList = evals.map { namedEvals => namedEvals(key) }
        val batchStats = BatchParseStats(statList.toVector)
        (key, batchStats)
      }.toMap
    }
    else {
      Map.empty
    }
  }


  def printIterationStats: Job[E, (E, NamedEvals)] = {
    job.listenToIteration { items =>
      val batchEvals = gatherBatchEvals(items)
      batchEvals.foreach {
        case (key, batchStats) =>
          logger.info(s"${job.name}.$key: $batchStats")
      }
    }
  }

  def logStatsToCsv(
    statsLogger: ParseStatsCsvLogger
  ): Job[E, (E, NamedEvals)] = {
    job.listenToItems {
      case (example, evals) =>
        evals.foreach {
          case (key, stats) =>
            statsLogger.logExampleStats(job.name, key, example.id, stats)
        }
    }
    job.listenToIteration { items =>
      val stats = gatherBatchEvals(items)
      stats.foreach {
        case (key, batchStats) =>
          statsLogger.logIterationStats(job.name, key, batchStats)
      }
    }
    job
  }

}
