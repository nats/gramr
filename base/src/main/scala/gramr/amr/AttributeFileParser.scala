package gramr.amr

import java.io.File

import scala.collection.LinearSeq
import scala.io.Source

/** Attribute files are AMR corpus files minus the AMRs: they consist only of comment sections
  * divided by empty lines.
  */
object AttributeFileParser {

  import AmrCorpusParser.{chunkRecords, parseAttributes}

  def parseString(content: String): Map[String, List[(String, String)]] =
    parseLines(content.split("\n").toList)

  def parseFile(fileName: File): Map[String, List[(String, String)]] =
    parseLines(Source.fromFile(fileName).getLines.toStream)

  def parseLines(lines: LinearSeq[String]): Map[String, List[(String, String)]] =
    chunkRecords(lines).map(parseRecord).foldLeft(Map.empty[String, List[(String, String)]])(_ ++ _)

  def parseRecord(lines: LinearSeq[String]): Map[String, List[(String, String)]] = {
    val attributes: List[(String, String)] = lines.flatMap(parseAttributes).toList
    val id: String = attributes.find(_._1 == "id") match {
      case Some((_, i)) => i
      case None => "None"
    }
    Map(id -> attributes.filter(_._1 != "id"))
  }

}
