package gramr
package split
package filters

import gramr.graph.Graph
import gramr.text.Token

import Graph.ops._

case class NoSingleConstants[MR : Graph]() extends SplitFilter[MR] {

  def apply(split: Split[MR], sentence: Vector[Token]): Boolean = {
    pred(split.left.mr) && pred(split.right.mr)
  }

  def pred(mr: MR): Boolean = mr.nodes.size != 1 || !mr.nodes.head.element.isConstantNode

}