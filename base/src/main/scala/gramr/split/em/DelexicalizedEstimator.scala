package gramr.split.em

import breeze.numerics.{digamma, exp}
import com.typesafe.scalalogging.LazyLogging
import gramr.ccg.{Derivation, StoredLexicon}
import gramr.graph.Graph
import gramr.graph.lexemes.LexemeDictionary
import gramr.split.SentenceSplittingAlgorithm
import gramr.split.em.DelexicalizedEstimator.Parameters
import gramr.text.Example
import gramr.util.SparseVector
import monocle.macros.GenLens

class DelexicalizedEstimator[MR : Graph, E: Example](
  exampleDerivationPairs: Iterable[(E, (String, Derivation[Unit]))],
  lexicon: StoredLexicon[MR],
  lexemes: LexemeDictionary,
  maxLexNodeCount: Int,
  sentenceSplitter: SentenceSplittingAlgorithm[MR, E],
  iterations: Int,
  adjustCounts: Parameters => Parameters = (p: Parameters) => p
) extends Estimator[MR, E, DelexicalizedModel[MR], Parameters](exampleDerivationPairs, lexicon, sentenceSplitter, iterations, adjustCounts)
  with LazyLogging {

  override def createModel: DelexicalizedModel[MR] = {
    DelexicalizedModel.randomUniform[MR](lexicon, lexemes, maxLexNodeCount)
  }

}

object DelexicalizedEstimator {
  case class Parameters(
    templateParameters: SparseVector,
    lexemeParameters: SparseVector
  ) {

    import Parameters._

    def add(parameters: Parameters): Parameters = {
      _templateParameters.modify(SparseVector.add(_, parameters.templateParameters)).
        andThen(_lexemeParameters.modify(SparseVector.add(_, parameters.lexemeParameters)))(this)
    }

    def normalize: Parameters = {
      _templateParameters.modify(_.normalize).andThen(_lexemeParameters.modify(_.normalize))(this)
    }

    def scale(factor: Double): Parameters = {
      _templateParameters.modify(_.scale(factor)).andThen(_lexemeParameters.modify(_.scale(factor)))(this)
    }

  }

  object Parameters {

    def empty: Parameters = Parameters(SparseVector.empty, SparseVector.empty)

    val _templateParameters = GenLens[Parameters](_.templateParameters)
    val _lexemeParameters = GenLens[Parameters](_.lexemeParameters)

  }

  def expDigamma(counts: Parameters): Parameters = {
    Parameters(
      counts.templateParameters.map { v => if(v > 0.0) exp(digamma(v)) else v },
      counts.lexemeParameters.map { v => if(v > 0.0) exp(digamma(v)) else v }
    )
  }
}
