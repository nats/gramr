package gramr.parse.chart

import java.util

import gramr.learn.{FeatureVector, LinearModel}
import gramr.text.Example

package object trimming {


  trait TrimmingPredicate[MR, F] {

    def instantiate(chart: Chart[MR, F]): State[MR, F] => Boolean

  }

  trait TrimRootStates[MR, F] extends TrimmingPredicate[MR, F] {

    def fromRootStatePredicate(
      chart: Chart[MR, F], rootStatePredicate: (Int, State[MR, F]) => Boolean
    ): State[MR, F] => Boolean = {
      val statesToKeep = new util.IdentityHashMap[State[MR, F], Unit]

      for ((rootState, index) <- chart.outputStates.zipWithIndex if rootStatePredicate(index, rootState)) {
        statesToKeep.put(rootState, ())

        def addPredecessors(state: State[MR, F]): Unit = state.actions.view.flatMap(_.predecessors).foreach { pred =>
          if(!statesToKeep.containsKey(pred)) {
            statesToKeep.put(pred, ())
            addPredecessors(pred)
          }
        }

        addPredecessors(rootState)
      }

      { state: State[MR, F] => statesToKeep.containsKey(state) }
    }

  }

  case class CompleteParses[MR, F]() extends TrimmingPredicate[MR, F] with TrimRootStates[MR, F] {
    override def instantiate(chart: Chart[MR, F]): State[MR, F] => Boolean = {
      fromRootStatePredicate(chart, (_, _) => true)
    }
  }

  case class BestScored[MR, F](
    epsilon: Double = 0.01
  ) extends TrimmingPredicate[MR, F] with TrimRootStates[MR, F] {

    def instantiate(chart: Chart[MR, F]): State[MR, F] => Boolean = {
      val bestScore: Double =
        if(chart.outputStates.isEmpty) Double.NegativeInfinity
        else chart.outputStates.view.map(_.score).max

      def predicate(index: Int, state: State[MR, F]): Boolean =
        state.score >= bestScore - epsilon

      fromRootStatePredicate(chart, predicate)
    }
  }

  case class BestEvaluated[MR, F, E](
    example: E,
    evaluate: (MR, E) => Double,
    epsilon: Double = 0.01
  ) extends TrimmingPredicate[MR, F] with TrimRootStates[MR, F] {

    def instantiate(chart: Chart[MR, F]): State[MR, F] => Boolean = {
      val scores = chart.outputStates.toVector.map { state => evaluate(state.mr, example) }
      val bestScore: Double = (Double.NegativeInfinity +: scores).max

      def predicate(index: Int, state: State[MR, F]): Boolean =
        scores(index) >= bestScore - epsilon

      fromRootStatePredicate(chart, predicate)
    }

  }

  case class BestRescored[MR, F: FeatureVector, E <: Example[MR]](
    model: LinearModel,
    epsilon: Double = 0.01
  ) extends TrimmingPredicate[MR, F] with TrimRootStates[MR, F] {

    def instantiate(chart: Chart[MR, F]): State[MR, F] => Boolean = {
      val scores = chart.outputStates.toVector.map { state => model.score(state.features) }
      val bestScore: Double = (Double.NegativeInfinity +: scores).max

      def predicate(index: Int, state: State[MR, F]): Boolean =
        scores(index) >= bestScore - epsilon

      fromRootStatePredicate(chart, predicate)
    }

  }

  case class Matching[MR, F, E <: Example[MR]](
    predicate: State[MR, F] => Boolean
  ) extends TrimmingPredicate[MR, F] with TrimRootStates[MR, F] {

    override def instantiate(chart: Chart[MR, F]): State[MR, F] => Boolean = {
      fromRootStatePredicate(chart, (_, state) => predicate(state))
    }

  }


}
