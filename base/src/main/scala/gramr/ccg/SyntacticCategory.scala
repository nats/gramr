package gramr.ccg

import scala.util.matching.Regex

sealed trait SyntacticCategory extends Ordered[SyntacticCategory] with Serializable {

  def compare(other: SyntacticCategory): Int = (this, other) match {
    case (Atom(ca), Atom(cb)) => ca.compare(cb)
    case (Atom(_), ForwardSlash(_, _)) => -1
    case (Atom(_), BackwardSlash(_, _)) => -1
    case (ForwardSlash(_, _), Atom(_)) => 1
    case (ForwardSlash(c1a, c2a), ForwardSlash(c1b, c2b)) =>
      val cmp1 = c1a.compare(c1b)
      if (cmp1 != 0) cmp1
      else c2a.compare(c2b)
    case (ForwardSlash(_, _), BackwardSlash(_, _)) => -1
    case (BackwardSlash(_, _), Atom(_)) => 1
    case (BackwardSlash(_, _), ForwardSlash(_, _)) => 1
    case (BackwardSlash(c1a, c2a), BackwardSlash(c1b, c2b)) =>
      val cmp1 = c1a.compare(c1b)
      if (cmp1 != 0) cmp1
      else c2a.compare(c2b)
  }

  def eraseFeatures: SyntacticCategory

  // Determines whether the two categories are the same after replacement of [X] feature wildcards.
  def matches(other: SyntacticCategory): Boolean

  def arity: Int = this match {
    case ForwardSlash(result, _) => result.arity + 1
    case BackwardSlash(result, _) => result.arity + 1
    case atom@Atom(_) => atom.category match {
      case "conj" => 2
      case "," | ";" => 2
      case _ if EasyccgCombinatorSystem.isPunctuation(atom) => 1
      case _ if atom.feature.contains("conj") => 1
      case "PP" => 1
      case _ => 0
    }
  }

}

object SyntacticCategory {

  import scala.util.parsing.combinator.RegexParsers
  import scala.util.parsing.input.CharSequenceReader
  import scala.util.Try

  def parse(s: String): Try[SyntacticCategory] = Try {
    import scala.language.reflectiveCalls
    val p = new Parser {
      def parse(s: String): SyntacticCategory = phrase(syncat)(new CharSequenceReader(s)) match {
        case Success(cat, _) => cat
        case NoSuccess(msg, _) => throw ParseException(s"Unable to parse SynCat $s: $msg")
      }
    }
    p.parse(s)
  }

  /**
    * Throws ParseException.
    */
  def apply(s: String): SyntacticCategory = parse(s).get

  trait Parser extends RegexParsers {

    def syncat: Parser[SyntacticCategory] = punctuation | recursive

    def recursive: Parser[SyntacticCategory] =
      slashCat | atom |
        "(" ~ syncat ~ ")" ^^ { case _ ~ c ~ _ => c }

    private def slashCat: Parser[SyntacticCategory] =
      "(" ~ slashCat ~ ")" ~ slash ~ syncat ^^ { case _ ~ left ~ _ ~ slash ~ right => slash(left, right) } |
        atom ~ slash ~ syncat ^^ { case left ~ slash ~ right => slash(left, right) }

    private def slash: Parser[(SyntacticCategory, SyntacticCategory) => SyntacticCategory] =
      "/" ^^ { _ => ForwardSlash } |
        "\\" ^^ { _ => BackwardSlash }

    private def atom: Parser[SyntacticCategory] =
      """[\w\[\]]+""".r ^^ {
        gramr.ccg.Atom(_)
      }

    // These are atomic categories that are emitted by the C&C parser for punctuation.
    private def punctuation: Parser[SyntacticCategory] =
      """([\.\,:;/]|\\)""".r ^^ {
        gramr.ccg.Atom(_)
      }

  }

  case class ParseException(msg: String) extends Exception(msg)


}

sealed abstract class SlashCategory(
  val left: SyntacticCategory, val right: SyntacticCategory
) extends SyntacticCategory {

  def slash: String

  override def toString: String = childToString(left) ++ slash ++ childToString(right)

  def childToString(c: SyntacticCategory): String = c match {
    case Atom(_) => c.toString
    case _ => "(" ++ c.toString ++ ")"
  }

}

@SerialVersionUID(1L)
case class ForwardSlash(override val left: SyntacticCategory, override val right: SyntacticCategory)
  extends SlashCategory(left, right) {
  override def slash = "/"

  def eraseFeatures: ForwardSlash = ForwardSlash(left.eraseFeatures, right.eraseFeatures)

  def matches(other: SyntacticCategory): Boolean = other match {
    case ForwardSlash(left1, right1) if (left matches left1) && (right matches right1) => true
    case _ => false
  }
}

@SerialVersionUID(1L)
case class BackwardSlash(override val left: SyntacticCategory, override val right: SyntacticCategory)
  extends SlashCategory(left, right) {
  override def slash = "\\"

  def eraseFeatures: BackwardSlash = BackwardSlash(left.eraseFeatures, right.eraseFeatures)

  def matches(other: SyntacticCategory): Boolean = other match {
    case BackwardSlash(left1, right1) if (left matches left1) && (right matches right1) => true
    case _ => false
  }
}

@SerialVersionUID(1L)
class Atom(val category: String, val feature: Option[String]) extends SyntacticCategory {

  val content: String = feature match {
    case Some(f) => s"$category[$f]"
    case None => category
  }

  override def toString: String = content

  override def equals(other: Any): Boolean = other match {
    case a@Atom(c) => c == content
    case _ => false
  }

  override def hashCode: Int = content.hashCode

  def eraseFeatures: Atom = new Atom(category, None)

  def matches(other: SyntacticCategory): Boolean = other match {
    // These conditions are copied from EasyCCG.
    case a@Atom(_) if a.category == category =>
      a.feature == feature || feature.contains("X") || a.feature.contains("X") ||
        a.feature.isEmpty || feature.isEmpty ||
        "N" == category || "NP" == category ||
        feature.contains("num") // Ignoring the "num" feature, which is inconsistently used.
    case _ => false
  }

}

object Atom {

  def apply(content: String): Atom = content match {
    case featureRE(cat, feat) => new Atom(cat, Some(feat))
    case _ => new Atom(content, None)
  }

  def unapply(atom: Atom): Option[String] = Some(atom.content)

  val featureRE: Regex = """(\w+)\[(\w+)\]""".r

}

