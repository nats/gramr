package gramr.split.em

import gramr.ccg._
import gramr.split.{SentenceSplittingAlgorithm, SplitOutcome}
import org.scalatest.FreeSpec

class IterationSpec extends FreeSpec {

  import gramr.split.ExampleData._

  "Splitting with an example lexicon" - {

    val parameters = LexicalizedModel(LexicalizedEstimator.Parameters(lexicon.items.map(item => (item.index, 1.0 / lexicon.itemCount)).toMap), lexicon)

    "on a one word sentence" in {
      val parameters = LexicalizedModel(LexicalizedEstimator.Parameters(Map(1 -> 1.0)), lexicon)

      val iteration = new Iteration[MR, E, LexicalizedModel[MR], LexicalizedEstimator.Parameters](
        parameters,
        lexicon,
        sentenceSplitter
      )

      val theseExamples = exampleDerivationPairs("anna")
      iteration.estimate(theseExamples)
    }

    "on a three word sentence" in {
      val iteration = new Iteration[MR, E, LexicalizedModel[MR], LexicalizedEstimator.Parameters](
        parameters,
        lexicon,
        sentenceSplitter
      )

      val theseExamples = exampleDerivationPairs("anna married manny")
      iteration.estimate(theseExamples)
    }

    "on two three word sentences" in {
      val iteration = new Iteration[MR, E, LexicalizedModel[MR], LexicalizedEstimator.Parameters](
        parameters,
        lexicon,
        sentenceSplitter
      )

      val theseExamples = List("anna married manny", "anna married hubert").flatMap(exampleDerivationPairs)
      iteration.estimate(theseExamples)
    }

    "on two three word sentences with 5 iterations" in {
      val theseExamples = List("anna married manny", "anna married hubert").flatMap(exampleDerivationPairs)

      var updatedParameters = parameters
      (1 to 5) foreach { i =>
        println(s"params=$parameters")

        val iteration = new Iteration[MR, E, LexicalizedModel[MR], LexicalizedEstimator.Parameters](
          updatedParameters,
          lexicon,
          sentenceSplitter
        )

        updatedParameters = iteration.estimate(theseExamples)
      }

      println(s"params=$updatedParameters")

    }

    "on a corpus of three word sentences with 10 iterations" in {
      val theseExamples = List(
        "anna married manny", "anna married hubert", "hubert married manny", "manny married bettina",
        "bettina met anna", "manny met hubert").flatMap(exampleDerivationPairs)

      var updatedParameters = parameters
      (1 to 10) foreach { i =>
        println(s"params=$updatedParameters")

        val iteration = new Iteration[MR, E, LexicalizedModel[MR], LexicalizedEstimator.Parameters](
          updatedParameters,
          lexicon,
          sentenceSplitter
        )

        updatedParameters = iteration.estimate(theseExamples)
      }

      println(s"params=$parameters")

    }

    "with some errors output by the sentence splitter" in {
      val sentenceSplitterWithErrors = new SentenceSplittingAlgorithm[MR, E] {
        override def split(example: E, derivation: Derivation[Unit], id: String): SplitOutcome[MR] = {
          if(example.sentence.exists(t => t.lemma == "manny")) {
            Left("This splitter is not a fan of Manny")
          }
          else {
            sentenceSplitter.split(example, derivation, id)
          }
        }
      }

      val iteration = new Iteration[MR, E, LexicalizedModel[MR], LexicalizedEstimator.Parameters](
        parameters,
        lexicon,
        sentenceSplitterWithErrors
      )

      val theseExamples = List("anna married manny", "anna married hubert").flatMap(exampleDerivationPairs)
      iteration.estimate(theseExamples)
    }

    "with only errors output by the sentence splitter" in {
      val sentenceSplitterWithErrors = new SentenceSplittingAlgorithm[MR, E] {
        override def split(example: E, derivation: Derivation[Unit], id: String): SplitOutcome[MR] = {
          Left("Not today")
        }
      }

      val iteration = new Iteration[MR, E, LexicalizedModel[MR], LexicalizedEstimator.Parameters](
        parameters,
        lexicon,
        sentenceSplitterWithErrors
      )

      val theseExamples = List("anna married manny", "anna married hubert").flatMap(exampleDerivationPairs)
      iteration.estimate(theseExamples)
    }
  }

}
