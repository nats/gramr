package gramr.jobs

import gramr.ccg.Derivation
import gramr.jobs.ExampleDerivationOps.ExampleDerivationJob
import gramr.jobs.SplitChartOps.SplitChartJob
import gramr.split.{SentenceSplittingAlgorithm, SplitOutcome}

class ExampleDerivationOps[T, MR, E](job: ExampleDerivationJob[T, MR, E]) {

  import job.scheduler

  def split(
    splitter: SentenceSplittingAlgorithm[MR, E],
    parallelism: Int,
    inOrder: Boolean
  ): SplitChartJob[T, MR, E] = {
    def computation(item: (E, (String, Derivation[Unit]))): (E, (String, SplitOutcome[MR])) = item match {
      case (example, (derivationName, derivation)) =>
        val outcome = splitter.split(example, derivation, derivationName)
        (example, (derivationName, outcome))
    }

    job.append(ParallelComputeStage(computation, parallelism, inOrder))
  }

}

object ExampleDerivationOps {

  type ExampleDerivationJob[T, MR, E] = Job[T, (E, (String, Derivation[Unit]))]

}
