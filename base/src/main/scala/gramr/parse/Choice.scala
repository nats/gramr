package gramr.parse

trait Decision[O] {
  def result: O
}

trait Choice[I, O] {

  def decisionSpace(i: I): Iterator[Decision[O]]

}