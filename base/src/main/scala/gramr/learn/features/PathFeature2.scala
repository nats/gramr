package gramr.learn.features

import gramr.learn._
import gramr.graph._
import Graph.ops._
import gramr.graph.visit.DepthFirstSearch

import scala.collection.mutable.ArrayBuffer


case class PathFeature2[MR: Graph, FV: FeatureVector, E](
  configs: PathFeatureConfig[MR, FV]*
) extends StateFeatureSet[MR, FV, E] {

  lazy val configsByLength: Map[Int, Seq[PathFeatureConfig[MR, FV]]] =
    configs.groupBy(_.outEdgeTransformers.length)

  def extract(mr: MR, example: E): FV = {
    // Perform a pass per path feature length
    val entries: ArrayBuffer[(String, Double)] = ArrayBuffer.empty
    configsByLength.foreach {
      case (length, lconfigs) =>
        val visitor = new PathFeature2.Visitor(mr, length, lconfigs, entries)
        // We disable restarting because a) we assume we’re dealing with connected graphs,
        // and b) it is expensive because a set of unvisited nodes has to be initialized.
        DepthFirstSearch.forwardEdgesPlusN(mr, visitor, extraSteps = length - 1)
    }
    implicitly[FeatureVector[FV]].from(entries)
  }

}


object PathFeature2 {

  class Visitor[MR: Graph, FV: FeatureVector](
    mr: MR,
    length: Int,
    configs: Iterable[PathFeatureConfig[MR, FV]],
    entries: ArrayBuffer[(String, Double)]
  ) extends gramr.graph.visit.Visitor {
    var edgeStack: List[Indexed[Edge]] = Nil

    override def visitEdge(edgeIndex: Int): Unit = {
      val indexedEdge@Indexed(edge, _) = mr.edge(edgeIndex)
      // Do we need to pop?
      while(edgeStack.nonEmpty && edge.source != edgeStack.head.element.target) {
        edgeStack = edgeStack.tail
      }
      edgeStack = indexedEdge :: edgeStack

      val edges = edgeStack.take(length).reverse
      configs.foreach { config =>
        mkFeature(edges, config).foreach(feature => entries.append(feature -> 1.0))
      }
    }

    def mkFeature(edges: Iterable[Indexed[Edge]], config: PathFeatureConfig[MR, FV]): Option[String] = {
      if (edgeStack.length < config.outEdgeTransformers.length) {
        None
      }
      else {
        if (edges.forall(config.edgeFilter.apply(_, mr))) {
          val stringBuilder = new StringBuilder("path(")
          val startNode = mr.node(edges.head.element.source)
          stringBuilder.append(config.initialNodeTransformer.det(startNode))
          (edges zip config.outEdgeTransformers).foreach {
            case (edge, (edgeTransformer, targetTransformer)) =>
              stringBuilder.append(", ")
              stringBuilder.append(edgeTransformer(edge.element))
              val target = mr.node(edge.element.target)
              stringBuilder.append(", ")
              stringBuilder.append(targetTransformer.det(target))
          }
          stringBuilder.append(")")
          Some(stringBuilder.mkString)
        }
        else None
      }
    }
  }

}
