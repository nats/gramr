package gramr.ccg


/**
  * A lexicon that forms the union of several sub-lexica.
  */
class UnionLexicon[MR](
  val lexica: List[Lexicon[MR]]
) extends Lexicon[MR] {

  def get(key: LexiconKey): Iterable[LexicalItem[MR]] = lexica.foldLeft(Iterable.empty[LexicalItem[MR]]) {
    case (items, lexicon) => items ++ lexicon.get(key)
  }

}
