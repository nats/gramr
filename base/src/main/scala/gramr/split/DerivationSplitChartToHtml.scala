package gramr.split

import gramr.ccg.presentation.DerivationWithStepsToHtml
import gramr.ccg.{Derivation, DerivationStep}
import gramr.graph.Graph
import gramr.graph.presentation.GraphWithTokens
import gramr.util.html.ToHtml
import scalatags.Text.all._


/** Outputs a derivation annotated with matching split chart entries.
 */
class DerivationSplitChartToHtml[MR: Graph: ToHtml](
  val derivation: Derivation[Set[MR]]
) extends DerivationWithStepsToHtml[Set[MR]] {

  def stepSubtitle(label: Set[MR]): String = s"${label.size} results"

  def stepDisplay(step: DerivationStep[Set[MR]]): List[Frag] = {
    import gramr.graph.AlignmentGraph.Implicits._
    step.label.toList.map { g =>
      val adjusted = g.clipAlignments(step.span.start to step.span.end).adjustAlignments(_ - step.span.start)
      GraphWithTokens.ToHTML.toHtml(new GraphWithTokens(adjusted, step.tokens.map(_.text)))
    }
  }

}
