package gramr
package ccg

trait CombinatorSystem {

  def combinators: Iterable[Combinator]

  def synApply(combinator: Combinator, arguments: SyntacticCategory*): Option[SyntacticCategory]

  def parse(string: String): Option[Combinator] = combinators.find(_.toString == string)

  def matchingCombinators(result: SyntacticCategory, arguments: List[SyntacticCategory]): Iterable[Combinator] = {
    val argsErased = arguments.map(_.eraseFeatures)
    val resultErased = result.eraseFeatures
    combinators filter { c =>
      synApply(c, argsErased: _*).map(_.eraseFeatures).contains(resultErased)
    }
  }

  def unaryConversions(synCat: SyntacticCategory): Iterable[(Combinator, SyntacticCategory)] = {
    val conversionResults = for {
      combinator <- combinators if combinator.canApply(synCat)
      converted <- combinator.synApply(synCat).toIterable
    } yield (combinator, converted)
    conversionResults
  }

}
