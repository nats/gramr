package gramr
package amr
package presentation

import graph._
import ccg._
import scalatags.Text.all._
import java.io.{BufferedWriter, File, FileWriter}

import gramr.util.html.ToHtml

class CorpusItemToHTML[MR : Graph : ToHtml](item: AmrCorpusItem)
  (implicit combinatorSystem: CombinatorSystem) {

  val mr: MR = implicitly[Graph[MR]].fromAmrTree(item.amr)

  def toHtml: Frag = table(
    tr(td("id"), td(item.id)),
    tr(td("amr"), td(displayAmr)),
    item.ccgDerivations(combinatorSystem).map {
      case (key, derivation) => tr(td(s"syntax $key"), td(displaySyntax(derivation))) }.toVector
  )

  def toHtmlFile(path: File): Unit = {
    val markup = html(
      head(title := item.id), body(
        toHtml
      )
    )

    val writer = new BufferedWriter(new FileWriter(path))
    writer.write(markup.toString)
    writer.close()
  }

  def displayAmr: Frag = {
    import graph.presentation._
    GraphWithTokens.ToHTML.toHtml(new GraphWithTokens(mr, item.sentenceTokens.map(_.text)))
  }

  def displaySyntax(derivation: Derivation[Unit]): Frag = {
    import ccg.presentation._
    implicit def unitToHTML = new ToHtml[Unit] {
      def toHtml(u: Unit): _root_.scalatags.Text.all.SeqFrag[_root_.scalatags.Text.all.Frag] = List[Frag]()
    }

    new DerivationToHtml[Unit]().toHtml(derivation)
  }

}
