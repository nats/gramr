package gramr.amr

import gramr.graph.AlignmentsAttribute
import org.scalatest.FlatSpec

class AmrUdAlignmentSpec extends FlatSpec {

  "Alignment element parser" should "parse simple node alignments" in {
    val examples = Seq(
      ("r/reopen    #    14/reopened", AmrUdAlignment.Element("r", Set(14))),
      ("none9/5    #    2/5", AmrUdAlignment.Element("none9", Set(2))),
      ("none2/Academy    #    7/academy", AmrUdAlignment.Element("none2", Set(7))),
      ("none2/Academy    #    10/Academy :compound 7/academy", AmrUdAlignment.Element("none2", Set(7, 10)))
    )

    examples.foreach {
      case (line, expected) =>
        val parsed = AmrUdAlignment.Element.parse(line)
        assert(parsed.isDefined)
        assert(parsed.get == expected)
    }
  }

  it should "reject subgraph alignments" in {
    val examples = Seq(
      "s/since :op1 a3/attack    #    24/attacks :case 19/since",
      "a/and :op1 i/international; a/and :op2 t/terrorism; a/and :op2 m/military    #    1/International :conj 5/terrorism; 1/International :conj 3/military",
      "n/name :op6 none5/Sciences; n/name :op5 none4/Security; n/name :op4 none3/for; n/name :op1 none0/Naif; n/name :op2 none1/Arab; n/name :op3 none2/Academy; u/university :name n/name    #    13/Sciences :compound 12/Security; 10/Academy :nmod 13/Sciences; 10/Academy :compound 8/Naif; 10/Academy :compound 9/Arab; 13/Sciences :case 11/for; 10/Academy :amod 5/pan-Arab; 10/Academy :compound 7/academy",
    )

    examples.foreach { line =>
      val parsed = AmrUdAlignment.Element.parse(line)
      assert(parsed.isEmpty)
    }
  }


  "Alignment parse function" should "parse a whole alignment block" in {
    val lines: Seq[String] =
      """# ::alignment-amr_ud none8/2    #    18/two-week
        |# ::alignment-amr_ud w2/week    #    18/two-week
        |# ::alignment-amr_ud none7/50    #    21/50
        |# ::alignment-amr_ud w/workshop    #    19/workshop
        |# ::alignment-amr_ud none6/Riyadh    #    2/Riyadh-based
        |# ::alignment-amr_ud none5/Sciences    #    8/Sciences
        |# ::alignment-amr_ud none4/Security    #    7/Security
        |# ::alignment-amr_ud s2/statement    #    12/statement
        |# ::alignment-amr_ud none3/for    #    6/for
        |# ::alignment-amr_ud none3/for    #    20/for
        |# ::alignment-amr_ud none2/Academy    #    5/Academy
        |# ::alignment-amr_ud none1/Arab    #    4/Arab
        |# ::alignment-amr_ud t2/terrorism    #    22/anti-terrorism
        |# ::alignment-amr_ud none0/Naif    #    3/Naif
        |# ::alignment-amr_ud s/say    #    9/said
        |# ::alignment-amr_ud e/expert    #    23/experts
        |# ::alignment-amr_ud r/run    #    16/running
        |# ::alignment-amr_ud n/name :op6 none5/Sciences; n/name :op4 none3/for; n/name :op5 none4/Security; n/name :op1 none0/Naif; n/name :op2 none1/Arab; n/name :op3 none2/Academy; u/university :name n/name    #    8/Sciences :case 6/for; 5/Academy :nmod 8/Sciences; 5/Academy :compound 3/Naif; 5/Academy :compound 4/Arab; 8/Sciences :compound 7/Security
        |# ::alignment-amr_ud n/name :op3 none2/Academy; n/name :op1 none0/Naif; n/name :op4 none3/for; n/name :op5 none4/Security; n/name :op6 none5/Sciences; n/name :op2 none1/Arab; u/university :name n/name    #    5/Academy :compound 3/Naif; 5/Academy :nmod 8/Sciences; 5/Academy :compound 4/Arab; 8/Sciences :case 6/for; 8/Sciences :compound 7/Security
        |# ::alignment-amr_ud r/run :ARG1 w/workshop    #    16/running :dobj 19/workshop
        |# ::alignment-amr_ud s/say :ARG0 u/university; u/university :name n/name; n/name :op3 none2/Academy; n/name :op4 none3/for; n/name :op5 none4/Security; n/name :op1 none0/Naif; n/name :op6 none5/Sciences; n/name :op2 none1/Arab    #    9/said :nsubj 5/Academy; 5/Academy :nmod 8/Sciences; 5/Academy :compound 3/Naif; 5/Academy :compound 4/Arab; 8/Sciences :case 6/for; 8/Sciences :compound 7/Security
        |# ::alignment-amr_ud n/name :op3 none2/Academy; u/university :name n/name; u/university :ARG1-of b/base; b/base :location c/city; c/city :name n2/name; n2/name :op1 none6/Riyadh; n/name :op4 none3/for; n/name :op5 none4/Security; n/name :op1 none0/Naif; n/name :op6 none5/Sciences; n/name :op2 none1/Arab    #    5/Academy :amod 2/Riyadh-based; 5/Academy :nmod 8/Sciences; 5/Academy :compound 3/Naif; 5/Academy :compound 4/Arab; 8/Sciences :case 6/for; 8/Sciences :compound 7/Security
        |# ::alignment-amr_ud n2/name :op1 none6/Riyadh; c/city :name n2/name    #    2/Riyadh-based
        |# ::alignment-amr_ud s/say :ARG1 r/run    #    9/said :ccomp 16/running
        |# ::alignment-amr_ud n/name :op3 none2/Academy; n/name :op6 none5/Sciences; n/name :op4 none3/for; n/name :op5 none4/Security; n/name :op1 none0/Naif; n/name :op2 none1/Arab; u/university :name n/name    #    5/Academy :nmod 8/Sciences; 5/Academy :compound 3/Naif; 5/Academy :compound 4/Arab; 8/Sciences :case 6/for; 8/Sciences :compound 7/Security
        |# ::alignment-amr_ud n/name :op6 none5/Sciences; n/name :op5 none4/Security; n/name :op4 none3/for; n/name :op1 none0/Naif; n/name :op2 none1/Arab; n/name :op3 none2/Academy; u/university :name n/name    #    8/Sciences :compound 7/Security; 5/Academy :nmod 8/Sciences; 5/Academy :compound 3/Naif; 5/Academy :compound 4/Arab; 8/Sciences :case 6/for
        |# ::alignment-amr_ud n/name :op3 none2/Academy; n/name :op2 none1/Arab; n/name :op4 none3/for; n/name :op5 none4/Security; n/name :op1 none0/Naif; n/name :op6 none5/Sciences; u/university :name n/name    #    5/Academy :compound 4/Arab; 5/Academy :nmod 8/Sciences; 5/Academy :compound 3/Naif; 8/Sciences :case 6/for; 8/Sciences :compound 7/Security
        |# ::alignment-amr_ud s/say :medium s2/statement    #    9/said :nmod 12/statement""".
        stripMargin.replace("# ::alignment-amr_ud ", "").split("\n")
    val alignment = AmrUdAlignment.parse(lines)
    assert(alignment.elements.size == 17) // number of single-node alignments
  }


  "Alignment elements" should "annotate themselves on an AMR" in {
    val amrString = """(e / establish-01
                      |  :ARG1 (m / model
                      |          :mod (i / innovate-01
                      |                 :ARG1 (i2 / industry))))""".stripMargin
    val amr: AmrTree = AmrTreeParser.parseString(amrString)

    // Test that every alignment is annotated individually
    val annotated1 = AmrUdAlignment.Element("e", Set(1)).annotate(amr)
    assert(annotated1.attributes("alignments").asInstanceOf[AlignmentsAttribute].edges == Set(0))

    val annotated2 = AmrUdAlignment.Element("m", Set(2)).annotate(amr).asInstanceOf[InnerNode]
    assert(annotated2.children(1).subTree.attributes("alignments").asInstanceOf[AlignmentsAttribute].edges == Set(1))
  }

}
