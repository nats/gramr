package gramr.graph.edit.sgraph

import Combine._
import Split._
import gramr.graph.Graph

object Operators {

  case class Identity[MR : Graph]() extends SemanticOperator[MR] {
    override def combine: CombineOperator[MR] = semIdentity[MR]
    override def split: SplitOperator[MR] = splitSemIdentity[MR]
  }

  case class Application[MR : Graph](
    maxCorefs: Int = 1
  ) extends SemanticOperator[MR] {
    override def combine: CombineOperator[MR] = semApply[MR]()
    override def split: SplitOperator[MR] = splitSemApply[MR](maxCorefs)
  }

  case class Modification[MR : Graph](
    depth: Int,
    unique: Boolean = true,
    maxCorefs: Int = 1
  ) extends SemanticOperator[MR] {
    override def combine: CombineOperator[MR] = semModify[MR](depth, unique)
    override def split: SplitOperator[MR] = splitSemModify[MR](depth, unique, maxCorefs)
  }

  case class Composition[MR: Graph](
    maxCorefs: Int = 1
  ) extends SemanticOperator[MR] {
    override def combine: CombineOperator[MR] = semCompose[MR]
    override def split: SplitOperator[MR] = splitSemCompose[MR](maxCorefs)
  }

  case class Substitution[MR : Graph](
    maxCorefs: Int = 1
  ) extends SemanticOperator[MR] {
    override def combine: CombineOperator[MR] = semSubstitute[MR]
    override def split: SplitOperator[MR] = splitSemSubstitute[MR](maxCorefs)
  }

}
