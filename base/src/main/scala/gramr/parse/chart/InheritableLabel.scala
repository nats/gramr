package gramr.parse.chart

import ChartLabel._
import gramr.learn.FeatureVector

import scala.language.reflectiveCalls

/** Covers the common case where the chart label is cleanly decomposed into a local and an inheritable portion.
  *
  */
case class InheritableLabel[L : ChartLabel](
  local: L,
  inheritable: L
) {

  val value: L = add(local, inheritable)

}

object InheritableLabel {

  implicit def chartLabel[L : ChartLabel]: ChartLabel[InheritableLabel[L]] = new ChartLabel[InheritableLabel[L]] {

    override def score(l: InheritableLabel[L]): Double = ChartLabel.score(l.value)

    override def zero: InheritableLabel[L] = InheritableLabel(ChartLabel.zero[L], ChartLabel.zero[L])

    override def add(l: InheritableLabel[L], r: InheritableLabel[L]): InheritableLabel[L] = {
      InheritableLabel(
        ChartLabel.add(l.local, r.local),
        ChartLabel.add(l.inheritable, r.inheritable)
      )
    }

    override def subtract(l: InheritableLabel[L], r: InheritableLabel[L]): InheritableLabel[L] = {
      InheritableLabel(
        ChartLabel.subtract(l.local, r.local),
        ChartLabel.subtract(l.inheritable, r.inheritable)
      )
    }

    override def mul(l: InheritableLabel[L], r: InheritableLabel[L]): InheritableLabel[L] = {
      InheritableLabel(
        ChartLabel.mul(l.local, r.local),
        ChartLabel.mul(l.inheritable, r.inheritable)
      )
    }

    override def inheritablePortion(l: InheritableLabel[L]): InheritableLabel[L] = InheritableLabel(ChartLabel.zero[L], l.inheritable)

  }

  implicit def hasFeatures[FV : FeatureVector, L : ChartLabel : ({type λ[l] = HasFeatures[FV, l]})#λ]: HasFeatures[FV, InheritableLabel[L]] =
    (l: InheritableLabel[L]) => implicitly[HasFeatures[FV, L]].features(l.value)

  implicit def ordering[L : Ordering]: Ordering[InheritableLabel[L]] =
    (x: InheritableLabel[L], y: InheritableLabel[L]) => implicitly[Ordering[L]].compare(x.value, y.value)

}
