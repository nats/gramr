package gramr.graph
package edit

import cats.Monad

import scala.annotation.tailrec

import Graph.ops._

/** A monad for editing meaning graphs.
  *
  * Each computation step starts at an EditState and may produce an arbitrary number of pairs of
  * result values and states. This models a non-deterministic / branching computation.
  *
  * In essence, this is a ListT[State[EditState[MR]]], but combined into a single monad.
  *
  * @param f the wrapped function
  * @tparam MR the meaning representation
  * @tparam A a result type
  */
case class Edit[MR : Graph, A](
  f: EditState[MR] => (Iterable[(EditState[MR], A)])
) {

  def map[B](g: A => B): Edit[MR, B] = Edit[MR, B] { state: EditState[MR] =>
    f(state) map { case (aState, a) => (aState, g(a)) }
  }


  def flatMap[B](g: A => Edit[MR, B]): Edit[MR, B] = Edit[MR, B] { state: EditState[MR] =>
    f(state) flatMap { case (aState, a) => g(a).f(aState) }
  }

  def perform(mr: MR): Iterable[(MR, A)] =
    f(EditState.initial(mr)).map { case (s, a) => (s.mr.clearAttribute("markers"), a) }

}


object Edit {

  implicit def editMonad[MR : Graph] =
    new Monad[({ type λ[T] = Edit[MR, T] })#λ] {
      override def pure[A](x: A): Edit[MR, A] = Edit.pure[MR, A](x)

      override def flatMap[A, B](fa: Edit[MR, A])(f: A => Edit[MR, B]): Edit[MR, B] = fa.flatMap(f)

      override def tailRecM[A, B](a: A)(f: A => Edit[MR, Either[A, B]]): Edit[MR, B] = {
        @tailrec
        def go(remaining: Iterable[(EditState[MR], Either[A, B])], result: Iterable[(EditState[MR], B)]): Iterable[(EditState[MR], B)] = {
          remaining.headOption match {
            case None => result
            case Some((nextState, Left(nextA))) =>
              go(f(nextA).f(nextState) ++ remaining.tail, result)
            case Some((nextState, Right(b))) =>
              go(remaining.tail, result ++ Iterable((nextState, b)))
          }
        }

        Edit[MR, B] { state =>
          go(f(a).f(state), Iterable.empty)
        }
      }
    }


  def pure[MR : Graph, A](value: A): Edit[MR, A] = Edit(s => Iterable((s, value)))

  def noop[MR : Graph]: Edit[MR, Unit] = pure(())

  def fail[MR : Graph]: Edit[MR, Unit] = Edit(_ => Iterable.empty)

  def inspect[MR : Graph, A](f: EditState[MR] => A): Edit[MR, A] = Edit[MR, A] { state =>
    Iterable((state, f(state)))
  }

  def get[MR : Graph]: Edit[MR, EditState[MR]] = inspect(s => s)

  def getMR[MR : Graph]: Edit[MR, MR] = inspect(s => s.mr)

  def modify[MR : Graph](f: EditState[MR] => EditState[MR]): Edit[MR, Unit] = Edit[MR, Unit] { state =>
    Iterable((f(state), Unit))
  }

  def modifyMR[MR : Graph](f: MR => MR): Edit[MR, Unit] = modify(s => s.copy(mr = f(s.mr)))

  def set[MR : Graph](state: EditState[MR]): Edit[MR, Unit] = modify(_ => state)

  def setMR[MR : Graph](mr: MR): Edit[MR, Unit] = modify(_.copy(mr = mr))

  def branch[MR : Graph](f: EditState[MR] => Iterable[EditState[MR]]): Edit[MR, Unit] = Edit[MR, Unit] { state =>
    f(state).map { s => (s, ()) }
  }

  def modifyMROrFail[MR : Graph](f: MR => Option[MR]): Edit[MR, Unit] =
    branch(s => f(s.mr).map(mr => s.copy(mr = mr)))

  def choose[MR : Graph, A](opts: Iterable[A]): Edit[MR, A] = Edit[MR, A] { state =>
    opts.map { a => (state, a) }
  }

}
