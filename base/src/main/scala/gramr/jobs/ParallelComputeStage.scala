package gramr.jobs

import monix.eval.Task
import monix.execution.Scheduler
import monix.reactive.Observable

import scala.collection.mutable.ListBuffer

case class ParallelComputeStage[A, B](
  computation: A => B,
  parallelism: Int,
  inOrder: Boolean
)(implicit scheduler: Scheduler) extends ListeningStage[A, B] {

  private def mapParallel(input: Observable[A])(f: A => Task[B]): Observable[B] = {
    if(inOrder) {
      input.mapParallelOrdered(parallelism)(f)
    }
    else {
      input.mapParallelUnordered(parallelism)(f)
    }
  }

  def observable(input: Observable[A]): Observable[B] = {
    mapParallel(input) { a =>
      Task {
        val item = computation(a)
        listeners.foreach(_.item(item))
        item
      }
    }
  }

}
