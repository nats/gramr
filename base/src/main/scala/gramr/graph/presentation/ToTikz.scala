package gramr.graph
package presentation

import Graph.ops._

class ToTikz[MR: Graph] {

  val prelude: String =
    ("""% Compile with lualatex
      |\documentclass{standalone}
      |\""" + """usepackage{tikz}
      |\""" + """usetikzlibrary{graphdrawing}
      |\""" + """usetikzlibrary{shapes.geometric}
      |\""" + """usegdlibrary{force}
      |""").stripMargin
  // Strings with \ u in them are difficult to construct in Scala

  val begin: String =
    """\begin{document}
      |\begin{tikzpicture}[spring electrical layout,node distance=2cm,electric charge=2]
      |""".stripMargin

  val end: String =
    """\end{tikzpicture}
      |\end{document}
      |""".stripMargin

  val rootNodeOptions: String = "[draw,shape=diamond,aspect=4]"

  def apply(mr: MR): String = {
    val builder = new StringBuilder()
    builder ++= prelude
    builder ++= begin

    mr.nodes foreach {
      case Indexed(ReferentNode(label, _), index) =>
        builder ++= node(index, label, index == mr.root)
      case Indexed(ConstantNode(label, _), index) =>
        builder ++= node(index, label, index == mr.root)
      case Indexed(ExternalNode(ExternalReference(placeholder), _), index) =>
        builder ++= node(index, s"<$placeholder>", index == mr.root)
    }

    mr.edges foreach {
      case Indexed(Edge(label, source, target, _), _) =>
        builder ++= edge(label, source, target)
    }

    builder ++= end
    builder.mkString
  }

  private def node(index: Int, label: String, root: Boolean=false): String = {
    val opts = if(root) rootNodeOptions else ""
    s"\\node$opts (n$index) {${mangle(label)}};\n"
  }

  private def edge(label: String, source: Int, target: Int): String = s"\\path (n$source) edge[->] node[above,sloped] {\\scriptsize \\textsf{${mangle(label)}}} (n$target);\n"

  def mangle(text: String): String = text.replaceAll("""([$&%])""", """\\$1""")

}
