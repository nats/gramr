package gramr.amr

import gramr.graph.AlignmentsAttribute

case class JamrAlignment(elements: Vector[(Range, Set[Vector[Int]])]) {

  import JamrAlignment._
  import scala.language.implicitConversions

  private implicit def mkAlignmentsAttribute(tokens: Set[Int]): AlignmentsAttribute =
    AlignmentsAttribute(tokens)

  def annotate(item: AmrCorpusItem, attributeName: String = "alignments"): AmrCorpusItem = {

    def collectInnerNodeNames(amr: AmrTree): Set[String] = amr match {
      case InnerNode(label, links, attributes) =>
        links.foldLeft(Set.empty[String]) { case (set, l) => set ++ collectInnerNodeNames(l.subTree) } + label
      case _ => Set.empty[String]
    }

    val innerNodeNames = collectInnerNodeNames(item.amr)

    def annotateEmptyAlignment(amr: AmrTree): AmrTree = amr match {
      case InnerNode(label, links, attributes) =>
        val newLinks = links.map { case link@Link(_, subTree, _) =>
          link.copy(subTree = annotateEmptyAlignment(subTree))
        }
        InnerNode(label, newLinks, attributes + (attributeName -> Set.empty[Int]))
      case Leaf(label, attributes) => Leaf(label, attributes + (attributeName -> Set.empty[Int]))
      case other => other
    }

    val withEmptyAlignments = annotateEmptyAlignment(item.amr)

    def annotateJamrPath(amr: AmrTree, tokenRange: Range, path: Vector[Int]): AmrTree = path match {
      case Vector() =>
        val newAlignments = amr.attributes(attributeName).asInstanceOf[AlignmentsAttribute].edges ++ tokenRange.toSet
        amr match {
          case InnerNode(label, links, attributes) =>
            InnerNode(label, links, attributes + (attributeName -> newAlignments))
          case Leaf(label, attributes) =>
            Leaf(label, attributes + (attributeName -> newAlignments))
            case other => other
        }
      case _ =>
        amr match {
          case InnerNode(label, links, attributes) =>
            var linkIndex = 0
            val numberedLinks = for {
              l <- links
            } yield {
              l match {
                case Link(llabel, Leaf(lllabel, attributes), lattributes) =>
                  if (llabel != "instance" && !innerNodeNames.contains(lllabel)) {
                    val r = (Some(linkIndex), l)
                    linkIndex += 1
                    r
                  }
                  else {
                    (None, l)
                  }
                case _ =>
                  val r = (Some(linkIndex), l)
                  linkIndex += 1
                  r
              }
            }
            val newLinks = numberedLinks.map { case (i, l@Link(_, subTree, _)) =>
              if (i.contains(path.head))
                l.copy(subTree = annotateJamrPath(subTree, tokenRange, path.tail))
              else l
            }
            InnerNode(label, newLinks, attributes)
          case _ => throw StuckAtPathException(path, amr)
        }
    }

    val aligned = elements.foldLeft(withEmptyAlignments) { case (amr, (tokenRange, paths)) =>
      paths.foldLeft(amr) { case (amr, path) =>
        try {
          annotateJamrPath(amr, tokenRange, path.tail)
        } catch {
          case e: StuckAtPathException => throw InvalidPathException(path, amr, e)
        }
      }
    }
    new AmrCorpusItem(amr = aligned, attributes = item.attributes)
  }

}

object JamrAlignment {

  def parse(s: String): JamrAlignment = try {
    val elements = s.trim.split("""\s+""").toVector.filter(!_.isEmpty).map { span =>
      val Array(tokensString, pathsString) = span.split("""\|""")
      val tokensRange = {
        val tokensRangePattern = """(\d+)-(\d+)""".r
        tokensString match {
          case tokensRangePattern(start, end) => Integer.parseInt(start) until Integer.parseInt(end)
        }
      }
      val paths = pathsString.split("""\+""").map { path =>
        path.split("""\.""").toVector.map {
          Integer.parseInt
        }
      }.toSet
      (tokensRange, paths)
    }
    JamrAlignment(elements)
  } catch {
    case e: MatchError => throw new Exception(s"MatchError while parsing alignment string $s", e)
  }

  case class StuckAtPathException(remainder: Vector[Int], amr: AmrTree)
    extends Exception

  case class InvalidPathException(path: Vector[Int], amr: AmrTree, location: StuckAtPathException)
    extends Exception(s"Invalid JAMR path $path: cannot resolve ${location.remainder} at ${location.amr}\n$amr")

}
