package gramr.amr

import org.scalatest._
import scala.util.parsing.input.CharSequenceReader
import scala.util.parsing.combinator._
import scala.util.parsing.combinator.Parsers

class CorpusSpec extends FlatSpec {

  "AmrTreeParser" should "produce the expected parse trees" in {
    import gramr.amr._
    import AmrTreeParser._
    val text =
      """(e / establish-01
        |  :ARG1 (m / model
        |          :mod (i / innovate-01
        |                 :ARG1 (i2 / industry))))""".stripMargin
    val tree =
      InnerNode(
        "e", List(
          Link("instance", Leaf("establish-01")),
          Link(
            "ARG1", InnerNode(
              "m", List(
                Link("instance", Leaf("model")),
                Link(
                  "mod", InnerNode(
                    "i", List(
                      Link("instance", Leaf("innovate-01")),
                      Link(
                        "ARG1", InnerNode(
                          "i2", List(
                            Link("instance", Leaf("industry"))
                          )
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        )
      )

    val tree2 = parseString(text)
    assert(tree == tree2)
  }

  "AmrCorpusParser" should "read in a simple corpus file" in {
    import gramr.amr._
    import AmrCorpusParser._

    val text =
      """# AMR release (generated on Mon Jan 27, 2014 at 20:44:25)

# ::id bolt12_07_4800.1 ::date 2012-12-19T12:53:14 ::annotator SDL-AMR-09 ::preferred
# ::snt Establishing Models in Industrial Innovation
# ::save-date Wed Oct 30, 2013 ::file bolt12_07_4800_1.txt
(e / establish-01
  :ARG1 (m / model
          :mod (i / innovate-01
                 :ARG1 (i2 / industry))))

# ::id bolt12_10510_9873.13 ::date 2012-12-19T18:20:50 ::annotator SDL-AMR-09 ::preferred
# ::snt And who were the proprietors of these companies?
# ::save-date Mon Nov 11, 2013 ::file bolt12_10510_9873_13.txt
(a / and
      :op2 (p / proprietor
            :domain (a2 / amr-unknown)
            :mod (c / company
                  :mod (t / this))))"""

    val records = List(
      new AmrCorpusItem(
        InnerNode(
          "e", List(
            Link("instance", Leaf("establish-01")),
            Link(
              "ARG1", InnerNode(
                "m", List(
                  Link("instance", Leaf("model")),
                  Link(
                    "mod", InnerNode(
                      "i", List(
                        Link("instance", Leaf("innovate-01")),
                        Link(
                          "ARG1", InnerNode(
                            "i2", List(
                              Link("instance", Leaf("industry"))
                            )
                          )
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        ),
        List(
          "id" -> "bolt12_07_4800.1",
          "date" -> "2012-12-19T12:53:14",
          "annotator" -> "SDL-AMR-09",
          "preferred" -> "",
          "snt" -> "Establishing Models in Industrial Innovation",
          "save-date" -> "Wed Oct 30, 2013",
          "file" -> "bolt12_07_4800_1.txt"
        )
      ),
      new AmrCorpusItem(
        InnerNode(
          "a", List(
            Link("instance", Leaf("and")),
            Link(
              "op2", InnerNode(
                "p", List(
                  Link("instance", Leaf("proprietor")),
                  Link(
                    "domain", InnerNode(
                      "a2", List(
                        Link("instance", Leaf("amr-unknown"))
                      )
                    )
                  ),
                  Link(
                    "mod", InnerNode(
                      "c", List(
                        Link("instance", Leaf("company")),
                        Link(
                          "mod", InnerNode(
                            "t", List(
                              Link("instance", Leaf("this"))
                            )
                          )
                        )
                      )
                    )
                  )
                )
              )
            )
          )
        ),
        List(
          "id" -> "bolt12_10510_9873.13",
          "date" -> "2012-12-19T18:20:50",
          "annotator" -> "SDL-AMR-09",
          "preferred" -> "",
          "snt" -> "And who were the proprietors of these companies?",
          "save-date" -> "Mon Nov 11, 2013",
          "file" -> "bolt12_10510_9873_13.txt"
        )
      )
    ).map(Right(_))

    val records2 = parseString(text).toList
    assert(records == records2)
  }

}