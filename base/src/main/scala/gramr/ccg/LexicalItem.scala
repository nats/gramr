package gramr.ccg

import gramr.text.Token

/**
  * A CCG lexical item combines a list of tokens with a syntactic and a semantic category.
  *
  * Sub classes of lexical items may add other information such as how the item was obtained
  * (e.g. through grammar induction or heuristic generation). However, this information is not
  * considered part of the semantic content of the item and is ignored in equality checks.
  */
trait LexicalItem[MR] {
  def tokens: Vector[Token]

  def synCat: SyntacticCategory

  def meaning: MR

  def lexiconKey: LexiconKey = LexiconKey(tokens)

  def toPlainLexicalItem = PlainLexicalItem(tokens, synCat, meaning)

  /**
    * Since we put lexical items into sets, we would like for the equality operator to
    * work semantically without regard to the specific implementation.
    */
  final override def equals(other: Any): Boolean = other match {
    case i: LexicalItem[MR] => tokens == i.tokens && synCat == i.synCat && meaning == i.meaning
    case _ => false
  }

  /**
    * Since we put lexical items into sets, we would like for the equality operator to
    * work semantically without regard to the specific implementation.
    */
  final override def hashCode: Int = {
    var code = 0
    code = code + tokens.hashCode
    code = code * 31 + synCat.hashCode
    code = code * 31 + meaning.hashCode
    code
  }

}

case class IndexedLexicalItem[MR](
  tokens: Vector[Token],
  synCat: SyntacticCategory,
  meaning: MR,
  index: Int
) extends LexicalItem[MR]

case class GeneratedLexicalItem[MR](
  tokens: Vector[Token],
  synCat: SyntacticCategory,
  meaning: MR,
  identifier: String
) extends LexicalItem[MR]


case class PlainLexicalItem[MR](
  tokens: Vector[Token],
  synCat: SyntacticCategory,
  meaning: MR
) extends LexicalItem[MR]

object PlainLexicalItem {

  import scalaz._, Scalaz._
  import scala.language.higherKinds

  def fromDerivationStep[MR, F[_] : Functor](step: DerivationStep[F[MR]]): F[PlainLexicalItem[MR]] =
    step.label.map { mr => PlainLexicalItem(step.tokens, step.synCat, mr) }

}