package gramr.util

import org.scalatest._

class ScoredSpec extends FlatSpec {

  "Enumeration of fixed-size best combinations" should "enumerate as expected" in {

    val data = Vector(
      Scored("A", 1.0),
      Scored("B", 0.7),
      Scored("C", 0.5),
      Scored("D", 0.1)
    )

    val best = Stream(
      Vector("A", "B"),
      Vector("A", "C"),
      Vector("B", "C"),
      Vector("A", "D"),
      Vector("B", "D"),
      Vector("C", "D")
    )

    assert(Scored.bestCombinationsOfN(2)(data).map(_.map(_.value)) == best)

  }

  it should "work even if the count is larger than the number of options" in {

    val data = Vector(
      Scored("A", 1.0),
      Scored("B", 0.7),
      Scored("C", 0.5),
      Scored("D", 0.1)
    )

    val best = Stream(
      Vector("A", "B", "C", "D")
    )

    assert(Scored.bestCombinationsOfN(5)(data).map(_.map(_.value)) == best)

  }

  "Enumeration of variable-sized best combinations" should "enumerate as expected" in {

    val data = Vector(
      Scored("A", 1.0),
      Scored("B", 0.7),
      Scored("C", 0.5),
      Scored("D", 0.1)
    )

    val best = Stream(
      Vector("A", "B", "C"),
      Vector("A", "B", "D"),
      Vector("A", "B"),
      Vector("A", "C", "D"),
      Vector("A", "C"),
      Vector("B", "C", "D"),
      Vector("B", "C"),
      Vector("A", "D"),
      Vector("A"),
      Vector("B", "D"),
      Vector("B"),
      Vector("C", "D"),
      Vector("C"),
      Vector("D"),
      Vector()
    )

    assert(Scored.bestCombinations(0 to 3)(data).map(_.map(_.value)) == best)

  }

  it should "work with a specific buggy configuration" in {

    val data = Vector(
      Scored("A", 0.19090909090909092),
      Scored("B", 0.19090909090909092),
      Scored("C", 0.19090909090909092),
      Scored("D", 0.19090909090909092),
      Scored("E", 0.045454545454545456)
    )

    val alg = new Scored.BestCombinationsAlgorithm(data)
    assert(alg.bestFrom(Vector(0, 1)).contains(Vector(0, 4)))

    assert(Scored.bestCombinationsOfN(2)(data).map(_.map(_.value)).contains(Vector("A", "E")))

  }

}
