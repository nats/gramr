package gramr.split.filters

import com.typesafe.scalalogging.LazyLogging
import gramr.graph.Graph
import gramr.split.{PartialMR, Split, SplitFilter}
import gramr.text.Token

import Graph.ops._

case class LimitNodeCount[MR : Graph](f: Vector[Token] => Int) extends SplitFilter[MR] with LazyLogging {

  def apply(split: Split[MR], sentence: Vector[Token]): Boolean = {
    val (ltokens, rtokens) = split.step.tokens.splitAt(split.left.span.size)
    pred(ltokens, split.left) && pred(rtokens, split.right)
  }

  def pred(tokens: Vector[Token], partialMR: PartialMR[MR]): Boolean = {
    val result = partialMR.mr.nodes.toVector.size <= f(tokens)
    if(!result) {
      logger.trace(s"reject ${partialMR.mr} for $tokens (${f(tokens)} tokens allowed)")
    }
    result
  }

}

object LimitNodeCount {

  def forSingleTokens[MR : Graph](groups: (Int, Iterable[String])*): LimitNodeCount[MR] = {
    val unpacked = for {
      (count, strings) <- groups
      string <- strings
    } yield (string.toLowerCase, count)
    val mapping = unpacked.toMap
    LimitNodeCount({
      case Vector(t) => mapping.getOrElse(t.text.toLowerCase, Integer.MAX_VALUE)
      case _ => Integer.MAX_VALUE
    })
  }

}