package gramr.ccg

import org.scalatest._

class SyntacticCategorySpec extends FreeSpec {

  import scala.language.implicitConversions

  implicit def parseSynCat(string: String): SyntacticCategory =
    SyntacticCategory.parse(string).get

  "Syntactic category matching" - {
    "should handle wildcards" in {

      def matches(a: String, b: String): Boolean =
        parseSynCat(a) matches parseSynCat(b)

      assert(matches("""S[X]""", """S[dcl]"""))
      assert(matches("""S[X]/NP""", """S[dcl]/NP"""))
      assert(!matches("""S[X]\NP""", """S[dcl]/NP"""))
    }
  }

}
