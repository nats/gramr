package gramr.learn.util

/** Indexes features based on the feature name's hash code.
  *
  * @param featureCount number of distinct feature IDs to produce
  */
@SerialVersionUID(1L)
class FeatureHasher(
  val featureCount: Int
) extends FeatureIndex with Serializable {


  def apply(featureName: String): Int = {
    Math.abs(featureName.hashCode % featureCount)
  }

  def reverse(featureId: Int): String = {
    featureId.toString
  }

}
