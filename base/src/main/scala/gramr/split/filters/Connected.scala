package gramr
package split
package filters

import gramr.text.Token
import gramr.graph._

import Graph.ops._

case class Connected[MR : Graph]() extends SplitFilter[MR] {

  def apply(split: Split[MR], sentence: Vector[Token]): Boolean = {
    split.left.mr.isConnected && split.right.mr.isConnected
  }

}
