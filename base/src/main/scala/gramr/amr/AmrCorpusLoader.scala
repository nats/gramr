package gramr.amr

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import gramr.graph._
import gramr.text.Example
import Graph.ops._
import Example.ops._

import scala.util.Random


/** The direct route to loading an AMR corpus and getting a sequence of examples.
  *
  * Construct the loader in a fluent style and then load your corpora:
  *
  *     val loader = AmrCorpusLoader
  *     val trainExamples = loader.load(trainFile)
  *     val valExamples = loader.load(valFile)
  *
  *  Parse errors are written to a logger.
  *
  * @param preprocessors a sequence of functions to apply on the AMR tree
  * @param postprocessors a sequence of functions to apply on the meaning representation
  * @tparam MR type of meaning representation to parse into
  * @tparam E type of example to produce
  */
class AmrCorpusLoader[MR: Graph, E: Example](
  preprocessors: Seq[AmrCorpusItem => AmrCorpusItem],
  postprocessors: Seq[(AmrCorpusItem, MR) => MR]
)(implicit
  amrToExample: (AmrCorpusItem, MR) => E
) extends LazyLogging {

  /** Loads all examples from an AMR corpus file.
    *
    * @param fileName path of the file to load
    * @return a sequence of examples
    */
  def loadFile(fileName: String): Seq[E] = {
    logger.info(s"Loading AMR corpus $fileName")

    val file = new File(fileName)

    // Load the AMR corpus while logging parse errors
    val items: Stream[AmrCorpusItem] = AmrCorpusParser.parseFile(file).flatMap {
      case Left(parseError) =>
        logger.error(parseError.getMessage)
        None
      case Right(item) =>
        Some(item)
    }

    // Apply preprocessors to the item
    val preprocessedItems: Stream[AmrCorpusItem] = items.map { item =>
      preprocessors.foldLeft(item) { (item, pp) => pp(item) }
    }

    // Create meaning representations
    val mrs: Stream[MR] = preprocessedItems.map { item =>
      Graph.fromAmrTree(item.amr)
    }

    // Apply postprocessors to the meaning representations
    val postprocessedMRs: Stream[MR] = (preprocessedItems zip mrs) map {
      case (item, mr) =>
        postprocessors.foldLeft(mr) { (mr, pp) => pp(item, mr) }
    }

    val examples: Stream[E] = (preprocessedItems zip postprocessedMRs) map {
      case (item, mr) =>
        amrToExample(item, mr)
    }

    val examplesVector = examples.toVector

    logger.info(s"Loaded ${examplesVector.size} examples")
    examplesVector
  }

  /** Adds a preprocessing step to this loader.
    *
    * The preprocessor is executed after all previously defined preprocessors and may therefore
    * depend on their results.
    *
    * @param preprocessor a function processing corpus items
    * @return the loader with an added preprocessing step
    */
  def addPreprocessor(preprocessor: AmrCorpusItem => AmrCorpusItem): AmrCorpusLoader[MR, E] = {
    new AmrCorpusLoader[MR, E](preprocessors :+ preprocessor, postprocessors)
  }

  /** Adds a postprocessing step to this loader.
    *
    * @param postprocessor a function processing meaning representations
    * @return the loader with an added postprocessing step
    */
  def addPostprocessor(postprocessor: (AmrCorpusItem, MR) => MR): AmrCorpusLoader[MR, E] = {
    new AmrCorpusLoader[MR, E](preprocessors, postprocessors :+ postprocessor)
  }

  /** Annotates alignments given in JAMR format to the elements of the AMR tree.
    *
    * @param sourceAttributeName name of the attribute to read
    * @param targetAttributeName name of the attribute to create
    * @return the loader with an added preprocessing step
    */
  def useJamrAlignment(
    sourceAttributeName: String = "alignments",
    targetAttributeName: String = "alignments"): AmrCorpusLoader[MR, E] =
    addPreprocessor(_.withJamrAlignment(sourceAttributeName, targetAttributeName))

  /** Annotates alignments given in ISI format to the elements of the AMR tree.
    *
    * @param sourceAttributeName name of the attribute to read
    * @param targetAttributeName name of the attribute to create
    * @return the loader with an added preprocessing step
    */
  def useIsiAlignment(
    sourceAttributeName: String = "alignments",
    targetAttributeName: String = "alignments",
  ): AmrCorpusLoader[MR, E] =
    addPreprocessor(_.withIsiAlignment(sourceAttributeName, targetAttributeName))


  /** Annotates alignments given in amr_ud format to the elements of the AMR tree.
    *
    * @param sourceAttributeName name of the attribute to read
    * @param targetAttributeName name of the attribute to create
    * @return the loader with an added preprocessing step
    */
  def useAmrUdAlignment(
    sourceAttributeName: String = "alignments",
    targetAttributeName: String = "alignments",
  ): AmrCorpusLoader[MR, E] =
    addPreprocessor { item =>
      val alignment = AmrUdAlignment.parse(item.getAttribute(sourceAttributeName).toSeq)
      new AmrCorpusItem(alignment.annotate(item.amr, targetAttributeName), item.attributes)
    }


  /** Adds some attributes to each item.
    *
    * @param attributes dictionary from example IDs to attribute lists
    * @return the loader with an added preprocessing step
    */
  def extendAttributes(attributes: Map[String, List[(String, String)]]): AmrCorpusLoader[MR, E] = {
    addPreprocessor { item =>
      item.extendAttributes(attributes.getOrElse(item.id, Nil))
    }
  }

  /** Combine two alignment layers using a combiner function.
    *
    * Output alignments are written to the `alignments` attribute. The required combiner function can be taken from
    * `gramr.amr.alignments.Combiners`.
    *
    * @param attrName1 name of the first alignment attribute layer
    * @param attrName2 name of the second alignment attribute layer
    * @param combiner function to combine an individual element's alignment
    * @return the corpus loader with the added postprocessing step
    */
  def combineAlignments(
    attrName1: String,
    attrName2: String,
    combiner: (Set[Int], Set[Int]) => Set[Int]
  ): AmrCorpusLoader[MR, E] = {
    def combinerWrapper(alignmentSets: Seq[Set[Int]]): Set[Int] = {
      alignmentSets match {
        case Seq(set1, set2) =>
          combiner(set1, set2)
      }
    }
    combineAlignments(combinerWrapper _, Seq(attrName1, attrName2), "alignments")
  }


  /** Combine several alignment layers using a combiner function.
    *
    * Output alignments are written to the specified target attribute.
    *
    * @param combiner function to combine an individual element's alignment
    * @param sourceAttributes attributes containing alignments which will be combined
    * @param targetAttribute attribute to write the combined alignments to
    * @return the corpus loader with the added postprocessing step
    */
  def combineAlignments(
    combiner: Seq[Set[Int]] => Set[Int],
    sourceAttributes: Seq[String],
    targetAttribute: String = "alignments"
  ): AmrCorpusLoader[MR, E] = {
    addPostprocessor { (item, mr) =>
      mr.mapAttributes { case Indexed(element, _) =>
        val alignmentSets = sourceAttributes.map { attributeName =>
          AlignmentGraph.getElementAlignments(element, attributeName)
        }
        val newAlignments = combiner(alignmentSets)
        element.attributes + (targetAttribute -> AlignmentsAttribute(newAlignments))
      }
    }
  }

}


object AmrCorpusLoader {

  /** Create an AmrCorpusLoader.
    *
    * @param amrToExample constructor for the example type; this function is called with a processed corpus item and meaning representation
    * @tparam MR type of meaning representation
    * @tparam E type of example
    * @return an AmrCorpusLoader that does not do any processing
    */
  def apply[MR: Graph, E: Example](implicit
    amrToExample: (AmrCorpusItem, MR) => E
  ): AmrCorpusLoader[MR, E] =
    new AmrCorpusLoader(Seq.empty, Seq.empty)


  /** A simple all-in-one function to load a data set with a few extra options.
    *
    * @param loader loader to use for reading the items
    * @param path path to the file to load
    * @param sampleSize if given, the number of examples to sample from the data set
    * @param maxSentenceLength if given, filters sentences longer than this number of tokens from the data set
    * @return
    */
  def loadDataSet[MR: Graph, E: Example](
    loader: AmrCorpusLoader[MR, E],
    path: String,
    sampleSize: Option[Int] = None,
    maxSentenceLength: Option[Int] = None,
    shuffle: Boolean = true
  )(implicit random: Random): Vector[E] = {

    val exampleStream = loader.loadFile(path)
    val lengthLimited = maxSentenceLength match {
      case Some(limit) => exampleStream.filter(_.sentence.length <= limit)
      case None => exampleStream
    }
    val loadedExamples = lengthLimited.toVector

    val shuffledExamples = if(shuffle) random.shuffle(loadedExamples) else loadedExamples

    val examples = sampleSize match {
      case Some(limit) => shuffledExamples.take(limit)
      case None => shuffledExamples
    }

    examples
  }
}
