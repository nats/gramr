package gramr.parse

import gramr.graph._
import gramr.learn._

import Graph.ops._

/**
  * Captures details about the context in which an edge occurs.
  *
  * The purpose of this class is to avoid creating the complete union graph for each hypothesised
  * edge operation.
  */
case class EdgeFeatureContext[MR : Graph](
  edge: Edge,
  source: Node,
  target: Node
)

object EdgeFeatureContext {

  def apply[MR : Graph](g: MR, e: Edge): EdgeFeatureContext[MR] = {
    val edge = e.unflip
    EdgeFeatureContext(edge, g.node(edge.source).element, g.node(edge.target).element)
  }

}


object EdgeFeatures {

  def default[MR : Graph, F : FeatureVector] =
    new ToFeatures[EdgeFeatureContext[MR], F] {
      def features(context: EdgeFeatureContext[MR]): F = {
        val EdgeFeatureContext(edge, source, target) = context
        implicitly[FeatureVector[F]].from(
          Seq(
            (edge.label, 1.0),
            (s"${source.content} -- ${edge.label}", 1.0),
            (s"${edge.label} -> ${target.content}", 1.0),
            (s"${source.content} -- ${edge.label} -> ${target.content}", 1.0)
          )
        )
      }
    }

}
