package gramr.util

import java.io.{BufferedWriter, File, FileWriter}

import scalatags.Text.all._

package object html {

  def writeToFile[T: ToHtml](file: File, thing: T): Unit = {
    val markup = scalatags.Text.all.html(
      head(),
      body(
        implicitly[ToHtml[T]].toHtml(thing)
      )
    )

    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(markup.toString)
    bw.close()
  }

}
