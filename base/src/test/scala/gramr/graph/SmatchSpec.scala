package gramr.graph

import gramr.graph.evaluate.Smatch
import org.scalatest._

class SmatchSpec extends FlatSpec {

  import gramr.util.Parsing._
  import gramr.util._

  "Smatch hill-climbing" should "identify the correct best matchings" in {

    implicit val mgl = AdjacencyMeaningGraph.graphInstance

    val g1 = parseAmr(
      """(w / want-01
        |   :ARG0 (b / boy)
        |   :ARG1 (g / go-01
        |            :ARG0 b))""".stripMargin
    )

    val g2 = parseAmr(
      """(w / want-01
        |   :ARG0 (b / boy)
        |   :ARG1 (f / football))""".stripMargin
    )

    val (mapping, stats) = new Smatch().randomInit(1, g1, g2)
    assert(stats == SimpleRetrievalStats(6, 5, 2))

    val (mapping2, stats2) = new Smatch().smartInit(g1, g2)
    assert(stats2 == SimpleRetrievalStats(6, 5, 2))

  }

  "Smatch hill-climbing" should "not overrate an imperfect parse" in {

    implicit val mgl = AdjacencyMeaningGraph.graphInstance

    val g1 = parseAmr(
      """(c / country :name (n / name :op1 "Saudi" :op2 "Arabia" :op1-of (n2 / name :name-of (o / organization))))"""
    )
    val g2 = parseAmr(
      """(c / country :name (n / name :op1 "Saudi" :op2 "Arabia"))"""
    )

    val alg = new Smatch[AdjacencyMeaningGraph]()

    val (mapping, stats) = new Smatch().randomInit(1, g1, g2)
    assert(stats.f1 < 1.0)
  }

  it should "downscore extra nodes and edges" in {

    implicit val mgl = AdjacencyMeaningGraph.graphInstance
    import mgl.Building._

    val gold = mgl.build { for(d <- mkReferentNode("date-entity"); year <- mkConstantNode("2002"); month <- mkConstantNode("1"); day <- mkConstantNode("1"); _ <- mkEdge("year", d, year); _ <- mkEdge("month", d, month); _ <- mkEdge("day", d, day)) yield () }
    val faulty = mgl.build { for(d <- mkReferentNode("date-entity"); year <- mkConstantNode("2002"); month <- mkConstantNode("1"); month2 <- mkConstantNode("1"); day <- mkConstantNode("1"); _ <- mkEdge("year", d, year); _ <- mkEdge("month", d, month); _ <- mkEdge("month", d, month2); _ <- mkEdge("day", d, day)) yield () }

    val smatch = new Smatch[AdjacencyMeaningGraph]()

    assert(smatch.smartInit(gold, faulty)._2.f1 < 1.0)
    assert(smatch.smartInit(faulty, gold)._2.f1 < 1.0)
  }

  it should "recognise distinctly labelled constant nodes" in {
    implicit val mgl = AdjacencyMeaningGraph.graphInstance
    import mgl.Building._

    val gold = mgl.build { for(d <- mkReferentNode("date-entity"); year <- mkConstantNode("2002"); month <- mkConstantNode("1"); day <- mkConstantNode("1"); _ <- mkEdge("year", d, year); _ <- mkEdge("month", d, month); _ <- mkEdge("day", d, day)) yield () }
    val faulty = mgl.build { for(d <- mkReferentNode("date-entity"); year <- mkConstantNode("2003"); month <- mkConstantNode("1"); day <- mkConstantNode("1"); _ <- mkEdge("year", d, year); _ <- mkEdge("month", d, month); _ <- mkEdge("day", d, day)) yield () }

    val smatch = new Smatch[AdjacencyMeaningGraph]()

    assert(smatch.smartInit(gold, faulty)._2.f1 < 1.0)
    assert(smatch.smartInit(faulty, gold)._2.f1 < 1.0)
  }
}
