package gramr.jobs

import java.io.File

import gramr.ccg.{DelexedLexicon, StoredLexicon}
import gramr.graph.Graph
import gramr.jobs.SplitChartOps.SplitChartJob
import gramr.split.{DerivationSplitChartToHtml, SplitChart, SplitChartFilter, SplitOutcome}
import gramr.text.Example
import Example.ops._
import com.typesafe.scalalogging.LazyLogging
import gramr.split.em.DelexicalizedModel
import gramr.tagging.TaggedDataSet
import gramr.util.html.ToHtml

import scala.collection.mutable

class SplitChartOps[T, MR, E](job: SplitChartJob[T, MR, E]) extends LazyLogging {

  import job.scheduler

  def filter(chartFilter: E => SplitChartFilter[MR]): SplitChartJob[T, MR, E] = {
    def computation(item: (E, (String, SplitOutcome[MR]))): (E, (String, SplitOutcome[MR])) = item match {
      case (example, (derivationName, Right(splitChart))) =>
        splitChart.filter(chartFilter(example))
        (example, (derivationName, Right(splitChart)))
      case result => result
    }

    job.append(ComputeStage(computation))
  }

  def addToLexicon(
    lexiconBuilder: StoredLexicon.Builder[MR],
    eraseSyntacticAttributes: Boolean,
  )(implicit graphInstance: Graph[MR]): SplitChartJob[T, MR, E] = {
    job.listenToItems {
      case (example, (derivationName, Right(splitChart))) =>
        gramr.ccg.StoredLexicon.splitChartItems(splitChart).foreach { item =>
          val withErasedAttributes = if(eraseSyntacticAttributes) item.copy(synCat = item.synCat.eraseFeatures) else item
          lexiconBuilder.synchronized {
            lexiconBuilder.add(withErasedAttributes.toPlainLexicalItem)
          }
        }
      case _ =>
        ()
    }
  }

  def trackSplitQualities(
    buffer: mutable.Buffer[(String, String, Double)]
  )(implicit exampleInstance: Example[E]): SplitChartJob[T, MR, E] = {
    job.listenToItems {
      case (example, (derivationName, splitOutcome)) =>
        buffer.synchronized {
          buffer.append((example.id, derivationName, splitOutcome.map(_.splitQuality).getOrElse(0.0)))
        }
    }
  }

  def trackTokenLevelCoverage(
    buffer: mutable.Buffer[(String, String, Double)]
  )(implicit exampleInstance: Example[E]): SplitChartJob[T, MR, E] = {
    job.listenToItems {
      case (example, (derivationName, splitOutcome)) =>
        buffer.synchronized {
          buffer.append((example.id, derivationName, splitOutcome.map(_.tokenLevelCoverage).getOrElse(0.0)))
        }
    }
  }

  def createTaggedDataSet(
    builder: TaggedDataSet.Builder,
    parameters: DelexicalizedModel[MR],
    lexicon: DelexedLexicon[MR]
  )(implicit exampleInstance: Example[E]): SplitChartJob[T, MR, E] = {
    job.listenToItems {
      case (example, (_, Right(splitChart))) =>
        logger.trace(s"Example ${example.id}: Generating tagged example")
        val taggedExample = TaggedDataSet.tagsFromSplitChart(example, splitChart, parameters, lexicon)
        builder.synchronized {
          builder.add(taggedExample)
          logger.trace(s"Example ${example.id}: Added tagged example")
        }
      case (example, (_, Left(error))) =>
        logger.trace(s"Example ${example.id}: Not generating tagged example because of error: $error")
        builder.add(None)
    }
  }

  def dumpSplitCharts(
    directory: File,
    prefix: String = "splitchart"
  )(implicit exampleInstance: Example[E], graphInstance: Graph[MR], graphToHtml: ToHtml[MR]): SplitChartJob[T, MR, E] = {
    job.listenToItems {
      case (example, (derivationName, Right(splitChart))) =>
        val derivFile = new File(directory, s"${prefix}der_${example.id}_$derivationName.html")
        val derivation = example.indexedDerivations.find(_._1 == derivationName).get._2
        val annotatedDerivation = splitChart.annotateDerivation(derivation)
        new DerivationSplitChartToHtml(annotatedDerivation).writeToFile(derivFile)
      case _ =>
        ()
    }
  }

}

object SplitChartOps {

  type SplitChartJob[T, MR, E] = Job[T, (E, (String, SplitOutcome[MR]))]

}
