package gramr.experiments

import java.io.File

import com.typesafe.scalalogging.Logger
import gramr.amr.{AmrCorpusItem, LexicalGenerators}
import gramr.ccg.{CombinatorSystem, DelexedLexicon, GeneratedLexicon, SyntacticCategory}
import gramr.graph.{AdjacencyMeaningGraph, Graph}
import gramr.graph.lexemes.WildcardFiller
import gramr.parse.{LexicalChoice, lexicalChoiceOps}

import Example.ops._

object ScriptSupport {

  val logger = Logger("script")

  implicit def amrToExample(item: AmrCorpusItem, mr: AdjacencyMeaningGraph)(implicit combinatorSystem: CombinatorSystem): Example[AdjacencyMeaningGraph] = {
    new gramr.experiments.Example[AdjacencyMeaningGraph](item, mr)
  }

  def loadLexicon[MR: Graph](lexDir: File, wildcardFillers: Iterable[WildcardFiller]): LexicalChoice[Example[MR], MR] = {
    val templateFile = new File(lexDir, "templates.txt")
    val lexemeFile = new File(lexDir, "lexemes.txt")

    val delexedLexicon = DelexedLexicon.load(
      templateFile=templateFile,
      lexemeFile=lexemeFile,
      wildcardFillers=wildcardFillers
    )

    val generatedLexicon = new GeneratedLexicon(
      "thing" -> LexicalGenerators.singleNode("thing", Set(SyntacticCategory("N"))),
      "emptymod" -> LexicalGenerators.identity,
      "numericDate" -> LexicalGenerators.numericDate,
      "dashDate" -> LexicalGenerators.dashDate,
      "americanSlashDate" -> LexicalGenerators.americanSlashDate
    )

    val delexedChoice: LexicalChoice[Example[MR], MR] = delexedLexicon.supertagLimitedLexicalChoice(
      extractTemplateTags = (e: Example[MR]) => e.supertags("supertags-template-id")//,
      //extractLabelTags = Some((e: Example[AdjacencyMeaningGraph]) => e.supertags("supertags-lex-label"))
    )
    val generatedChoice: LexicalChoice[Example[MR], MR] = generatedLexicon.lexicalChoice
    lexicalChoiceOps(delexedChoice) union generatedChoice
  }
}
