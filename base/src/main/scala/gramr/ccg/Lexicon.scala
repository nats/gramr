package gramr.ccg

import gramr.parse._

/**
  * A lexicon indexes a set of lexical items by their syntactic properties.
  */
trait Lexicon[MR] {

  /**
    * Retrieves all lexical items for the given key.
    */
  def get(key: LexiconKey): Iterable[LexicalItem[MR]]

  /**
    * A generic lexical choice that works with any lexicon.
    *
    * @tparam E an arbitrary type for examples
    * @return a lexical choice function
    */
  def lexicalChoice[E]: LexicalChoice[E, MR] = (_: E) => (i: SyntaxInformation) => {
    val tokens = i.span.in(i.sentence)
    val key = LexiconKey(tokens)
    for (candidate <- get(key).toIterator) yield {
      LexicalDecision(candidate, i.sentence, i.span)
    }
  }

}
