package gramr.learn

import gramr.learn.util.FeatureIndex

import scala.collection.immutable.HashMap
import scala.util.Random

/**
  * A model can score things (usually feature vectors).
  */
trait Model {

  def score[V: BaseVector](features: V): Double

}


@SerialVersionUID(2L)
class LinearModel(
  private val params: HashMap[Int, Double],
  private val default: Int => Double
)(implicit
  val featureIndex: FeatureIndex
) extends Model with Serializable {

  import FeatureVector.Syntax._

  def get(index: Int): Double = params.getOrElse(index, default(index))

  def score[V: BaseVector](features: V): Double = {
    implicitly[BaseVector[V]].entries(features).view.map { case (idx, value) =>
      value * get(idx)
    }.sum
  }

  def map(f: Double => Double): LinearModel = {
    val newParams = params.map { case (k, v) => (k, f(v)) }
    new LinearModel(newParams, default)
  }

  def pointwise(f: (Double, Double) => Double)(entries: Iterable[(Int, Double)]): LinearModel = {
    val newParams: HashMap[Int, Double] = entries.foldLeft(params) { case (acc, (index, value)) =>
      acc + (index -> f(acc.getOrElse(index, default(index)), value))
    }
    new LinearModel(newParams, default)
  }

  def scale(factor: Double): LinearModel = map(_ * factor)

  def add[V: FeatureVector](v: V): LinearModel = pointwise(_ + _)(v.indexedEntries)

  def add(o: LinearModel): LinearModel = pointwise(_ + _)(o.params)

  def subtract[V: FeatureVector](v: V): LinearModel = pointwise(_ - _)(v.indexedEntries)

  def subtract(o: LinearModel): LinearModel = pointwise(_ - _)(o.params)

  def size: Int = params.size
}

object LinearModel {

  def withDefault(default: Int => Double)(implicit featureIndex: FeatureIndex): LinearModel =
    new LinearModel(HashMap.empty, default)

  def empty(implicit featureIndex: FeatureIndex): LinearModel = withDefault(_ => 0.0)

  def smallRandom(std: Double = 0.001)(implicit featureIndex: FeatureIndex): LinearModel = withDefault { index =>
    val seed = featureIndex.reverse(index).hashCode
    val rnd = new Random(seed)
    rnd.nextGaussian() * std
  }

  implicit def vector: BaseVector[LinearModel] = new BaseVector[LinearModel] {

    override def entries(v: LinearModel): Iterable[(Int, Double)] = v.params
  }

}
