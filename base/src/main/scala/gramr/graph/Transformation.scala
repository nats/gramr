package gramr.graph

trait Transformation[MR] {

  def apply(mr: MR): MR

}
