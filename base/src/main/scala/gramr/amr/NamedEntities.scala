package gramr.amr

object NamedEntities {

  /** Describes how tags annotated by a named entity recogniser such as spaCy's might be translated to AMR named
    * entities.
    *
    * TODO: This list hasn't actually been compared with what is output by a NER for these AMR entities. Doing so might
    * improve things a bit further.
    */
  lazy val allExpansions: Map[String, Set[String]] = Map(
    "PERSON" -> "person, family, animal".split(", ").toSet,
    "NORP" -> "nationality, ethnic-group, regional-group, religious-group".split(", ").toSet,
    "FACILITY" -> "facility, airport, station, port, tunnel, bridge, road, railway-line, canal, building, theater, museum, palace, hotel, worship-place, market, sports-facility, park, zoo, amusement-park".split(", ").toSet,
    "ORG" -> "organization, company, government-organization, military, criminal-organization, political-party, school, university, research-institute, team, league".split(", ").toSet,
    "GPE" -> "city, city-district, county, local-region, state, province, country, country-region, world-region, continent".split(", ").toSet,
    "LOC" -> "city, city-district, county, local-region, state, province, country, country-region, world-region, continent, location, country-region, world-region, continent, ocean, sea, lake, river, gulf, bay, strait, canal, peninsula, mountain, volcano, valley, canyon, island, desert, forest, moon, planet, star, constellation, natural-object, award, music-key".split(", ").toSet,
    "PRODUCT" -> "product, vehicle, ship, aircraft, aircraft-type, spaceship, car-make, work-of-art, picture, music, show, broadcast-program, publication, book, newspaper, magazine, journal, food-dish, law, treaty".split(", ").toSet,
    "EVENT" -> "event, incident, natural-disaster, earthquake, war, conference, game, festival, award".split(", ").toSet,
    "WORK_OF_ART" -> "publication, book".split(", ").toSet,
    "LANGUAGE" -> Set("language")
  )

  lazy val proxyExpansions: Map[String, Set[String]] = Map(
    "PERSON" -> "person, family".split(", ").toSet,
    "NORP" -> "nationality, ethnic-group, religious-group".split(", ").toSet,
    "FACILITY" -> "facility, airport, station, port, road, railway-line, building, theater, museum, hotel, worship-place, market".split(", ").toSet,
    "ORG" -> "organization, company, government-organization, military, criminal-organization, political-party, school, university, research-institute, team".split(", ").toSet,
    "GPE" -> "city, city-district, county, local-region, state, province, country, country-region, world-region, continent".split(", ").toSet,
    "LOC" -> "city, city-district, county, local-region, state, province, country, country-region, world-region, continent, location, ocean, sea, river, peninsula, mountain, valley, island, desert, award".split(", ").toSet,
    "PRODUCT" -> "product, ship, aircraft, aircraft-type, spaceship, car-make, broadcast-program, publication, book, newspaper, magazine, journal, law, treaty".split(", ").toSet,
    "EVENT" -> "event, war, conference, game, festival, award".split(", ").toSet,
    "WORK_OF_ART" -> "publication, book".split(", ").toSet,
    "LANGUAGE" -> Set("language")
  )

  def calculateReductions(expansions: Map[String, Set[String]]): Map[String, Set[String]] = {
    val pairs = for {
      (k, v) <- expansions.toIterable  // toIterable is important as we'll produce a Map otherwise
      expansion <- v
    } yield (k, expansion)
    val emptySets: Map[String, Set[String]] = Map.empty[String, Set[String]].withDefaultValue(Set.empty[String])
    val sets: Map[String, Set[String]] = pairs.foldLeft(emptySets) { case (m, (k, expansion)) =>
      val entities: Set[String] = m(expansion)
      m + (expansion -> (entities + k))
    }
    sets
  }

  lazy val allReductions: Map[String, Set[String]] = calculateReductions(allExpansions)

  lazy val proxyReductions: Map[String, Set[String]] = calculateReductions(proxyExpansions)

}