package gramr
package split
package labelling

import gramr.graph._
import ccg._
import Graph.ops._
import gramr.graph.edit.{SplitLeft, SplitRight, SplitUnconstrained}

case class LabelAlignments[MR : Graph]() extends DeterministicLabellingFunction[MR] {

  def labelDeterministic(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): PartialMR[MR] = {
    import gramr.graph.Indexed
    val PartialMR(mr, span, synCat) = partialMR
    val splitIndex = step.subSteps(1).span.start
    val labelledMR = mr mapAttributes { case Indexed(element, pointer) =>
      val direction = element.attributes.getOption[AlignmentsAttribute]("alignments") match {
        case Some(alignments) =>
          if (alignments.edges.isEmpty)
            SplitUnconstrained
          else if (alignments.edges.forall(_ < splitIndex))
            SplitLeft
          else if (alignments.edges.forall(_ >= splitIndex))
            SplitRight
          else
            SplitUnconstrained
        case None => SplitUnconstrained
      }
      element.attributes + ("splitDirection" -> direction)
    }
    PartialMR(labelledMR, span, synCat)
  }

}
