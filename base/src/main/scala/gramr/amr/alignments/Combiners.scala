package gramr.amr.alignments


object Combiners {

  type Combiner = Seq[Set[Int]] => Set[Int]

  /** If only one set of alignments is non-empty, keeps that set. Otherwise, intersects both sets.
    *
    * @param l first alignment set
    * @param r second alignment set
    * @return processed alignment set
    */
  def intersectOrKeep(l: Set[Int], r: Set[Int]): Set[Int] = {
    if (l.nonEmpty && r.nonEmpty) {
      l intersect r
    }
    else {
      if (r.isEmpty) l else r
    }
  }

  def vote(n: Int)(sets: Seq[Set[Int]]): Set[Int] = {
    val counts: Map[Int, Int] = sets.flatten.groupBy(x => x).mapValues(_.size)
    val accepted = for((tokenIndex, count) <- counts if count >= n) yield tokenIndex
    accepted.toSet
  }

  def union: Combiner = vote(1)

}
