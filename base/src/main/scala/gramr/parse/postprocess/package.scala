package gramr.parse

import gramr.graph.Graph
import gramr.learn.FeatureVector
import gramr.parse.chart._

package object postprocess {

  /** Adds a coreference-resolving stage to a parser.
    *
    * @param parser the parser to augment
    * @tparam MR type of meaning representation
    * @tparam FV type of feature vector
    * @tparam C type of context (example)
    * @tparam L type of chart label
    * @return the augmented parser
    */
  def resolveCorefs[MR: Graph, FV: FeatureVector, C, L](parser: ChartParser[MR, FV, L, C]): PostProcessedParser[MR, FV, C] = {
    val resolver = new CorefResolver[MR, FV, C, L](parser)
    new PostProcessedParser[MR, FV, C](parser, resolver.apply)
  }

  /** Trims away all but the best evaluated parse.
    *
    * Charts are copied before modification, so the input chart is not affected.
    *
    * @param parser parser whose output to trim
    * @param measure evaluation function
    * @param epsilon maximum difference for parses to be considered of equal score
    * @tparam MR type of meaning representation
    * @tparam FV type of feature vector
    * @tparam E type of example
    * @return the filtered parser
    */
  def keepBestEvaluatedParse[MR: Graph, FV, E](
    parser: Parser[MR, FV, E],
    measure: (MR, E) => Double,
    epsilon: Double = 0.01
  ): PostProcessedParser[MR, FV, E] = {
    def postProcess(chart: Chart[MR, FV], example: E): Chart[MR, FV] = {
      chart.deepTrimmed(trimming.BestEvaluated(example, measure, epsilon))
    }
    new PostProcessedParser[MR, FV, E](parser, postProcess)
  }

}
