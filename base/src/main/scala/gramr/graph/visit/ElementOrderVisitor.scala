package gramr.graph.visit

import scala.collection.mutable.ArrayBuffer

/** A visitor which remembers the order in which nodes were visited.
 *
 * After the traversal, `nodeOrder` indicates the order in which elements were visited.
 */
class ElementOrderVisitor extends Visitor {
  val elementOrder: ArrayBuffer[Int] = ArrayBuffer.empty

  override def visitNode(nodeIndex: Int): Unit = {
    elementOrder.append(nodeIndex)
  }

  override def visitEdge(edgeIndex: Int): Unit = {
    elementOrder.append(edgeIndex)
  }

}
