package gramr.util

import gramr.graph._
import gramr.amr._
import java.io.File

object Parsing {

  import scala.collection.LinearSeq

  def parseAmr[MR : Graph](amr: String): MR = {
    Graph.fromAmrTree[MR](AmrTreeParser.parseString(amr))
  }


  /**
    * Parses an AMR corpus with annotated JAMR-style alignments.
    */
  def parseAlignedAmrCorpusLines[MR : Graph](lines: LinearSeq[String]): Stream[MR] =
    AmrCorpusParser.parseLines(lines) map {
      case Right(item) => Graph.fromAmrTree[MR](item.withJamrAlignment("alignments", "alignments").amr)
      case Left(e) => throw e
    }

  /**
    * Parses a single AMR with JAMR-style alignments.
    */
  def parseAlignedAmr[MR : Graph](amr: String): MR = parseAlignedAmrCorpusLines(amr.split("\n").toList).head

}
