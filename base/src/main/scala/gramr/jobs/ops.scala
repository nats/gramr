package gramr.jobs

import gramr.graph.Graph
import gramr.jobs.ExampleDerivationOps.ExampleDerivationJob
import gramr.jobs.SplitChartOps.SplitChartJob
import gramr.learn.Update
import gramr.text.{Example, HasMR}

import scala.language.implicitConversions

object ops {

  implicit def toAnyOps[A, B](job: Job[A, B]): AnyOps[A, B] = new AnyOps(job)

  implicit def toIterableOps[A, B](job: Job[A, Iterable[B]]): IterableOps[A, B] = new IterableOps(job)

  implicit def toExampleOps[MR, E: Example](job: Job[E, E]): ExampleOps[MR, E] = new ExampleOps(job)

  implicit def toChartOps[MR: Graph, FV, E: Example](job: Job[E, (E, NamedParses[MR, FV])])(implicit hasMR: HasMR[E, MR]): ChartOps[MR, FV, E] = new ChartOps(job)

  implicit def toEvalOps[MR: Graph, FV, E: Example](job: Job[E, (E, NamedEvals)])(implicit hasMR: HasMR[E, MR]): EvalOps[MR, FV, E] = new EvalOps(job)

  implicit def toUpdateOps[MR, FV, E: Example](job: Job[E, (E, Iterable[Update[FV]])]): UpdateOps[MR, FV, E] = new UpdateOps(job)

  implicit def toExampleDerivationOps[T, MR, E](job: ExampleDerivationJob[T, MR, E]): ExampleDerivationOps[T, MR, E] = new ExampleDerivationOps(job)

  implicit def toSplitChartOps[T, MR, E](job: SplitChartJob[T, MR, E]): SplitChartOps[T, MR, E] = new SplitChartOps(job)

}
