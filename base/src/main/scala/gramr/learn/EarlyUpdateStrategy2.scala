package gramr.learn

import com.typesafe.scalalogging.LazyLogging
import gramr.parse.chart.{Chart, State}
import gramr.text.{Example, Span}
import Example.ops._

/** Implements an early update strategy.
 *
 * The strategy selects updates in the following way:
 *
 *     1. Find the “good” derivation along which the parser progresses furthest.
 *     2. Find the lowest violating cut across the derivation.
 *
 * A good derivation is a derivation contained within the “good chart”. The lowest violating cut is a sequence
 * of states whose successors are all violating, but whose predecessors are all not violating.
 *
 * Some of the states may in this cut may not be violating and are not included in the update. However, the violating
 * states in the cut need to be improved upon in order to allow the parser to progress, so they are used for the update.
 *
 * @param badChartName Parse chart containing potentially “bad” parses.
 * @param goodChartName Parse chart containing a good parse. All states in this chart are considered “good”,
 *                        i.e. leading to a favourable result.
 */
class EarlyUpdateStrategy2[MR, FV: FeatureVector, E: Example](
  val badChartName: String,
  val goodChartName: String
) extends UpdateStrategy[MR, FV, E] with LazyLogging {

  import EarlyUpdateStrategy2._

  override def compute(example: E, charts: Map[String, Chart[MR, FV]]): Iterable[StatePairUpdate[MR, FV]] = {
    val badChart = charts(badChartName)
    val goodChart = charts(goodChartName)

    goodChart.outputStates.headOption match {
      case None =>
        logger.trace(s"${example.id}: No update (no good parses)")
        Iterable.empty
      case Some(goodState) =>
        val cut = findLowestViolatingCut(goodState, badChart)
        val violatingStatePairs = cut.statePairs.filter(_.violating)
        val spans = violatingStatePairs.map(_.span)
        if (spans.nonEmpty) {
          logger.trace(s"${example.id}: Updating spans " +
            s"${spans.map(_.in(example.sentence).mkString(" ")).mkString("[", "] [", "]")}"
          )

          val updates = for(statePair <- violatingStatePairs) yield StatePairUpdate(statePair.good, statePair.bad)
          updates
        }
        else {
          // No violation found
          logger.trace(s"${example.id}: No update (no violation found)")
          Iterable.empty
        }
    }
  }

  /** Find the best set of update candidates for a given good state.
   *
   * Finds a sequence of (good, bad) state pairs where
   *
   *     - the good state is a predecessor or equal to goodState
   *     - the bad state is a corresponding state from badChart
   *     - the cell of the bad state may be violating, i.e. might not contain the good state
   *     - all predecessors of the bad states are non-violating
   *     - the sequence is as short as possible, i.e. it represents the “best” set of predecessors
   *
   * If there is such a sequence with at least one violating state, this function finds it. Otherwise it returns
   * a non-violating sequence.
   *
   * @param goodState represents a hypothesis we want to enforce
   * @param badChart contains all hypotheses found by the parser
   */
  def findLowestViolatingCut(goodState: State[MR, FV], badChart: Chart[MR, FV]): StatePairSequence[MR, FV] = {
    val violating = isViolating(goodState, badChart)

    // Find possible sub-sequences.
    val subSequences: Iterable[StatePairSequence[MR, FV]] = goodState.actions map { action =>
      val preStateSequences = action.predecessors.map(preState => findLowestViolatingCut(preState, badChart))
      // Only use sub-sequences which are fully represented in the badChart
      if(preStateSequences.exists(_.isEmpty)) {
        StatePairSequence.empty[MR, FV]
      }
      else {
        StatePairSequence.concat(preStateSequences)
      }
    }

    // Filter out non-violating sub-sequences
    val violatingSubSequences = subSequences.filter(_.violating)

    if(violatingSubSequences.nonEmpty) {
      // If there are any sub-sequences, this is not a lowest violating cut
      // Return the lowest cut
      violatingSubSequences.minBy(_.statePairs.length)
    }
    else {
      // This is the lowest (possibly violating) cut, return it
      badChart.states(goodState.span).headOption match {
        case Some(localBadState) =>
          StatePairSequence(Vector(StatePair(goodState, localBadState, violating)))
        case None =>
          StatePairSequence.empty
      }
    }
  }

  /** Checks whether an equivalent state to goodState is in badChart.
   *
   * @param goodState state to look for
   * @param badChart chart to look in
   * @return true if goodState is not in badChart
   */
  def isViolating(goodState: State[MR, FV], badChart: Chart[MR, FV]): Boolean = {
    !badChart.states(goodState.span).exists { badState =>
      // TODO check: is equality appropriate here? are graphs sufficiently normalised? are attributes problematic?
      badState.syntax.synCat == goodState.syntax.synCat && badState.mr == goodState.mr
    }
  }

}

object EarlyUpdateStrategy2 {

  case class StatePair[MR, FV](
    good: State[MR, FV],
    bad: State[MR, FV],
    violating: Boolean
  ) {
    def span: Span = good.span
  }

  case class StatePairSequence[MR, FV](
    statePairs: Vector[StatePair[MR, FV]]
  ) {
    val violating = statePairs.exists(_.violating)

    def isEmpty: Boolean = statePairs.isEmpty
  }

  object StatePairSequence {
    def empty[MR, FV]: StatePairSequence[MR, FV] = StatePairSequence(Vector.empty)

    def concat[MR, FV](sequences: Iterable[StatePairSequence[MR, FV]]): StatePairSequence[MR, FV] = {
      StatePairSequence(sequences.flatMap(_.statePairs).toVector)
    }
  }

}
