package gramr.amr

import org.scalatest._
import gramr.text.Token, Token.Implicits._
import gramr.ccg._
import gramr.graph._

class TokenBasedLexicalGeneratorSpec extends FlatSpec {

  import LexicalGenerators._

  type MR = AdjacencyMeaningGraph
  val mgl: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance

  implicit def meaningGraphLike: Graph[MR] = mgl

  import mgl.Building._

  import SubGraphOps.Implicits._

  "Lexical generator “empty”" should "produce an empty graph" in {

    val key = LexiconKey(Vector("."))
    val meanings = empty[MR]().generate(key.tokens)
    assert(meanings.size == 1)
    assert(meanings.head.meaning.isEquivalentTo(mgl.empty))

  }

  "Lexical generator “named organisation”" should "produce the expected graph" in {

    val expected = mgl.build {
      for {
        org <- mkReferentNode("organization")
        n <- mkReferentNode("name")
        _ <- mkEdge("name", org, n)
        t1 <- mkConstantNode("\"University\"")
        _ <- mkEdge("op1", n, t1)
        t2 <- mkConstantNode("\"of\"")
        _ <- mkEdge("op2", n, t2)
        t3 <- mkConstantNode("\"Hamburg\"")
        _ <- mkEdge("op3", n, t3)
      } yield ()
    }

    val key = LexiconKey(Vector("University", "of", "Hamburg"))
    val meanings = namedOrganisation(mgl).generate(key.tokens)
    assert(meanings.size == 1)
    assert(meanings.head.meaning.isEquivalentTo(expected))

  }

}
