package gramr.jobs

import scala.collection.mutable.ListBuffer

/** Base class for stages that manage their own list of listeners.
  */
abstract class ListeningStage[A, B] extends Stage[A, B] {

  protected val listeners: ListBuffer[Listener[B]] = ListBuffer.empty

  def addListener(listener: Listener[B]): Unit = listeners.append(listener)

  def message(m: Listener[_] => Unit): Unit = listeners.foreach(m)

}
