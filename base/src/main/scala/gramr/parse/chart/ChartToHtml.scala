package gramr.parse.chart

import gramr.ccg.DerivationStep
import gramr.text.Span
import gramr.util.html.ToHtml
import scalatags.Text.TypedTag
import scalatags.Text.all._

/** Provides a class for writing parse charts as HTML.
  */
class ChartToHtml[MR: ToHtml, FV] extends ToHtml[Chart[MR, FV]] {

  import ToHtml.Implicits._

  def toHtml(chart: Chart[MR, FV]): Frag = {

    import scalatags.Text.all._

    val spans = chart.spans.flatMap(displaySpan(chart))
    html(body(
      List(linkTable(chart)) ++ spans.toSeq: _*
    ))
  }

  def linkTable(chart: Chart[MR, FV]): TypedTag[String] = {
    table(
      tr(td(), chart.sentence.indices.map { end => td(b(s"${chart.sentence(end)}")) }),
      chart.sentence.indices.map { start =>
        tr(
          td(b(s"${chart.sentence(start)}")),
          chart.sentence.indices.map { end =>
            td(
              if(chart.states(Span(start, end)).nonEmpty)
                a(href := s"#$start-$end", s"${chart.states(Span(start, end)).size}")
              else
                ""
            )
          }
        )
      }
    )
  }

  def stepSubtitle(label: Iterable[State[MR, FV]]): String = s"${label.size} results"

  def stepDisplay(step: DerivationStep[Iterable[State[MR, FV]]]): List[Frag] = {
    val states = step.label.toVector
    List(
      table(
        stateTableHeader,
        states.map(displayState)
      )
    )
  }

  def stateTableHeader: TypedTag[String] = tr(th("syncat"), th("MR"), th("score"))

  def displaySpan(chart: Chart[MR, FV])(span: Span): List[TypedTag[String]] = {
    List(
      h2(a(name := s"${span.start}-${span.end}", span.in(chart.sentence).map(_.text).mkString(" "))),
      table(
        chart.states(span).map(displayState).toSeq: _*
      )
    )
  }

  def displayState(state: State[MR, FV]): TypedTag[String] = {
    tr(
      td(state.syntax.synCat.toString),
      td(state.mr),
      td(state.score)
    )
  }

}
