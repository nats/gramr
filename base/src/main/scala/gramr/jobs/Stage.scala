package gramr.jobs

import monix.reactive.Observable

trait Stage[A, B] {

  def observable(input: Observable[A]): Observable[B]

  def addListener(listener: Listener[B]): Unit

  /** Send a message to listeners.
    *
    */
  def message(m: Listener[_] => Unit): Unit

}
