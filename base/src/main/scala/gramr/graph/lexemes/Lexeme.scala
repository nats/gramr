package gramr.graph
package lexemes

import scala.util.matching.Regex

import Graph.ops._

case class Lexeme(
  tokens: Vector[String],
  conceptLabels: Vector[String],
  constantLabels: Vector[String],
  edgeLabels: Vector[String]
) {

  import Lexeme._

  def signature: (Int, Int, Int, Int) = (tokens.length, conceptLabels.length, constantLabels.length, edgeLabels.length)

  def labels: Vector[String] = conceptLabels ++ constantLabels ++ edgeLabels

  def addConceptLabel(label: String): Lexeme = Lexeme(tokens, conceptLabels :+ label, constantLabels, edgeLabels)

  def addConstantLabel(label: String): Lexeme = Lexeme(tokens, conceptLabels, constantLabels :+ label, edgeLabels)

  def addEdgeLabel(label: String): Lexeme = Lexeme(tokens, conceptLabels, constantLabels, edgeLabels :+ label)

  def lexicalize[MR : Graph](template: MR): Option[MR] = {
    if(lexConceptCount(template) != conceptLabels.length
      || lexConstantCount(template) != constantLabels.length
      || lexEdgeCount(template) != edgeLabels.length) {
      None
    }
    else {
      Some(
        template.mapElementContents {
          case Indexed(n@ReferentNode(concept, attributes), _) =>
            lexElementIndex(concept) match {
              case Some(i) => ReferentNode(conceptLabels(i), attributes)
              case _ => n
            }
          case Indexed(n@ConstantNode(constant, attributes), _) =>
            lexElementIndex(constant) match {
              case Some(i) => ConstantNode(constantLabels(i), attributes)
              case _ => n
            }
          case Indexed(e@Edge(label, source, target, attributes), _) =>
            lexElementIndex(label) match {
              case Some(i) => Edge(edgeLabels(i), source, target, attributes)
              case _ => e
            }
          case Indexed(el, _) => el
        }
      )
    }
  }

  override def toString: String = {
    tokens.mkString(" ") +
      " | " + conceptLabels.mkString(" ") +
      " | " + constantLabels.mkString(" ") +
      " | " + edgeLabels.mkString(" ")
  }

}

object Lexeme {

  val lexRe: Regex = """<lex-(\d+)>""".r

  def isLexNode[MR : Graph](el: Element): Boolean = el match {
    case n: Node => lexRe.pattern.matcher(n.content).matches()
    case _: Edge => false
  }

  def isLexEdge[MR : Graph](el: Element): Boolean = el match {
    case _: Node => false
    case e: Edge => lexRe.pattern.matcher(e.label).matches()
  }

  def lexElementIndex[MR : Graph](label: String): Option[Int] = label match {
    case lexRe(i) => Some(Integer.parseInt(i))
    case _ => None
  }

  def lexConceptCount[MR : Graph](mr: MR): Int = mr.elements.count {
    case Indexed(el@ReferentNode(_, _), _) => isLexNode(el)
    case _ => false
  }

  def lexConstantCount[MR : Graph](mr: MR): Int = mr.elements.count {
    case Indexed(el@ConstantNode(_, _), _) => isLexNode(el)
    case _ => false
  }

  def lexEdgeCount[MR : Graph](mr: MR): Int = mr.elements.count {
    case Indexed(el, _) => isLexEdge(el)
    case _ => false
  }

  def canDelexicalize(label: String): Boolean = !label.startsWith("<")

  /**
    * Delexicalize a meaning graph by decomposing it into lexemes and templates.
    *
    * @param maxLexNodes the maximum number of lex nodes to extract
    * @param tokens the tokens associated with the meaning graph (to create the lexeme)
    * @param mr meaning graph to delexicalize
    * @return an iterable of lexeme-template pairs
    */
  def delexicalize[MR : Graph](maxLexNodes: Int, delexicalizeEdges: Boolean = true)(tokens: Vector[String], mr: MR): Iterable[(Lexeme, MR)] = {

    case class DelexState(lexeme: Lexeme, mr: MR, availableElements: List[Indexed[Element]])

    // Delexes a specific element, returning the replaced label and the delexed template
    def delexElement(index: Int, element: Indexed[Element], state: DelexState): Option[DelexState] = element match {
      case Indexed(ReferentNode(concept, attributes), toReplace) if canDelexicalize(concept) =>
        Some(state.copy(
          mr = state.mr.replaceElement(toReplace, ReferentNode(s"<lex-$index>", attributes)),
          lexeme = state.lexeme.addConceptLabel(concept)
        ))
      case Indexed(ConstantNode(constant, attributes), toReplace) if canDelexicalize(constant) =>
        Some(state.copy(
          mr = state.mr.replaceElement(toReplace, ConstantNode(s"<lex-$index>", attributes)),
          lexeme = state.lexeme.addConstantLabel(constant)
        ))
      case Indexed(Edge(label, source, target, attributes), toReplace) if canDelexicalize(label) && delexicalizeEdges =>
        Some(state.copy(
          mr = state.mr.replaceElement(toReplace, Edge(s"<lex-$index>", source, target, attributes)),
          lexeme = state.lexeme.addEdgeLabel(label)
        ))
      case _ => None
    }

    // Delexes each of a given list of available nodes, returning label, template, and the list of nodes following the delexed node
    def delexOne(index: Int)(state: DelexState): Iterable[DelexState] = {
      val headTails = state.availableElements.tails.collect { case h :: t => (h, t) }

      headTails.flatMap {
        case (h, t) =>
          for(editedState <- delexElement(index, h, state)) yield editedState.copy(availableElements = t)
      }.toVector
    }

    def delexAll(fromIndex: Int, states: Iterable[DelexState]): Iterable[DelexState] = {
      if(fromIndex >= maxLexNodes) {
        states
      }
      else {
        val delexed = states.flatMap(delexOne(fromIndex))
        states ++ delexAll(fromIndex + 1, delexed)
      }
    }

    val init = Iterable(DelexState(Lexeme(tokens, Vector.empty, Vector.empty, Vector.empty), mr, mr.elements.toList))
    delexAll(0, init).map(state => (state.lexeme, state.mr))
  }

}
