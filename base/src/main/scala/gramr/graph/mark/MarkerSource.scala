package gramr.graph
package mark

import cats.data.State

import Graph.ops._

trait MarkerSource[S, MR] {

  implicit def mgl: Graph[MR]

  def readMR: State[S, MR]

  def mark(pointer: Int): State[S, Marker]

  def rootNode: State[S, Marker] = for {
    mr <- readMR
    marker <- mark(mr.root)
  } yield marker

  def hasOutermostExternalNode(depth: Int): State[S, Boolean] = hasExternalNode(0)

  def outermostExternalNode: State[S, Marker] = externalNode(0)

  def hasExternalNode(depth: Int): State[S, Boolean] = for {
    mr <- readMR
  } yield mr.findExternalNode(depth).isDefined

  def externalNode(depth: Int): State[S, Marker] = for {
    mr <- readMR
    maybeNode = mr.findExternalNode(depth)
    marker <- mark(maybeNode.get)
  } yield marker

}

