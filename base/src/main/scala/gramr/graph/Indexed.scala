package gramr.graph

import monocle.Lens


@SerialVersionUID(1L)
case class Indexed[+E <: Element](
  element: E,
  pointer: Int
) extends Serializable {

  def map[F <: Element](f: E => F): Indexed[F] =
    Indexed(f(element), pointer)

}

object Indexed {

  def _element[E <: Element]: Lens[Indexed[E], E] =
    Lens[Indexed[E], E](_.element)(el => ie => ie.copy(element = el))

  /** Orders indexed elements by their pointers.
    *
    * Since this ordering does not consider elements, it is not total.
    */
  def ordering[E <: Element] =
    new Ordering[Indexed[E]] {
      def compare(x: Indexed[E], y: Indexed[E]): Int = {
        val pointerCmp = Integer.compare(x.pointer, y.pointer)
        pointerCmp
      }
    }
}
