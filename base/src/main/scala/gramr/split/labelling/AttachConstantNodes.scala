package gramr
package split
package labelling

import com.typesafe.scalalogging.LazyLogging
import ccg._
import gramr.graph._
import Graph.ops._
import gramr.graph.edit.SplitDirection

/** Attaches each constant node to the split direction of its single neighbour.
  */
case class AttachConstantNodes[MR]()(implicit val mgl: Graph[MR]) extends DeterministicLabellingFunction[MR] with LazyLogging {

  override def labelDeterministic(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): PartialMR[MR] = {
    val PartialMR(mr, span, synCat) = partialMR
    val labelledMR = mr.mapNodeAttributes {
      case Indexed(ConstantNode(_, attr), p) =>
        val Vector(neighbour) = mr.neighbours(p).toVector
        SplitDirection.of(neighbour).label(attr)
      case Indexed(n, _) => n.attributes
    }
    PartialMR(labelledMR, span, synCat)
  }
}
