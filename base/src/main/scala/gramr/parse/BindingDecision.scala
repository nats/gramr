package gramr
package parse

import gramr.graph._
import learn._
import ccg._
import FeatureVector.Syntax._
import gramr.construction.SyntacticContext

import Graph.ops._

class BindingDecision[MR : Graph](
  val referent: Int,
  val placeholder: Int,
  val graph: MR, // The graph resulting from the binding
  val context: SyntacticContext
) extends Decision[(Int, Int)] with Ordered[BindingDecision[MR]] {

  if (referent == placeholder) throw new Error("Cannot bind a node to itself")

  val adjacentEdgeContexts: Set[EdgeFeatureContext[MR]] = {
    val edges = graph.edges.filter { case Indexed(Edge(_, source, target, _), _) =>
      source == placeholder || target == placeholder
    }
    val transformedEdges = edges.map(transformEdge)
    val contexts = transformedEdges.map(e => EdgeFeatureContext(graph, e.get.element))
    contexts.toSet
  }

  def result: (Int, Int) = (referent, placeholder)

  def applyTo(g: MR): Option[MR] = {
    assert(g.node(placeholder) match { case Indexed(ExternalNode(_, _), _) => true; case _ => false })

    val bound = for {
    // Move all edges adjacent to node2 over to node1.
      merged <- (g mapNonDeterministic { iel =>
        if(iel.element.isEdge) {
          transformEdge(iel.asInstanceOf[Indexed[Edge]]).toStream
        }
        else {
          Stream(iel)
        }
      }).headOption

      // Copy over the “root” attribute to the referent
      // The graph will be focused on the root by OnionBinding
      rerooted = if (g.node(placeholder).element.attributes.contains("root")) {
        merged.mapNodeAttributes { case Indexed(n@IsNode(), i) =>
          if (i == referent) {
            val old =
              if (n.attributes.contains("root")) n.attributes.get[DefaultAttribute[Boolean]]("root").value
              else false
            val `new` = g.node(placeholder).element.attributes.get[DefaultAttribute[Boolean]]("root").value
            n.attributes + ("root" -> DefaultAttribute(old || `new`))
          }
          else n.attributes
        }
      }
      else merged
    } yield rerooted remove placeholder

    bound
  }

  def insertDeferredBinding(g: MR, bindingGroup: Int): Option[MR] = {
    if (g.degree(placeholder) != 1) {
      //throw new Error(s"Deferred bindings can only be used with true half-edge graphs, where every external node has degree 1. The following graph violates this rule:\n$g")
      None
    }
    else {

      // Move the root attribute away from the placeholder
      val placeholderNode = g.node(placeholder)
      val rerooted =
        if (placeholderNode.element.attributes.contains(
          "root"
        ) && placeholderNode.element.attributes.get[DefaultAttribute[Boolean]]("root").value) {
          val placeholderNeighbour =
            g.edges.find(_.element.source == placeholder).map(_.element.target).getOrElse(
              g.edges.find(_.element.target == placeholder).map(_.element.source).get
            )
          g.mapNodeAttributes {
            case Indexed(node@IsNode(), pointer) =>
              if (pointer == placeholderNeighbour)
                node.attributes + ("root" -> DefaultAttribute(true))
              else
                node.attributes
          }
        }
        else {
          g
        }

      // Copy, transform and insert the (single) adjacent edge to the placeholder
      val halfEdge = (rerooted.edges find {
        case Indexed(edge@IsEdge(), _) =>
          edge.source == placeholder || edge.target == placeholder
      }).get
      for {
        Indexed(bindingEdge@IsEdge(), _) <- transformEdge(halfEdge)
        labelled = bindingEdge.setAttribute("deferredBindingGroup", DefaultAttribute(bindingGroup))
        edgeAdded = rerooted.addEdge(labelled)._1
      } yield edgeAdded
    }
  }

  def transformEdge(e: Indexed[Edge]): Option[Indexed[Edge]] = e match {
    case Indexed(Edge(label, source, target, attr), i) =>
      val newSource = if (source == placeholder) referent else source
      val newTarget = if (target == placeholder) referent else target

      if (newSource == newTarget)
        None // Binding created a loop
      else
        Some(Indexed(Edge(label, newSource, newTarget, attr), i))
  }

  def compare(that: BindingDecision[MR]): Int = {
    val node1Cmp = Integer.compare(referent, that.referent)
    if (node1Cmp != 0) node1Cmp
    else {
      Integer.compare(placeholder, that.placeholder)
    }
  }

  override def equals(that: Any): Boolean = that match {
    case d: BindingDecision[MR] => compare(d) == 0
    case _ => false
  }

  override def hashCode: Int = 41 * referent.hashCode + placeholder.hashCode

  override def toString = s"referent $referent to placeholder $placeholder"
}

object BindingDecision {

  def apply[MR : Graph](
    referent: Int,
    placeholder: Int,
    graph: MR,
    step: CombinatoryStep[Unit]
  ): BindingDecision[MR] = {
    new BindingDecision(referent, placeholder, graph, SyntacticContext.fromStep(step))
  }

  def toFeatures[MR : Graph, F : FeatureVector](
    implicit edgeFeatures: ToFeatures[EdgeFeatureContext[MR], F]
  ): ToFeatures[BindingDecision[MR], F] =
    (i: BindingDecision[MR]) => {
      val feats = i.adjacentEdgeContexts.toSeq.map { c => edgeFeatures.features(c) }
      feats.foldLeft(implicitly[FeatureVector[F]].empty) { (sum, f) => sum + f }
    }

}
