package gramr.parse

import gramr.ccg.{CombinatorSystem, SyntacticCategory}
import gramr.construction.SyntacticContext
import gramr.learn.FeatureVector
import gramr.parse.chart._
import gramr.text.{Span, Token}

import scala.collection.mutable

class AStarParser[MR, F : FeatureVector, C](
  val lexicalChoice: LexicalChoice[C, MR],
  val combinatoryChoice: CombinatoryChoice[MR],
  val combinatorSystem: CombinatorSystem,
  val decisionFeatures: (Decision[MR], C) => F,
  val stateFeatures: (MR, C) => F,
  val lexicalFeatures: (LexicalDecision[MR], C) => F,
  val modelScore: F => Double,
  val actionFilter: ActionPredicate[MR, F, C],
  val leafCategories: (C, Span) => Iterable[SyntacticCategory],
  val nbest: Int,
  val beamSize: Int,
  val maxActions: Long
) extends Parser[MR, F, C] with ChartParser[MR, F, InheritableLabel[TwoStageFeatures[F]], C] {

  import scala.language.implicitConversions

  type L = TwoStageFeatures[F]

  implicit val lcl: ChartLabel[L] = TwoStageFeatures.maxScoring[F]
  implicit val lhf: HasFeatures[F, L] = TwoStageFeatures.hasFeatures[F]
  implicit val lo: Ordering[L] = TwoStageFeatures.orderByScore[F]

  override def featureVector: FeatureVector[F] = implicitly[FeatureVector[F]]
  override def chartLabel: ChartLabel[InheritableLabel[L]] = InheritableLabel.chartLabel
  override def hasFeatures: HasFeatures[F, InheritableLabel[L]] = InheritableLabel.hasFeatures[F, L]
  override def labelOrdering: Ordering[InheritableLabel[L]] = InheritableLabel.ordering[L]

  def scoreAction(a: Action): Double = a.label.value.score

  def label(decision: Decision[MR], span: Span, context: C): InheritableLabel[L] = {
    val inheritable = decisionFeatures(decision, context)
    val local = stateFeatures(decision.result, context)
    val lexical = decision match {
      case ld: LexicalDecision[MR] => lexicalFeatures(ld, context)
      case _ => implicitly[FeatureVector[F]].empty
    }
    InheritableLabel(
      local = TwoStageFeatures(implicitly[FeatureVector[F]].empty, 0.0, local, modelScore(local)),
      inheritable = TwoStageFeatures(lexical, modelScore(lexical), inheritable, modelScore(inheritable))
    )
  }

  def stateLabelFeatures(state: State): F = state.label.value.features

  def priority(a: Action): Double = a.label.inheritable.priority

  override def parse(sentence: Vector[Token], context: C): Chart = {
    val chart = Chart.empty(sentence)
    val agenda = new Agenda()
    val filter = actionFilter(context)

    // Fill the heuristic map assigning a maximum outer score to each span
    def spans = for {
      spanSize <- 1 to sentence.size
      start <- 0 to (sentence.size - spanSize)
      end = start + spanSize - 1
    } yield Span(start, end)
    def lspans(span: Span) = for(start <- 0 until span.start) yield Span(start, span.start - 1)
    def rspans(span: Span) = for(end <- span.end + 1 until sentence.size) yield Span(span.end + 1, end)
    def spanSplits(span: Span) = for(split <- span.start + 1 to span.end) yield (Span(span.start, split - 1), Span(split, span.end))

    val bestSpanScores = mutable.Map.empty[Span, Double].withDefault { span: Span => if(span.size <= 0) 0.0 else Double.NegativeInfinity }
    val lexicalActions = spans.map { span =>
      // Enter the span scores from the shorter, subsumed spans
      for((l, r) <- spanSplits(span)) {
        val score = bestSpanScores(l) + bestSpanScores(r)
        if(bestSpanScores(span) < score) {
          bestSpanScores(span) = score
        }
      }

      val leafCats = leafCategories(context, span).toSet
      val lexicalChoiceInstance = lexicalChoice(context)
      for {
        synCat <- leafCats
        syntax = SyntaxInformation(sentence, span, synCat)
        decision <- lexicalChoiceInstance(syntax)
        // Note that we're looping over a set here, so each decision is kept only once.
        action = new Action(
          decision,
          label(decision, decision.syntax.span, context),
          decision.syntax,
          Vector.empty
        ) if filter(action)
      } yield {
        // Update the maximum score for this span
        val lexScore = modelScore(lexicalFeatures(decision, context))
        if (bestSpanScores(action.span) < lexScore) {
          bestSpanScores(action.span) = lexScore
        }
        action
      }
    }
    // Heuristics = Maximal sum of contiguous lexical scores to the left and right of the span
    def heuristic(span: Span): Double = bestSpanScores(Span(0, span.start - 1)) + bestSpanScores(Span(span.end + 1, sentence.size - 1))

    lexicalActions.flatten.foreach { action =>
      agenda.putAction(action, priority(action), heuristic(action.span))
    }

    var actionsTaken = 0L
    while(
      actionsTaken < maxActions &&
      !agenda.isEmpty &&
      chart.states(Span(0, sentence.size - 1)).size < nbest
    ) {
      actionsTaken += 1
      val action = agenda.take()
      val state = chart.putAction(action)
      val synCat = state.syntax.synCat
      chart.cell(action.span).enforce(beamSize)

      // Did this action lead to a new state?
      if(state.actions.size == 1 && !state.dropped) {
        // Add followup unary actions
        for {
          (cmb, converted) <- combinatorSystem.unaryConversions(synCat) if synCat != converted
        } {
          val action = new Action(
            UnaryDecision(
              result = state.mr,
              context = SyntacticContext(cmb, converted, List(synCat))
            ),
            ChartLabel.zero[InheritableLabel[L]],
            state.syntax.copy(synCat = converted),
            Vector(state)
          )
          if(filter(action)) {
            agenda.putAction(action, priority(action), heuristic(action.span))
          }
        }

        // Add followup binary actions
        for {
          (adjSpan, flipped) <- lspans(state.span).map((_, false)) ++ rspans(state.span).map((_, true))
          adjState <- chart.states(adjSpan).toVector
          (lstate, rstate) = if(!flipped) (adjState, state) else (state, adjState)
          lsyn = lstate.syntax.synCat
          rsyn = rstate.syntax.synCat
          combinator <- combinatorSystem.combinators if combinator.canApply(lsyn, rsyn)
          synCat <- combinator.synApply(lsyn, rsyn)
        } {
          val span = Span(lstate.span.start, rstate.span.end)
          val syntax = SyntaxInformation(sentence, span, synCat)
          val decisionSpace = combinatoryChoice.decisionSpace(
            (SyntacticContext(
              combinator, synCat, List(lsyn, rsyn)
            ), lstate.mr, rstate.mr)
          )
          val actions = decisionSpace.map { decision =>
            new Action(
              decision,
              label(decision, syntax.span, context),
              syntax,
              Vector(lstate, rstate)
            )
          }.filter(filter(_))
          actions foreach { action =>
            agenda.putAction(action, priority(action), heuristic(action.span))
          }
        }
      }

    }

    chart
  }


  /** The agenda for the A* parser.
    *
    * Two types of items are supported: Those that explicitly refer to an action, and those that can be *expanded*
    * to a sequence of actions.
    *
    * Expandable agenda items are used to provide some degree of laziness. If they are ever found in the head position
    * of the agenda, they are first expanded, meaning that each individual action they generate is pushed onto the
    * agenda. To achieve this, expandable agenda items are always ordered before other items with the same
    * heuristic value.
    */
  class Agenda {

    private implicit val cmpItems: Ordering[AgendaItem] = (x: AgendaItem, y: AgendaItem) => {
      if (x.priority.compareTo(y.priority) == 0) {
        0
      }
      else {
        scoreAction(x.action).compareTo(scoreAction(y.action))
      }
    }

    private val items = collection.mutable.PriorityQueue.empty[AgendaItem]

    def putAction(action: Action, innerScore: Double, outerScore: Double): Unit = {
      items.enqueue(AgendaItem(action, innerScore + outerScore))
    }

    def take(): Action = items.dequeue().action

    def isEmpty: Boolean = items.isEmpty
  }

  case class AgendaItem(
    action: Action,
    priority: Double
  )

}
