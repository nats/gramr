package gramr
package parse

import gramr.ccg.{CombinatorSystem, DerivationNode}
import gramr.construction.{Rule, SyntacticContext, Transform}
import gramr.graph._

class PatternChoice[MR : Graph](
  combinatorSystem: CombinatorSystem,
  patterns: Seq[Rule[MR]],
  transforms: Seq[Transform[MR]] = Seq.empty
) extends BinaryChoice[MR] {

  def decisionSpace(i: (DerivationNode[MR], DerivationNode[MR])): Iterator[CombinatoryDecision[MR]] = {
    val (l, r) = i
    for {
      combinator <- combinatorSystem.combinators.toIterator if combinator.canApply(l.synCat, r.synCat)
      context = SyntacticContext(combinator, combinator.synApply(l.synCat, r.synCat).get, List(l.synCat, r.synCat))
      pattern <- patterns if pattern.appliesTo(context)
      result <- pattern.combine(l.label, r.label, context)
      transformed <- expandTransforms(result, transforms)
    } yield PatternChoice.Decision(transformed, pattern, context)
  }

  def applicablePatterns(context: SyntacticContext): Stream[Rule[MR]] = patterns.toStream.filter(_.appliesTo(context))

  def expandTransforms(mr: MR, transforms: Seq[Transform[MR]]): Iterable[MR] = {
    transforms.foldLeft(Iterable(mr)) { (mrs, t) => mrs.flatMap(mr => t.transform(mr)) }
  }

}

object PatternChoice {

  case class Decision[MR : Graph](
    result: MR,
    pattern: Rule[MR],
    context: SyntacticContext
  ) extends CombinatoryDecision[MR]

}
