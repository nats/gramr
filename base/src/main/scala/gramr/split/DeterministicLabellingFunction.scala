package gramr
package split

import gramr.graph._, ccg._

trait DeterministicLabellingFunction[MR] extends LabellingFunction[MR] {
  self =>


  def labelDeterministic(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): PartialMR[MR]

  def label(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): Stream[PartialMR[MR]] =
    Stream(labelDeterministic(partialMR, step))

}
