package gramr

package object util {

  val intRe = """\d+""".r

  /** Fault-tolerant String-to-Int conversion without throwing any exceptions.
    *
    * Convertibility is checked using a refex before the actual conversion is performed.
    *
    * @param str a string
    * @return a corresponding Int if 'str' consists only of digits; None otherwise
    */
  def strToInt(str: String): Option[Int] = str match {
    case intRe() => Some(str.toInt)
    case _ => None
  }

}
