package gramr.jobs

class IterableOps[A, B](val job: Job[A, Iterable[B]]) {

  import job.scheduler

  def flatten: Job[A, B] = job.append(new FlattenStage)

}
