package gramr.learn

trait BaseVector[V] {

  def entries(v: V): Iterable[(Int, Double)]

}