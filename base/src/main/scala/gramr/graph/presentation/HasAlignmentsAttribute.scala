package gramr.graph
package presentation

import Graph.ops._

case class HasAlignmentsAttribute(hasAlignments: Boolean) extends Attribute {

  import scalatags.Text.all._, scalatags.Text.TypedTag

  private def bgColour = "#ffa0a0"

  override def mangleEdgeHtml(html: TypedTag[String]): TypedTag[String] =
    if (hasAlignments)
      html
    else
      span(border := "1px solid red", html)

  override def mangleNodeHtml(html: TypedTag[String]): TypedTag[String] =
    if (hasAlignments)
      html
    else
      span(border := "2px solid red", color := "red", html)

}

object HasAlignmentsAttribute {

  def annotate[G](g: G)(implicit meaningGraphLike: Graph[G]): G = {
    def mkAttr(a: Attributes): Option[Attribute] =
      for (attr <- a.getOption[AlignmentsAttribute]("alignments")) yield HasAlignmentsAttribute(attr.edges.nonEmpty)

    g.mapAttributes(
      { case Indexed(el, _) =>
        mkAttr(el.attributes) match {
          case Some(attr) => el.attributes + ("hasAlignments" -> attr)
          case None => el.attributes
        }
      }
    )
  }

}