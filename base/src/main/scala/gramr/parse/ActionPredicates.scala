package gramr.parse

import gramr.ccg.SyntacticCategory
import gramr.graph._
import gramr.text.{Example, HasMR, Span}
import Example.ops._
import Graph.ops._
import com.typesafe.scalalogging.LazyLogging
import gramr.graph.evaluate.Smatch

import scala.util.matching.Regex

object ActionPredicates {

  /** Turns a predicate over MRs into an ActionPredicate.
    *
    * @param f predicate over actions
    * @tparam MR type of meaning representation
    * @tparam FV type of feature vector
    * @tparam C arbitrary type of context
    * @return an ActionPredicate
    */
  def mrPredicate[MR: Graph, FV, C](f: MR => Boolean): ActionPredicate[MR, FV, C] = _ => action => f(action.mr)

  def isConnected[MR: Graph, FV, C]: ActionPredicate[MR, FV, C] = mrPredicate(_.isConnected)

  def isValidAMR[MR: Graph, FV, C]: ActionPredicate[MR, FV, C] = mrPredicate(mr => Predicates.isValidAmr(mr.kernel))

  def subgraphCorrect[MR: Graph, FV, C: Example](implicit hasMR: HasMR[C, MR]): ActionPredicate[MR, FV, C] =
    context => action => {
      import SubGraphOps.Implicits._
      context.reference.containsGraph(action.mr.kernel)
    }

  def subgraphMatches[MR: Graph, FV, C: Example](minPrecision: Double)(implicit hasMR: HasMR[C, MR]): ActionPredicate[MR, FV, C] =
    context => action => subgraphMatchPrecision(action.mr, context) >= minPrecision - 0.01


  def subgraphMatchPrecision[MR: Graph, E: Example](mr: MR, example: E)(implicit hasMR: HasMR[E, MR]): Double = {
    val (_, mappingStats) = new Smatch().randomInit(5, mr, example.reference)
    mappingStats.precision
  }

  /** A cheap, optimistic approximation for Smatch precision.
    * Looks at the proportion of (full) edge triples that are present in the
    * reference graph, but without assuming a single global alignment between
    * the test and reference graphs.
    */
  def edgeTriplePrecision[MR: Graph, FV, C: Example](minAccuracy: Double = 1.0)(implicit hasMR: HasMR[C, MR]): ActionPredicate[MR, FV, C] =
    context => action => EdgeTriplePrecision.precision(action.mr, context.reference) >= (minAccuracy - 0.01)

  object EdgeTriplePrecision {

    case class EdgeTriple(sourceLabel: String, edgeLabel: String, targetLabel: String)

    def isPlaceholder[MR : Graph](graph: MR, nodePointer: Int): Boolean = {
      val node = graph.node(nodePointer).element
      node.isExternalNode || node.content.startsWith("<")
    }

    def edgeTriple[MR : Graph](graph: MR, edge: Edge): Option[EdgeTriple] = {
      val normEdge = edge.unflip
      if (isPlaceholder(graph, edge.source) || isPlaceholder(graph, edge.target))
        None
      else
        Some(
          EdgeTriple(
            graph.node(normEdge.source).element.content, normEdge.label, graph.node(normEdge.target).element.content
          )
        )
    }

    def precision[MR : Graph](test: MR, ref: MR): Double = {
      val mrTriples = test.kernel.edges.flatMap(indexedEdge => edgeTriple(test, indexedEdge.element)).toSet
      val refTriples = ref.edges.flatMap(indexedEdge => edgeTriple(ref, indexedEdge.element)).toSet
      val precision =
        if (mrTriples.nonEmpty)
          (mrTriples & refTriples).size.toDouble / mrTriples.size.toDouble
        else
          1.0
      precision
    }
  }

  /** Attempts to approximate a subgraph check by enumerating all length-2 paths that don't involve placeholders.
    */
  def graphElementsCorrect[MR: Graph, FV, C: Example](permittedErrors: Int = 0, path2: Boolean = false)(implicit hasMR: HasMR[C, MR]): ActionPredicate[MR, FV, C] = {
    import GraphElementsCorrect._

    context => {
      val refElements = elements(context.reference, path2 = path2)
      action => {
        val actionElements = elements(action.mr, path2 = path2)
        val difference = actionElements diff refElements
        difference.size <= permittedErrors
      }
    }

  }

  object GraphElementsCorrect {

    val opRe: Regex = """op(\d+)""".r

    def processEdgeLabel(label: String): String = label match {
      case opRe(_) => "op"
      case _ => label
    }

    def elements[MR: Graph](mr: MR, path2: Boolean = false): Vector[String] = {
      val edgeElements = for {
        edge <- mr.edges.map(_.element.unflip)
        source = mr.node(edge.source).element
        target = mr.node(edge.target).element
        label = processEdgeLabel(edge.label)
        element <- Iterable(
          Some(s":$label"),
          if(acceptNode(source)) Some(s"${source.content} :$label") else None,
          if(acceptNode(target)) Some(s":$label ${target.content}") else None,
          if(acceptNode(source) && acceptNode(target)) Some(s"${source.content} :$label ${target.content}") else None,
        ).flatten
      } yield element
      val path2Elements =
        if(path2) {
          for {
            edge <- mr.edges.map(_.element.unflip)
            source = mr.node(edge.source).element
            target = mr.node(edge.target).element
            label = processEdgeLabel(edge.label)
            edge2 <- mr.outEdges(edge.target).map(_.element.unflip)
            label2 = processEdgeLabel(edge2.label)
            target2 = mr.node(edge2.target).element
            element <- Iterable(
              Some(s":$label"),
              if(acceptNode(source) && acceptNode(target)) Some(s"${source.content} :$label ${target.content} :$label2") else None,
              if(acceptNode(target) && acceptNode(target2)) Some(s":$label ${target.content} :$label2 ${target2.content}") else None,
              if(acceptNode(source) && acceptNode(target) && acceptNode(target2)) Some(s"${source.content} :$label ${target.content} :$label2 ${target2.content}") else None
            ).flatten
          } yield element
        }
        else {
          Iterable.empty
        }
      val nodeElements = for(node <- mr.nodes.map(_.element) if acceptNode(node)) yield node.content
      (edgeElements ++ path2Elements ++ nodeElements).toVector
    }

    def acceptNode(node: Node): Boolean = {
      !node.isExternalNode && node.content != "<coref>"
    }

    def precision[MR : Graph](test: MR, ref: MR): Double = {
      val refElements = elements(ref)
      val testElements = elements(test)
      val difference = testElements diff refElements

      if(testElements.isEmpty)
        1.0
      else
        1.0 - (difference.size.toDouble / testElements.size.toDouble)
    }

  }

  def alignmentsFulfilled[MR: Graph, FV, C: Example](permittedErrors: Int = 0)(implicit hasMR: HasMR[C, MR]): ActionPredicate[MR, FV, C] =
    context => action => {
      val (present, expected) = AlignmentsFulfilled.presentExpected(action.mr, action.syntax.span, context)
      val requiredNodes = expected.toDouble - permittedErrors
      present >= requiredNodes
    }

  object AlignmentsFulfilled extends LazyLogging {

    def expectedNodes[MR : Graph, E: Example](
      span: Span, example: E
    )(implicit hasMR: HasMR[E, MR]): Iterable[Indexed[Node]] = {
      example.reference.nodes.filter { n =>
        if (n.element.attributes.contains("alignments")) {
          val alignedTokens = n.element.attributes.get[AlignmentsAttribute]("alignments").edges
          alignedTokens.forall(span.contains) && alignedTokens.nonEmpty
        }
        else {
          logger.warn(s"Node is missing 'alignments' attribute in ${example.id}")
          false
        }
      }
    }

    def presentExpected[MR : Graph, E: Example](
      mr: MR, span: Span, example: E
    )(implicit hasMR: HasMR[E, MR]): (Int, Int) = {
      val kernel = mr.kernel
      // Collect the nodes that are aligned to the current span
      val expectedNodeLabels = expectedNodes(span, example).map(_.element.content).toSet
      val presentNodes = expectedNodeLabels.filter(label => kernel.nodes.exists(_.element.content == label))
      (presentNodes.size, expectedNodeLabels.size)
    }

    def accuracy[MR : Graph, E: Example](
      mr: MR, span: Span, example: E
    )(implicit hasMR: HasMR[E, MR]): Double = {
      val (present, expected) = presentExpected(mr, span, example)
      if (expected == 0)
        1.0
      else
        present.toDouble / expected.toDouble
    }

  }

  def matchSyntacticArity[MR: Graph, FV, C]: ActionPredicate[MR, FV, C] = _ => action => {
    action.mr.externalLayerCount <= action.syntax.synCat.arity
  }

  def rootActionSmatchFilter[MR: Graph, FV, E: Example](minSmatch: Double)(implicit hasMR: HasMR[E, MR]): ActionPredicate[MR, FV, E] =
    context => action => {
      val rootSpan = Span(0, action.syntax.sentence.size - 1)
      if (action.syntax.span == rootSpan) {
        val smatch = new Smatch[MR]()
        smatch.smartInit(action.mr, context.reference)._2.f1 >= minSmatch
      }
      else {
        true
      }
    }

  def rootSingleConstantFilter[MR: Graph, FV, C]: ActionPredicate[MR, FV, C] =
    _ => action => {
      val rootSpan = Span(0, action.syntax.sentence.size - 1)
      if (action.syntax.span == rootSpan) {
        !(action.mr.nodes.size == 1 && action.mr.nodes.head.element.isConstantNode)
      }
      else {
        true
      }
    }

  def rootCategoryFilter[MR: Graph, FV, C](categories: C => Set[SyntacticCategory]): ActionPredicate[MR, FV, C] =
    context => action => {
      val rootSpan = Span(0, action.syntax.sentence.size - 1)
      if (action.syntax.span == rootSpan) {
        categories(context).contains(action.syntax.synCat)
      }
      else {
        true
      }
    }

}
