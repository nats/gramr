package gramr
package split

import text._
import ccg._
import ccg.StoredLexicon.SplitLexicalItem
import gramr.graph.Graph

import Graph.ops._

/** Represents a part of a meaning representation that corresponds to a particular span in a sentence.
  */
case class PartialMR[MR](
  mr: MR,
  span: Span,
  synCats: Set[SyntacticCategory]
) {

  def matchesSynCat(synCat: SyntacticCategory): Boolean = synCats.exists(_ matches synCat)

}

object PartialMR {

  def lexicalItems[MR : Graph](partialMR: PartialMR[MR], sentence: Vector[Token]): Iterable[SplitLexicalItem[MR]] = {
    val PartialMR(mr, span, synCats) = partialMR
    for(synCat <- synCats) yield SplitLexicalItem(span.in(sentence), synCat, mr.clearAttributes, span)
  }

}
