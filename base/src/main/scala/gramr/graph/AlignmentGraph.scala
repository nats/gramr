package gramr.graph

import gramr.text._

import Graph.ops._

// Graphs with alignments

object AlignmentGraph {

  object Implicits {

    import scala.language.implicitConversions

    implicit def toAlignmentGraph[T : Graph](g: T): AlignmentGraph[T] =
      AlignmentGraph(g)

  }

  def getElementAlignments(
    el: Element,
    attrName: String = "alignments"
  ): Set[Int] = (for {
    attr <- el.attributes.getOption[AlignmentsAttribute](attrName)
  } yield attr.edges).getOrElse(Set.empty)

}


/**
  * A decorator for MeaningGraph that provides methods for working with JAMR-style alignments.
  */
case class AlignmentGraph[T : Graph](g: T) {


  val meaningGraphLike: Graph[T] = implicitly[Graph[T]]

  def alignmentsFor(p: Int): Set[Int] = {
    g.elementAttributes(p).getOption[AlignmentsAttribute]("alignments").map(_.edges).getOrElse(Set.empty)
  }

  def hasAlignmentsFor(p: Int): Boolean = alignmentsFor(p).nonEmpty

  def annotateAlignedTokens(
    snt: IndexedSeq[String],
    alignmentAttr: String = "alignments",
    attr: String = "alignedTokens"
  ): T = {
    g.mapAttributes(
      { case Indexed(el, _) =>
        if (el.attributes.contains(alignmentAttr))
          el.attributes + (attr -> AlignedTokensAttribute.from(
            el.attributes.get[AlignmentsAttribute](alignmentAttr).edges, snt
          ))
        else
          el.attributes
      }
    )
  }

  def limitAlignments(span: Span): T = g.mapAttributes { case Indexed(e, pointer) =>
    e.attributes.getOption[AlignmentsAttribute]("alignments") match {
      case Some(attr) => e.attributes + ("alignments" -> attr.copy(edges = attr.edges.filter(span.contains)))
      case None => e.attributes
    }
  }

  /**
    * Returns a histogram of the number of alignment edges per node.
    */
  def countNodeAlignments: Map[Int, Int] =
    g.nodes.map(_.element.attributes.get[AlignmentsAttribute]("alignments").edges.size).groupBy(x => x).mapValues(
      _.size
    )

  def countEdgeAlignments: Map[Int, Int] =
    g.edges.map(_.element.attributes.get[AlignmentsAttribute]("alignments").edges.size).groupBy(x => x).mapValues(
      _.size
    )

  /**
    * Applies a function to every alignment edge.
    */
  def adjustAlignments(f: Int => Int): T = g.mapAttributes { case Indexed(e, pointer) =>
    e.attributes.getOption[AlignmentsAttribute]("alignments") match {
      case Some(attr) => e.attributes + ("alignments" -> attr.copy(edges = attr.edges.map(f)))
      case None => e.attributes
    }
  }

  def clipAlignments(r: Range): T = g.mapAttributes { case Indexed(el, pointer) =>
    el.attributes.getOption[AlignmentsAttribute]("alignments") match {
      case Some(attr) => el.attributes + ("alignments" -> attr.copy(edges = attr.edges.filter(r.contains)))
      case None => el.attributes
    }
  }

}


// AlignmentsAttribute

case class AlignmentsAttribute(edges: Set[Int]) extends Attribute


// AlignedTokensAttribute

case class AlignedTokensAttribute(tokens: List[String]) extends Attribute {

  import scalatags.Text.all._
  import scalatags.Text.TypedTag

  override def mangleEdgeHtml(html: TypedTag[String]): TypedTag[String] = span(html, sup(tokens.mkString(", ")))

  override def mangleNodeHtml(html: TypedTag[String]): TypedTag[String] = span(html, sup(tokens.mkString(", ")))

}

object AlignedTokensAttribute {

  def from(indices: Set[Int], sentence: IndexedSeq[String]): AlignedTokensAttribute = try {
    AlignedTokensAttribute(
      indices.toList.sorted.flatMap { i =>
        if (i >= 0 && i < sentence.size)
          List(sentence(i))
        else
          List()
      }
    )
  }
  catch {
    case e: ArrayIndexOutOfBoundsException => throw new Error(s"Invalid indices $indices in sentence $sentence")
  }

}
