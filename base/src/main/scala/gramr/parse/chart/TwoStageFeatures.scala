package gramr.parse.chart

import gramr.learn.FeatureVector

case class TwoStageFeatures[FV](
  priorityFeatures: FV,
  priority: Double,
  features: FV,
  score: Double
)

object TwoStageFeatures {

  implicit def maxScoring[FV : FeatureVector]: ChartLabel[TwoStageFeatures[FV]] = new ChartLabel[TwoStageFeatures[FV]] {

    import FeatureVector.Syntax._

    override def score(l: TwoStageFeatures[FV]): Double = l.score

    override def zero: TwoStageFeatures[FV] =
      TwoStageFeatures(
        implicitly[FeatureVector[FV]].empty,
        0.0,
        implicitly[FeatureVector[FV]].empty,
        0.0
      )

    override def add(l: TwoStageFeatures[FV], r: TwoStageFeatures[FV]): TwoStageFeatures[FV] = {
      TwoStageFeatures(
        l.priorityFeatures + r.priorityFeatures,
        l.priority + r.priority,
        l.features + r.features,
        l.score + r.score
      )
    }

    override def subtract(l: TwoStageFeatures[FV], r: TwoStageFeatures[FV]): TwoStageFeatures[FV] = {
      TwoStageFeatures(
        l.priorityFeatures - r.priorityFeatures,
        l.priority - r.priority,
        l.features - r.features,
        l.score - r.score
      )
    }

    override def mul(l: TwoStageFeatures[FV], r: TwoStageFeatures[FV]): TwoStageFeatures[FV] = {
      if(r.score > l.score) r else l
    }

    override def inheritablePortion(l: TwoStageFeatures[FV]): TwoStageFeatures[FV] = l

  }

  implicit def hasFeatures[FV: FeatureVector]: HasFeatures[FV, TwoStageFeatures[FV]] = (l: TwoStageFeatures[FV]) => l.features

  implicit def orderByScore[FV : FeatureVector]: Ordering[TwoStageFeatures[FV]] =
    (x: TwoStageFeatures[FV], y: TwoStageFeatures[FV]) => x.score.compareTo(y.score)


}
