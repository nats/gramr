package gramr.util

import org.scalatest.FlatSpec
import org.scalatest.prop.PropertyChecks

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random

class SparseVectorSpec extends FlatSpec with PropertyChecks {

  val v1: SparseVector = SparseVector(Map(1 -> 1.0, 2 -> 2.0))
  val v2: SparseVector = SparseVector(Map(2 -> 5.0, 4 -> 6.0))

  def mkLargeVector(size: Int): SparseVector = {
    val indices = (1 to 10000000).toArray
    val values = indices.map(_ => Random.nextDouble())
    SparseVector(indices, values)
  }

  "SparseVector" should "support retrieval of elements" in {
    assert(v1(1) == 1.0)
    assert(v1(2) == 2.0)
  }

  it should "give 0 for not-contained indices" in {
    assert(v1(-1) == 0.0)
    assert(v1(999) == 0.0)
  }

  it should "fail when constructed with negative values" in {
    assertThrows[AssertionError] {
      SparseVector(Map(1 -> -1.0))
    }
  }

  it should "support mapping" in {
    val mapped = v1.map { v => v * 2.0 }
    assert(mapped(1) == 2.0)
    assert(mapped(2) == 4.0)
  }

  it should "support scaling" in {
    val scaled = v1.scale(0.5)
    assert(scaled(1) == 0.5)
    assert(scaled(2) == 1.0)
  }

  it should "support normalization" in {
    val norm = v1.normalize
    assert(norm(1) == 1.0 / 3.0)
    assert(norm(2) == 2.0 / 3.0)
  }

  it should "remain at zero if values are zero" in {
    val zeroes = SparseVector(Map(1 -> 0.0, 2 -> 0.0))
    val norm = zeroes.normalize
    assert(norm(1) == 0.0)
    assert(norm(2) == 0.0)
  }

  "SparseVector addition" should "add two vectors together" in {
    val added = SparseVector.add(v2, v1)
    assert(added(1) == 1.0)
    assert(added(2) == 7.0)
    assert(added(3) == 0.0)
    assert(added(4) == 6.0)
    val added2 = SparseVector.add(v1, v2)
    assert(added2(1) == 1.0)
    assert(added2(2) == 7.0)
    assert(added2(3) == 0.0)
    assert(added2(4) == 6.0)
  }

  it should "leave the vector unmodified when adding an empty vector" in {
    val added = SparseVector.add(v1, SparseVector.empty)
    assert(added(1) == 1.0)
    assert(added(2) == 2.0)
  }

  it should "be fast when adding a small vector to a large one" in {
    val large = mkLargeVector(10000000)
    val small = mkLargeVector(1000)
    val f = Future {
      SparseVector.add(large, small)
    }
    Await.result(f, 5.seconds)
  }

}
