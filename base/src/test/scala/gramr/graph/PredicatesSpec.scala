package gramr.graph

import org.scalatest._

class PredicatesSpec extends FlatSpec {

  import gramr.graph._

  val mgl: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance

  implicit def meaningGraphLike: Graph[AdjacencyMeaningGraph] = mgl

  import mgl.Building._
  import SubGraphOps.Implicits._

  import Predicates._

  "Polarity edge checking" should "detect incorrect polarity edges" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkConstantNode("something")
        _ <- mkEdge("polarity", a, b)
      } yield ()
    }

    assert(!checkPolarityEdges(g1))

    val g2 = AdjacencyMeaningGraph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkConstantNode("-")
        _ <- mkEdge("polarity", a, b)
      } yield ()
    }

    assert(checkPolarityEdges(g2))

  }

  it should "allow edges to corefs and external nodes" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("polarity", a, b)
      } yield ()
    }

    assert(checkPolarityEdges(g1))

    val g2 = AdjacencyMeaningGraph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("<coref>")
        _ <- mkEdge("polarity", a, b)
      } yield ()
    }

    assert(checkPolarityEdges(g2))

  }

  "Constant node degree checking" should "detect constant nodes with degree > 1" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkConstantNode("-")
        _ <- mkEdge("polarity", a, b)
      } yield ()
    }

    assert(checkConstantDegrees(g1))

    val g2 = AdjacencyMeaningGraph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkConstantNode("-")
        c <- mkReferentNode("c")
        _ <- mkEdge("polarity", a, b)
        _ <- mkEdge("polarity-of", b, c)
      } yield ()
    }

    assert(!checkConstantDegrees(g2))
  }

  it should "allow single constant node graphs" in {


    val g1 = AdjacencyMeaningGraph.build {
      for {
        _ <- mkConstantNode("-")
      } yield ()
    }

    assert(checkConstantDegrees(g1))

  }

  "ARG edge checking" should "detect ARG edges whose parents have no sense tag" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        v <- mkReferentNode("verb-01")
        r <- mkReferentNode("agent")
        _ <- mkEdge("ARG0", v, r)
      } yield ()
    }

    assert(checkArgEdges(g1))

    val g2 = AdjacencyMeaningGraph.build {
      for {
        v <- mkReferentNode("verb")
        r <- mkReferentNode("agent")
        _ <- mkEdge("ARG0", v, r)
      } yield ()
    }

    assert(!checkArgEdges(g2))

  }

  it should "allow coref and external node parents" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        v <- mkReferentNode("<coref>")
        r <- mkReferentNode("agent")
        _ <- mkEdge("ARG0", v, r)
      } yield ()
    }

    assert(checkArgEdges(g1))

    val g2 = AdjacencyMeaningGraph.build {
      for {
        v <- mkExternalNode(ExternalReference(0))
        r <- mkReferentNode("agent")
        _ <- mkEdge("ARG0", v, r)
      } yield ()
    }

    assert(checkArgEdges(g2))

  }

  "Numbered edge uniqueness checking" should "allow uniquely labelled graphs" in {
    val g1 = AdjacencyMeaningGraph.build {
      for {
        v <- mkReferentNode("a")
        r <- mkReferentNode("b")
        _ <- mkEdge("op1", v, r)
        _ <- mkEdge("op2", v, r)
        _ <- mkEdge("op3", v, r)
      } yield ()
    }

    assert(checkNumberedEdgesUnique(g1))

    val g2 = AdjacencyMeaningGraph.build {
      for {
        v <- mkReferentNode("a")
        r <- mkReferentNode("b")
        _ <- mkEdge("op2", v, r)
        _ <- mkEdge("op3", v, r)
      } yield ()
    }

    assert(checkNumberedEdgesUnique(g2))
  }

  it should "reject non-uniquely labelled graphs" in {
    val g1 = AdjacencyMeaningGraph.build {
      for {
        v <- mkReferentNode("a")
        r <- mkReferentNode("b")
        _ <- mkEdge("op1", v, r)
        _ <- mkEdge("op1", v, r)
      } yield ()
    }
    assert(!checkNumberedEdgesUnique(g1))
  }

  "Instance concept checking" should "allow usual concepts" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        _ <- mkReferentNode("send-01")
      } yield ()
    }
    assert(checkInstanceConcepts(g1))

    val g2 = AdjacencyMeaningGraph.build {
      for {
        _ <- mkReferentNode("plan")
      } yield ()
    }
    assert(checkInstanceConcepts(g2))

    val g3 = AdjacencyMeaningGraph.build {
      for {
        _ <- mkReferentNode("date-entity")
      } yield ()
    }
    assert(checkInstanceConcepts(g3))
  }

  it should "allow corefs" in {

    val g1 = AdjacencyMeaningGraph.build {
      for {
        _ <- mkReferentNode("<coref>")
      } yield ()
    }
    assert(checkInstanceConcepts(g1))

  }

  it should "reject concepts that don't start with a lowercase letter" in {
    val g1 = AdjacencyMeaningGraph.build {
      for {
        _ <- mkReferentNode("\"Quoted\"")
      } yield ()
    }
    assert(!checkInstanceConcepts(g1))

    val g2 = AdjacencyMeaningGraph.build {
      for {
        _ <- mkReferentNode("Uppercase")
      } yield ()
    }
    assert(!checkInstanceConcepts(g2))
  }

}
