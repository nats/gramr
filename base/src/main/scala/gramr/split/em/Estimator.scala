package gramr.split.em

import com.typesafe.scalalogging.LazyLogging
import gramr.ccg.{Derivation, StoredLexicon}
import gramr.graph.Graph
import gramr.split.SentenceSplittingAlgorithm
import gramr.text.Example

abstract class Estimator[MR : Graph, E: Example, M, P](
  exampleDerivationPairs: Iterable[(E, (String, Derivation[Unit]))],
  lexicon: StoredLexicon[MR],
  sentenceSplitter: SentenceSplittingAlgorithm[MR, E],
  iterations: Int,
  adjustCounts: P => P
)(implicit val emModel: EMModel[M, MR, P]) extends LazyLogging {

  def createModel: M

  def estimate: M = {

    var model = createModel

    (1 to iterations) foreach { i =>
      logger.info(s"EM iteration $i/$iterations")

      val iteration = new Iteration[MR, E, M, P](
        model,
        lexicon,
        sentenceSplitter,
        adjustCounts = adjustCounts
      )

      model = iteration.estimate(exampleDerivationPairs)

    }

    model
  }

}
