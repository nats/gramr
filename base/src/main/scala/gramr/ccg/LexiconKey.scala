package gramr.ccg

import gramr.text.Token

/**
  * A lexical item without the meaning, but put into its own class for convenience
  */
case class LexiconKey(
  tokens: Vector[Token]
) extends Ordered[LexiconKey] {

  def compare(other: LexiconKey): Int = {
    val tokensCmp = gramr.util.StreamUtils.lexicographicOrdering[String].compare(
      tokens.toStream.map(_.text), other.tokens.toStream.map(_.text)
    )
    tokensCmp
  }

}

object LexiconKey {

  def fromDerivationStep(step: DerivationStep[_]): LexiconKey = LexiconKey(step.tokens)

}
