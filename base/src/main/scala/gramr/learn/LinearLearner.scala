package gramr.learn

trait LinearLearner[MR, FV, E] {

  /** Updates the parameters directly according to a gradient vector.
    *
    * This still takes learning rates etc. into account.
    *
    * @param gradients the gradients to update towards
    */
  def updateGradients(gradients: Iterable[FV]): Unit

  /** Applies several updates.
    *
    * @param updates the updates to apply
    */
  def updateBatch(updates: Iterable[Update[FV]]): Unit = {
    updateGradients(updates.map(_.gradient))
  }

  /** Returns the model to use for decoding during training.
    */
  def trainModel: LinearModel

  /** Returns the model to use for decoding during testing.
    *
    * This differs from the trainModel e.g. in the case of the averaged perceptron.
    */
  def testModel: LinearModel

}
