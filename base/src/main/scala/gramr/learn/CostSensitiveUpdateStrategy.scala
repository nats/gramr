package gramr.learn

import com.typesafe.scalalogging.LazyLogging
import gramr.parse.chart.{Chart, State}
import gramr.text.Example
import Example.ops._
import FeatureVector.Syntax._

/** Implements a loss-sensitive perceptron, closely following Singh-Miller and Collins,
  * Trigger-Based Language Modeling Using A Loss-Sensitive Perceptron Algorithm.
  */
class CostSensitiveUpdateStrategy[MR, FV, E: Example](
  val loss: (State[MR, FV], E) => Double,
  // how good does the best update need to be to make a positive update?
  val maxLoss: Double = Double.MaxValue
)(
  implicit
  val fv: FeatureVector[FV]
) extends UpdateStrategy[MR, FV, E] with LazyLogging {

  def feat(state: State[MR, FV]): FV = state.features

  override def compute(example: E, charts: Map[String, Chart[MR, FV]]): Iterable[CostSensitiveUpdate[FV]] = {
    val rootStates = charts.values.flatMap(_.outputStates).toVector

    if (rootStates.nonEmpty) {
      val lossStates = rootStates.map(s => (s, loss(s, example)))
      // The lowest loss achieved by any state.
      val lowestLoss = lossStates.map(_._2).min

      if(lowestLoss <= maxLoss) {
        // Define a state’s relative loss.
        def delta(ls: (State[MR, FV], Double)): Double = ls._2 - lowestLoss

        // Separate the states into g (optimal loss) and b (non-optimal loss) states.
        val (gStates, bStates) = lossStates.partition { s => delta(s) < 0.01 }
        // Get the margin-violating states
        val cStates = gStates.filter { c => bStates.exists { z => c._1.score - z._1.score < delta(z) } }
        val eStates = bStates.filter { e => gStates.exists { y => y._1.score - e._1.score < delta(e) } }

        if (cStates.nonEmpty) {
          // Define the weights for each state
          // c states are all weighted equally
          val cWeight = 1.0 / cStates.size.toDouble

          // e states are weighted according to the number of c states they are too close to
          def violates(c: (State[MR, FV], Double), e: (State[MR, FV], Double)) =
            if (c._1.score - e._1.score < delta(e)) 1.0 else 0.0

          def totalViolations(c: (State[MR, FV], Double)) = eStates.map(e => violates(c, e)).sum

          def eWeight(e: (State[MR, FV], Double)): Double = cStates.map(
            c => violates(c, e) / (cStates.size * totalViolations(c))
          ).sum

          val positiveUpdate = cStates.map(c => feat(c._1) * cWeight).foldLeft(fv.empty)(_ + _)
          val negativeUpdate = eStates.map(e => feat(e._1) * eWeight(e)).foldLeft(fv.empty)(_ + _)

          logger.trace(s"${example.id}: Update (lowest loss: $lowestLoss)")
          logger.trace(s"Positive update: $positiveUpdate")
          logger.trace(s"Negative update $negativeUpdate")

          Iterable(CostSensitiveUpdate(positiveUpdate, negativeUpdate))
        }
        else {
          logger.trace(s"${example.id}: No update because no margin-violating states were found")
          Iterable()
        }
      }
      else {
        logger.trace(s"${example.id}: No update due to high loss: $lowestLoss")
        Iterable()
      }
    }
    else {
      logger.trace(s"${example.id}: No update because no parses were found")
      Iterable()
    }
  }

}
