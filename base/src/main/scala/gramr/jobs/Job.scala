package gramr.jobs

import com.typesafe.scalalogging.LazyLogging
import monix.execution.Scheduler
import monix.reactive.Observable

import scala.concurrent.duration.Duration

class Job[A, B](
  val input: Seq[A],
  val stage: Stage[A, B],
  val name: String = "job"
)(implicit val scheduler: Scheduler) extends LazyLogging {

  def append[C](next: Stage[B, C]): Job[A, C] = new Job(input, ChainedStage(stage, next), name)

  def size: Int = input.size

  def run(): Unit = {
    logger.trace(s"Starting iteration job $name")
    stage.message(_.beginIteration())
    runBatch(input)
    stage.message(_.finishIteration())
    logger.trace(s"Finished iteration job $name")
  }

  def runBatched(batchSize: Int): Unit = {
    logger.trace(s"Starting minibatch job $name")
    stage.message(_.beginIteration())
    val batches = input.sliding(batchSize, batchSize)
    batches.foreach { batch =>
      runBatch(batch)
    }
    stage.message(_.finishIteration())
    logger.trace(s"Finished minibatch job $name")
  }

  def runBatch(batch: Seq[A]): Unit = {
    logger.trace(s"Starting batch of size ${batch.size} for job $name")
    stage.message(_.beginBatch())
    stage.observable(Observable.fromIterable(batch))
      .onErrorHandle { error: Throwable =>
        logger.error(error.toString)
      }
      .completedL.runSyncUnsafe(Duration.Inf)
    stage.message(_.finishBatch())
    logger.trace(s"Finished batch of size ${batch.size} for job $name")
  }

  def listenToItems(f: B => Unit): Job[A, B] = {
    stage.addListener(Listener.item(f))
    this
  }

  def listenToBatch(f: Seq[B] => Unit): Job[A, B] = {
    stage.addListener(Listener.batch(f))
    this
  }

  def listenToIteration(f: Seq[B] => Unit): Job[A, B] = {
    stage.addListener(Listener.iteration(f))
    this
  }

  def onBeginIteration(f: () => Unit): Job[A, B] = {
    stage.addListener(Listener.onBeginIteration(f))
    this
  }

  def onFinishIteration(f: () => Unit): Job[A, B] = {
    stage.addListener(Listener.onFinishIteration(f))
    this
  }

  def addListener(listener: Listener[B]): Job[A, B] = {
    stage.addListener(listener)
    this
  }

}

object Job {

  import monix.execution.Scheduler.Implicits.global

  def apply[A](input: Seq[A], name: String = "job"): Job[A, A] = {
    new Job(input, ComputeStage(a => a), name)
  }

}
