package gramr.parse

import com.typesafe.scalalogging.LazyLogging
import gramr.ccg.{CombinatorSystem, SyntacticCategory}
import gramr.graph.Graph
import gramr.learn.FeatureVector
import gramr.parse.chart._
import gramr.text.{Span, Token}

import scala.collection.mutable.ArrayBuffer

/** Implements a shift-reduce algorithm for AMR/CCG parsing.
  *
  * Only supports lexical decisions for single tokens!
  *
  * @param lexicalChoice the lexical choice
  * @param combinatoryChoice the combinatory choice
  * @param combinatorSystem combinator system within which inference takes place
  * @param decisionFeatures extractor for features of a parser decision
  * @param stateFeatures extractor for features of a meaning representation
  * @param modelScore scoring function for feature vectors
  * @param actionFilter filter for allowed parser decisions
  * @param leafCategories allowed leaf syncats for every span
  * @param beamSize beam size
  * @param bindingBeamSize number of decisions to consider per combinatory action
  * @param limitLexicalActions whether shift actions should be exempt from beam size limitations
  * @param limitUnaryActions whether unary actions should be exempt from beam size limitations
  * @tparam MR type of meaning representation
  * @tparam F type of feature vector
  * @tparam C type of example context
  */
class ShiftReduceParser[MR : Graph, F : FeatureVector, C](
  val lexicalChoice: LexicalChoice[C, MR],
  val combinatoryChoice: CombinatoryChoice[MR],
  val combinatorSystem: CombinatorSystem,
  val decisionFeatures: (Decision[MR], C) => F,
  val stateFeatures: (MR, C) => F,
  val modelScore: F => Double,
  val actionFilter: ActionPredicate[MR, F, C],
  val leafCategories: (C, Span) => Iterable[SyntacticCategory],
  val beamSize: Int,
  val bindingBeamSize: Int,
  val limitLexicalActions: Boolean = true,
  val limitUnaryActions: Boolean = true
) extends ChartParser[MR, F, InheritableLabel[ScoredFeatures[F]], C] with LazyLogging {

  type L = ScoredFeatures[F]

  implicit val lcl: ChartLabel[L] = ScoredFeatures.maxScoring[F]
  implicit val lhf: HasFeatures[F, L] = ScoredFeatures.hasFeatures[F]
  implicit val lo: Ordering[L] = ScoredFeatures.orderByScore[F]

  override def featureVector: FeatureVector[F] = implicitly[FeatureVector[F]]
  override def chartLabel: ChartLabel[InheritableLabel[L]] = InheritableLabel.chartLabel
  override def hasFeatures: HasFeatures[F, InheritableLabel[L]] = InheritableLabel.hasFeatures[F, L]
  override def labelOrdering: Ordering[InheritableLabel[L]] = InheritableLabel.ordering[L]

  override def label(decision: Decision[MR], span: Span, context: C): InheritableLabel[ScoredFeatures[F]] = {
    val inheritable = decisionFeatures(decision, context)
    val local = stateFeatures(decision.result, context)
    InheritableLabel(
      local = ScoredFeatures(local, modelScore(local)),
      inheritable = ScoredFeatures(inheritable, modelScore(inheritable))
    )
  }

  override def parse(sentence: Vector[Token], context: C): Chart = {
    val lexicalChoiceInstance = lexicalChoice(context)
    val filter = actionFilter(context)

    // For each index, contains the parser states that are predecessors for this index.
    val predecessors = Array.fill(sentence.length)(ArrayBuffer.empty[ParserState])

    val chart = Chart.empty(sentence)


    def reduce(state: ParserState): Iterable[ParserState] = {
      val converted = reduceUnary(state)

      ???
    }

    def reduceUnary(parserState: ParserState): Iterable[ParserState] = {
      val List(state) = parserState.top(1)

      ???
    }


    sentence.indices foreach { i =>
      val token = sentence(i)
      val span = Span(i, i)
      val synCats = leafCategories(context, span).toVector
      val syn = synCats.map { synCat => SyntaxInformation(sentence, span, synCat) }

      val lexical = syn.flatMap(lexicalChoiceInstance)
      val lexicalStates = lexical.flatMap { decision =>
        // First perform filtering on an unlabelled action, to save on feature extraction
        val unlabelledAction = Action.unlabelled(decision, decision.syntax, Vector.empty)
        if (filter(unlabelledAction)) {
          val action = Action.label(unlabelledAction, label(decision, decision.syntax.span, context))
          val state = chart.putAction(action)
          Some(state)
        }
        else {
          None
        }
      }
      predecessors(i) ++= lexicalStates.map(ParserState.lexical)

      val reducible = predecessors(i).toVector
      val reduced = reducible.flatMap(reduce)

    }

    ???
  }

  case class ParserState(
    stack: List[State]
  ) {

    def top(n: Int): List[State] = stack.take(n)

    def shift(decision: LexicalDecision[MR]): ParserState = {
      ???
    }

    def unary(decision: UnaryDecision[MR]): ParserState = {
      ???
    }

    def binary(decision: CombinatoryDecision[MR]): ParserState = {
      ???
    }

  }

  object ParserState {

    def lexical(state: State): ParserState = ParserState(List(state))

  }

}
