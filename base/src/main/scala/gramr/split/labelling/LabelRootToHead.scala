package gramr
package split
package labelling

import gramr.graph._
import ccg._
import Graph.ops._
import gramr.graph.edit.{SplitLeft, SplitRight}

/** Labels the root node of the graph to belong to the (syntactic) head side of the split.
  */
case class LabelRootToHead[MR : Graph]() extends DeterministicLabellingFunction[MR] {

  def labelDeterministic(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): PartialMR[MR] = {
    import gramr.graph.Indexed
    val PartialMR(mr, span, synCat) = partialMR
    val headIsLeft = step.combinator.headIndex == 0
    val direction = if (headIsLeft) SplitLeft else SplitRight
    val labelledMR = mr.mapNodeAttributes { case Indexed(node, pointer) =>
      if (pointer == mr.root) {
        node.attributes + ("splitDirection" -> direction)
      }
      else {
        node.attributes
      }
    }
    PartialMR(labelledMR, span, synCat)
  }

}
