package gramr.graph

import gramr.ccg.{DelexedLexicon, PlainLexicalItem, StoredLexicon}
import gramr.text.Token

package object lexemes {

  def delexicalizeLexicon[MR : Graph](
    lexicon: StoredLexicon[MR],
    maxLexNodes: Int,
    wildcardFillers: Iterable[WildcardFiller] = Iterable.empty
  ): DelexedLexicon[MR] = {
    val lexiconBuider = new StoredLexicon.Builder[MR](lexicon.keyExtractor)
    val lexemeBuilder = new LexemeDictionary.Builder

    val (lexemes, templates) = (for {
      (key, items) <- lexicon.storedItems.toVector
      item <- items
      (lexeme, template) <- Lexeme.delexicalize(maxLexNodes)(key.tokens, item.meaning)
      // tokenPlaceholder = Vector.fill(item.tokens.size)(Token("*", "*"))
      tokenPlaceholder = Vector(Token("*", "*"))
    } yield (lexeme, PlainLexicalItem(tokenPlaceholder, item.synCat, template))).unzip

    templates.foreach(lexiconBuider.add)
    wildcardFillers.foreach(lexemeBuilder.add)
    lexemes.foreach(lexemeBuilder.add)

    new DelexedLexicon[MR](lexiconBuider.build, lexemeBuilder.build)
  }

}