package gramr.util

object StreamUtils {

  def lexicographicOrdering[A: Ordering]: Ordering[Stream[A]] = new Ordering[Stream[A]] {
    def compare(x: Stream[A], y: Stream[A]): Int = {
      def doCompare(x: Stream[A], y: Stream[A]): Int = (x, y) match {
        case (hx #:: rx, hy #:: ry) =>
          val hcompare = implicitly[Ordering[A]].compare(hx, hy)
          if (hcompare == 0) doCompare(rx, ry)
          else hcompare
        case (Stream.Empty, hy #:: ry) => -1
        case (hx #:: rx, Stream.Empty) => 1
        case (Stream.Empty, Stream.Empty) => 0
      }

      doCompare(x, y)
    }
  }

  def mergeSortedStreams[A: Ordering](streams: Vector[Stream[A]]): Stream[A] = {
    val nonEmptyStreams = streams.filter(_.nonEmpty)
    if (nonEmptyStreams.isEmpty) Stream.Empty
    else {
      val bestStream = nonEmptyStreams.indices.toSeq.minBy(nonEmptyStreams(_).head)
      val bestHead #:: bestTail = nonEmptyStreams(bestStream)
      bestHead #:: mergeSortedStreams(streams.updated(bestStream, bestTail))
    }
  }

  /**
    * For some funny reason, Stream’s zipAll is non-streaming… so I wrote my own lazy version.
    */
  def zipAll[A, B](l: Stream[A], r: Stream[B], ldefault: => A, rdefault: => B): Stream[(A, B)] = (l, r) match {
    case (lhead #:: lrest, rhead #:: rrest) => (lhead, rhead) #:: zipAll(lrest, rrest, ldefault, rdefault)
    case (Stream.Empty, rhead #:: rrest) => (ldefault, rhead) #:: zipAll(Stream.Empty, rrest, ldefault, rdefault)
    case (lhead #:: lrest, Stream.Empty) => (lhead, rdefault) #:: zipAll(lrest, Stream.Empty, ldefault, rdefault)
    case (Stream.Empty, Stream.Empty) => Stream.Empty
  }
}
