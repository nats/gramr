package gramr
package split
package chartFilters

import ccg._

/** Selects all split items that belong to a “lowest cross-section” of the split chart with
  * one of the derivations.
  */
case class LowestCrossSection[MR](derivations: Seq[Derivation[Unit]], onlyOne: Boolean = true) extends SplitChartFilter[MR] {

  def instantiate(splitChart: SplitChart[MR]): SplitChartFilter.ItemFilter[MR] = new SplitChartFilter.ItemFilter[MR] {

    val crossSections: Seq[Vector[DerivationStep[Set[PartialMR[MR]]]]] = derivations.map { derivation => splitChart.lowestCoveredCrossSection(derivation) }
    //val lowestCrossSectionLength: Int = if(crossSections.nonEmpty) crossSections.map(_.length).max else 0
    //val lowestCrossSection: Option[Vector[DerivationStep[Set[PartialMR[MR]]]]] = crossSections.find(_.length == lowestCrossSectionLength)

    def apply(item: PartialMR[MR]): Boolean = {
      crossSections.exists { crossSection =>
        crossSection.exists { step => step.span == item.span && item.matchesSynCat(step.synCat) }
      }
    }

  }

}
