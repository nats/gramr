package gramr.graph
package presentation

import scalatags.Text.all._
import Graph.ops._
import gramr.util.html.ToHtml
import scalatags.Text

class GraphToHtml[G : Graph] extends ToHtml[G] {

  override def toHtml(graph: G): Text.all.Frag = {
    import AmrTreeConversion._

    val indentSize = 8

    def indent(indentation: Int): Frag = " " * (indentSize * indentation)

    def break: Frag = "\n"

    def mangleNode(n: Node, x: List[Frag]): List[Frag] = List(n.attributes.mangleNodeHtml(span(x)))

    def mangleEdge(e: Edge, flip: Boolean): Frag = {
      val label = if(flip) e.flip.label else e.label
      List(e.attributes.mangleEdgeHtml(span(stringFrag(":" + label))), stringFrag(" "))
    }

    def sourceNodeHtml(n: Indexed[Node]): List[Frag] = {
      n.element match {
        case rn: ReferentNode => mangleNode(n.element, List(graph.nodeName(n.pointer).toString, " / ", rn.concept))
        case cn: ConstantNode => mangleNode(n.element, List(cn.name))
        case en: ExternalNode => mangleNode(n.element, List(en.reference.toString))
      }
    }

    def parseNode(walkTree: VisitNode, indentation: Int): Frag = {
      val node = graph.node(walkTree.nodePointer)
      if(walkTree.firstTime) {
        val subTrees = walkTree.subTrees.map(st => parseLink(st, indentation))
        val html: Seq[Frag] = Seq(
          "(",
          sourceNodeHtml(node),
          subTrees.flatMap { e => break :: indent(indentation + 1) :: e },
          ")"
        )
        html
      }
      else {
        val html = node.element match {
          case rn: ReferentNode => List(graph.nodeName(node.pointer).toString) // not mangled to reduce clutter
          case en: ExternalNode => List(en.reference.toString)
          case cn: ConstantNode => List(cn.content)
        }
        html
      }
    }

    def parseLink(walkTree: VisitEdge, indentation: Int): List[Frag] = {
      val edge = graph.edge(walkTree.edgePointer).element
      val tree = parseNode(walkTree.subTree, indentation + 1)
      val flip = walkTree.subTree.nodePointer != edge.target
      val link = List(mangleEdge(edge, flip), tree)
      link
    }

    if(!graph.isEmpty) {
      div(whiteSpace := "pre", parseNode(graph.walkTree(graph.root), 0))
    }
    else {
      i("empty")
    }
  }

}