package gramr.ccg.presentation

import java.io.File

import gramr.ccg.{Derivation, DerivationStep}
import gramr.util.html.ToHtml
import scalatags.Text.all._

/** Outputs a derivation as HTML, adding a section below the derivation itself for every derivation step.
 *
 * This can be used e.g. for outputting parse charts, where every derivation step has a number of associated labels.
 * Must be extended with concrete implementations that define outputting the derivation steps.
 */
trait DerivationWithStepsToHtml[T] {

  def derivation: Derivation[T]

  def stepSubtitle(label: T): String

  def stepDisplay(step: DerivationStep[T]): List[Frag]

  def writeToFile(file: File): Unit = {
    import java.io.{BufferedWriter, FileWriter}
    implicit val intToHtml: ToHtml[Frag] = new ToHtml[Frag] {
      def toHtml(t: Frag): Frag = t
    }
    val derivationToHtml = new DerivationToHtml[Frag]

    var i = 0
    // crude internal link naming relies on map and steps order being the same
    val der = derivationToHtml.toHtml(
      derivation.map(
        { cells =>
          i += 1; span(a(href := s"#$i", stepSubtitle(cells)))
        }
      )
    )

    i = 0
    val steps = derivation.steps.flatMap { step =>
      i += 1
      Seq(
        h3(a(name := s"$i", s"${step.tokens.mkString(" ")} (${step.synCat})"))
      ) ++ stepDisplay(step)
    }

    val markup = html(body(der, steps))
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(markup.toString)
    bw.close()
  }

}

