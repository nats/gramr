package gramr.parse

import gramr.ccg.{Derivation, GeneratedLexicalItem}

/** A filter that only allows parses that are anchored in one of the given syntactic derivations.
  *
  * This means that every leaf (i.e. lexical step) must be represented with its span and
  * syntactic category in one of the derivations. Every intermediate node (i.e. combinatorial step)
  * must cover a span of leaves that is likewise found in one of the derivations (i.e., the
  * leaves covered must not occur in different derivations).
  */
object AnchoredSyntaxFilter {

  def apply[MR, F, C](generatedOnly: Boolean = false)(implicit ev: C => Seq[Derivation[Unit]]): ActionPredicate[MR, F, C] = {
    context => {

      val derivations: Seq[Derivation[Unit]] = ev(context)

      action => {
        if (action.predecessors.isEmpty) {
          // Check if it's a generated lexical item
          if (!generatedOnly || !action.decision.isInstanceOf[LexicalDecision[MR]] ||
            action.decision.asInstanceOf[LexicalDecision[MR]].item.isInstanceOf[GeneratedLexicalItem[MR]]) {
            derivations.exists { derivation =>
              findNodeInDerivation(derivation, action.syntax)
            }
          }
          else {
            true
          }
        }
        else {
          true
        }
      }
    }
  }

  def findNodeInDerivation(derivation: Derivation[Unit], syntax: SyntaxInformation): Boolean = {
    // TODO it would be more efficient to search through the derivation based on the span.
    derivation.steps.exists { step =>
      step.span == syntax.span && (step.synCat matches syntax.synCat)
    }
  }

}
