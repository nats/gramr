package gramr.parse.chart

import java.util

import gramr.learn.FeatureVector

import scala.collection.JavaConverters._
import gramr.parse.chart.trimming.TrimmingPredicate
import gramr.parse.{Decision, SyntaxInformation}
import gramr.text.{Span, Token}

import scala.collection.mutable.ArrayBuffer
import scala.language.reflectiveCalls

/** Implements the nuts and bolts of a chart-based parser.
  *
  * This trait is intended as a mixin that provides an implementation of chart parsing functionality to parsers.
  *
  * The trait implements the concrete classes Chart, Action, and State, which inherit from the corresponding
  * traits in gramr.parse.chart.
  *
  * To build a Parser class backed by a ChartParsing implementation, inherit from the ChartParser trait.
  *
  * @tparam MR type of meaning representation
  * @tparam FV type of feature vector
  * @tparam L type of chart label
  */
trait ChartParsing[MR, FV, L] {

  implicit def featureVector: FeatureVector[FV]
  implicit def chartLabel: ChartLabel[L]
  implicit def hasFeatures: HasFeatures[FV, L]
  implicit def labelOrdering: Ordering[L]

  import ChartLabel.{add, sum, product}

  class Action(
    val decision: Decision[MR],
    val localLabel: L,
    override val syntax: SyntaxInformation,
    override val predecessors: Vector[State]
  ) extends gramr.parse.chart.Action[MR, FV] {

    def mr: MR = decision.result

    def span: Span = syntax.span

    var label: L = add(
      localLabel,
      sum(predecessors.view.map { state =>
        ChartLabel.inheritablePortion(state.label)
      })
    )

    def updateLabel(): Unit = label = add(
      localLabel,
      sum(predecessors.view.map { state =>
        ChartLabel.inheritablePortion(state.label)
      })
    )

    override def score: Double = implicitly[ChartLabel[L]].score(label)

    override def features: FV = implicitly[HasFeatures[FV, L]].features(label)
  }

  object Action {

    /** Creates an unlabelled action.
      */
    def unlabelled(decision: Decision[MR], syntax: SyntaxInformation, predecessors: Vector[State]): Action =
      new Action(decision, chartLabel.zero, syntax, predecessors)

    /** Adds a local label to an unlabelled action.
      *
      * @param action an unlabelled action
      * @param label a local label to set for the action
      * @return the labelled action
      */
    def label(action: Action, label: L): Action =
      new Action(action.decision, label, action.syntax, action.predecessors)

  }

  class State(
    initialAction: Action
  ) extends gramr.parse.chart.State[MR, FV] {

    val mr: MR = initialAction.mr

    var syntax: SyntaxInformation = initialAction.syntax
    var actions: Vector[Action] = Vector(initialAction)

    var label: L = initialAction.label

    def updateLabel(): Unit = {
      label = product(actions.map(_.label))
      successors.keySet().asScala.foreach { succ =>
        succ.actions.foreach { _.updateLabel() }
        succ.updateLabel()
      }
    }

    // Indicates whether the state was dropped from the chart
    var dropped: Boolean = false

    override def score: Double = implicitly[ChartLabel[L]].score(label)

    override def features: FV = implicitly[HasFeatures[FV, L]].features(label)

    var successors: util.IdentityHashMap[State, Unit] = new util.IdentityHashMap[State, Unit]()

    def addAction(action: Action): Unit = {
      actions = actions :+ action
      updateLabel()
    }

    def drop(): Unit = {
      dropped = true
      // Remove the state from successor lists to allow it to be garbage collected
      actions.view.flatMap(_.predecessors).foreach { _.removeSuccessor(this) }
    }

    def addSuccessor(state: State): Unit = {
      successors.put(state, ())
    }

    def removeSuccessor(state: State): Unit = {
      successors.remove(state)
    }

  }

  class Cell(
    var states: Vector[State]
  ) {

    def add(action: Action): State = synchronized {
      // Check whether there is a state with this MR and syncat
      val state = states.find { state => state.mr == action.mr && state.syntax == action.syntax } match {
        case Some(existingState) =>
          // Add the action to the existing state
          existingState.addAction(action)
          existingState
        case _ =>
          // Create a new state for this MR
          val newState = new State(action)
          states = states :+ newState
          newState
      }
      action.predecessors.foreach { _.addSuccessor(state) }
      state
    }

    /** Enforces the beam size criterion, removing all states that are not among the n best.
      *
      * @return the states that were dropped from the beam
      */
    def enforce(capacity: Int): Vector[State] = synchronized {
      sortStates()
      val (kept, dropped) = states.splitAt(capacity)
      states = kept
      dropped.foreach { _.drop() }
      dropped
    }

    def sortStates(): Unit = synchronized {
      states = states.sortBy(state => -implicitly[ChartLabel[L]].score(state.label))
    }

    def clear(): Unit = synchronized {
      states = Vector.empty
    }

    def copy: Cell = new Cell(states)

    def trim(predicate: (Int, State) => Boolean): Unit = synchronized {
      states = for {
        (state, index) <- states.zipWithIndex if predicate(index, state)
      } yield state
    }

  }

  object Cell {

    def empty: Cell = new Cell(Vector.empty)

  }

  class Chart(
    val sentence: Vector[Token],
    private val chart: Array[Array[Cell]],
    val outputStates: ArrayBuffer[State]
  ) extends gramr.parse.chart.Chart[MR, FV] {

    def cell(span: Span): Cell = chart(span.start)(span.end)

    def root: Cell = chart(0)(sentence.size - 1)

    def states(span: Span): Iterable[State] = {
      cell(span).states
    }

    override def successors(state: gramr.parse.chart.State[MR, FV]): Iterable[State] =
      state.asInstanceOf[State].successors.asScala.keys

    /** The spans contained in the chart in an unspecified order.
      */
    def spans: Iterable[Span] = for {
      start <- sentence.indices
      end <- start until sentence.size
    } yield Span(start, end)

    def spansShortToLong: Iterable[Span] = for {
      length <- sentence.indices
      start <- 0 until sentence.length - length
      end = start + length
    } yield Span(start, end)

    def copy: Chart = new Chart(sentence, Array.tabulate(sentence.size, sentence.size) { (x, y) => chart(x)(y).copy }, ArrayBuffer(outputStates: _*))

    def putAction(action: Action): State = {
      val span = action.span
      chart(span.start)(span.end).add(action)
    }

    /** Trims the chart by removing all root states that don’t match a given predicate, as well as all their
      * predecessor states..
      *
      * Note: This only modifies the content of the chart’s cells. The underlying packed forest of states and actions
      * is not touched.
      */
    def trim(predicate: TrimmingPredicate[MR, FV]): Unit = {
      val predicateInstance = predicate.instantiate(this)
      for (Span(start, end) <- spans) {
        chart(start)(end).trim { (_, s) => predicateInstance(s) }
      }
    }

    /** Copies the chart, then trims and returns the copy.
      */
    def trimmed(predicate: TrimmingPredicate[MR, FV]): Chart = {
      val cpy = copy
      cpy.trim(predicate)
      cpy
    }

    /** Builds a copy of the chart that contains only the states validated by predicate – and only the
      * actions leading to those states. All states in the trimmed chart are copies of the original states
      * with possibly reduced action lists.
      *
      * @param predicate a trimming predicate which is only applied to the root states
      */
    def deepTrimmed(predicate: TrimmingPredicate[MR, FV]): Chart = {
      val trimmed = Chart.empty(sentence)
      val predicateInstance = predicate.instantiate(this)
      val stateMapping: util.IdentityHashMap[State, State] = new util.IdentityHashMap()
      def mapStates(states: Iterable[State]): Iterable[State] = states.flatMap { state =>
        if(stateMapping.containsKey(state)) {
          Iterable(stateMapping.get(state))
        }
        else {
          Iterable()
        }
      }

      def createState(state: State): State = {
        if(stateMapping.keySet.contains(state)) {
          stateMapping.get(state)
        }
        else {
          val action = state.actions.maxBy(_.score)
          val predecessors = action.predecessors.map(createState)
          val trimmedAction = new Action(
            decision = action.decision,
            localLabel = action.localLabel,
            syntax = action.syntax,
            predecessors = predecessors
          )
          val trimmedState = trimmed.putAction(trimmedAction)
          stateMapping.put(state, trimmedState)
          trimmedState
        }
      }

      val newRootStates = outputStates.filter(predicateInstance).map(createState)
      trimmed.outputStates.appendAll(newRootStates)
      trimmed
    }
  }

  object Chart {

    def empty(sentence: Vector[Token]): Chart = new Chart(sentence, Array.fill(sentence.size, sentence.size)(Cell.empty), ArrayBuffer.empty)

  }

}
