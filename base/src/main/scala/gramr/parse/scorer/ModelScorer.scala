package gramr.parse.scorer

import gramr.learn.FeatureVector
import gramr.parse.Decision
import gramr.parse.chart.{ChartLabel, HasFeatures, InheritableLabel, ScoredFeatures}
import gramr.text.Span

/** Scores decisions using a feature-based scoring function – usually derived from a model.
  *
  * @param decisionFeatures feature extractor for decisions
  * @param stateFeatures feature extractor for meaning representations
  * @param modelScore scoring function
  * @tparam MR type of meaning representation
  * @tparam FV type of feature vector
  * @tparam C type of context
  */
class ModelScorer[MR, FV: FeatureVector, C](
  decisionFeatures: (Decision[MR], C) => FV,
  stateFeatures: (MR, C) => FV,
  modelScore: FV => Double
) extends DecisionScorer[MR, C, InheritableLabel[ScoredFeatures[FV]]] {

  type L = ScoredFeatures[FV]

  implicit val lcl: ChartLabel[L] = ScoredFeatures.maxScoring[FV]
  implicit val lhf: HasFeatures[FV, L] = ScoredFeatures.hasFeatures[FV]
  implicit val lo: Ordering[L] = ScoredFeatures.orderByScore[FV]


  override def label(decision: Decision[MR], span: Span, context: C): InheritableLabel[ScoredFeatures[FV]] = {
    val inheritable = decisionFeatures(decision, context)
    val local = stateFeatures(decision.result, context)
    InheritableLabel(
      local = ScoredFeatures(local, modelScore(local)),
      inheritable = ScoredFeatures(inheritable, modelScore(inheritable))
    )
  }

}
