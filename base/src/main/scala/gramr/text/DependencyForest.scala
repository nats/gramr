package gramr.text

case class DependencyForest(
  headEdge: Map[Int, (Int, String)]
) {

  def head(index: Int): Int = headEdge(index)._1

  lazy val children: Map[Int, Set[Int]] = {
    val entries = for {
      headIndex <- headEdge.keySet
    } yield (headIndex, headEdge.keySet.filter { c => head(c) == headIndex })
    entries.toMap
  }

  lazy val roots: Set[Int] = headEdge.keySet.filter { c => head(c) == c }

  def path(from: Int, to: Int): Vector[DependencyForest.PathStep] = {
    import DependencyForest.{UpStep, DownStep}

    def nodesFromRoot(index: Int): Vector[Int] =
      if(head(index) == index) Vector(index)
      else nodesFromRoot(head(index)) :+ index

    def nodesFromCommonAncestor(nodes1: Vector[Int], nodes2: Vector[Int]): (Vector[Int], Vector[Int]) = {
      if(nodes1.isEmpty || nodes2.isEmpty || nodes1.head != nodes2.head) (nodes1, nodes2)
      else nodesFromCommonAncestor(nodes1.tail, nodes2.tail)
    }

    val (fromNodes, toNodes) = nodesFromCommonAncestor(nodesFromRoot(from), nodesFromRoot(to))
    val upSteps = for(n <- fromNodes.reverse if !roots.contains(n); (h, l) = headEdge(n)) yield UpStep(h, l)
    val downSteps = for(n <- toNodes if !roots.contains(n); (h, l) = headEdge(n)) yield DownStep(n, l)
    upSteps ++ downSteps
  }
}

object DependencyForest {

  def fromAttributeString(attribute: String): DependencyForest = {
    // the attribute string consists of triples of the form <headIndex>,<childIndex>,<label>
    val edges = for {
      triple <- attribute.split(" ")
      List(h, c, l) = triple.split(",").toList
    } yield (Integer.parseInt(c), (Integer.parseInt(h), l))
    DependencyForest(edges.toMap)
  }

  trait PathStep
  case class UpStep(node: Int, label: String) extends PathStep
  case class DownStep(node: Int, label: String) extends PathStep

}
