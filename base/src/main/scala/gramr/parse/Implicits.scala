package gramr.parse

import gramr.graph.Graph
import gramr.learn.FeatureVector
import gramr.text.Example

import scala.language.implicitConversions

object Implicits {

  implicit def toParserOps[MR: Graph, FV: FeatureVector, E: Example](parser: Parser[MR, FV, E]): ParserOps[MR, FV, E] =
    new ParserOps(parser)

}
