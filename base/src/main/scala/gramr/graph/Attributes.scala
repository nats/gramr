package gramr.graph

import scalatags.Text.TypedTag

trait Attribute {
  def mangleEdgeHtml(html: TypedTag[String]): TypedTag[String] = html

  def mangleNodeHtml(html: TypedTag[String]): TypedTag[String] = html
}

@SerialVersionUID(1L)
case class Attributes(attributes: Map[String, Attribute]) extends Serializable {

  /**
    * Retrieves an attribute by name and type.
    *
    * Throws NoSuchAttributeException if the attribute does not exist.
    * Throws ClassCastException if the attribute exists, but its value has a different type.
    */
  def get[T](name: String): T = getOption[T](name) match {
    case Some(x) => x
    case None => throw NoSuchAttributeException(name)
  }

  def getOption[T](name: String): Option[T] = attributes.get(name) match {
    case Some(x) => Some(x.asInstanceOf[T])
    case None => None
  }

  def contains(name: String): Boolean = attributes.contains(name)

  def extend[T <: Attribute](name: String, v: T): Attributes = {
    new Attributes(attributes + (name -> v))
  }

  def +[T <: Attribute](v: (String, T)): Attributes = {
    new Attributes(attributes + v)
  }

  def -(k: String): Attributes = {
    new Attributes(attributes - k)
  }

  def mangleEdgeHtml(html: TypedTag[String]): TypedTag[String] =
    attributes.foldLeft(html)((h, a) => a._2.mangleEdgeHtml(h))

  def mangleNodeHtml(html: TypedTag[String]): TypedTag[String] =
    attributes.foldLeft(html)((h, a) => a._2.mangleNodeHtml(h))

  case class NoSuchAttributeException(name: String)
    extends Exception(s"No such attribute: $name")

}

object Attributes {
  def empty: Attributes = new Attributes(Map.empty)

}


/**
  * A generic attribute that doesn’t require any specific code.
  */
case class DefaultAttribute[T](value: T) extends Attribute


object OptionAttribute {

  object Implicits {

    import scala.language.implicitConversions

    implicit def fromOption[A <: Attribute](v: Option[A]): OptionAttribute[A] = OptionAttribute(v)

  }

}


/**
  * Wraps an Option in an Attribute.
  */
case class OptionAttribute[A <: Attribute](value: Option[A]) extends Attribute {

  override def toString: String = value.toString

  override def mangleEdgeHtml(html: TypedTag[String]): TypedTag[String] = value match {
    case Some(v) => v.mangleEdgeHtml(html)
    case _ => html
  }

  override def mangleNodeHtml(html: TypedTag[String]): TypedTag[String] = value match {
    case Some(v) => v.mangleNodeHtml(html)
    case _ => html
  }

}