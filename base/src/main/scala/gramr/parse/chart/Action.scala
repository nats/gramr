package gramr.parse.chart

import gramr.parse.{Decision, SyntaxInformation}

trait Action[MR, FV] {

  def decision: Decision[MR]

  def mr: MR

  def syntax: SyntaxInformation

  def score: Double

  def features: FV

  def predecessors: Iterable[State[MR, FV]]

}
