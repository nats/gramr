package gramr
package split
package filters

import gramr.text.Token
import gramr.graph._

case class SplitFilterTrue[MR : Graph]() extends SplitFilter[MR] {

  def apply(split: Split[MR], sentence: Vector[Token]): Boolean = true

}
