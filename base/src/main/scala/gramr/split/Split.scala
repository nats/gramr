package gramr.split

import gramr.ccg._

case class Split[MR](
  left: PartialMR[MR],
  right: PartialMR[MR],
  step: DerivationStep[Unit]
)

object Split {

  def fromMRs[MR](mrs: (MR, MR), step: DerivationStep[Unit]) = Split(
    PartialMR(mrs._1, step.subSteps(0).span, synCats(step.subSteps(0))),
    PartialMR(mrs._2, step.subSteps(1).span, synCats(step.subSteps(1))),
    step
  )

  private def synCats(step: DerivationStep[Unit]): Set[SyntacticCategory] = {
    if (step.subSteps.size == 1) synCats(step.subSteps(0)) + step.synCat
    else Set(step.synCat)
  }

}
