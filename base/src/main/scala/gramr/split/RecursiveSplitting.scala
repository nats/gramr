package gramr
package split

import com.typesafe.scalalogging._
import text._
import ccg._
import Example.ops._
import gramr.util.TimeoutException

class RecursiveSplitting[MR, E: Example](
  splitGeneratorFactory: E => SplitGenerator[MR],
  splitFilter: SplitFilter[MR],
  maxItems: Option[Int] = None
)(implicit hasMR: HasMR[E, MR]) extends SentenceSplittingAlgorithm[MR, E] with LazyLogging {

  def split(example: E, derivation: Derivation[Unit], id: String): SplitOutcome[MR] = {
    val splitGenerator = splitGeneratorFactory(example)
    var agenda = Vector.empty[AgendaItem]
    val chart = SplitChart.empty[MR](example.sentence, Vector(derivation))

    def addSplit(parentId: Int, partialMRs: List[PartialMR[MR]]) = {
      val ids = chart.addSplit(parentId, partialMRs)
      ids collect { case (Some(newId), partialMR) =>
        agenda = agenda :+ AgendaItem(newId, partialMR)
      }
    }

    val rootMR = PartialMR(example.reference, Span(0, example.sentence.size - 1), Set(derivation.root.synCat))
    val rootId = chart.addRoot(rootMR).get
    agenda = agenda :+ AgendaItem(rootId, rootMR)

    try {
      while (agenda.nonEmpty) {
        val AgendaItem(parentId, parent) = agenda.head
        agenda = agenda.tail
        val potentialSplits = splitGenerator.splits(parent, derivation).toVector
        val acceptedSplits = potentialSplits.filter(s => splitFilter(s, derivation.sentence))
        acceptedSplits.foreach { split =>
          addSplit(parentId, List(split.left, split.right))
        }

        maxItems match {
          case Some(count) if chart.itemCount > count =>
            throw gramr.util.TimeoutException(s"Max item count of $maxItems reached")
          case _ => ()
        }
      }
      Right(chart)
    }
    catch {
      case e: TimeoutException =>
        logger.trace(s"${example.id}: ${e.getMessage}")
        Left(e.getMessage)
      case e: Exception =>
        logger.warn(s"${example.id}: ${e.toString}")
        Left(e.toString)
    }
  }

  case class AgendaItem(
    id: Int,
    partialMR: PartialMR[MR]
  )

}
