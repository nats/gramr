package gramr.graph
package edit

import gramr.graph.edit.sgraph.Combine.NoPlaceholderException
import gramr.graph.mark.Marker

import Edit._
import Graph.ops._

package object sgraph {

  type CombineOperator[MR] = (MR, MR) => Edit[MR, Unit]

  type SplitOperator[MR] = MR => Iterable[MR]

  /** Pairs up combining and splitting operators that are inverses of each other.
    *
    */
  trait SemanticOperator[MR] {
    def combine: CombineOperator[MR]
    def split: SplitOperator[MR]
  }


  /** Retrieves the placeholder at a given depth.
    *
    * Depths are counted from the outside, so the highest-indexed placeholder has depth 0.
    *
    * @param mr the mr to search in
    * @param depth the depth to look at
    * @return a pointer to the placeholder
    */
  def placeholder[MR : Graph](mr: MR, depth: Int): Int = {
    mr.findExternalNode(depth) match {
      case Some(n) => n
      case None => throw NoPlaceholderException(mr, depth)
    }
  }

  /** Changes a placeholder's index.
    *
    * @param placeholderMarker the placeholder to modify
    * @param newIndex the index to assign the placeholder
    */
  def movePlaceholder[MR : Graph](placeholderMarker: Marker, newIndex: Int): Edit[MR, Unit] = {
    for {
      placeholder <- resolve(placeholderMarker)
      _ <- modifyMR { mr: MR =>
        val placeholderAttributes = mr.node(placeholder).element.attributes
        mr.replaceElement(placeholder, ExternalNode(ExternalReference(newIndex), placeholderAttributes))
      }
    } yield ()
  }
}
