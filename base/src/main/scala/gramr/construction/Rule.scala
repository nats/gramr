package gramr.construction

import gramr.ccg.CombinatoryStep
import gramr.graph.Graph
import gramr.split.{PartialMR, Split}

trait Rule[MR] {

  def meaningGraphLike: Graph[MR]

  def name: String

  /** Defines the syntactic environment in which patterns apply.
    */
  def appliesTo(context: SyntacticContext): Boolean

  /** Generates the available splits.
    */
  def split(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): Iterator[Split[MR]] =
    Iterator.empty

  /** Generates the available recombinations.
    */
  def combine(left: MR, right: MR, context: SyntacticContext): Iterator[MR] =
    Iterator.empty

}
