package gramr
package split

trait SplitChartFilter[MR] {
  self =>

  import SplitChartFilter._

  def instantiate(splitChart: SplitChart[MR]): ItemFilter[MR]

  def boolean(
    other: SplitChartFilter[MR], op: (Boolean, Boolean) => Boolean
  ): SplitChartFilter[MR] = new SplitChartFilter[MR] {

    def instantiate(splitChart: SplitChart[MR]) = new ItemFilter[MR] {

      val i1: ItemFilter[MR] = self.instantiate(splitChart)
      val i2: ItemFilter[MR] = other.instantiate(splitChart)

      def apply(item: PartialMR[MR]): Boolean =
        op(i1.apply(item), i2.apply(item))

    }

  }

  def and(other: SplitChartFilter[MR]): SplitChartFilter[MR] = boolean(other, _ && _)

  def or(other: SplitChartFilter[MR]): SplitChartFilter[MR] = boolean(other, _ || _)

}


object SplitChartFilter {

  trait ItemFilter[MR] {

    def apply(item: PartialMR[MR]): Boolean

  }

  def noFilter[MR]: SplitChartFilter[MR] = (splitChart: SplitChart[MR]) => { (item: PartialMR[MR]) => true }

}
