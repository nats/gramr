package gramr.util

object VectorUtils {

  /**
    * A lexicographic ordering for vectors.
    */
  def lexicographicOrdering[T: Ordering]: Ordering[Vector[T]] = new Ordering[Vector[T]] {

    def compare(a: Vector[T], b: Vector[T]): Int = {
      if (a.isEmpty && b.isEmpty) 0
      else if (a.isEmpty) -1
      else if (b.isEmpty) -1
      else {
        val hCompare = implicitly[Ordering[T]].compare(a.head, b.head)
        if (hCompare != 0) hCompare
        else compare(a.tail, b.tail)
      }
    }

  }

  case class Bin[T](
    pred: T => Boolean,
    min: Option[Int] = None,
    max: Option[Int] = None
  ) {

    def accepts(item: T, bin: Vector[T]): Boolean = {
      pred(item) && max.forall(bin.size < _)
    }

    def isValid(bin: Vector[T]): Boolean = {
      min.forall(bin.size >= _)
    }

  }

  def binnings[T](input: Vector[T], bins: Vector[Bin[T]]): Iterable[Vector[Vector[T]]] = {
    val indexedBins = bins.zipWithIndex

    def recurse(remainingInput: Vector[T], assignedBins: Vector[Vector[T]]): Iterable[Vector[Vector[T]]] = {
      if(remainingInput.isEmpty) {
        Iterable(assignedBins)
      }
      else {
        val item = remainingInput.head
        for {
          (binDef, binIndex) <- indexedBins
          bin = assignedBins(binIndex)
          updatedBins = assignedBins.updated(binIndex, bin :+ item) if binDef.accepts(item, bin)
          finalBins <- recurse(remainingInput.tail, updatedBins)
        } yield finalBins
      }
    }

    val binnings = recurse(input, bins.map { _ => Vector.empty[T] })
    val validBinnings = binnings.filter { _.zipWithIndex.forall { case (bin, binIndex) => bins(binIndex).isValid(bin) } }
    validBinnings
  }

}