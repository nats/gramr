package gramr.graph
package lexemes

import gramr.text.Token
import gramr.util.MultiMap
import org.apache.commons.csv.CSVFormat

import scala.io.Source
import scala.collection.JavaConverters._

trait WildcardFiller {

  def createLexemes(tokens: Iterable[Token]): Iterable[Lexeme]
}


case class EmptyFiller() extends WildcardFiller {
  override def createLexemes(tokens: Iterable[Token]): Iterable[Lexeme] =
    Iterable(
      Lexeme(
        tokens.toVector.map(_.text.toLowerCase),
        Vector.empty,
        Vector.empty,
        Vector.empty
      )
    )
}


case class LemmaFiller() extends WildcardFiller {
  override def createLexemes(tokens: Iterable[Token]): Iterable[Lexeme] =
    // Lemmas can appear as concepts or constants
    Iterable(
      Lexeme(
        tokens.toVector.map(_.text.toLowerCase),
        tokens.toVector.map(_.lemma.toLowerCase),
        Vector.empty,
        Vector.empty
      ),
      Lexeme(
        tokens.toVector.map(_.text.toLowerCase),
        tokens.toVector.map(_.lemma.toLowerCase),
        Vector.empty,
        Vector.empty
      )
    )

}


case class QuotedFiller() extends WildcardFiller {
  override def createLexemes(tokens: Iterable[Token]): Iterable[Lexeme] =
    // Quoted constants can appear as constants
    Iterable(Lexeme(
      tokens.toVector.map(_.text.toLowerCase),
      Vector.empty,
      tokens.toVector.map(t => "\"" + t.text + "\""),
      Vector.empty
    ))
}


case class PropbankFiller() extends WildcardFiller {
  val reader = Source.fromResource("propbank_frames.csv").bufferedReader()
  val records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(reader).asScala
  val frames = records.foldLeft(MultiMap.empty[String, String]) { (mmap, record) => mmap + (record.get("lemma"), record.get("name")) }

  override def createLexemes(tokens: Iterable[Token]): Iterable[Lexeme] =
    if(tokens.size == 1) {
      frames.get(tokens.head.lemma).map { frame =>
        Lexeme(
          Vector(tokens.head.text),
          Vector(frame), // concept label
          Vector.empty,
          Vector.empty
        )
      }
    }
    else {
      Iterable.empty
    }
}
