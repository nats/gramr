package gramr.learn.features

import gramr.graph.{AdjacencyMeaningGraph, Graph}
import gramr.learn.MapFeatureVector
import org.scalatest.FlatSpec
import AdjacencyMeaningGraph.ops._
import MapFeatureVector.ops._
import AdjacencyMeaningGraph.graphInstance.Building._
import gramr.learn.util.{FeatureIndex, StringStore}
import gramr.text.Example
import gramr.util.Parsing.parseAmr

class PathFeature2Spec extends FlatSpec {

  implicit val featureIndex: FeatureIndex = new StringStore

  type MR = AdjacencyMeaningGraph
  type FV = MapFeatureVector

  case class TestExample(
    id: String,
    mr: MR
  )

  class TestExampleInstance extends Example[TestExample] {
    override def id(a: TestExample): String = a.id

    override def sentence(a: TestExample) = ???

    override def indexedDerivations(a: TestExample) = ???
  }

  val singleNodeExample: TestExample = TestExample(
    "1",
    AdjacencyMeaningGraph.build {
      for {
        _ <- mkReferentNode("a")
      } yield ()
    }
  )

  val singleEdgeExample: TestExample = TestExample(
    "2",
    AdjacencyMeaningGraph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        _ <- mkEdge("ab", a, b)
      } yield ()
    }
  )

  val twoEdgeChainExample: TestExample = TestExample(
    "3",
    AdjacencyMeaningGraph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }
  )

  val doubleDiamondExample: TestExample = TestExample(
    "4",
    AdjacencyMeaningGraph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        d <- mkReferentNode("d")
        e <- mkReferentNode("e")
        f <- mkReferentNode("f")
        g <- mkReferentNode("g")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
        _ <- mkEdge("bd", b, d)
        _ <- mkEdge("cd", c, d)
        _ <- mkEdge("de", d, e)
        _ <- mkEdge("df", d, f)
        _ <- mkEdge("eg", e, g)
        _ <- mkEdge("fg", f, g)
      } yield ()
    }
  )

  val amr1 = """(s / see-01
               |      :ARG0 (i / i)
               |      :ARG1 (p / picture
               |            :mod (m / magnificent)
               |            :location (b2 / book :wiki -
               |                  :name (n / name :op1 "True" :op2 "Stories" :op3 "from" :op4 "Nature")
               |                  :topic (f / forest
               |                        :mod (p2 / primeval))))
               |      :mod (o / once)
               |      :time (a / age-01
               |            :ARG1 i
               |            :ARG2 (t / temporal-quantity :quant 6
               |                  :unit (y / year))))""".stripMargin
  val graph1 = TestExample("graph1", parseAmr(amr1))

  val configLength1: PathFeatureConfig[MR, FV] = PathFeatureConfig(
    EdgeFilter.True(),
    NodeTransformer.Content,
    List((EdgeTransformer.Label, NodeTransformer.Content))
  )
  val configLength2: PathFeatureConfig[MR, FV] = PathFeatureConfig(
    EdgeFilter.True(),
    NodeTransformer.Content,
    List(
      (EdgeTransformer.Label, NodeTransformer.Content),
      (EdgeTransformer.Label, NodeTransformer.Content)
    )
  )
  val configLength3: PathFeatureConfig[MR, FV] = PathFeatureConfig(
    EdgeFilter.True(),
    NodeTransformer.Content,
    List(
      (EdgeTransformer.Label, NodeTransformer.Content),
      (EdgeTransformer.Label, NodeTransformer.Content),
      (EdgeTransformer.Label, NodeTransformer.Content)
    )
  )
  val configLength4: PathFeatureConfig[MR, FV] = PathFeatureConfig(
    EdgeFilter.True(),
    NodeTransformer.Content,
    List(
      (EdgeTransformer.Label, NodeTransformer.Content),
      (EdgeTransformer.Label, NodeTransformer.Content),
      (EdgeTransformer.Label, NodeTransformer.Content),
      (EdgeTransformer.Label, NodeTransformer.Content)
    )
  )

  "PathFeature2" should "return a feature vector" in {
    val feature = PathFeature2[MR, FV, TestExample](configLength1)

    feature.extract(singleNodeExample.mr, singleNodeExample)
  }

  it should "produce length 1 features" in {
    val feature = PathFeature2[MR, FV, TestExample](configLength1)

    val fv = feature.extract(singleEdgeExample.mr, singleEdgeExample)
    assert(fv.size == 1)
    assert(fv.get("path(a, ab, b)") == 1.0)
  }

  it should "produce length 1 and 2 features" in {
    val feature = PathFeature2[MR, FV, TestExample](
      configLength1, configLength2
    )

    val fv = feature.extract(twoEdgeChainExample.mr, twoEdgeChainExample)
    assert(fv.size == 3)
    assert(fv.get("path(a, ab, b)") == 1.0)
    assert(fv.get("path(b, bc, c)") == 1.0)
    assert(fv.get("path(a, ab, b, bc, c)") == 1.0)
  }

  it should "extract all features in a double diamond" in {
    val feature = PathFeature2[MR, FV, TestExample](
      configLength1, configLength2, configLength3, configLength4
    )

    val fv = feature.extract(doubleDiamondExample.mr, doubleDiamondExample)
    assert(fv.size == 28)
    assert(fv.get("path(a, ab, b)") == 1.0)
    assert(fv.get("path(a, ac, c)") == 1.0)
    assert(fv.get("path(b, bd, d)") == 1.0)
    assert(fv.get("path(c, cd, d)") == 1.0)
    assert(fv.get("path(d, de, e)") == 1.0)
    assert(fv.get("path(d, df, f)") == 1.0)
    assert(fv.get("path(e, eg, g)") == 1.0)
    assert(fv.get("path(f, fg, g)") == 1.0)
    assert(fv.get("path(a, ab, b, bd, d)") == 1.0)
    assert(fv.get("path(a, ac, c, cd, d)") == 1.0)
    assert(fv.get("path(b, bd, d, de, e)") == 1.0)
    assert(fv.get("path(c, cd, d, de, e)") == 1.0)
    assert(fv.get("path(b, bd, d, df, f)") == 1.0)
    assert(fv.get("path(c, cd, d, df, f)") == 1.0)
    assert(fv.get("path(d, de, e, eg, g)") == 1.0)
    assert(fv.get("path(d, df, f, fg, g)") == 1.0)
    assert(fv.get("path(a, ab, b, bd, d, de, e)") == 1.0)
    assert(fv.get("path(a, ab, b, bd, d, df, f)") == 1.0)
    assert(fv.get("path(a, ac, c, cd, d, de, e)") == 1.0)
    assert(fv.get("path(a, ac, c, cd, d, df, f)") == 1.0)
    assert(fv.get("path(a, ab, b, bd, d, de, e)") == 1.0)
    assert(fv.get("path(b, bd, d, de, e, eg, g)") == 1.0)
    assert(fv.get("path(b, bd, d, df, f, fg, g)") == 1.0)
    assert(fv.get("path(c, cd, d, de, e, eg, g)") == 1.0)
    assert(fv.get("path(c, cd, d, df, f, fg, g)") == 1.0)
    assert(fv.get("path(a, ab, b, bd, d, de, e, eg, g)") == 1.0)
    assert(fv.get("path(a, ab, b, bd, d, df, f, fg, g)") == 1.0)
    assert(fv.get("path(a, ac, c, cd, d, de, e, eg, g)") == 1.0)
    assert(fv.get("path(a, ac, c, cd, d, df, f, fg, g)") == 1.0)
  }

  it should "be faster than the old PathFeatureSet extractor" in {
    val feature = PathFeature2[MR, FV, TestExample](
      configLength1, configLength2, configLength3, configLength4
    )
    val oldFeature = PathFeatureSet[MR, FV, TestExample](configLength1, configLength2, configLength3, configLength4)

    // Warm up
    (1 to 1000) foreach { _ =>
      feature.extract(graph1.mr, graph1)
    }
    (1 to 1000) foreach { _ =>
      oldFeature.extract(graph1.mr, graph1)
    }

    val start1 = System.currentTimeMillis()
    (1 to 1000) foreach { _ =>
      feature.extract(graph1.mr, graph1)
    }
    val stop1 = System.currentTimeMillis()
    val elapsed1 = stop1 - start1

    val start2 = System.currentTimeMillis()
    (1 to 1000) foreach { _ =>
      oldFeature.extract(graph1.mr, graph1)
    }
    val stop2 = System.currentTimeMillis()
    val elapsed2 = stop2 - start2

    System.err.println(s"New implementation: $elapsed1 ms")
    System.err.println(s"Old implementation: $elapsed2 ms")
    assert(elapsed2 > elapsed1)
  }

  it should "give the same result as the old PathFeatureSet extractor" in {
    val feature = PathFeature2[MR, FV, TestExample](
      configLength1, configLength2, configLength3, configLength4
    )
    val oldFeature = PathFeatureSet[MR, FV, TestExample](configLength1, configLength2, configLength3, configLength4)

    val fv1 = feature.extract(graph1.mr, graph1)
    val fv2 = oldFeature.extract(graph1.mr, graph1)

    val fv1Keys = fv1.entries.map(_._1).toSet
    val fv2Keys = fv2.entries.map(_._1).toSet
    assert(fv1Keys == fv2Keys)

    fv1Keys.foreach { key =>
      assert(fv2.get(key) == fv1.get(key))
    }
  }
}
