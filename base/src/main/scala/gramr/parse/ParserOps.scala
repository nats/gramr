package gramr.parse

import com.typesafe.scalalogging.LazyLogging
import gramr.graph.Graph
import gramr.learn.FeatureVector
import gramr.parse.chart.{Chart, ChartParser, State, trimming}
import gramr.text.{Example, Span, Token}

import Example.ops._

class ParserOps[MR: Graph, FV: FeatureVector, E: Example](parser: Parser[MR, FV, E]) {

  def resolveCorefs: Parser[MR, FV, E] = {
    val cp = parser.asInstanceOf[ChartParser[MR, FV, _, E]]
    postprocess.resolveCorefs(cp)
  }

  def rejecting(f: E => Boolean): Parser[MR, FV, E] = {
    new Parser[MR, FV, E] with LazyLogging {
      override def parse(sentence: Vector[Token], context: E): Chart[MR, FV] = {
        if(!f(context)) {
          parser.parse(sentence, context)
        }
        else {
          logger.trace(s"Rejecting ${context.id} (${sentence.map(_.text).mkString(" ")})")
          val extSentence = sentence
          // return a dummy empty chart
          new Chart[MR, FV] { self =>
            override def sentence = extSentence

            override def spans = Iterable()

            override def copy = self

            override def states(span: Span) = Iterable()

            override def successors(state: State[MR, FV]) = Iterable()

            override def outputStates = Iterable()

            override def trim(predicate: trimming.TrimmingPredicate[MR, FV]): Unit = ()

            override def trimmed(predicate: trimming.TrimmingPredicate[MR, FV]) = self

            override def deepTrimmed(predicate: trimming.TrimmingPredicate[MR, FV]) = self
          }
        }
      }
    }
  }

  /** Invokes another parser if the first one is empty.
    */
  def fallBack(other: Parser[MR, FV, E]) = {
    new Parser[MR, FV, E] {
      override def parse(sentence: Vector[Token], context: E): Chart[MR, FV] = {
        val chart = parser.parse(sentence, context)
        if(chart.outputStates.nonEmpty) {
          chart
        }
        else {
          other.parse(sentence, context)
        }
      }
    }
  }

}
