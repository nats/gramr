package gramr.ccg.presentation

import gramr.ccg.Derivation
import gramr.util.html.ToHtml

object Implicits {

  import scala.language.implicitConversions

  implicit def derivationToHtml[A: ToHtml]: ToHtml[Derivation[A]] = new DerivationToHtml[A]

}
