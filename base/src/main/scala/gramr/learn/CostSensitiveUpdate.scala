package gramr.learn

import FeatureVector.Syntax._

case class CostSensitiveUpdate[FV: FeatureVector](
  positiveFeatures: FV,
  negativeFeatures: FV
) extends Update[FV] {

  val gradient: FV = negativeFeatures - positiveFeatures

}
