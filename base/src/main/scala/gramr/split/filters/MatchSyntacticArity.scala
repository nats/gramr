package gramr
package split
package filters

import gramr.text.Token
import gramr.graph._

import Graph.ops._

/** Enforces a match between a partial MR’s syntactic and semantic arity.
  *
  * Syntactic arity is determined by the syntactic category. Semantic arity
  * is determined by the number of layers of external nodes. An arbitrary operator for comparing the two can be given.
  * For ease of use, use the companion object's predefined instances.
  *
  * @param operator Operator used to compare the semantic (left) and syntactic (right) arity of a candidate MR
  */
case class MatchSyntacticArity[MR : Graph](
  operator: (Int, Int) => Boolean
) extends SplitFilter[MR] {

  def apply(split: Split[MR], sentence: Vector[Token]): Boolean = {
    pred(split.left) && pred(split.right)
  }

  def pred(partialMR: PartialMR[MR]): Boolean = {
    partialMR.synCats.exists { cat => operator(partialMR.mr.externalLayerCount, cat.arity) }
  }

}

object MatchSyntacticArity {

  def equal[MR: Graph]: MatchSyntacticArity[MR] = MatchSyntacticArity(_ == _)

  def lessOrEqual[MR: Graph]: MatchSyntacticArity[MR] = MatchSyntacticArity(_ <= _)

}
