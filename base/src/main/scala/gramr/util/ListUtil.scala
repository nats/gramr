package gramr.util

object ListUtil {

  /** Attaches an index to those elements for which pred holds.
    * Elements for which pred does not hold are not counted and are labelled with None.
    */
  def indexSome[A](s: List[A])(pred: A => Boolean): List[(A, Option[Int])] = {
    val r = s.foldLeft((0, List.empty[(A, Option[Int])])) { (acc, a) =>
      val (idx, l) = acc
      if (pred(a))
        (idx + 1, (a, Some(idx)) :: l)
      else
        (idx, (a, None) :: l)
    }
    r._2.reverse
  }

  /**
    * Enumerates all elements of the p-th power of s, i.e. the p-fold cartesian product of s with itself.
    */
  def power[A](s: List[A], p: Int): Stream[List[A]] = {
    if (p < 1)
      Stream(List())
    else if (p == 1)
      s.map(List(_)).toStream
    else {
      val str: Stream[A] = s.toStream
      for {
        x <- str
        r <- power(s, p - 1)
      } yield x :: r
    }
  }

  /**
    * Enumerates all sub-sets of s up to a given size.
    */
  def subSets[A](s: List[A], maxSize: Int): Stream[List[A]] = s match {
    case x :: xs => for {
      subSet <- subSets(xs, maxSize)
      extended <-
      if (subSet.size < maxSize)
        Stream(subSet, x :: subSet)
      else
        Stream(subSet)
    } yield extended
    case Nil => Stream(Nil)
  }

}
