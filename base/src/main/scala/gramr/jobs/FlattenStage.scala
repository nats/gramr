package gramr.jobs
import monix.execution.Scheduler
import monix.reactive.Observable

class FlattenStage[A](implicit scheduler: Scheduler) extends ListeningStage[Iterable[A], A] {

  override def observable(input: Observable[Iterable[A]]): Observable[A] = {
    val flattened = input.map(Observable.fromIterable).flatten
    flattened.map { item =>  // Use map to make sure listeners are executed synchronously
      listeners.foreach { l => l.item(item) }
      item
    }
  }

}
