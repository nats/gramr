package gramr.jobs

import monix.execution.Scheduler
import monix.reactive.Observable

import scala.collection.mutable.ListBuffer

case class ComputeStage[A, B](
  computation: A => B
)(implicit scheduler: Scheduler) extends ListeningStage[A, B] {

  def observable(input: Observable[A]): Observable[B] = {
    input.map { a =>
      val item = computation(a)
      listeners.foreach(_.item(item))
      item
    }
  }

}
