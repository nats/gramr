package gramr.graph.visit

import gramr.graph.Graph
import Graph.ops._

import scala.collection.SortedSet

/** A visitor which keeps track of unvisited nodes.
  *
  * At any given time, `nextNode` returns the lowest-indexed unvisited node.
  *
  * @param graph the graph which the visitor will be used on
  */
class UnvisitedNodesVisitor[MR: Graph](graph: MR) extends Visitor {
  private var unvisited: SortedSet[Int] = SortedSet.empty[Int] ++ graph.nodes.map(_.pointer)

  def nextNode: Option[Int] = unvisited.headOption

  override def visitNode(nodeIndex: Int): Unit = {
    unvisited = unvisited - nodeIndex
  }
}
