package gramr
package parse

import gramr.graph.{Edge, _}
import learn._

case class EdgeInventionDecision[MR : Graph](context: EdgeFeatureContext[MR])
  extends Decision[Edge] with Ordered[EdgeInventionDecision[MR]] {
  def result: Edge = context.edge

  def compare(that: EdgeInventionDecision[MR]): Int = result.compare(that.result)
}

object EdgeInventionDecision {

  import FeatureVector.Syntax._

  def toFeatures[MR : Graph, F : FeatureVector](
    implicit edgeFeatures: ToFeatures[EdgeFeatureContext[MR], F]
  ) =
    new ToFeatures[EdgeInventionDecision[MR], F] {
      def features(i: EdgeInventionDecision[MR]): F = implicitly[FeatureVector[F]].from(
        Seq(
          (s"inventEdge", 1.0)
          // source / target degrees, distance from root…
        )
      ) + edgeFeatures.features(i.context)
    }

}
