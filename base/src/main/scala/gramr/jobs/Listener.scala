package gramr.jobs

import com.typesafe.scalalogging.LazyLogging
import gramr.graph.Graph
import gramr.parse.evaluate.{BatchParseStats, ParseStats}
import gramr.text.{Example, HasMR}
import gramr.util.{ParseStatsCsvLogger, SimpleRetrievalStats}
import Example.ops._

import scala.collection.mutable.ArrayBuffer

trait Listener[A] {

  def item(a: A): Unit = ()

  def beginBatch(): Unit = ()

  def finishBatch(): Unit = ()

  def beginIteration(): Unit = ()

  def finishIteration(): Unit = ()

}


object Listener extends LazyLogging {

  def item[A](f: A => Unit): Listener[A] = new Listener[A] {
    override def item(a: A): Unit = f(a)
  }

  def batch[A](f: Seq[A] => Unit): Listener[A] = new Listener[A] {
    val buffer: ArrayBuffer[A] = ArrayBuffer.empty

    override def item(a: A): Unit = synchronized {
      buffer.append(a)
    }

    override def finishBatch(): Unit = {
      f(buffer)
      buffer.clear()
    }
  }

  def iteration[A](f: Seq[A] => Unit): Listener[A] = new Listener[A] {
    val buffer: ArrayBuffer[A] = ArrayBuffer.empty

    override def item(a: A): Unit = synchronized {
      buffer.append(a)
    }

    override def finishIteration(): Unit = {
      f(buffer)
      buffer.clear()
    }
  }

  def onBeginIteration[A](f: () => Unit): Listener[A] = new Listener[A] {
    override def beginIteration(): Unit = f()
  }

  def onFinishIteration[A](f: () => Unit): Listener[A] = new Listener[A] {
    override def finishIteration(): Unit = f()
  }

  def evaluate[MR: Graph, FV, E: Example](
    evaluationFunction: (MR, E) => SimpleRetrievalStats,
    parseConsumers: Seq[(E, String, ParseStats) => Unit] = Seq.empty,
    batchConsumers: Seq[(String, BatchParseStats) => Unit] = Seq.empty,
    iterationConsumers: Seq[(String, BatchParseStats) => Unit] = Seq.empty,
  )(implicit
    hasMr: HasMR[E, MR]
  ): Listener[(E, NamedParses[MR, FV])] = new Listener[(E, NamedParses[MR, FV])] {
    private val emptyStats: Map[String, Vector[ParseStats]] = Map.empty.withDefault(_ => Vector.empty)
    private var batchStats: Map[String, Vector[ParseStats]] = emptyStats
    private var iterationStats: Map[String, Vector[ParseStats]] = emptyStats

    override def item(a: (E, NamedParses[MR, FV])): Unit = synchronized {
      a match {
        case (example, namedParses) =>
          namedParses.foreach {
            case (key, chart) =>
              val stats = gramr.parse.evaluate.evaluateParse(chart, example, evaluationFunction)
              batchStats += (key -> (batchStats(key) :+ stats))
              iterationStats += (key -> (iterationStats(key) :+ stats))
              parseConsumers.foreach { consumer =>
                consumer(example, key, stats)
              }
          }
      }
    }

    override def finishBatch(): Unit = {
      logger.trace(s"Finish batch (stat count: ${batchStats.mapValues(_.size)})")
      batchStats.foreach {
        case (key, statList) =>
          val batchParseStats = BatchParseStats(statList)
          batchConsumers.foreach { consumer =>
            consumer(key, batchParseStats)
          }
      }
      batchStats = emptyStats
    }

    override def finishIteration(): Unit = {
      logger.trace(s"Finish iteration (stat count: ${iterationStats.mapValues(_.size)})")
      iterationStats.foreach {
        case (key, statList) =>
          val batchParseStats = BatchParseStats(statList)
          iterationConsumers.foreach { consumer =>
            consumer(key, batchParseStats)
          }
      }
      iterationStats = emptyStats
    }
  }

  def printIterationStats(name: String)(key: String, stats: BatchParseStats): Unit = {
    logger.info(s"$name.$key: $stats")
  }

  def logItemStatsToCsv[E: Example](name: String, statsLogger: ParseStatsCsvLogger)(example: E, key: String, stats: ParseStats): Unit = {
    statsLogger.logExampleStats(name, key, example.id, stats)
  }

  def logIterationStatsToCsv(name: String, statsLogger: ParseStatsCsvLogger)(key: String, stats: BatchParseStats): Unit = {
    statsLogger.logIterationStats(name, key, stats)
  }

}
