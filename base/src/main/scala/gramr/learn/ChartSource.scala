package gramr
package learn

import parse._, chart._, trimming._, text._
import Example.ops._

/** Connects the learn package to the parse package by abstracting over ways
  * of obtaining parse charts.
  */
trait ChartSource[MR, F, E] {
  self =>

  def apply(example: E): Chart[MR, F]

  def bestScoredDerivations: ChartSource[MR, F, E] =
    (example: E) => self(example).trimmed(BestScored())

  def bestEvaluatedDerivations(evaluate: (MR, E) => Double): ChartSource[MR, F, E] =
    (example: E) => self(example).trimmed(BestEvaluated(example, evaluate))

  def bestRescoredDerivations(model: => LinearModel)(implicit fv: FeatureVector[F]): ChartSource[MR, F, E] =
    (example: E) => self(example).trimmed(BestRescored(model))

}

object ChartSource {

  def fromFreeParser[MR, F, E: Example](parser: Parser[MR, F, E]): ChartSource[MR, F, E] =
    (example: E) => parser.parse(example.sentence, example)

  def fromForcedParser[MR, F, E: Example](parser: Parser[MR, F, E]): ChartSource[MR, F, E] =
    (example: E) => parser.parse(example.sentence, example)

  def fromChart[MR, F, E: Example](chart: Chart[MR, F]): ChartSource[MR, F, E] =
    (_: E) => chart

}

