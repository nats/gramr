package gramr.graph

import cats.data.State, State._

package object mark {

  /** A utility function that is shared by the recombination and split sub-packages.
    */
  def inventMarker[S: MarkerInventor]: State[S, Marker] = for {
    state <- inspect { s: S => s }
    (marker, newState) = implicitly[MarkerInventor[S]].inventMarker(state)
    _ <- modify { _: S => newState }
  } yield marker

}
