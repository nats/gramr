package gramr.text

case class NamedEntitySpan(
  span: Span,
  entityType: String
)

object NamedEntitySpan {

  /** Parses NamedEntitySpans from an attribute string containing start,end,entity annotations.
    *
    * @param attr the attribute string
    * @return a list of NamedEntitySpans
    */
  def fromAttribute(attr: String): List[NamedEntitySpan] = {
    for {
      span <- attr.split(" ").toList
      trimmedSpan = span.trim if trimmedSpan.nonEmpty
      neSpan = trimmedSpan.split(",").toList match {
        case start :: end :: entity :: Nil =>
          NamedEntitySpan(Span(Integer.parseInt(start), Integer.parseInt(end) - 1), entity)
        case _ =>
          throw new Exception(s"Unexpected named entity span string: '$span', should be of the form start,end,entity")
      }
    } yield neSpan
  }

}
