package gramr.ccg

import gramr.parse.SyntaxInformation

trait LexicalGenerator[MR] {

  def generate(syntax: SyntaxInformation): Set[GeneratedLexicalItem[MR]]

}

object LexicalGenerator {
  case class Config[MR](
    patternName: String,
    generator: LexicalGenerator[MR]
  ) {
    def generate(syntax: SyntaxInformation): Set[GeneratedLexicalItem[MR]] = {
      val generated = generator.generate(syntax)
      generated.map(_.copy(identifier = patternName))
    }
  }
}

