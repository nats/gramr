package gramr.graph.lexemes

import java.io._

import gramr.graph.Graph
import gramr.text.Token
import org.apache.commons.csv.{CSVFormat, CSVPrinter}

class LexemeDictionary(
  val lexemeEntries: Map[Vector[String], Iterable[LexemeDictionary.LexemeItem]],
  val wildcardFillers: Vector[LexemeDictionary.WildcardFillerItem]
) {

  import LexemeDictionary._

  def get(tokens: Seq[Token]): Iterable[LexemeItem] = {
    val matchingLexemes = lexemeEntries.get(tokens.map(_.text.toLowerCase).toVector).toIterable.flatten
    val specialized = wildcardFillers.flatMap(_.specialize(tokens))
    val specializedLexemes = specialized.map(_.lexeme).toSet

    specialized ++ matchingLexemes.filter { item => !specializedLexemes.contains(item.lexeme) }
  }

  def lexemeItems: Iterable[LexemeItem] = lexemeEntries.values.flatten

  def items: Iterable[Item] = wildcardFillers ++ lexemeItems

  def filter(pred: Item => Boolean): LexemeDictionary = from(lexemeItems.filter(pred), wildcardFillers.filter(pred))

  def save(file: File): Unit = {
    val writer = new FileWriter(file)
    val format = CSVFormat.DEFAULT
    val printer = new CSVPrinter(writer, format)

    lexemeEntries.values.flatten.foreach { entry =>
      printer.printRecord(
        entry.id.toString,
        entry.lexeme.tokens.mkString(" "),
        entry.lexeme.conceptLabels.mkString(" "),
        entry.lexeme.constantLabels.mkString(" "),
        entry.lexeme.edgeLabels.mkString(" ")
      )
    }

    writer.close()
  }

  lazy val size: Int = lexemeEntries.values.view.map(_.size).sum

}

object LexemeDictionary {

  def from(lexemeItems: Iterable[LexemeItem], wildcardFillerItems: Iterable[WildcardFillerItem]): LexemeDictionary = {
    new LexemeDictionary(lexemeItems.groupBy(_.lexeme.tokens), wildcardFillerItems.toVector)
  }

  def load[MR : Graph](file: File, wildcardFillers: Iterable[WildcardFiller]): LexemeDictionary = {
    import scala.collection.JavaConverters._

    val reader = new BufferedReader(new FileReader(file))
    val records = CSVFormat.DEFAULT.parse(reader)
    val lexemes = records.asScala.map { record =>
      (record.get(0).toInt, Lexeme(
        splitCsvField(record.get(1)),
        splitCsvField(record.get(2)),
        splitCsvField(record.get(3)),
        splitCsvField(record.get(4))
      )) }
    val builder = new Builder

    // Wildcard fillers are added first to ensure there IDs are properly restored
    wildcardFillers.foreach(builder.add)

    lexemes.foreach {
      case (lexemeId, lexeme) =>
        builder.add(lexemeId, lexeme)
    }
    reader.close()

    builder.build
  }

  private def splitCsvField(field: String): Vector[String] = {
    field.split(" ").toVector.filter(_.nonEmpty)
  }


  sealed trait Item {
    def id: Int
  }

  case class LexemeItem(
    id: Int,
    lexeme: Lexeme
  ) extends Item

  case class WildcardFillerItem(
    id: Int,
    filler: WildcardFiller
  ) extends Item {

    def specialize(tokens: Iterable[Token]): Iterable[LexemeItem] = {
      for(lexeme <- filler.createLexemes(tokens)) yield LexemeItem(id, lexeme)
    }

  }


  class Builder {

    private var lexemes: Set[Lexeme] = Set.empty
    private var lexemeItems: Vector[LexemeItem] = Vector.empty
    private var wildcardFillerItems: Vector[WildcardFillerItem] = Vector.empty
    private var nextId = 1

    def add(lexeme: Lexeme): Unit = {
      if(!lexemes.contains(lexeme)) {
        add(nextId, lexeme)
      }
    }

    def add(lexemeId: Int, lexeme: Lexeme): Unit = {
      lexemes = lexemes + lexeme
      lexemeItems = lexemeItems :+ LexemeItem(lexemeId, lexeme)
      nextId = lexemeId + 1
    }

    def add(filler: WildcardFiller): Unit = {
      wildcardFillerItems = wildcardFillerItems :+ WildcardFillerItem(nextId, filler)
      nextId += 1
    }

    def build: LexemeDictionary = from(lexemeItems, wildcardFillerItems)

  }

}
