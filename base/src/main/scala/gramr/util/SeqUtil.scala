package gramr.util

object SeqUtil {

  def mapWhere[A](s: Seq[A], pred: A => Boolean)(f: A => A): Seq[A] = {
    s.map { elem => if (pred(elem)) f(elem) else elem }
  }

}
