package gramr.graph
package presentation

import gramr.util.html.ToHtml

class GraphWithTokens[G](val graph: G, val tokens: Vector[String])

object GraphWithTokens {

  implicit def ToHTML[G : Graph : ToHtml]: ToHtml[GraphWithTokens[G]] = new ToHtml[GraphWithTokens[G]] {

    import scalatags.Text.all._
    import scalatags.Text.TypedTag
    import AlignmentGraph.Implicits._

    def toHtml(c: GraphWithTokens[G]): TypedTag[String] = {
      val tokens = {
        // Tokens with their sub-scripted indices
        val alignments = Alignments.of(c.graph)

        def isAligned(i: Int): Boolean = alignments.coversToken(i)

        c.tokens.zipWithIndex.flatMap(
          { case (t, i) =>
            val borderAttrib = if (isAligned(i)) "0px" else "2px solid red"
            List(
              span(
                backgroundColor := HighlightAttribute.colours(i), border := borderAttrib,
                stringFrag(t), sub(i)
              ), stringFrag(" ")
            )
          }
        )

      }
      val graph = HasAlignmentsAttribute.annotate(
        HighlightAttribute.annotate(
          c.graph.annotateAlignedTokens(c.tokens)
        )
      )

      table(
        tr(
          td(
            tokens
          )
        ),
        tr(
          td(
            implicitly[ToHtml[G]].toHtml(graph)
          )
        )
      )
    }

  }

}
