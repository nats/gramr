package gramr.graph

import gramr.amr.AmrTree
import gramr.graph.visit.{ConnectedVisitor, DepthFirstSearch}

import scala.language.higherKinds

/** A type class for graph-based meaning representations.
  */
trait Graph[T] {

  // Basic operators

  def isEmpty(graph: T): Boolean

  def getOption(graph: T, index: Int): Option[Indexed[Element]]

  def elements(graph: T): Iterable[Indexed[Element]]

  def root(graph: T): Int

  def rootOption(graph: T): Option[Int]

  def outEdges(graph: T, n: Int): Iterable[Indexed[Edge]]

  def inEdges(graph: T, n: Int): Iterable[Indexed[Edge]]

  /** Compare two meaning graphs disregarding attributes.
    *
    */
  def ~=(graph1: T, graph2: T): Boolean

  def addNode(graph: T, n: Node): (T, Int)

  def addEdge(graph: T, e: Edge): (T, Int)

  /** Removes an element from the graph.
    *
    * Note: When removing a node, it is up to the user to make sure that no edges are adjacent to this node.
    * The graph may end up in an invalid state otherwise.
    *
    * @param graph a graph
    * @param index an element index
    * @return the graph with the element removed
    */
  def remove(graph: T, index: Int): T

  def replaceElement(graph: T, p: Int, replacement: Element): T

  /** Construct the union of two graphs.
    *
    * The element indices of the two graphs are required to be disjoint.
    *
    * @param graph1 a graph
    * @param graph2 another graph
    * @return the union of both graphs
    * @throws NotDisjointException if the indices of the two graphs are not disjunct
    */
  def union(graph1: T, graph2: T): T

  /** Map element contents without changing graph structure.
    *
    * The mapping function may not change edge sources and targets, otherwise the graph's integrity is violated.
    *
    * @param graph a graph
    * @param f a function that modifies elements, without changing edge sources and targets
    * @return a graph
    */
  def mapElementContents(graph: T)(f: Indexed[Element] => Element): T

  def map(graph: T)(f: (Indexed[Element] => Indexed[Element])): T

  // TODO switch to cats
  def traverse[M[_]](graph: T)(f: Indexed[Element] => M[Indexed[Element]])(implicit app: scalaz.Applicative[M]): M[T]

  def filter(graph: T)(pred: Indexed[Element] => Boolean): T

  /**
    * Splits the graph into its connected components.
    *
    * If the graph is empty, it will be returned in a single-element stream. Otherwise, it will be split
    * into all of its non-empty components, i.e. the resulting stream will not contain an empty graph.
    */
  def components(graph: T): Iterable[T]

  def walkTree(graph: T, source: Int): VisitNode

  /**
    * Rebuilds the graph with the specified node as the new focus, i.e. the node at path 0. All node paths are
    * adjusted accordingly.
    *
    * Throws NotConnectedException if the graph is not connected.
    */
  def refocus(graph: T, p: Int): T

  /**
    * Builds the graph in a “standard” form to facilitate == comparisons. Of course, == will never check
    * complete equivalence, but graphs with the same root as well as edge order should be considered
    * equal after being normalised.
    */
  def normalise(graph: T): T

  /**
    * Renames the elements in this graph to not collide with those in the “with” graph
    */
  def preventCollisions(graph: T, wit: T): T


  // Convenience access to elements

  def get(graph: T, p: Int): Indexed[Element] = getOption(graph, p).get

  def nodeOption(graph: T, index: Int): Option[Indexed[Node]] = getOption(graph, index) match {
    case Some(in@Indexed(IsNode(), _)) => Some(in.asInstanceOf[Indexed[Node]])
    case _ => None
  }

  def node(graph: T, index: Int): Indexed[Node] = nodeOption(graph, index).get

  def edgeOption(graph: T, index: Int): Option[Indexed[Edge]] = getOption(graph, index) match {
    case Some(ie@Indexed(IsEdge(), _)) => Some(ie.asInstanceOf[Indexed[Edge]])
    case _ => None
  }

  def edge(graph: T, index: Int): Indexed[Edge] = edgeOption(graph, index).get

  def nodes(graph: T): Iterable[Indexed[Node]] =
    elements(graph).filter(_.element.isNode).map(_.asInstanceOf[Indexed[Node]])

  def edges(graph: T): Iterable[Indexed[Edge]] =
    elements(graph).filter(_.element.isEdge).map(_.asInstanceOf[Indexed[Edge]])

  def externalNodes(graph: T): Iterable[Indexed[ExternalNode]] =
    nodes(graph).filter(_.element.isExternalNode).map(_.asInstanceOf[Indexed[ExternalNode]])

  def isRooted(graph: T): Boolean = rootOption(graph).isDefined

  def adjacentEdges(graph: T, n: Int): Iterable[Indexed[Edge]] = outEdges(graph, n) ++ inEdges(graph, n)

  def neighbours(graph: T, n: Int): Iterable[Indexed[Node]] =
    adjacentEdges(graph, n).map { case Indexed(edge@Edge(_, _, _, _), _) =>
      if(edge.source == n) node(graph, edge.target) else node(graph, edge.source)
    }

  def outDegree(graph: T, n: Int): Int = outEdges(graph, n).size

  def inDegree(graph: T, n: Int): Int = inEdges(graph, n).size

  def degree(graph: T, n: Int): Int = outDegree(graph, n) + inDegree(graph, n)

  def isEquivalentTo(graph: T, other: T): Boolean = new SubGraphOps[T](graph)(this).isEquivalentTo(other)


  // TODO: unify these

  def elementAttributes(graph: T, e: Int): Attributes = get(graph, e).element.attributes

  def nodeAttributes(graph: T, n: Int): Attributes = elementAttributes(graph, n)

  def getNodeAttributes[A](graph: T, n: Int, attr: String): A = node(graph, n).element.attributes.get[A](attr)

  // TODO: partly move these outside of MeaningGraph

  def allEdgesValid(graph: T): Boolean = {
    edges(graph).map(_.element).forall { e =>
      nodeOption(graph, e.source).isDefined && nodeOption(graph, e.target).isDefined }
  }

  def isSane(graph: T): Boolean = allEdgesValid(graph)

  /**
    * Checks rooted connectedness: True iff. all nodes and edges are reachable from the root via forward edges.
    */
  def isConnected(graph: T): Boolean = {
    implicit val self: Graph[T] = this
    val visitor = new ConnectedVisitor(graph)
    DepthFirstSearch(graph, visitor)
    visitor.isConnected
  }

  def isLoopFree(graph: T): Boolean = edges(graph).forall {
    case Indexed(Edge(_, source, target, _), _) =>
      source != target
  }

  def isOpEdge(indexed: Indexed[Edge]): Boolean = indexed.element.label match {
    case opRe(_) => true
    case _ => false
  }

  def opEdgesUnique(graph: T): Boolean = nodes(graph).forall { indexed =>
    val edges = outEdges(graph, indexed.pointer).filter(isOpEdge).map(_.element.label).toList
    edges.distinct.size == edges.size
  }

  def isSaneAMR(graph: T): Boolean =
    isSane(graph) && isConnected(graph) && opEdgesUnique(graph)

  def mapNodes(graph: T)(f: (Indexed[Node] => Indexed[Node])): T =
    map(graph) {
      case in@Indexed(IsNode(), _) => f(in.asInstanceOf[Indexed[Node]])
      case el => el
    }

  def mapEdges(graph: T)(f: (Indexed[Edge] => Indexed[Edge])): T =
    map(graph) {
      case ie@Indexed(IsEdge(), _) => f(ie.asInstanceOf[Indexed[Edge]])
      case el => el
    }

  def mapAttributes(graph: T)(f: Indexed[Element] => Attributes): T =
    mapElementContents(graph) { case iel@Indexed(el, p) => el.setAttributes(f(iel)) }

  def mapNodeAttributes(graph: T)(f: Indexed[Node] => Attributes): T = {
    mapAttributes(graph) { iel =>
      if(iel.element.isNode) {
        f(iel.asInstanceOf[Indexed[Node]])
      }
      else {
        iel.element.attributes
      }
    }
  }

  def mapEdgeAttributes(graph: T)(f: Indexed[Edge] => Attributes): T = {
    mapAttributes(graph) { iel =>
      if(iel.element.isEdge) {
        f(iel.asInstanceOf[Indexed[Edge]])
      }
      else {
        iel.element.attributes
      }
    }
  }

  /**
    * Non-deterministic mapping allows providing several possible outcomes per element.
    * The result is the cross-product of all possible per-element outcomes.
    */
  def mapNonDeterministic(graph: T)(f: Indexed[Element] => Stream[Indexed[Element]]): Stream[T] = {
    import scalaz._, Scalaz._
    traverse(graph)(f)
  }

  def mapAttributesNonDeterministic(graph: T)(f: Indexed[Element] => Stream[Attributes]): Stream[T] =
    mapNonDeterministic(graph) { el => for (a <- f(el)) yield Indexed(el.element.setAttributes(a), el.pointer) }

  def mapNodeAttributesNonDeterministic(graph: T)(f: Indexed[Node] => Stream[Attributes]): Stream[T] = {
    mapAttributesNonDeterministic(graph) { iel =>
      if(iel.element.isNode) {
        f(iel.asInstanceOf[Indexed[Node]])
      }
      else {
        Stream(iel.element.attributes)
      }
    }
  }

  def mapEdgeAttributesNonDeterministic(graph: T)(f: Indexed[Edge] => Stream[Attributes]): Stream[T] = {
    mapAttributesNonDeterministic(graph) { iel =>
      if(iel.element.isEdge) {
        f(iel.asInstanceOf[Indexed[Edge]])
      }
      else {
        Stream(iel.element.attributes)
      }
    }
  }

  def clearAttributes(graph: T): T = mapAttributes(graph) { _ => Attributes(Map.empty) }

  def clearAttribute(graph: T, name: String): T = mapAttributes(graph) { el => el.element.attributes - name }

  def renameAttribute(graph: T, name: String, newName: String): T = mapAttributes(graph) { el =>
    el.element.attributes.getOption[Attribute](name) map { value =>
      el.element.attributes - name + (newName -> value)
    } getOrElse {
      el.element.attributes
    }
  }

  /**
    * A meaning graph’s kernel is the graph with all external nodes and their adjacent edges removed.
    */
  def kernel(graph: T): T = filter(graph) { el: Indexed[Element] =>
    el.element match {
      case ExternalNode(_, _) => false
      case Edge(_, source, target, _) =>
        !(node(graph, source).element.isExternalNode
          || node(graph, target).element.isExternalNode)
      case _ => true
    }
  }

  /** Groups external nodes by their indices.
    */
  def externalLayers(graph: T): Map[Int, Vector[Indexed[Node]]] =
    externalNodes(graph).toVector.groupBy(_.element.reference.index)

  /** Counts the layers of external nodes, i.e., the graph’s semantic arity.
    */
  def externalLayerCount(graph: T): Int = externalLayers(graph).size

  def findExternalNode(graph: T, depth: Int): Option[Int] = {
    val layers = externalLayers(graph)
    val layerIndices = layers.keys.toVector.distinct.sortBy(-_)
    for {
      layer <- if (depth >= layerIndices.size) None else Some(layers(layerIndices(depth)))
      node <- if (layer.size != 1) None else Some(layer.head.pointer)
    } yield node

  }





  // toString template
  def graphToString(graph: T): String = {
    implicit val mgl: Graph[T] = this
    import AmrTreeConversion._
    components(graph) map { c => if (isEmpty(graph)) "ε" else c.toAmrTree.toString } mkString "\n"
  }


  // Class methods

  def empty: T

  def fromIndexedElements(
    els: Iterable[Indexed[Element]],
    root: Option[Int] = None): T

  def builder: GraphBuilder[T]

  /** Creates a builder that already contains a given graph.
    */
  def builder(graph: T, normalise: Boolean = false): GraphBuilder[T]


  import gramr.util.Reader, Reader._

  type Building[A] = Reader[GraphBuilder[T], A]

  object Building extends Serializable {

    def mkReferentNode(concept: String, attributes: Attributes = Attributes.empty): Building[Int] =
      for (b <- ask) yield b.addReferentNode(concept, attributes)

    def mkConstantNode(concept: String, attributes: Attributes = Attributes.empty): Building[Int] =
      for (b <- ask) yield b.addConstantNode(concept, attributes)

    def mkExternalNode(
      reference: ExternalReference, attributes: Attributes = Attributes.empty
    ): Building[Int] =
      for (b <- ask) yield b.addExternalNode(reference, attributes)

    def mkEdge(label: String, source: Int, target: Int, attributes: Attributes = Attributes.empty): Building[Int] =
      for (b <- ask) yield b.addEdge(Edge(label, source, target, attributes))

    def remove(pointer: Int): Building[Unit] =
      for (b <- ask) yield b.remove(pointer)

    def setRoot(pointer: Int): Building[Unit] =
      for (b <- ask) yield b.setRoot(pointer)

    def normalise(value: Boolean): Building[Unit] =
      for (b <- ask) yield b.setNormalise(value)

    import scalaz.Applicative

    implicit def applicative: Applicative[Building] = new Applicative[Building] {
      def point[A](a: => A): Building[A] = Reader(_ => a)

      def ap[A, B](fa: => Building[A])(f: => Building[A => B]): Building[B] = for {
        b <- ask
      } yield {
        val ff = f.run(b)
        val a = fa.run(b)
        ff(a)
      }
    }
  }

  def build(f: Building[Unit]): T = buildReturning(f)._1

  def buildReturning[A](f: Building[A]): (T, A) = {
    val bldr = builder
    val a = f.run(bldr)
    (bldr.build, a)
  }


  def ordering: Ordering[T] = new Ordering[T] {

    import gramr.util.StreamUtils._

    implicit val indexedOrdering: Ordering[Indexed[Element]] =
      Indexed.ordering[Element]

    def compare(x: T, y: T): Int =
      lexicographicOrdering[Indexed[Element]].compare(
        elements(x).toStream,
        elements(y).toStream
      )
  }

  def fromAmrTree: FromAmrTree[T] = new GraphFromAmrTree[T]()(this)

}


object Graph {

  import scala.language.implicitConversions

  object ops {
    implicit def toGraphOps[T](target: T)(implicit tc: Graph[T]): GraphOps[T] = new GraphOps[T] {
      val self = target
      val typeClassInstance = tc
    }
  }

  def fromAmrTree[MR : Graph](amr: AmrTree): MR = implicitly[Graph[MR]].fromAmrTree(amr)

}
