package gramr.jobs

import monix.reactive.Observable

case class ChainedStage[A, B, C](
  first: Stage[A, B],
  second: Stage[B, C]
) extends Stage[A, C] {

  def observable(input: Observable[A]): Observable[C] = {
    second.observable(first.observable(input))
  }

  def addListener(listener: Listener[C]): Unit = second.addListener(listener)

  override def message(m: Listener[_] => Unit): Unit = {
    first.message(m)
    second.message(m)
  }

}
