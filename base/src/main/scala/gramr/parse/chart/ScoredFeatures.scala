package gramr.parse.chart

import gramr.learn.FeatureVector

case class ScoredFeatures[FV: FeatureVector](
  features: FV,
  score: Double
)

object ScoredFeatures {

  /** A ChartLabel implementation that selects the single max-scoring derivation.
    *
    * The add operator forms the sum of features and scores, while the mul operator selects
    * the operand with the maximum score.
    */
  implicit def maxScoring[FV : FeatureVector]: ChartLabel[ScoredFeatures[FV]] = new ChartLabel[ScoredFeatures[FV]] {

    import FeatureVector.Syntax._

    override def score(l: ScoredFeatures[FV]): Double = l.score

    override def zero: ScoredFeatures[FV] = ScoredFeatures(implicitly[FeatureVector[FV]].empty, 0.0)

    override def add(l: ScoredFeatures[FV], r: ScoredFeatures[FV]): ScoredFeatures[FV] = {
      ScoredFeatures(l.features + r.features, l.score + r.score)
    }

    override def subtract(l: ScoredFeatures[FV], r: ScoredFeatures[FV]): ScoredFeatures[FV] = {
      ScoredFeatures(l.features - r.features, l.score - r.score)
    }

    override def mul(l: ScoredFeatures[FV], r: ScoredFeatures[FV]): ScoredFeatures[FV] = {
      if(r.score > l.score) r else l
    }

    override def inheritablePortion(l: ScoredFeatures[FV]): ScoredFeatures[FV] = l

  }

  implicit def hasFeatures[FV: FeatureVector]: HasFeatures[FV, ScoredFeatures[FV]] = (l: ScoredFeatures[FV]) => l.features

  implicit def orderByScore[FV : FeatureVector]: Ordering[ScoredFeatures[FV]] =
    (x: ScoredFeatures[FV], y: ScoredFeatures[FV]) => x.score.compareTo(y.score)

}
