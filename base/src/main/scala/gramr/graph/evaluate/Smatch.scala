package gramr.graph.evaluate

import gramr.graph._
import gramr.util._
import gramr.graph.Graph.ops._

import scala.collection.immutable.IndexedSeq

/**
  * Implements the random-initialisation hill-climbing method presented in [Cai and Knight, 2013].
  */
class Smatch[G](implicit meaningGraphLike: Graph[G]) {

  def randomInit(repeats: Int, g1: G, g2: G)
  : (Mapping, SimpleRetrievalStats) = {
    val solvers = (1 to repeats) map { _ => new RandomInitHillClimber(g1, g2) }
    val combinedSolver = new CombinedSolver(solvers.toList, g1, g2)
    val mapping = combinedSolver.bestMapping
    (mapping, combinedSolver.evaluate(mapping))
  }

  def smartInit(g1: G, g2: G): (Mapping, SimpleRetrievalStats) = {
    val solver = new SmartInitHillClimber(g1, g2)
    val mapping = solver.bestMapping
    (mapping, solver.evaluate(mapping))
  }

  def smartAndRandomInit(
    smartRepeats: Int,
    randomRepeats: Int,
    g1: G,
    g2: G
  ): (Mapping, SimpleRetrievalStats) = {
    val smartSolvers = (1 to smartRepeats) map { _ => new SmartInitHillClimber(g1, g2) }
    val randomSolvers = (1 to randomRepeats) map { _ => new RandomInitHillClimber(g1, g2) }
    val combinedSolver = new CombinedSolver((smartSolvers ++ randomSolvers).toList, g1, g2)
    val mapping = combinedSolver.bestMapping
    (mapping, combinedSolver.evaluate(mapping))
  }

  def tripleCount(g: G): Int = {
    val solver = new RandomInitHillClimber(g, g)
    solver.grtriples.size
  }

  case class Mapping(
    g1NodeMapping: Map[Int, Int],
    g2NodeMapping: Map[Int, Int],
    mapping: Array[Option[Int]]
  ) {

    def mapNode(node: Int): Option[Int] =
      if (node < mapping.length) mapping(node) else None

    def apply(t: Triple): MappedTriple = MappedTriple.fromOption(applyOption(t))

    def applyOption(triple: Triple): Option[Triple] = triple match {
      case Relation(l, s, t) => for {
        ms <- mapNode(s)
        mt <- mapNode(t)
      } yield Relation(l, ms, mt)
      case Attribute(l, s, t, tn) => for {
        ms <- mapNode(s)
        mt <- mapNode(tn)
      } yield Attribute(l, ms, t, mt)
      case Instance(l, s) => for {
        ms <- mapNode(s)
      } yield Instance(l, ms)
    }

    def mappedTriples(ts: Set[Triple]): Set[MappedTriple] = for {
      unmappedTriple <- ts
      mappedTriple = apply(unmappedTriple)
    } yield mappedTriple

    def neighbours: IndexedSeq[Mapping] = for {
      i <- 0 until (mapping.length - 1)
      j <- (i + 1) until mapping.length
      newMapping = mapping.indices map { k =>
        if (k == i) mapping(j) else if (k == j) mapping(
          i
        ) else mapping(k)
      }
    } yield Mapping(g1NodeMapping, g2NodeMapping, newMapping.toArray)

    override def toString: String = {
      val nodePairs = for {
        (np1, idx1) <- g1NodeMapping
        (np2, idx2) <- g2NodeMapping if mapping(idx1).contains(idx2)
      } yield s"$np1 -> $np2"
      nodePairs.mkString(", ")
    }

  }

  sealed trait Triple

  case class Relation(label: String, source: Int, target: Int) extends Triple {
    override val hashCode: Int = scala.util.hashing.MurmurHash3.productHash(this)
  }

  case class Attribute(label : String, source : Int, target : String, targetNode: Int) extends Triple {
    override val hashCode: Int = scala.util.hashing.MurmurHash3.productHash(this)
  }

  case class Instance(label: String, source: Int) extends Triple {
    override val hashCode: Int = scala.util.hashing.MurmurHash3.productHash(this)
  }

  sealed trait MappedTriple

  object MappedTriple {
    def mapped(t: Triple): MappedTriple = Mapped(t)

    def fromOption(o: Option[Triple]): MappedTriple = o match {
      case Some(t) => Mapped(t)
      case None => Unmapped()
    }
  }

  case class Mapped(triple: Triple) extends MappedTriple

  case class Unmapped() extends MappedTriple

  def triples(g: G, nodeMapping: Map[Int, Int]): Set[Triple] = {
    val edgeTriples = g.edges.map(_.element.unflip) map { e =>
      g.node(e.target).element match {
        case _ : ReferentNode => Relation(e.label, nodeMapping(e.source), nodeMapping(e.target))
        case cn : ConstantNode => Attribute(e.label, nodeMapping(e.source), cn.name, nodeMapping(e.target))
        case _ : ExternalNode => Relation(e.label, nodeMapping(e.source), nodeMapping(e.target))
      }
    }
    val instanceTriples: Iterable[Triple] = for {
      Indexed(n, pointer) <- g.nodes
      concept <- n match {
        case ReferentNode(c, _) => Some(c)
        case _ => None
      }
    } yield Instance(concept, nodeMapping(pointer))
    val topTriple = g.rootOption.map { root =>
      Attribute("TOP", nodeMapping(root), g.node(root).element.content, nodeMapping(g.root))
    }.toSet
    topTriple ++ edgeTriples.toSet ++ instanceTriples.toSet
  }

  trait Solver {
    def bestMapping: Mapping

    def g1: G

    def g2: G

    val (gl, gr) = (g1, g2)

    lazy val glNodeMapping: Map[Int, Int] = gl.nodes.map(_.pointer).zipWithIndex.toMap
    lazy val grNodeMapping: Map[Int, Int] = gr.nodes.map(_.pointer).zipWithIndex.toMap

    lazy val gltriples: Set[Triple] = triples(gl, glNodeMapping)
    lazy val grtriples: Set[Triple] = triples(gr, grNodeMapping)

    def mappedTriples(m: Mapping): (Set[MappedTriple], Set[MappedTriple]) =
      (m.mappedTriples(gltriples), grtriples.map(MappedTriple.mapped(_)))

    def matchingTriples(m: Mapping): Int = {
      val (tl, tr) = mappedTriples(m)
      (tl & tr).size
    }

    def evaluate(m: Mapping): SimpleRetrievalStats = {
      val (tl, tr) = mappedTriples(m)
      val correct = tl & tr
      SimpleRetrievalStats(tr.size, correct.size, (tl -- correct).size)
    }

  }

  class CombinedSolver(
    val solvers: List[Solver],
    val g1: G,
    val g2: G
  ) extends Solver {

    def bestMapping: Mapping = {
      val mappings = solvers.map { s => s.bestMapping }
      mappings.maxBy(m => evaluate(m).f1)
    }

  }

  trait HillClimber extends Solver {

    def initialMapping: Mapping

    @scala.annotation.tailrec
    final def improveMapping(mapping: Mapping, mcurrentScore: Option[Int] = None): Mapping = {
      val currentScore = mcurrentScore match {
        case Some(s) => s
        case None => matchingTriples(mapping)
      }
      val ns = mapping.neighbours
      val (newScore, bestNeighbour) =
        if (ns.isEmpty) {
          (currentScore, mapping)
        }
        else {
          ns.view.map(n => (matchingTriples(n), n)).maxBy(_._1)
        }
      if (newScore > currentScore)
        improveMapping(bestNeighbour)
      else
        mapping
    }

    def bestMapping: Mapping = improveMapping(initialMapping)

  }

  class RandomInitHillClimber(
    val g1: G,
    val g2: G
  ) extends HillClimber {

    def initialMapping: Mapping = {
      import scala.util.Random
      val r = new Random(1)

      val rNodes = gr.nodes.toVector
      val mappingSize = Math.max(glNodeMapping.size, grNodeMapping.size)
      val sequentialMapping = (0 until mappingSize) map { i =>
        if (i < rNodes.size)
          Some(grNodeMapping(rNodes(i).pointer))
        else
          None
      }

      Mapping(glNodeMapping, grNodeMapping, r.shuffle(sequentialMapping).toArray)
    }

  }

  class SmartInitHillClimber(
    val g1: G,
    val g2: G
  ) extends HillClimber {

    import scala.util.Random

    def initialMapping: Mapping = {
      val lNodes = gl.nodes.toVector
      val rNodes = gr.nodes.map(n => (grNodeMapping(n.pointer), n)).toMap

      // Find a partner for every left node.
      // mappedNodes is a map containing matching pairs between left and right nodes.
      // unmappedRNodes is a set of right nodes that couldn’t be matched.
      val startAcc = (Map.empty[Int, Int], rNodes.keySet)
      val (mappedNodes, unmappedRNodes) = lNodes.foldLeft(startAcc) { (acc, lNode) =>
        acc._2.find { rni => rNodes(rni).element.content == lNode.element.content } match {
          case Some(rni) =>
            val newMap = acc._1 + (glNodeMapping(lNode.pointer) -> rni)
            (newMap, acc._2 - rni)
          case None => acc
        }
      }

      // Assign left node indices to unmapped right nodes.
      // This works by taking the unused indices in the node list and shuffling them around.
      val mappingSize = Math.max(glNodeMapping.size, grNodeMapping.size)
      val random = new Random(1)
      val unmappedIndices = random.shuffle(((0 until mappingSize).toSet -- mappedNodes.keySet).toVector)
      val unmappedIndexedNodes: Map[Int, Int] = (unmappedIndices zip unmappedRNodes).toMap

      // The mappings we have determined so far.
      val nodePairs = mappedNodes ++ unmappedIndexedNodes

      val mapping = (0 until mappingSize) map { i =>
        nodePairs.lift(i)
      }
      Mapping(glNodeMapping, grNodeMapping, mapping.toArray)
    }
  }


}
