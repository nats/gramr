# archive

This directory contains settings files for old versions of gramr. Some of them (e.g. `German.scala`) implement
things that are currently otherwise undocumented, therefore the files are preserved.

All of these files are outdated and won't work with current versions of gramr. They are here so that those that are
still relevant can be adapted in the future.
