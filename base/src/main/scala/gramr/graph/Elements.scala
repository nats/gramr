package gramr.graph


sealed trait Element extends Serializable {
  def attributes: Attributes

  def setAttributes(a: Attributes): Element

  //noinspection MutatorLikeMethodIsParameterless
  def deleteAttributes: Element = setAttributes(Attributes.empty)

  def isNode: Boolean = false

  def isEdge: Boolean = false

  /** Compares two elements disregarding attributes.
    */
  def ~=(other: Element): Boolean
}


sealed trait Node extends Element {

  override def attributes: Attributes

  def setAttributes(a: Attributes): Node

  /**
    * A string describing the node’s semantic significance.
    *
    * For referent and concept nodes, this is the node’s concept.
    */
  def content: String

  override def isNode: Boolean = true

  def isReferentNode: Boolean = false

  def isConstantNode: Boolean = false

  def isExternalNode: Boolean = false

}


/** Extractor object for nodes that just checks whether an element is a node.
  *
  * This is provided so that no more specific type needs to be provided and the number of arguments
  * is left open.
  */
object IsNode {

  def unapply(e: Node): Boolean = e.isNode

}


@SerialVersionUID(1L)
case class ReferentNode(
  concept: String,
  override val attributes: Attributes = Attributes.empty
) extends Node {

  def setAttributes(a: Attributes): ReferentNode = copy(attributes = a)

  def content: String = concept

  override def isReferentNode = true

  override def ~=(other: Element): Boolean = other match {
    case ReferentNode(tconcept, _) => concept == tconcept
    case _ => false
  }
}


@SerialVersionUID(1L)
case class ConstantNode(
  name: String,
  override val attributes: Attributes = Attributes.empty
) extends Node {

  def setAttributes(a: Attributes): ConstantNode = copy(attributes = a)

  def content: String = name

  override def isConstantNode = true

  override def ~=(other: Element): Boolean = other match {
    case ConstantNode(tname, _) => name == tname
    case _ => false
  }
}


@SerialVersionUID(1L)
case class ExternalNode(
  reference: ExternalReference,
  override val attributes: Attributes = Attributes.empty
) extends Node {

  def setAttributes(a: Attributes): ExternalNode = copy(attributes = a)

  def content = "<ext>"

  override def isExternalNode = true

  override def ~=(other: Element): Boolean = other match {
    case ExternalNode(tref, _) => reference == tref
    case _ => false
  }
}


@SerialVersionUID(1L)
case class Edge(
  label: String,
  source: Int,
  target: Int,
  override val attributes: Attributes = Attributes.empty
)
  extends Element with Ordered[Edge] {

  def setSource(s: Int): Edge = copy(source = s)

  def setTarget(t: Int): Edge = copy(target = t)

  override def isEdge: Boolean = true

  def isReversed: Boolean =
    label.endsWith("-of") && label != "consists-of"

  def flip: Edge = {
    val newLabel =
      if (isReversed)
        label.substring(0, label.length - 3)
      else
        label + "-of"
    copy(label = newLabel, source = target, target = source)
  }

  /**
    * If the edge is reversed, flips it back to its logical direction.
    */
  def unflip: Edge = if (isReversed) flip else this

  /**
    * Flips the edge if its source node is ordered after its target node.
    */
  def normaliseDirection: Edge =
    if (source > target) flip else this

  def setAttribute(name: String, value: Attribute): Edge = copy(attributes = attributes + (name -> value))

  def setAttributes(a: Attributes): Edge = copy(attributes = a)

  def compare(b: Edge): Int = {
    val sourceCmp = Integer.compare(source, b.source)
    if (sourceCmp != 0) sourceCmp
    else {
      val targetCmp = Integer.compare(target, b.target)
      if (targetCmp != 0) targetCmp
      else {
        label.compare(b.label)
      }
    }
  }

  override def ~=(other: Element): Boolean = other match {
    case Edge(tlabel, tsource, ttarget, _) =>
      label == tlabel && source == tsource && target == ttarget
    case _ => false
  }
}


/** Extractor object for edges that just checks whether an element is an edge.
  *
  * This is provided so that the number of arguments is left open.
  */
object IsEdge {

  def unapply(e: Edge): Boolean = e.isEdge

}

