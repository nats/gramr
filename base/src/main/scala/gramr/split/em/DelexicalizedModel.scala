package gramr.split.em

import gramr.ccg.{IndexedLexicalItem, StoredLexicon, SyntacticCategory}
import gramr.graph.Graph
import gramr.graph.lexemes.{Lexeme, LexemeDictionary}
import gramr.split.SplitChart
import gramr.text.{Span, Token}
import gramr.util.{MultiMap, SparseVector}

import scala.util.Random
import Graph.ops._
import breeze.numerics.log
import com.typesafe.scalalogging.LazyLogging
import gramr.split.em.DelexicalizedEstimator.Parameters

case class DelexicalizedModel[MR : Graph](
  parameters: Parameters,
  lexicon: StoredLexicon[MR],
  lexemes: LexemeDictionary,
  maxLexNodes: Int
) extends LazyLogging {

  import DelexicalizedModel._

  val templateIds: MultiMap[(SyntacticCategory, MR), IndexedLexicalItem[MR]] = MultiMap.from {
    lexicon.items.map { item => ((item.synCat.eraseFeatures, item.meaning.clearAttributes), item) }
  }

  def score(entry: SplitChart.Entry[MR], chart: SplitChart[MR]): Double = {
    val tokens = entry.span.in(chart.sentence)

    val scores = for {
      (lexeme, template) <- Lexeme.delexicalize(maxLexNodes)(tokens.map(_.text.toLowerCase), entry.partialMR.mr)
    } yield scoreLexemeTemplatePair(chart)(entry, lexeme, template)
    scores.sum
  }

  /** Finds the best lexeme-template pair that can be derived from an entry.
    */
  def bestDelexicalization(entry: SplitChart.Entry[MR], chart: SplitChart[MR]): (Double, Lexeme, MR) = {
    val tokens = entry.span.in(chart.sentence)

    val scored = for {
      (lexeme, template) <- Lexeme.delexicalize(maxLexNodes)(tokens.map(_.text.toLowerCase), entry.partialMR.mr)
    } yield (scoreLexemeTemplatePair(chart)(entry, lexeme, template), lexeme, template)

    scored.maxBy(_._1)
  }

  def count(count: Double, entry: SplitChart.Entry[MR], chart: SplitChart[MR]): Parameters = {
    // We delexicalize the meaning representation and weight each possible delexicalization according to its score
    val tokens = entry.span.in(chart.sentence)
    val parameters = for {
      (lexeme, template) <- Lexeme.delexicalize(maxLexNodes)(tokens.map(_.text.toLowerCase), entry.partialMR.mr)
      synCat <- entry.partialMR.synCats.toVector
      weight = scoreLexemeTemplatePair(chart)(entry, lexeme, template)
      tid <- templateIds(entry, template)
      lid <- lexemeId(tokens, lexemes)(lexeme)
    } yield Parameters(
      SparseVector(Map(tid -> weight)),
      SparseVector(Map(lid -> weight))
    )

    val sum = parameters.foldLeft(Parameters.empty)(_ add _)
    val norm = sum.normalize
    val scaled = norm.scale(count)
    scaled
  }

  /** Gives the combined probability for a template and a lexeme.
    *
    * Returns 0 if either the template or the lexeme are not in the lexicon.
    */
  def scoreLexemeTemplatePair(chart: SplitChart[MR])(entry: SplitChart.Entry[MR], lexeme: Lexeme, template: MR): Double = {
    val tokens = entry.span.in(chart.sentence)
    val tmplIds = templateIds(entry, template)  // there can be several or no template ID (the latter can be true if the template has been filtered out beforehand)
    val lexId = lexemeId(tokens, lexemes)(lexeme)  // there can be no lexeme ID if the lexeme has been filtered out beforehand
    val pairedScores = for {
      templateId <- tmplIds
      lexemeId <- lexId
    } yield parameters.templateParameters(templateId) * parameters.lexemeParameters(lexemeId)
    pairedScores.sum
  }

  def update(counts: Parameters): DelexicalizedModel[MR] = {
    val ncounts = counts.normalize
    DelexicalizedModel[MR](ncounts, lexicon, lexemes, maxLexNodes)
  }

  /** Returns the IDs of all templates matching a lexical entry and a delexicalized meaning representation.
    */
  def templateIds(entry: SplitChart.Entry[MR], template: MR): Iterable[Int] = {
    for {
      synCat <- entry.partialMR.synCats
      item <- templateIds.get((synCat.eraseFeatures, template.clearAttributes)) if (synCat matches item.synCat) && (template ~= item.meaning)
    } yield item.index
  }

  /** Returns the set of (lexeme, template) pairs that correspond to the best full analysis of the sentence. */
  def bestSplit(chart: SplitChart[MR]): Iterable[(SplitChart.Entry[MR], Lexeme, MR)] = {
    case class ScoreChartEntry(logProb: Double, entries: Vector[(SplitChart.Entry[MR], Lexeme, MR)])

    // Maps chart entry IDs to log probabilities
    val scoreCache = collection.mutable.Map.empty[Int, ScoreChartEntry].withDefault { _ =>
      ScoreChartEntry(Double.NegativeInfinity, Vector.empty)
    }

    for {
      len <- chart.sentence.indices
      i <- 0 until chart.sentence.length - len
      j = i + len
      entry <- chart.get(Span(i, j))
    } {
      val cachedLogProb = scoreCache(entry.id).logProb
      val splits = chart.splits.filter(_.parentId == entry.id)
      for(split <- splits) {
        val entries = split.childIds.map(scoreCache)
        val combinedScore = entries.map(_.logProb).sum
        val combinedChildren = entries.toVector.flatMap(_.entries)
        // Enter any combined entry even if it has 0 prob to avoid gaps in the output
        if(combinedScore > cachedLogProb || cachedLogProb == Double.NegativeInfinity) {
          scoreCache(entry.id) = ScoreChartEntry(combinedScore, combinedChildren)
        }
      }

      val (entryScore, lexeme, template) = bestDelexicalization(entry, chart)
      val entryLogProb = log(entryScore)
      // Enter any single-token entry even if it has 0 prob to avoid gaps in the output
      if(entry.partialMR.span.size == 1 && (entryLogProb > cachedLogProb || cachedLogProb == Double.NegativeInfinity)) {
        scoreCache(entry.id) = ScoreChartEntry(entryLogProb, Vector((entry, lexeme, template)))
      }
    }

    val rootIds = chart.get(Span(0, chart.sentence.length - 1)).map(_.id)
    val bestRoot = rootIds.maxBy(root => scoreCache(root).logProb)
    val best = scoreCache(bestRoot).entries
    best
  }

  /** Override toString to speed up debugging.
    */
  override def toString: String = "DelexicalizedModel"

}

object DelexicalizedModel extends LazyLogging {

  import scala.language.implicitConversions

  def randomUniform[MR : Graph](lexicon: StoredLexicon[MR], lexemes: LexemeDictionary, maxLexNodes: Int): DelexicalizedModel[MR] = {
    val rnd = new Random()

    val tmpParametersMap = lexicon.items.map(item => (item.index, rnd.nextDouble())).toMap
    val tmpParametersSum = tmpParametersMap.values.sum
    val tmpParameters = SparseVector(tmpParametersMap.mapValues(v => v / tmpParametersSum).withDefault(_ => 0.0))
    val lexParametersMap = lexemes.items.map(item => (item.id, rnd.nextDouble())).toMap
    val lexParametersSum = lexParametersMap.values.sum
    val lexParameters = SparseVector(lexParametersMap.mapValues(v => v / lexParametersSum).withDefault(_ => 0.0))
    DelexicalizedModel(Parameters(tmpParameters, lexParameters), lexicon, lexemes, maxLexNodes)
  }

  def lexemeId[MR](tokens: Seq[Token], lexemes: LexemeDictionary)(lexeme: Lexeme): Option[Int] = {
    lexemes.get(tokens).find(_.lexeme == lexeme).map(_.id)
  }


  implicit def emModel[MR]: EMModel[DelexicalizedModel[MR], MR, Parameters] = new EMModel[DelexicalizedModel[MR], MR, Parameters] {

    override def ops(model: DelexicalizedModel[MR]) = emModelOps(model)

    /** Produces a count object for a given split chart entry.
      *
      * @param count fractional count weight assigned to the entry
      * @param entry the entry itself
      * @param chart the sentence's chart
      * @return a count object
      */
    override def count(model: DelexicalizedModel[MR], chart: SplitChart[MR])(count: Double, entry: SplitChart.Entry[MR]): Parameters = {
      model.count(count, entry, chart)
    }

    /** Updates the model parameters given a count object.
      *
      * @param counts the counts to add to the model
      * @return the updated model
      */
    override def update(model: DelexicalizedModel[MR], counts: Parameters) = model.update(counts)

    override def emptyCounts = Parameters.empty

    override def addCounts(c1: Parameters, c2: Parameters) = c1.add(c2)
  }

  def emModelOps[MR](model: DelexicalizedModel[MR]): EMModelOps[DelexicalizedModel[MR], MR, Parameters] =
    new EMModelOps[DelexicalizedModel[MR], MR, Parameters] {
      override def score(entry: SplitChart.Entry[MR], chart: SplitChart[MR]) = model.score(entry, chart)

      override def update(counts: Parameters) = model.update(counts)
    }

}