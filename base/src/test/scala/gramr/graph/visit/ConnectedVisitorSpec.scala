package gramr.graph.visit

import gramr.graph.{AdjacencyMeaningGraph, Graph}
import org.scalatest.FlatSpec

class ConnectedVisitorSpec extends FlatSpec {

  implicit val graphInstance: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance
  import graphInstance.Building._

  "ConnectedVisitor" should "detect the connectedness of a simple graph" in {
    val simpleGraph = graphInstance.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
      } yield ()
    }

    val visitor = new ConnectedVisitor(simpleGraph)
    DepthFirstSearch(simpleGraph, visitor)
    assert(visitor.isConnected)
  }

  it should "detect the connectedness of a graph with backward edges" in {
    val withBackward = graphInstance.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ca", c, a)
      } yield ()
    }

    val visitor = new ConnectedVisitor(withBackward)
    DepthFirstSearch(withBackward, visitor)
    assert(visitor.isConnected)
  }

  it should "detect the connectedness of non-tree-shaped graphs" in {
    val doubleDiamond = graphInstance.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        d <- mkReferentNode("d")
        e <- mkReferentNode("e")
        f <- mkReferentNode("f")
        g <- mkReferentNode("g")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
        _ <- mkEdge("bd", b, d)
        _ <- mkEdge("cd", c, d)
        _ <- mkEdge("de", d, e)
        _ <- mkEdge("df", d, f)
        _ <- mkEdge("eg", e, g)
        _ <- mkEdge("eg", f, g)
      } yield ()
    }

    val visitor = new ConnectedVisitor(doubleDiamond)
    DepthFirstSearch(doubleDiamond, visitor)
    assert(visitor.isConnected)
  }

  it should "detect an unconnected graph" in {
    val disconnected = graphInstance.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        // The "d, e, f" component is unreachable from a
        d <- mkReferentNode("d")
        e <- mkReferentNode("e")
        f <- mkReferentNode("f")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
        _ <- mkEdge("de", d, e)
        _ <- mkEdge("df", d, f)
        _ <- normalise(false)
      } yield (a, b, c)
    }

    val visitor = new ConnectedVisitor(disconnected)
    DepthFirstSearch(disconnected, visitor)
    assert(!visitor.isConnected)
  }

}
