package gramr.text

case class Token(
  text: String,
  lemma: String,
  coarsePos: Option[String] = None
) {

  lazy val lowerCased: String = text.toLowerCase

  override def toString: String = text

}

object Token {

  object Implicits {

    import scala.language.implicitConversions

    implicit def stringToToken(s: String): Token = Token(s, s)

  }

}