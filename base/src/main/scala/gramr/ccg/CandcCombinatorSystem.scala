package gramr
package ccg

class CandcCombinatorSystem extends CombinatorSystem {

  import CandcCombinatorSystem._

  def combinators: Iterable[Combinator] = Iterable(
    FA(), BA(), FC(), BC(), FXC(), BXC(), TR(), LTC(), RTC(),
    LP(), RP(), RC(), FX(), BX(), GBX(), GFC(), Lex(), Conj(), Funny()
  )

  def synApply(combinator: Combinator, arguments: SyntacticCategory*): Option[SyntacticCategory] =
    combinator.synApply(arguments: _*)


}

object CandcCombinatorSystem {

  import scalatags.Text.all._

  // Note: The arities of some rarer combinators are omitted because I want to look at their usage first
  sealed trait CandcCombinator extends ccg.Combinator {
    def arity: Int

    def headIndex: Int

    def display: Frag

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = ???

    protected def toFrag(frags: Frag*): Frag = SeqFrag(frags)
  }

  case class FA() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    def display = ">"

    override def toString = "fa"
  }

  case class BA() extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    def display = "<"

    override def toString = "ba"
  }

  case class FC() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    def display: _root_.scalatags.Text.all.Frag = toFrag(">", b("B"))

    override def toString = "fc"
  }

  case class BC() extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    def display: _root_.scalatags.Text.all.Frag = toFrag("<", b("B"))

    override def toString = "bc"
  }

  case class FXC() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    def display: _root_.scalatags.Text.all.Frag = toFrag(">", b("B"), sub("×"))

    override def toString = "fxc"
  }

  case class BXC() extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    def display: _root_.scalatags.Text.all.Frag = toFrag("<", b("B"), sub("×"))

    override def toString = "bxc"
  }

  case class GFC(n: Int = 0) extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    def display: _root_.scalatags.Text.all.Frag = toFrag(">", b("B"), sup(i(n)))

    override def toString = "gfc"
  }

  case class GBC(n: Int = 0) extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    def display: _root_.scalatags.Text.all.Frag = toFrag("<", b("B"), sup(i(n)))

    override def toString = "gbc"
  }

  case class GFXC(n: Int) extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    def display: _root_.scalatags.Text.all.Frag = toFrag(">", b("B"), sup(i(n), sub("×")))

    override def toString = "gfxc"
  }

  case class GBXC(n: Int) extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    def display: _root_.scalatags.Text.all.Frag = toFrag("<", b("B"), sup(i(n), sub("×")))

    override def toString = "gbxc"
  }

  // TR subsumes FTR and BTR. It’s a laziness hack, as C&C doesn’t directly indicate the direction of the type raising
  // (which is however easy to read off the produced category).
  sealed class TR() extends CandcCombinator {
    def arity = 1

    def headIndex = 0

    def display: Frag = b("T")

    override def toString = "tr"
  }

  object TR {
    def apply(): TR = new TR()
  }

  case class FTR() extends TR() {
    override def display: _root_.scalatags.Text.all.Frag = toFrag(">", b("T"))

    override def toString = "ftr"
  }

  case class BTR() extends TR() {
    override def display: _root_.scalatags.Text.all.Frag = toFrag("<", b("T"))

    override def toString = "btr"
  }

  case class LTC() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    def display: _root_.scalatags.Text.all.Frag = toFrag("ltc")

    override def toString = "ltc"
  }

  case class RTC() extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    def display: _root_.scalatags.Text.all.Frag = toFrag("rtc")

    override def toString = "rtc"
  }

  case class Conj() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    // I think… hope this is consistent
    def display: _root_.scalatags.Text.all.Frag = toFrag("conj")

    override def toString = "conj"
  }

  case class FS() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    def display: _root_.scalatags.Text.all.Frag = toFrag(">", b("S"))

    override def toString = "fs"
  }

  case class BS() extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    def display: _root_.scalatags.Text.all.Frag = toFrag("<", b("S"))

    override def toString = "bs"
  }

  case class FXS() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    def display: _root_.scalatags.Text.all.Frag = toFrag(">", b("S"), sub("×"))

    override def toString = "fxs"
  }

  case class BXS() extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    def display: _root_.scalatags.Text.all.Frag = toFrag("<", b("S"), sub("×"))

    override def toString = "bxs"
  }

  // used for punctuation by C&C
  case class LP() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    override def display: _root_.scalatags.Text.all.Frag = toFrag("lp")

    override def toString = "lp"
  }

  case class RP() extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    override def display: _root_.scalatags.Text.all.Frag = toFrag("rp")

    override def toString = "rp"
  }

  // used for commas by C&C
  case class RC() extends CandcCombinator {
    def arity = ???

    def headIndex = ???

    override def display: _root_.scalatags.Text.all.Frag = toFrag("rc")

    override def toString = "rc"
  }

  // used by C&C for N->NP conversion
  case class Lex() extends CandcCombinator {
    def arity = 1

    def headIndex = 0

    override def display: _root_.scalatags.Text.all.Frag = toFrag("lex")

    override def toString = "lex"
  }

  case class FX() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    // I think…
    override def display: _root_.scalatags.Text.all.Frag = toFrag("fx")

    override def toString = "fx"
  }

  case class BX() extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    // I think…
    override def display: _root_.scalatags.Text.all.Frag = toFrag("bx")

    override def toString = "bx"
  }

  case class GBX() extends CandcCombinator {
    def arity = 2

    def headIndex = 1

    override def display: _root_.scalatags.Text.all.Frag = toFrag("gbx")

    override def toString = "gbx"
  }

  case class Funny() extends CandcCombinator {
    def arity = 2

    def headIndex = 0

    // because I saw it that way once… don’t really expect this to be consistent
    override def display: _root_.scalatags.Text.all.Frag = toFrag("funny")

    override def toString = "funny"
  }


}
