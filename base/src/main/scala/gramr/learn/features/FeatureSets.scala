package gramr.learn.features

import breeze.linalg.max
import breeze.numerics.log
import cats.instances.vector._
import cats.syntax.traverse._
import gramr.amr.NamedEntities
import gramr.ccg._
import gramr.graph.Graph.ops._
import gramr.graph._
import gramr.learn.FeatureVector
import gramr.learn.FeatureVector.Syntax._
import gramr.parse.{CombinatoryDecision, Decision, LexicalDecision, PatternChoice, TagDistribution}
import gramr.text.{DependencyForest, Example, Span}

import scala.collection.concurrent.TrieMap
import scala.util.matching.Regex

case class FeatureSet[MR, FV, E](
  stateFeatures: StateFeatureSet[MR, FV, E],
  derivationFeatures: DerivationFeatureSet[MR, FV, E]
)

trait StateFeatureSet[MR, FV, E] {

  def extract(mr: MR, example: E): FV

}

case class PreprocessedStateFeature[MR, FV, E](
  processor: MR => MR,
  feature: StateFeatureSet[MR, FV, E]
) extends StateFeatureSet[MR, FV, E] {

  def extract(mr: MR, example: E): FV =
    feature.extract(processor(mr), example)

}

case class StateFeatureCollection[MR, FV: FeatureVector, E](features: StateFeatureSet[MR, FV, E]*) extends StateFeatureSet[MR, FV, E] {

  override def extract(mr: MR, example: E): FV =
    if (features.isEmpty)
      implicitly[FeatureVector[FV]].empty
    else
      features.view.map(_.extract(mr, example)).reduce(_ + _)

}

object StateFeatureCollection {
  def preprocessed[MR, FV: FeatureVector, E](processor: MR => MR)(features: StateFeatureSet[MR, FV, E]*): StateFeatureSet[MR, FV, E] =
    PreprocessedStateFeature(processor, StateFeatureCollection(features: _*))
}

case class TripleFeature[MR: Graph, FV: FeatureVector, E](
  filter: EdgeFilter[MR],
  sourceTransformer: NodeTransformer,
  edgeTransformer: EdgeTransformer,
  targetTransformer: NodeTransformer
) extends StateFeatureSet[MR, FV, E] {

  def extract(mr: MR, example: E): FV = {
    val relevantEdges = mr.edges.view.map(_.map(_.unflip)).filter(e => filter(e, mr))
    val triples = for {
      edge <- relevantEdges
      transformedSource <- sourceTransformer(mr.node(edge.element.source))
      transformedEdge = edgeTransformer(edge.element)
      transformedTarget <- targetTransformer(mr.node(edge.element.target))
    } yield s"triple($transformedSource, $transformedEdge, $transformedTarget)"
    val kvPairs = triples.map(_ -> 1.0)
    implicitly[FeatureVector[FV]].from(kvPairs)
  }

}

case class PathFeature[MR: Graph, FV: FeatureVector, E](
  length: Int,
  edgeFilter: EdgeFilter[MR],
  nodeTransformer: NodeTransformer,
  edgeTransformer: EdgeTransformer
) extends StateFeatureSet[MR, FV, E] {

  def extract(mr: MR, example: E): FV = {
    val allPaths = gramr.graph.allPaths(mr, length)
    val transformed: Iterable[Vector[Vector[String]]] = allPaths.map { p =>
      p.map {
        case in@Indexed(IsNode(), _) => nodeTransformer(in.asInstanceOf[Indexed[Node]]).toVector
        case Indexed(e@IsEdge(), _) => Iterable(edgeTransformer(e)).toVector
      }
    }
    val features = for(f <- transformed.flatMap(_.sequence)) yield s"path(${f.mkString(", ")})" -> 1.0
    implicitly[FeatureVector[FV]].from(features)
  }

}

case class PathFeatureConfig[MR, FV](
  edgeFilter: EdgeFilter[MR],
  initialNodeTransformer: DeterministicNodeTransformer,
  outEdgeTransformers: List[(EdgeTransformer, DeterministicNodeTransformer)]
) {

  def toFeature(path: Vector[Indexed[Element]], mr: MR): Iterable[String] = {
    val initial = initialNodeTransformer.det(path.head.asInstanceOf[Indexed[Node]])
    val edges = path.tail.grouped(2).map(_.head)
    if (edges.forall(e => edgeFilter(e.asInstanceOf[Indexed[Edge]], mr))) {
      val rest = path.tail.grouped(2).zip(outEdgeTransformers.iterator).flatMap {
        case (Vector(e, n), (et, nt)) =>
          val te = et(e.asInstanceOf[Indexed[Edge]].element)
          val tn = nt.det(n.asInstanceOf[Indexed[Node]])
          Iterable(te, tn)
      }
      Iterable(s"path($initial, ${rest.mkString(", ")})")
    }
    else {
      Iterable()
    }
  }

}

case class PathFeatureSet[MR: Graph, FV: FeatureVector, E](
  configs: PathFeatureConfig[MR, FV]*
) extends StateFeatureSet[MR, FV, E] {

  lazy val configsByLength: Map[Int, Seq[PathFeatureConfig[MR, FV]]] =
    configs.groupBy(_.outEdgeTransformers.length)
  lazy val maxLength: Int = configsByLength.keys.max

  def extract(mr: MR, example: E): FV = {
    val entries = for {
      startNode <- mr.nodes
      l <- 1 to maxLength  // this starts a new traversal for each length, but maybe that's good enough
      path <- pathsFromNode(mr, startNode, l)
      config <- configsByLength.getOrElse(l, Seq.empty)
      feature <- config.toFeature(path, mr)
    } yield (feature, 1.0)
    implicitly[FeatureVector[FV]].from(entries)
  }

}

object WordDistanceUtil {

  def tokenSpan[MR: Graph](mr: MR, e: Int): Option[Span] = {
    mr.get(e).element.attributes.getOption[DefaultAttribute[Span]]("tokenSpan").map(_.value)
  }

  def wordDistance[MR: Graph](mr : MR, edge: Edge, operator: DistanceOperator): Option[Int] = for {
    sSpan <- tokenSpan(mr, edge.source)
    tSpan <- tokenSpan(mr, edge.target)
    distances = for {
      s <- List(sSpan.start, sSpan.end)
      t <- List(tSpan.start, tSpan.end)
      d = t - s if d >= 0
    } yield d
    distance = operator match {
      case MinDistance => if (distances.isEmpty) 0 else distances.min
      case MaxDistance => if (distances.isEmpty) 0 else distances.max
    }
  } yield distance

}

case class WordDistanceBinFeature[MR: Graph, FV: FeatureVector, E](
  bins: List[Int],
  minimumDistance: Int = 1,
  edgeTransformer: EdgeTransformer = EdgeTransformer.Constant(""),
  operator: DistanceOperator = MinDistance
) extends StateFeatureSet[MR, FV, E] {

  import WordDistanceBinFeature.bin
  
  def extract(mr: MR, example: E): FV = {
    val features = for {
      indexedEdge <- mr.edges.view
      transformedEdge = edgeTransformer(indexedEdge.element)
      Indexed(e, _) = indexedEdge
      distance <- WordDistanceUtil.wordDistance(mr, e, operator) if distance >= minimumDistance
    } yield s"worddist-bin(${bin(distance, bins)}, $transformedEdge)" -> 1.0
    implicitly[FeatureVector[FV]].from(features.toSeq)
  }

}

object WordDistanceBinFeature {

  def bin(value: Int, restBins: List[Int], curBin: Int = 0): String = restBins match {
    case nextBin :: _ if value <= nextBin => s"$curBin-$nextBin"
    case nextBin :: rest => bin(value, rest, nextBin)
    case _ => s"$curBin--"
  }
  
}

case class WordDistanceFeature[MR: Graph, FV: FeatureVector, E](
  operator: DistanceOperator = MinDistance,
  scale: DistanceScale = LogarithmicScale,
  edgeTransformer: EdgeTransformer = EdgeTransformer.Constant(""),
  normalise: Boolean = false
) extends StateFeatureSet[MR, FV, E] {

  def tokenSpan(mr: MR, e: Int): Option[Span] = {
    mr.get(e).element.attributes.getOption[DefaultAttribute[Span]]("tokenSpan").map(_.value)
  }

  def extract(mr: MR, example: E): FV = {
    val features = for {
      indexedEdge <- mr.edges.view
      Indexed(e, _) = indexedEdge
      distance <- WordDistanceUtil.wordDistance(mr, e, operator)
      scaledDistance = scale match {
        case LinearScale => distance.toDouble - 1.0
        case LogarithmicScale => Math.log(distance.toDouble) / Math.log(2.0)
      }
      normalised = if (normalise) scaledDistance / mr.edges.size.toDouble else scaledDistance
      transformedEdge = edgeTransformer(indexedEdge.element)
    } yield {
      s"worddist($transformedEdge)" -> normalised
    }
    implicitly[FeatureVector[FV]].from(features.toSeq)
  }

}

sealed trait DistanceOperator

case object MinDistance extends DistanceOperator

case object MaxDistance extends DistanceOperator

sealed trait DistanceScale

case object LinearScale extends DistanceScale

case object LogarithmicScale extends DistanceScale


case class DependencyDistanceFeature[MR: Graph, FV: FeatureVector, E](
  bins: List[Int],
  edgeTransformer: EdgeTransformer,
  distanceOperator: DistanceOperator = MinDistance,
  dependencyTree: E => DependencyForest
) extends StateFeatureSet[MR, FV, E] {

  def extract(mr: MR, example: E): FV = {
    val features = for {
      indexedEdge <- mr.edges
      distance <- calculateDistance(mr, indexedEdge.element, dependencyTree(example)) if distance > 0
      b = WordDistanceBinFeature.bin(distance, bins)
      transformedEdge = edgeTransformer(indexedEdge.element)
    } yield s"dependency-distance($b, $transformedEdge)" -> 1.0
    implicitly[FeatureVector[FV]].from(features.toSeq)
  }

  def tokenSpan(mr: MR, e: Int): Option[Span] = {
    mr.get(e).element.attributes.getOption[DefaultAttribute[Span]]("tokenSpan").map(_.value)
  }

  def calculateDistance(mr: MR, edge: Edge, depTree: DependencyForest): Option[Int] = {
    for {
      sSpan <- tokenSpan(mr, edge.source)
      tSpan <- tokenSpan(mr, edge.target)
      distances = for {
        s <- List(sSpan.start, sSpan.end)
        t <- List(tSpan.start, tSpan.end)
        d = depTree.path(s, t).size
      } yield d
      distance = distanceOperator match {
        case MinDistance => if (distances.isEmpty) 0 else distances.min
        case MaxDistance => if (distances.isEmpty) 0 else distances.max
      }
    } yield distance
  }

}

case class DependencyPathFeature[MR: Graph, FV: FeatureVector, E](
  edgeTransformer: EdgeTransformer,
  dependencyTree: E => DependencyForest
) extends StateFeatureSet[MR, FV, E] {

  def extract(mr: MR, example: E): FV = {
    val features = for {
      indexedEdge <- mr.edges
      path <- shortestPaths(mr, indexedEdge.element, dependencyTree(example)) if path.nonEmpty
      transformedEdge = edgeTransformer(indexedEdge.element)
    } yield s"dependency-path(${pathToString(path)}, $transformedEdge)" -> 1.0
    implicitly[FeatureVector[FV]].from(features.toSeq)
  }

  def tokenSpan(mr: MR, e: Int): Iterable[Span] = {
    mr.get(e).element.attributes.getOption[DefaultAttribute[Span]]("tokenSpan").map(_.value)
  }

  def shortestPaths(mr: MR, edge: Edge, depTree: DependencyForest): Iterable[Vector[DependencyForest.PathStep]] = {
    for {
      sSpan <- tokenSpan(mr, edge.source) if sSpan.size == 1
      tSpan <- tokenSpan(mr, edge.target) if sSpan.size == 1
      paths = (for {
        s <- List(sSpan.start, sSpan.end)
        t <- List(tSpan.start, tSpan.end)
        p = depTree.path(s, t)
      } yield p).toSet
      minLength = if(paths.isEmpty) -1 else paths.minBy(_.size).size
      path <- paths if path.size == minLength
    } yield path
  }

  def pathToString(path: Vector[DependencyForest.PathStep]): String = {
    path.map {
      case DependencyForest.UpStep(_, label) => s"<$label"
      case DependencyForest.DownStep(_, label) => s">$label"
    }.mkString("-")
  }
}


/** Triggers a feature if two identically labelled and directed edges are adjacent to a node.
  */
case class DuplicateEdgeFeature[MR: Graph, FV: FeatureVector, E](
  nodeTransformer: NodeTransformer = NodeTransformer.Constant(""),
  edgeTransformer: EdgeTransformer = EdgeTransformer.Constant(""),
  normalise: Boolean = false
) extends StateFeatureSet[MR, FV, E] {

  def acceptNode(node: Indexed[Node]): Boolean = {
    node.element.isReferentNode && node.element.content != "<coref>"
  }

  def extract(mr: MR, example: E): FV =
    outEdgeFeatures(mr) + inEdgeFeatures(mr)

  def outEdgeFeatures(mr: MR): FV = {
    val features = for {
      node <- mr.nodes.view if acceptNode(node)
      outEdgeGroups = mr.outEdges(node.pointer).map(_.element).groupBy(_.label).filter(_._2.size > 1)
      edgeGroup <- outEdgeGroups
      transformedNode <- nodeTransformer(node)
      transformedEdge = edgeTransformer(edgeGroup._2.head)
      value = edgeGroup._2.size.toDouble
      normalised = if (normalise) value / mr.edges.size.toDouble else value
    } yield (s"dup-edge($transformedNode -> $transformedEdge)", normalised)
    implicitly[FeatureVector[FV]].from(features.toSeq)
  }

  def inEdgeFeatures(mr: MR): FV = {
    val features = for {
      node <- mr.nodes.view if acceptNode(node)
      inEdgeGroups = mr.inEdges(node.pointer).map(_.element).groupBy(_.label).filter(_._2.size > 1)
      edgeGroup <- inEdgeGroups
      transformedNode <- nodeTransformer(node)
      transformedEdge = edgeTransformer(edgeGroup._2.head)
      value = edgeGroup._2.size.toDouble
      normalised = if (normalise) value / mr.edges.size.toDouble else value
    } yield (s"dup-edge($transformedNode <- $transformedEdge)", normalised)
    implicitly[FeatureVector[FV]].from(features.toSeq)
  }

}

/** Triggers a feature if a node has two equal neighbours.
  */
case class DuplicateNeighbourFeature[MR: Graph, FV: FeatureVector, E](
  nodeTransformer: NodeTransformer,
  neighbourTransformer: NodeTransformer,
  normalise: Boolean = false
) extends StateFeatureSet[MR, FV, E] {

  def acceptNode(node: Indexed[Node]): Boolean = {
    node.element.isReferentNode && node.element.content != "<coref>"
  }

  def extract(mr: MR, example: E): FV = implicitly[FeatureVector[FV]].from {
    for {
      node <- mr.nodes.view if acceptNode(node)
      transformedNode <- nodeTransformer(node)
      outNeighbours = for {
        edge <- mr.outEdges(node.pointer).view
      } yield mr.node(edge.element.target)
      outGroups = outNeighbours.groupBy(_.element.deleteAttributes).filter(_._2.size > 1)
      inNeighbours = for {
        edge <- mr.inEdges(node.pointer).view
      } yield mr.node(edge.element.source)
      inGroups = inNeighbours.groupBy(_.element.deleteAttributes).filter(_._2.size > 1)
      group <- outGroups ++ inGroups
      transformedNeighbour <- neighbourTransformer(group._2.head)
      value = group._2.size.toDouble
      normalised = if (normalise) value / mr.nodes.size.toDouble else value
    } yield (s"dup-neighbour($transformedNode, $transformedNeighbour)", value)
  }

}

/** Triggers a feature if two identically labelled and directed edges are adjacent to a node.
  */
case class SignatureFeature[MR: Graph, FV: FeatureVector, E](
  nodeTransformer: NodeTransformer = NodeTransformer.Constant(""),
  edgeTransformer: EdgeTransformer = EdgeTransformer.Constant("")
) extends StateFeatureSet[MR, FV, E] {

  def acceptNode(node: Indexed[Node]): Boolean = {
    node.element.isReferentNode && node.element.content != "<coref>"
  }

  def extract(mr: MR, example: E): FV = implicitly[FeatureVector[FV]].from {
    for {
      node <- mr.nodes.view if acceptNode(node)
      transformedNode <- nodeTransformer(node)
      outEdges = mr.outEdges(node.pointer).map(indexedEdge => edgeTransformer(indexedEdge.element)).toVector
    } yield (s"signature($transformedNode, ${outEdges.mkString(",")})", 1.0)
  }
}

sealed trait EdgeFilter[MR] { self =>
  def apply(indexedEdge: Indexed[Edge], mr: MR): Boolean

  def and(other: EdgeFilter[MR]): EdgeFilter[MR] = new EdgeFilter[MR] {
    def apply(indexedEdge: Indexed[Edge], mr: MR): Boolean = {
      self.apply(indexedEdge, mr) && other.apply(indexedEdge, mr)
    }
  }
}

object EdgeFilter {

  case class True[MR]() extends EdgeFilter[MR] {
    def apply(indexedEdge: Indexed[Edge], mr: MR): Boolean = true
  }

  case class ExcludeHalfEdges[MR: Graph](
    source: Boolean = true,
    target: Boolean = true
  ) extends EdgeFilter[MR] {
    def apply(indexedEdge: Indexed[Edge], mr: MR): Boolean =
      !(source && mr.node(indexedEdge.element.source).element.isExternalNode) &&
        !(target && mr.node(indexedEdge.element.target).element.isExternalNode)
  }

  case class ExcludeCorefEdges[MR: Graph](
    source: Boolean = true,
    target: Boolean = true
  ) extends EdgeFilter[MR] {
    def apply(indexedEdge: Indexed[Edge], mr: MR): Boolean =
      !(source && mr.node(indexedEdge.element.source).element.content == "<coref>") &&
        !(target && mr.node(indexedEdge.element.target).element.content == "<coref>")
  }

  case class ExcludeLexicalEdges[MR: Graph]() extends EdgeFilter[MR] {
    def apply(indexedEdge: Indexed[Edge], mr: MR): Boolean = {
      val dist = WordDistanceUtil.wordDistance(mr, indexedEdge.element, MinDistance)
      dist.isEmpty || dist.get > 0.0
    }
  }

}

sealed trait NodeTransformer {
  def apply(indexedNode: Indexed[Node]): Iterable[String]
}

sealed trait DeterministicNodeTransformer extends NodeTransformer {
  def det(indexedNode: Indexed[Node]): String

  override def apply(indexedNode: Indexed[Node]): Iterable[String] = Iterable(det(indexedNode))
}

object NodeTransformer {
  val senseTagRE = """.+-\d\d""".r
  val quotedRE = """".+"""".r
  val numericRE = """\d+""".r
  val corefRE = """<.*>""".r

  case class Constant(value: String = "") extends DeterministicNodeTransformer {
    def det(indexedNode: Indexed[Node]): String = value
  }

  case object Content extends DeterministicNodeTransformer {
    def det(indexedNode: Indexed[Node]): String = indexedNode.element.content
  }

  val classes = List("[quoted]", "[num]", "[verbal]", "[name]", "[named-entity]", "[nominal]")

  def nodeClass(indexedNode: Indexed[Node]): String = {
    if(indexedNode.element.isConstantNode) {
      val n = indexedNode.element.asInstanceOf[ConstantNode]
      n.name match {
        case quotedRE() => "[quoted]"
        case numericRE() => "[num]"
        case corefRE() => "[coref]"
        case _ => n.name
      }
    }
    else if(indexedNode.element.isReferentNode) {
      val n = indexedNode.element.asInstanceOf[ReferentNode]
      n.concept match {
        case senseTagRE() => "[verbal]"
        case c if c == "name" => "[name]"
        case c if NamedEntities.allReductions.contains(c) => "[named-entity]"
        case _ => "[nominal]"
      }
    }
    else if(indexedNode.element.isExternalNode) {
      "[ext]"
    }
    else {
      throw new Exception(s"Unknown node type in ${indexedNode.element}")
    }
  }

  case object Class extends DeterministicNodeTransformer {
    def det(indexedNode: Indexed[Node]): String = nodeClass(indexedNode)
  }

  case object NegativeClass extends NodeTransformer {
    def apply(indexedNode: Indexed[Node]): Iterable[String] = {
      val nc = nodeClass(indexedNode)
      for(clas <- classes if clas != nc) yield "^" + clas
    }
  }

}

sealed trait EdgeTransformer {
  def apply(edge: Edge): String
}

object EdgeTransformer {
  val argRE = """ARG\d+""".r
  val labelRE = """([a-zA-Z]+)\d+""".r

  case class Constant(value: String = "") extends EdgeTransformer {
    def apply(edge: Edge): String = value
  }

  case object Label extends EdgeTransformer {
    def apply(edge: Edge): String = edge.unflip.label
  }

  case object LabelDenumeralised extends EdgeTransformer {
    def apply(edge: Edge): String = {
      edge.unflip.label match {
        case labelRE(prefix) => prefix
        case label => label
      }
    }
  }

  case object LabelDenumeralisedExceptArg extends EdgeTransformer {
    def apply(edge: Edge): String = {
      edge.unflip.label match {
        case label@argRE() => label
        case labelRE(prefix) => prefix
        case label => label
      }
    }
  }

}


trait DerivationFeatureSet[MR, FV, E] {

  def extract(decision: Decision[MR], example: E): FV = extract(decision)

  protected def extract(decision: Decision[MR]): FV = ???

}

case class DerivationFeatureCollection[MR, FV: FeatureVector, E](
  features: DerivationFeatureSet[MR, FV, E]*
) extends DerivationFeatureSet[MR, FV, E] {

  override def extract(decision: Decision[MR], example: E): FV =
    if (features.isEmpty)
      implicitly[FeatureVector[FV]].empty
    else
      features.view.map(_.extract(decision, example)).reduce(_ + _)

}

case class LexicalDecisionCount[MR, FV: FeatureVector, E]() extends DerivationFeatureSet[MR, FV, E] {

  override def extract(decision: Decision[MR]): FV = decision match {
    case LexicalDecision(_, _, _) =>
      implicitly[FeatureVector[FV]].from(List((s"lexicalDecisionCount", 1.0)))
    case _ =>
      implicitly[FeatureVector[FV]].empty
  }

}

/** Counts every indexed lexical item, lexeme, and template individually. */
case class LexicalItemIdentity[MR, FV: FeatureVector, E]() extends DerivationFeatureSet[MR, FV, E] {

  import gramr.ccg._
  
  override def extract(decision: Decision[MR]): FV = decision match {
    case LexicalDecision(IndexedLexicalItem(_, _, _, index), _, _) =>
      implicitly[FeatureVector[FV]].from(List((s"lex $index", 1.0)))
    case LexicalDecision(DelexedLexicalItem(_, _, _, lexemeItem, _, templateId), _, _) =>
      implicitly[FeatureVector[FV]].from(List((s"lexeme ${lexemeItem.id}", 1.0), (s"template $templateId", 1.0)))
    case _ => implicitly[FeatureVector[FV]].empty
  }

}

/** Gives the log confidence of the CCG supertagger for the syntactic category in a lexical decision.
 */
case class TokenSyncatConfidence[MR, FV: FeatureVector, E](
  ccgTags: E => Vector[Vector[(SyntacticCategory, Double)]],
  eraseFeatures: Boolean = true,
  minConfidence: Double = 0.001,
) extends DerivationFeatureSet[MR, FV, E] {

  def eraseFeatures(synCat: SyntacticCategory): SyntacticCategory = {
    if(eraseFeatures) synCat.eraseFeatures else synCat
  }

  override def extract(decision: Decision[MR], example: E): FV = decision match {
    case LexicalDecision(item, _, span) if span.size == 1 =>
      if(ccgTags(example).size > span.start) {
        val syncats = ccgTags(example)(span.start).filter { entry =>
          eraseFeatures(entry._1) == eraseFeatures(item.synCat)
        }
        val confidence = syncats.map(_._2).sum
        val logconf = log(max(confidence, minConfidence))
        implicitly[FeatureVector[FV]].from(List("syncat-confidence" -> logconf))
      }
      else {
        implicitly[FeatureVector[FV]].empty
      }
    case _ =>
      implicitly[FeatureVector[FV]].empty
  }
}

/** Gives the log confidence of the supertagger for templates selected in lexical decisions.
 */
case class SupertaggerConfidence[MR, FV: FeatureVector, E](
  extractSupertags: E => Seq[TagDistribution],
  minConfidence: Double = 0.001
) extends DerivationFeatureSet[MR, FV, E] {

  override def extract(decision: Decision[MR], example: E): FV = decision match {
    case LexicalDecision(DelexedLexicalItem(_, _, _, _, _, templateId), _, span) if span.size == 1 =>
      val supertags = extractSupertags(example)
      if(supertags.size > span.start) {
        val tagDistribution = supertags(span.start)
        (tagDistribution.tags zip tagDistribution.confidences).find(_._1 == templateId.toString) match {
          case Some((_, confidence)) =>
            val logconf = log(max(confidence, minConfidence))
            implicitly[FeatureVector[FV]].from(List((s"supertagger-confidence", logconf)))
          case None =>
            val logconf = log(minConfidence)
            implicitly[FeatureVector[FV]].from(List((s"supertagger-confidence", logconf)))
        }
      }
      else {
        implicitly[FeatureVector[FV]].empty
      }
    case _ => implicitly[FeatureVector[FV]].empty
  }

}

case class CombinatorFeature[MR: Graph, FV: FeatureVector, E](
  combinators: Seq[Regex],
  includeSemanticRoot: Boolean = false
) extends DerivationFeatureSet[MR, FV, E] {

  override def extract(decision: Decision[MR]): FV = {
    decision match {
      case c: CombinatoryDecision[MR] =>
        cmbFeatures(c.result, c.combinator)
      case _ => implicitly[FeatureVector[FV]].empty
    }
  }

  def cmbFeatures(mr: MR, combinator: Combinator): FV = {
    val cmbString = combinator.toString

    if (combinators.exists(r => r.pattern.matcher(cmbString).matches())) {
      val cmbFeature = Seq((s"cmb-$cmbString", 1.0))

      val semanticRootFeature: Seq[(String, Double)] = {
        if(includeSemanticRoot) {
          val semRoot =
            if(!mr.isEmpty) {
              mr.node(mr.root).element.content
            }
            else {
              "EMPTY"
            }
          Seq(s"cmb-$cmbString-$semRoot" -> 1.0)
        }
        else {
          Seq.empty
        }
      }

      implicitly[FeatureVector[FV]].from(
        cmbFeature ++ semanticRootFeature
      )
    }
    else {
      implicitly[FeatureVector[FV]].empty
    }

  }

}

/** Generates an indicator feature for each rule that is used.
  */
case class RuleFeature[MR: Graph, FV: FeatureVector, E](
  combinators: Seq[Regex],
  includeSemanticRoot: Boolean = false
) extends DerivationFeatureSet[MR, FV, E] {
  override def extract(decision: Decision[MR]): FV = {
    decision match {
      case c: PatternChoice.Decision[MR] =>
        val ruleName = c.pattern.name

        if (combinators.exists(r => r.pattern.matcher(ruleName).matches())) {
          val cmbFeature = Seq((s"rule-$ruleName", 1.0))

          val semanticRootFeature: Seq[(String, Double)] = {
            if(includeSemanticRoot) {
              val mr = decision.result
              val semRoot =
                if(!mr.isEmpty) {
                  decision.result.node(decision.result.root).element.content
                }
                else {
                  "EMPTY"
                }
              Seq(s"rule-$ruleName-$semRoot" -> 1.0)
            }
            else {
              Seq.empty
            }
          }

          implicitly[FeatureVector[FV]].from(
            cmbFeature ++ semanticRootFeature
          )
        }
        else {
          implicitly[FeatureVector[FV]].empty
        }
      case _ => implicitly[FeatureVector[FV]].empty
    }
  }

}

case class GeneratorFeature[MR, FV: FeatureVector, E](
  nameRE: Regex,
  context: List[GeneratorFeature.Context]
) extends DerivationFeatureSet[MR, FV, E] {

  import GeneratorFeature._
  import gramr.ccg._
  import gramr.parse._

  override def extract(decision: Decision[MR]): FV = decision match {
    case LexicalDecision(GeneratedLexicalItem(tokens, synCat: SyntacticCategory, _, genName@nameRE(_*)), _, _) =>
      val contextInfo = context.map {
        case GeneratorName => s"gen=$genName"
        case SynCat(includeFeatures) =>
          if (includeFeatures)
            s"syn=$synCat"
          else
            s"syn=${synCat.eraseFeatures}"
        case Tokens => "tok=" + tokens.map(_.text.toLowerCase).mkString(" ")
        case TokenCount(bins) => "ntok=" + WordDistanceBinFeature.bin(tokens.length, bins)
        case Constant(str) => str
      }
      val featStr = s"gen(${contextInfo.mkString(", ")})"
      implicitly[FeatureVector[FV]].from(List((featStr, 1.0)))
    case _ => implicitly[FeatureVector[FV]].empty
  }

}

object GeneratorFeature {

  sealed trait Context

  case object GeneratorName extends Context

  case class SynCat(includeFeatures: Boolean = true) extends Context

  case object Tokens extends Context

  case class TokenCount(bins: List[Int]) extends Context

  case class Constant(constant: String) extends Context

}

/** Counts pairs of token and node content in lexical decisions. */
case class LexicalWordMeaningCooccurrence[MR: Graph, FV: FeatureVector, E](
  normalise: Boolean = false
) extends DerivationFeatureSet[MR, FV, E] {

  override def extract(decision: Decision[MR]): FV = decision match {
    case LexicalDecision(item, _, _) =>
      val feats = for {
        token <- item.tokens.toList
        node <- item.meaning.nodes if !node.element.isExternalNode && node.element.content != "<coref>"
        str = s"lex-wm-cooc(${token.lemma}, ${node.element.content})"
        value = if (normalise) 1.0 / (item.tokens.size * item.meaning.nodes.size).toDouble else 1.0
      } yield (str, value)
      implicitly[FeatureVector[FV]].from(feats)
    case _ => implicitly[FeatureVector[FV]].empty
  }

}

/** Counts the assignment of a lexeme to a list of tokens by lemma */
case class LexemeLemma[MR, FV: FeatureVector, E]() extends DerivationFeatureSet[MR, FV, E] {

  override protected def extract(decision: Decision[MR]) = decision match {
    case LexicalDecision(DelexedLexicalItem(tokens, _, _, lexemeItem, _, templateId), _, _) =>
      implicitly[FeatureVector[FV]].from(List((s"lemmas='${lexemeItem.lexeme.tokens.mkString(" ")}' lexeme=${lexemeItem.id}", 1.0)))
    case _ => implicitly[FeatureVector[FV]].empty
  }

}

/** Counts the assignment of a template to a list of tokens by lemma */
case class TemplateLemma[MR, FV: FeatureVector, E]() extends DerivationFeatureSet[MR, FV, E] {

  override protected def extract(decision: Decision[MR]) = decision match {
    case LexicalDecision(DelexedLexicalItem(tokens, _, _, lexemeItem, _, templateId), _, _) =>
      implicitly[FeatureVector[FV]].from(List((s"lemmas='${lexemeItem.lexeme.tokens.mkString(" ")}' template=${templateId}", 1.0)))
    case _ => implicitly[FeatureVector[FV]].empty
  }

}

case class LexicalNodeTokenRatio[MR: Graph, FV: FeatureVector, E]() extends DerivationFeatureSet[MR, FV, E] {

  override def extract(decision: Decision[MR]): FV = decision match {
    case LexicalDecision(item, _, _) =>
      val value = item.meaning.nodes.size.toDouble / item.tokens.size.toDouble
      implicitly[FeatureVector[FV]].from(List(("lex-node-token-ratio", value)))
    case _ => implicitly[FeatureVector[FV]].empty
  }

}

case class LexicalTokenNodeRatio[MR: Graph, FV: FeatureVector, E]() extends DerivationFeatureSet[MR, FV, E] {

  override def extract(decision: Decision[MR]): FV = decision match {
    case LexicalDecision(item, _, _) =>
      val value =
        if (item.meaning.nodes.isEmpty)
          0.0
        else
          item.tokens.size.toDouble / item.meaning.nodes.size.toDouble
      implicitly[FeatureVector[FV]].from(List(("lex-token-node-ratio", value)))
    case _ => implicitly[FeatureVector[FV]].empty
  }

}


/** Counts the syntactic category together with the node content of the root */
case class SyncatRootFeature[MR: Graph, FV: FeatureVector, E](
  nodeTransformer: NodeTransformer
) extends DerivationFeatureSet[MR, FV, E] {
  override def extract(decision: Decision[MR]): FV = {
    val synCat = decision match {
      case d: LexicalDecision[MR] => Some(d.syntax.synCat)
      case d: CombinatoryDecision[MR] => Some(d.context.synCat)
      case _ => None
    }
    val rootNode = decision.result.rootOption
    val kvPairs = for {
      sc <- synCat.toIterable
      r <- rootNode.toIterable
      rc <- nodeTransformer(decision.result.node(r))
    } yield s"syncat-root($sc, $rc)" -> 1.0
    implicitly[FeatureVector[FV]].from(kvPairs)
  }
}

case class LexicalSkeletonFeature[MR: Graph, FV: FeatureVector, E](
  nodeTransformer: NodeTransformer = NodeTransformer.Constant("_"),
  edgeTransformer: EdgeTransformer = EdgeTransformer.Label
) extends DerivationFeatureSet[MR, FV, E] {

  val skeletonIds = TrieMap.empty[MR, Int]
  val lexEntryMapping = TrieMap.empty[Int, Int]
  var nextId: Int = 0

  override def extract(decision: Decision[MR]): FV = {
    decision match {
      case LexicalDecision(IndexedLexicalItem(_, _, mr, index), _, _) =>
        lexEntryMapping.get(index) match {
          case Some(skelId) =>
            implicitly[FeatureVector[FV]].from(Seq(s"skel($skelId)" -> 1.0))
          case None =>
            val skel = skeleton(mr)
            skeletonIds.get(skel) match {
              case Some(skelId) =>
                lexEntryMapping.put(index, skelId)
                implicitly[FeatureVector[FV]].from(Seq(s"skel($skelId)" -> 1.0))
              case None =>
                val skelId = nextId
                nextId += 1
                skeletonIds.put(skel, skelId)
                lexEntryMapping.put(index, skelId)
                implicitly[FeatureVector[FV]].from(Seq(s"skel($skelId)" -> 1.0))
            }
        }
      case _ =>
        implicitly[FeatureVector[FV]].empty
    }
  }

  def skeleton(mr: MR): MR = {
    mr.mapElementContents {
      case el@Indexed(ReferentNode(_, attributes), _) =>
        ReferentNode(nodeTransformer(el.asInstanceOf[Indexed[ReferentNode]]).head, attributes)
      case el@Indexed(ConstantNode(_, attributes), _) =>
        ConstantNode(nodeTransformer(el.asInstanceOf[Indexed[ConstantNode]]).head, attributes)
      case Indexed(e@Edge(_, source, target, attributes), _) =>
        Edge(edgeTransformer(e), source, target, attributes)
      case Indexed(el, _) =>
        el
    }
  }
}
