package gramr.graph.edit.sgraph

import gramr.graph.edit._
import gramr.graph.edit.sgraph.Combine._
import gramr.graph.edit.sgraph.Split.splitSemModify
import gramr.graph.{AdjacencyMeaningGraph, Attributes, ExternalReference, Graph}
import gramr.util.Parsing
import org.scalatest.FlatSpec

class CombineSpec extends FlatSpec {

  import scala.language.existentials

  implicit val graph: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance
  import graph.Building._

  "Markers in a graph" should "leave no trace" in {
    val g = Parsing.parseAmr("(a / a :ARG0 (b / b) :ARG1 (c / c :mod b))")
    val results = (for {
      _ <- mark[AdjacencyMeaningGraph](g.root)
    } yield ()).perform(g)

    assert(results.size == 1)
    val result = results.head._1

    assert(g == result)
  }

  they should "be resolvable if there's only one" in {
    val g = Parsing.parseAmr("(a / a :ARG0 (b / b) :ARG1 (c / c :mod b))")
    val results = (for {
      marker <- mark[AdjacencyMeaningGraph](g.root)
      resolved <- resolve(marker)
    } yield resolved).perform(g)

    assert(results.size == 1)
    val (edited, resolved) = results.head

    assert(g.root == resolved)
  }

  they should "be resolvable if there's several" in {
    val (g, (a, b, c, d)) = graph.buildReturning {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        d <- mkReferentNode("d")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        _ <- mkEdge("cd", c, d)
        _ <- normalise(false)
      } yield (a, b, c, d)
    }
    val results = (for {
      ma <- mark[AdjacencyMeaningGraph](a)
      mb <- mark[AdjacencyMeaningGraph](b)
      mc <- mark[AdjacencyMeaningGraph](c)
      md <- mark[AdjacencyMeaningGraph](d)
      ra <- resolve(ma)
      rb <- resolve(mb)
      rc <- resolve(mc)
      rd <- resolve(md)
    } yield (ra, rb, rc, rd)).perform(g)

    assert(results.size == 1)
    val (edited, (ra, rb, rc, rd)) = results.head

    assert(a == ra)
    assert(b == rb)
    assert(c == rc)
    assert(d == rd)
  }

  "Merging two nodes" should "behave as expected" in {
    val expected = graph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        d <- mkReferentNode("d")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        _ <- mkEdge("cd", c, d)
        _ <- normalise(false)
      } yield ()
    }

    val (original, (cp, c)) = graph.buildReturning {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        cp <- mkExternalNode(ExternalReference(0))
        c <- mkReferentNode("c")
        d <- mkReferentNode("d")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, cp)
        _ <- mkEdge("cd", c, d)
        _ <- normalise(false)
      } yield (cp, c)
    }

    val results = (for {
      cpm <- mark[AdjacencyMeaningGraph](cp)
      cm <- mark[AdjacencyMeaningGraph](c)
      _ <- mergeNodes(cm, cpm)
    } yield ()).perform(original)

    assert(results.size == 1)
    val merged = results.head._1

    assert(merged.normalise == expected.normalise)
  }

  "Merging placeholders" should "behave as expected" in {
    val expected = graph.build {
      for {
        a <- mkReferentNode("a")
        p <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("ap1", a, p)
        _ <- mkEdge("ap2", a, p)
        b <- mkReferentNode("b")
        _ <- mkEdge("p1b", p, b)
        _ <- mkEdge("p2b", p, b)
      } yield ()
    }

    val original = graph.build {
      for {
        a <- mkReferentNode("a")
        p1 <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("ap1", a, p1)
        p2 <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("ap2", a, p2)
        b <- mkReferentNode("b")
        _ <- mkEdge("p1b", p1, b)
        _ <- mkEdge("p2b", p2, b)
      } yield ()
    }

    val results = mergePlaceholders().perform(original)

    assert(results.size == 1)
    val merged = results.head._1

    assert(merged.normalise == expected.normalise)
  }

  "Moving placeholders" should "work correctly with a single placeholder" in {
    val expected = graph.build {
      for {
        a <- mkReferentNode("a")
        p <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("ap", a, p)
      } yield ()
    }

    val original = graph.build {
      for {
        a <- mkReferentNode("a")
        p <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ap", a, p)
      } yield ()
    }

    val results = (for {
      p <- mark[AdjacencyMeaningGraph](placeholder(original, 0))
      _ <- movePlaceholder(p, 0)
    } yield()).perform(original)

    assert(results.size == 1)
    val result = results.head._1

    assert(result.normalise == expected.normalise)
  }

  "Semantic application" should "correctly perform a simple first-order application" in {
    val a = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p <- mkExternalNode(ExternalReference(0))
        manny <- mkReferentNode("manny")
        _ <- mkEdge("ARG0", marry, p)
        _ <- mkEdge("ARG1", marry, manny)
      } yield ()
    }

    val b = graph.build {
      for {
        _ <- mkReferentNode("anna")
      } yield ()
    }

    val expected = graph.build {
      for {
        marry <- mkReferentNode("marry")
        anna <- mkReferentNode("anna")
        manny <- mkReferentNode("manny")
        _ <- mkEdge("ARG0", marry, anna)
        _ <- mkEdge("ARG1", marry, manny)
      } yield ()
    }

    val op = semApply[AdjacencyMeaningGraph]() _
    val results = performSemanticOperator(op)(a, b, backward = false)

    assert(results.size == 1)
    val result = results.head

    assert(result.normalise == expected.normalise)
  }

  it should "correctly perform a first-order application when the function term is binary" in {
    val a = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val b = graph.build {
      for {
        _ <- mkReferentNode("manny")
      } yield ()
    }

    val expected = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p <- mkExternalNode(ExternalReference(0))
        manny <- mkReferentNode("manny")
        _ <- mkEdge("ARG0", marry, p)
        _ <- mkEdge("ARG1", marry, manny)
      } yield ()
    }

    val op = semApply[AdjacencyMeaningGraph]() _
    val results = performSemanticOperator(op)(a, b, backward = false)

    assert(results.size == 1)
    val result = results.head

    assert(result.normalise == expected.normalise)
  }

  it should "work in a simple composition" in {
    val a = graph.build {
      for {
        possible <- mkReferentNode("possible")
        p <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("domain", possible, p)
      } yield ()
    }

    val b = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val expected = graph.build {
      for {
        possible <- mkReferentNode("possible")
        marry <- mkReferentNode("marry")
        _ <- mkEdge("domain", possible, marry)
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val op = semApply[AdjacencyMeaningGraph]() _
    val results = performSemanticOperator(op)(a, b, backward = false)

    assert(results.size == 1)
    val result = results.head

    assert(result.normalise == expected.normalise)
  }

  it should "not accept an empty argument" in {
    val f = graph.build {
      for {
        _ <- mkReferentNode("something")
      } yield ()
    }

    val a = graph.empty

    val op = semApply[AdjacencyMeaningGraph]() _
    val results = performSemanticOperator(op)(f, a, backward = false)

    assert(results.isEmpty)
  }

  "Semantic modify" should "work like apply at depth=0" in {
    val a = graph.build {
      for {
        p <- mkExternalNode(ExternalReference(0))
        no <- mkConstantNode("-")
        _ <- mkEdge("polarity", p, no)
      } yield ()
    }

    val b = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val expected = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        no <- mkConstantNode("-")
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
        _ <- mkEdge("polarity", marry, no)
      } yield ()
    }

    val op = semModify[AdjacencyMeaningGraph](0) _
    val results = performSemanticOperator(op)(a, b, backward = false)

    assert(results.size == 1)
    val result = results.head

    assert(result.normalise == expected.normalise)
  }

  it should "not accept an empty argument" in {
    val f = graph.build {
      for {
        _ <- mkReferentNode("something")
      } yield ()
    }

    val a = graph.empty

    val op = semModify[AdjacencyMeaningGraph](0) _
    val results = performSemanticOperator(op)(f, a, backward = false)

    assert(results.isEmpty)
  }

  "Semantic modify" should "apply to the node at the right depth" in {
    val a = graph.build {
      for {
        p <- mkExternalNode(ExternalReference(0))
        x <- mkReferentNode("x")
        _ <- mkEdge("mod", p, x)
      } yield ()
    }

    val b = graph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
      } yield ()
    }

    val expected0 = graph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        x <- mkReferentNode("x")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        _ <- mkEdge("mod", a, x)
      } yield ()
    }

    val expected1 = graph.build {
      for {
        a <- mkReferentNode("a")
        b <- mkReferentNode("b")
        c <- mkReferentNode("c")
        x <- mkReferentNode("x")
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        _ <- mkEdge("mod", b, x)
      } yield ()
    }

    val op0 = semModify[AdjacencyMeaningGraph](0) _
    val results0 = performSemanticOperator(op0)(a, b, backward = false)

    assert(results0.size == 1)
    val result0 = results0.head

    assert(result0.normalise == expected0.normalise)

    val op1 = semModify[AdjacencyMeaningGraph](1) _
    val results1 = performSemanticOperator(op1)(a, b, backward = false)

    assert(results1.size == 1)
    val result1 = results1.head

    assert(result1.normalise == expected1.normalise)
  }

  it should "observe the uniqueness constraint" in {
    val left = graph.build {
      for {
        p <- mkExternalNode(ExternalReference(0))
        c <- mkReferentNode("c")
        _ <- mkEdge("b2c", p, c)
      } yield ()
    }

    val right = graph.build {
      for {
        a <- mkReferentNode("a")
        b1 <- mkReferentNode("b1")
        b2 <- mkReferentNode("b2")
        _ <- mkEdge("ab1", a, b1)
        _ <- mkEdge("ab2", a, b2)
      } yield ()
    }  // Level 1 is not unique, there are both b1 and b2

    val expected1 = graph.build {
      for {
        a <- mkReferentNode("a")
        b1 <- mkReferentNode("b1")
        b2 <- mkReferentNode("b2")
        c <- mkReferentNode("c")
        _ <- mkEdge("ab1", a, b1)
        _ <- mkEdge("ab2", a, b2)
        _ <- mkEdge("b2c", b2, c)
      } yield ()
    }

    val expected2 = graph.build {
      for {
        a <- mkReferentNode("a")
        b1 <- mkReferentNode("b1")
        b2 <- mkReferentNode("b2")
        c <- mkReferentNode("c")
        _ <- mkEdge("ab1", a, b1)
        _ <- mkEdge("ab2", a, b2)
        _ <- mkEdge("b2c", b1, c)
      } yield ()
    }

    val op1 = semModify[AdjacencyMeaningGraph](1, unique = false) _
    val results1 = performSemanticOperator(op1)(left, right, backward = false)

    assert(results1.size == 2)
    assert(results1.exists(_ ~= expected1))
    assert(results1.exists(_ ~= expected2))

    val opUnique = semModify[AdjacencyMeaningGraph](1) _
    val resultsUnique = performSemanticOperator(opUnique)(left, right, backward = false)

    assert(resultsUnique.isEmpty)
  }


  "Semantic compose" should "apply to the second placeholder" in {
    val a = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val b = graph.build {
      for {
        _ <- mkReferentNode("manny")
      } yield ()
    }

    val expected = graph.build {
      for {
        marry <- mkReferentNode("marry")
        manny <- mkReferentNode("manny")
        p0 <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("ARG0", marry, manny)
        _ <- mkEdge("ARG1", marry, p0)
      } yield ()
    }

    val op = semCompose[AdjacencyMeaningGraph] _
    val results = performSemanticOperator(op)(a, b, backward = false)

    assert(results.size == 1)
    val result = results.head

    assert(result.normalise == expected.normalise)
  }

  "Semantic compose" should "apply to the second placeholder even if the function graph has more than two placeholders" in {
    val a = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        p2 <- mkExternalNode(ExternalReference(2))
        _ <- mkEdge("domain", marry, p0)
        _ <- mkEdge("ARG0", marry, p1)
        _ <- mkEdge("ARG1", marry, p2)
      } yield ()
    }

    val b = graph.build {
      for {
        _ <- mkReferentNode("manny")
      } yield ()
    }

    val expected = graph.build {
      for {
        marry <- mkReferentNode("marry")
        manny <- mkReferentNode("manny")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("domain", marry, p0)
        _ <- mkEdge("ARG0", marry, manny)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val op = semCompose[AdjacencyMeaningGraph] _
    val results = performSemanticOperator(op)(a, b, backward = false)

    assert(results.size == 1)
    val result = results.head

    assert(result.normalise == expected.normalise)
  }

  it should "fail if the argument is a function" in {
    val a = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val b = graph.build {
      for {
        verb <- mkReferentNode("eat")
        p0 <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("ARG0", verb, p0)
      } yield ()
    }

    val op = semCompose[AdjacencyMeaningGraph] _
    val results = performSemanticOperator(op)(a, b, backward = false)

    assert(results.isEmpty)
  }

  it should "fail if the argument is empty" in {
    val a = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val b = graph.empty

    val op = semCompose[AdjacencyMeaningGraph] _
    val results = performSemanticOperator(op)(a, b, backward = false)

    assert(results.isEmpty)
  }

  it should "fail if the function graph has less than two placeholders" in {
    val a = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        _ <- mkEdge("ARG0", marry, p0)
      } yield ()
    }

    val b = graph.build {
      for {
        _ <- mkReferentNode("manny")
      } yield ()
    }

    val op = semCompose[AdjacencyMeaningGraph] _
    val results = performSemanticOperator(op)(a, b, backward = false)

    assert(results.isEmpty)
  }

  "Semantic substitution" should "work as expected in a conjunction" in {
    val coffee = graph.build {
      for {
        _ <- mkReferentNode("coffee")
      } yield ()
    }

    val and = graph.build {
      for {
        and <- mkReferentNode("and")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("op1", and, p0)
        _ <- mkEdge("op2", and, p1)
      } yield ()
    }

    val tea = graph.build {
      for {
        _ <- mkReferentNode("tea")
      } yield ()
    }

    val expected1 = graph.build {
      for {
        and <- mkReferentNode("and")
        p0 <- mkExternalNode(ExternalReference(0))
        tea <- mkReferentNode("tea")
        _ <- mkEdge("op1", and, p0)
        _ <- mkEdge("op2", and, tea)
      } yield ()
    }

    val expected2 = graph.build {
      for {
        and <- mkReferentNode("and")
        coffee <- mkReferentNode("coffee")
        tea <- mkReferentNode("tea")
        _ <- mkEdge("op1", and, coffee)
        _ <- mkEdge("op2", and, tea)
      } yield ()
    }

    val andteas = performSemanticOperator(semSubstitute[AdjacencyMeaningGraph])(and, tea, backward = false)

    assert(andteas.size == 1)
    val andtea = andteas.head

    assert(andtea.normalise == expected1.normalise)

    val op = semApply[AdjacencyMeaningGraph]() _
    val results = performSemanticOperator(op)(andtea, coffee, backward = false)

    assert(results.size == 1)
    val result = results.head

    assert(result.normalise == expected2.normalise)
  }

  it should "work in a conjunction of functional terms" in {
    val met = graph.build {
      for {
        meet <- mkReferentNode("meet")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", meet, p0)
        _ <- mkEdge("ARG1", meet, p1)
      } yield ()
    }

    val and = graph.build {
      for {
        and <- mkReferentNode("and")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("op1", and, p0)
        _ <- mkEdge("op2", and, p1)
      } yield ()
    }

    val married = graph.build {
      for {
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
      } yield ()
    }

    val expAndmarried = graph.build {
      for {
        and <- mkReferentNode("and")
        op1 <- mkExternalNode(ExternalReference(2))
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
        _ <- mkEdge("op1", and, op1)
        _ <- mkEdge("op2", and, marry)
      } yield ()
    }

    val expMetandmarried = graph.build {
      for {
        and <- mkReferentNode("and")
        meet <- mkReferentNode("meet")
        marry <- mkReferentNode("marry")
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", marry, p0)
        _ <- mkEdge("ARG1", marry, p1)
        _ <- mkEdge("ARG0", meet, p0)
        _ <- mkEdge("ARG1", meet, p1)
        _ <- mkEdge("op1", and, meet)
        _ <- mkEdge("op2", and, marry)
      } yield ()
    }

    val andmarrieds = performSemanticOperator(semSubstitute[AdjacencyMeaningGraph])(and, married, backward = false)

    assert(andmarrieds.size == 1)
    val andmarried = andmarrieds.head

    assert(andmarried.normalise == expAndmarried.normalise)

    val op = semApply[AdjacencyMeaningGraph]() _
    val metandmarrieds = performSemanticOperator(op)(andmarried, met, backward = false)

    assert(metandmarrieds.size == 1)
    val metandmarried = metandmarrieds.head

    assert(metandmarried.normalise == expMetandmarried.normalise)
  }

  it should "correctly handle a syntactic substitution" in {
    //file : (S\NP)/NP : (f / file :ARG0 <0> :ARG1 <1>)
    //without : ((S\NP)\(S\NP))/(S\NP) : (<0> :manner (<1> :polarity -))
    //reading : (S\NP)/NP : (r / read :ARG0 <0> :ARG1 <1>)

    val file = graph.build {
      for {
        file <- mkReferentNode("file")
        arg0 <- mkExternalNode(ExternalReference(0))
        arg1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", file, arg0)
        _ <- mkEdge("ARG1", file, arg1)
      } yield ()
    }

    val without = graph.build {
      for {
        p0 <- mkExternalNode(ExternalReference(0))
        p1 <- mkExternalNode(ExternalReference(1))
        minus <- mkConstantNode("-")
        _ <- mkEdge("manner", p0, p1)
        _ <- mkEdge("polarity", p1, minus)
      } yield ()
    }

    val reading = graph.build {
      for {
        read <- mkReferentNode("read")
        arg0 <- mkExternalNode(ExternalReference(0))
        arg1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", read, arg0)
        _ <- mkEdge("ARG1", read, arg1)
      } yield ()
    }

    val expWithoutReading = graph.build {
      for {
        p0 <- mkExternalNode(ExternalReference(2))
        read <- mkReferentNode("read")
        arg0 <- mkExternalNode(ExternalReference(0))
        arg1 <- mkExternalNode(ExternalReference(1))
        minus <- mkConstantNode("-")
        _ <- mkEdge("manner", p0, read)
        _ <- mkEdge("ARG0", read, arg0)
        _ <- mkEdge("ARG1", read, arg1)
        _ <- mkEdge("polarity", read, minus)
      } yield ()
    }

    val expFileWithoutReading = graph.build {
      for {
        file <- mkReferentNode("file")
        read <- mkReferentNode("read")
        arg0 <- mkExternalNode(ExternalReference(0))
        arg1 <- mkExternalNode(ExternalReference(1))
        minus <- mkConstantNode("-")
        _ <- mkEdge("manner", file, read)
        _ <- mkEdge("ARG0", file, arg0)
        _ <- mkEdge("ARG1", file, arg1)
        _ <- mkEdge("ARG0", read, arg0)
        _ <- mkEdge("ARG1", read, arg1)
        _ <- mkEdge("polarity", read, minus)
      } yield ()
    }

    val withoutReadings = performSemanticOperator(semSubstitute[AdjacencyMeaningGraph])(without, reading, backward = false)

    assert(withoutReadings.size == 1)
    val withoutReading = withoutReadings.head

    assert(withoutReading.normalise == expWithoutReading.normalise)

    val op = semApply[AdjacencyMeaningGraph]() _
    val fileWithoutReadings = performSemanticOperator(op)(withoutReading, file, backward = false)

    assert(fileWithoutReadings.size == 1)
    val fileWithoutReading = fileWithoutReadings.head

    assert(fileWithoutReading.normalise == expFileWithoutReading.normalise)
  }

}
