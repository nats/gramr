package gramr.split.em

import breeze.numerics.{digamma, exp}
import com.typesafe.scalalogging.LazyLogging
import gramr.ccg.{Derivation, StoredLexicon}
import gramr.split.SentenceSplittingAlgorithm
import gramr.text.Example
import LexicalizedEstimator._
import gramr.graph.Graph
import gramr.util.SparseVector

class LexicalizedEstimator[MR : Graph, E: Example](
  exampleDerivationPairs: Iterable[(E, (String, Derivation[Unit]))],
  lexicon: StoredLexicon[MR],
  sentenceSplitter: SentenceSplittingAlgorithm[MR, E],
  iterations: Int,
  adjustCounts: Parameters => Parameters = (p: Parameters) => p
) extends Estimator[MR, E, LexicalizedModel[MR], Parameters](exampleDerivationPairs, lexicon, sentenceSplitter, iterations, adjustCounts)
  with LazyLogging {

  override def createModel: LexicalizedModel[MR] = LexicalizedModel.randomUniform(lexicon)

}

object LexicalizedEstimator {

  type Parameters = SparseVector
  val Parameters: SparseVector.type = SparseVector

  def expDigamma(counts: Parameters): Parameters = {
    counts.map { v => if(v > 0.0) exp(digamma(v)) else v }
  }

}
