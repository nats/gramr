package gramr.graph.edit.sgraph

import gramr.ccg.EasyccgCombinatorSystem._
import gramr.ccg.{PlainLexicalItem, StoredLexicon, SyntacticCategory}
import gramr.construction.SyntacticContext
import gramr.construction.sgraph.SGraphRule
import gramr.graph.{AdjacencyMeaningGraph, ExternalReference, Graph}
import gramr.text.Token
import gramr.util.Parsing

object TestGrammar {

  type MR = AdjacencyMeaningGraph

  implicit val mgl: Graph[MR] = AdjacencyMeaningGraph.graphInstance
  import mgl.Building._

  val lexicon: StoredLexicon[MR] = StoredLexicon.fromItems[MR](Iterable(
    PlainLexicalItem(
      Vector(Token("anna", "anna")),
      SyntacticCategory("NP"),
      Parsing.parseAmr("(p / person :name (n / name :op1 \"anna\"))")
    ),
    PlainLexicalItem(
      Vector(Token("manny", "manny")),
      SyntacticCategory("NP"),
      Parsing.parseAmr("(p / person :name (n / name :op1 \"manny\"))")
    ),
    PlainLexicalItem(
      Vector(Token("married", "marry")),
      SyntacticCategory("(S\\NP)/NP"),
      mgl.build {
        for {
          m <- mkReferentNode("marry")
          a0 <- mkExternalNode(ExternalReference(0))
          a1 <- mkExternalNode(ExternalReference(1))
          _ <- mkEdge("ARG0", m, a0)
          _ <- mkEdge("ARG1", m, a1)
        } yield ()
      }
    )
  ))

  val patterns: Vector[SGraphRule[MR]] = Vector(
    new SGraphRule[MR](
      "fa",
      (context: SyntacticContext) => context.combinator == FA,
      Operators.Application[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "fa-mod1",
      (context: SyntacticContext) => context.combinator == FA && context.childCats(0) == SyntacticCategory("N/N"),
      Operators.Modification[MR](1),
      backward = false
    ),
    new SGraphRule[MR](
      "ba",
      (context: SyntacticContext) => context.combinator == BA,
      Operators.Application[MR](),
      backward = true
    )
  )
}