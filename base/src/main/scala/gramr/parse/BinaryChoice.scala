package gramr.parse

import gramr.ccg.DerivationNode

trait BinaryChoice[MR] extends Choice[(DerivationNode[MR], DerivationNode[MR]), MR] {

  def decisionSpace(i: (DerivationNode[MR], DerivationNode[MR])): Iterator[CombinatoryDecision[MR]]

}
