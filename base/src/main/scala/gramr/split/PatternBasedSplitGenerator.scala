package gramr
package split

import com.typesafe.scalalogging.LazyLogging
import gramr.graph._
import ccg._
import gramr.construction.{Rule, SyntacticContext}
import Graph.ops._
import gramr.graph.edit.SplitDirection

class PatternBasedSplitGenerator[MR : Graph](
  /** A labelling function producing full node labellings.
    */
  labellingFunction: LabellingFunction[MR],
  patterns: Vector[Rule[MR]]
) extends SplitGenerator[MR] with LazyLogging {

  override def splits(partialMR: PartialMR[MR], derivation: Derivation[Unit]): Stream[Split[MR]] = for {
    step <- splittingSteps(partialMR, derivation)
    pattern <- applicablePatterns(step)
    labelledMR <- labelNodes(partialMR, step)
    split <- pattern.split(labelledMR, step)
  } yield split

  def splittingSteps(partialMR: PartialMR[MR], derivation: Derivation[Unit]): Stream[CombinatoryStep[Unit]] = for {
    combinatorialStep <- derivation.steps.toStream.collect { case step@CombinatoryStep(_, _, _, _, _) => step }
    filteredStep = combinatorialStep if (filteredStep.span == partialMR.span
      && partialMR.matchesSynCat(filteredStep.synCat))
    binStep <- nextBinaryStep(filteredStep).toStream
  } yield binStep

  def nextBinaryStep(step: CombinatoryStep[Unit]): Option[CombinatoryStep[Unit]] = step.subSteps match {
    case List(l, r) => Some(step)
    case List(sub@CombinatoryStep(_, _, _, _, _)) => nextBinaryStep(sub)
    case List() | List(_) => None
    case _ => throw new Error(s"Cannot process derivation step $step")
  }

  def applicablePatterns(step: CombinatoryStep[Unit]): Stream[Rule[MR]] = {
    patterns.toStream.filter(_.appliesTo(SyntacticContext.fromStep(step)))
  }

  /** Creates all full node labellings possible without violating the graph’s alignments.
    */
  def labelNodes(mr: PartialMR[MR], step: CombinatoryStep[Unit]): Stream[PartialMR[MR]] = {
    for (labelledGraph <- labellingFunction.label(mr, step)) yield {
      val labelledMR = labelledGraph.mr
      if(labelledMR.nodes exists { in => SplitDirection.query(in).isEmpty }) {
        throw new Error("Incomplete node labelling")
      }
      labelledGraph.copy(mr = labelledMR)
    }
  }

}
