package gramr.util

import org.scalatest._

class ListUtilSpec extends FlatSpec {

  "Powers of sets" should "produce a correct enumeration" in {

    assert(
      ListUtil.power(List(1, 2, 3), 2) == Stream(
        List(1, 1), List(1, 2), List(1, 3),
        List(2, 1), List(2, 2), List(2, 3),
        List(3, 1), List(3, 2), List(3, 3)
      )
    )

  }

  "Power sets (sub set generation)" should "produce a correct enumeration" in {

    assert(
      ListUtil.subSets(List(1, 2, 3), 3).toSet == Set(
        List(), List(1), List(2), List(3), List(1, 2), List(1, 3), List(2, 3), List(1, 2, 3)
      )
    )

  }

}