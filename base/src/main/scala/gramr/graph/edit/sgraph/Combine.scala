package gramr.graph
package edit
package sgraph

import cats.syntax.traverse._
import cats.instances.vector._
import gramr.graph.mark.Marker
import Transform._
import Edit._
import Graph.ops._

object Combine {
    /**
    * Inserts an argument into a placeholder.
    *
    * This is the same as mergeNodes, but with flipped arguments.
    *
    * @param placeholder a marker pointing to a placeholder
    * @param argument a marker pointing to a node to be inserted at the placeholder
    */
  def fillPlaceholder[MR](placeholder: Marker, argument: Marker)(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    mergeNodes(argument, placeholder)
  }

  /** Merges all same-numbered placeholders.
    */
  def mergePlaceholders[MR : Graph](): Edit[MR, Unit] = {
    for {
      mr <- getMR[MR]
      externalLayers = mr.externalLayers.values.toVector
      _ <- externalLayers traverse { layer =>
        for {
          markers <- layer traverse { node: Indexed[Node] => mark(node.pointer) }
          _ <- markers.tail traverse { marker: Marker => mergeNodes(markers.head, marker) }
        } yield ()
      }
    } yield ()
  }

  def semIdentity[MR : Graph](fGraph: MR, aGraph: MR): Edit[MR, Unit] = {
    if(fGraph.isEmpty) {
      noop
    }
    else {
      fail
    }
  }

  /** The semantic application operator.
    *
    * Inserts an argument sub-graph into a placeholder slot and merges all remaining same-labelled placeholders.
    *
    * @param fGraph the function term of the application
    * @param aGraph the argument term of the application
    */
  def semApply[MR]()(fGraph: MR, aGraph: MR)(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    if(aGraph.isEmpty  // Cannot apply to empty arguments
    || fGraph.externalLayerCount < 1  // need to have at least one placeholder to insert the argument
    ) {
      fail
    }
    else {
      val fGraphIsModifier = isModifier(fGraph)
      for {
        rootMarker <- if(fGraphIsModifier) mark(aGraph.root) else mark(fGraph.root)
        placeholderPointer = placeholder(fGraph, 0)
        placeholderMarker <- mark(placeholderPointer)
        argumentMarker <- mark(aGraph.root)
        _ <- fillPlaceholder(placeholderMarker, argumentMarker)
        _ <- mergePlaceholders()
        _ <- setRoot(rootMarker)
      } yield ()
    }
  }

  /** The semantic modify operator.
    *
    * Inserts an argument sub-graph into a placeholder slot and merges all remaining same-labelled placeholders.
    *
    * @param depth distance of the modified node from the root
    * @param unique if true, only perform the operation if there is one unique node at the specified depth
    * @param fGraph the function term (modifier)
    * @param aGraph the argument term (modifiee)
    */
  def semModify[MR](depth: Int, unique: Boolean = true)(fGraph: MR, aGraph: MR)(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    if(aGraph.isEmpty  // Cannot apply to empty arguments
      || fGraph.externalLayerCount < 1  // need to have at least one placeholder to insert the argument
      || !isModifier(fGraph)  // Must have the outermost placeholder at the root
    ) {
      fail
    }
    else {
      for {
        rootMarker <- mark(aGraph.root)
        placeholderPointer = placeholder(fGraph, 0)
        placeholderMarker <- mark(placeholderPointer)
        argumentLevel = levels(aGraph).applyOrElse(depth, (_: Int) => Set.empty)
        // If so requested, check that the node at the given depth is unique
        argumentNode <- if (!unique || argumentLevel.size == 1) choose(argumentLevel) else choose(Set.empty)
        argumentMarker <- mark(argumentNode)
        _ <- fillPlaceholder(placeholderMarker, argumentMarker)
        _ <- mergePlaceholders()
        _ <- setRoot(rootMarker)
      } yield ()
    }
  }

  def isModifier[MR : Graph](mr: MR): Boolean = placeholder(mr, 0) == mr.root

  /** The semantic composition operator.
    *
    * Inserts an argument sub-graph into the second-outermost placeholder slot.
    * The argument must not have any placeholders.
    *
    * @param fGraph the function term of the application
    * @param aGraph the argument term of the application
    */
  def semCompose[MR](fGraph: MR, aGraph: MR)(implicit mgl: Graph[MR]): Edit[MR, Unit] = {
    if(aGraph.isEmpty) {
      // Don’t accept empty arguments
      fail
    }
    else if (aGraph.externalLayerCount > 0) {
      // For now we don’t treat the case where aGraph is a function
      fail
    }
    else if (fGraph.externalLayerCount < 2) {
      // need to have at least two placeholders
      fail
    }
    else {
      for {
        rootMarker <- mark(fGraph.root)
        firstPlaceholder <- mark(placeholder(fGraph, 0))
        secondPlaceholder <- mark(placeholder(fGraph, 1))
        secondPlaceholderIndex = fGraph.get(placeholder(fGraph, 1)).element.asInstanceOf[ExternalNode].reference.index
        argumentMarker <- mark(aGraph.root)
        _ <- fillPlaceholder(secondPlaceholder, argumentMarker)
        _ <- movePlaceholder(firstPlaceholder, secondPlaceholderIndex)
        _ <- mergePlaceholders()
        _ <- setRoot(rootMarker)
      } yield ()
    }
  }

  /** The semantic substitution operator.
    *
    * Inserts an argument sub-graph into the outermost placeholder slot. Shifts the remaining placeholder of the
    * function term to the outside of the argument's placeholders.
    *
    * @param aGraph the function term of the substitution
    * @param bGraph the argument term of the substitution
    */
  def semSubstitute[MR : Graph](aGraph: MR, bGraph: MR): Edit[MR, Unit] = {
    if(aGraph.externalLayerCount != 2) {
      fail
    }
    else {
      if(!bGraph.isEmpty) {
        for {
          outerPlaceholderMarker <- mark(placeholder(aGraph, 0))
          innerPlaceholderMarker <- mark(placeholder(aGraph, 1))
          bGraphRootMarker <- mark(bGraph.root)
          _ <- fillPlaceholder(outerPlaceholderMarker, bGraphRootMarker)
          _ <- movePlaceholder(innerPlaceholderMarker, bGraph.externalLayerCount)
        } yield ()
      }
      else {
        noop
      }
    }
  }

  /** Applies a semantic operator to a pair of graphs.
    *
    * @param operator the operator to apply
    * @param left the left graph
    * @param right the right graph
    * @return the resulting graph
    */
  def performSemanticOperator[MR: Graph](operator: CombineOperator[MR])(left: MR, right: MR, backward: Boolean): Iterable[MR] = {
    val (a, b) = if(backward) (right, left) else (left, right)
    val bRenamed = b.preventCollisions(a)
    val combined = a union bRenamed
    for {
      (result, _) <- operator(a, bRenamed).perform(combined)
      finalised <- finaliseOperatorResult(result)
    } yield finalised
  }

  def finaliseOperatorResult[MR : Graph](mr: MR): Iterable[MR] = {
    flattenConjunctions.perform(mr).map(_._1.normalise)
  }

  case class NoPlaceholderException[MR : Graph](
    mr: MR,
    depth: Int
  ) extends EditException(
    s"No unique placeholder at depth $depth in:\n$mr"
  )

  case class ArityException[MR : Graph](
    mr: MR,
    actualArity: Int,
    expectedArity: Int
  ) extends EditException(
    s"Expected arity $expectedArity instead of $actualArity in:\n$mr"
  )

  case class NonEmptyException[MR : Graph](mr: MR) extends EditException(
    s"Expected graph to be empty:\n$mr"
  )

}