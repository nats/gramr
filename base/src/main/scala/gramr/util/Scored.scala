package gramr.util

/**
  * Represents something that has been assigned a score.
  */
case class Scored[+T](value: T, score: Double)

object Scored {

  /** An ordering that compares two Scored instances only by their score. If
    * the score is equal, this ordering will consider the instances equal
    * regardless of their content.
    */
  def descendingScoreOnly[T]: Ordering[Scored[T]] = new Ordering[Scored[T]] {
    def compare(x: Scored[T], y: Scored[T]): Int = y.score.compare(x.score)
  }

  /** An ordering that compares two Scored instances only by their score. If
    * the score is equal, this ordering will consider the instances equal
    * regardless of their content.
    */
  def ascendingScoreOnly[T]: Ordering[Scored[T]] = new Ordering[Scored[T]] {
    def compare(x: Scored[T], y: Scored[T]): Int = x.score.compare(y.score)
  }

  /** An ordering that compares two Scored instances first by their score and
    * then by their value.
    */
  def descending[T: Ordering]: Ordering[Scored[T]] = new Ordering[Scored[T]] {
    def compare(x: Scored[T], y: Scored[T]): Int = {
      val scoreCmp = y.score.compare(x.score)
      if (scoreCmp != 0) scoreCmp
      else implicitly[Ordering[T]].compare(y.value, x.value)
    }
  }

  /** An ordering that compares two Scored instances first by their score and
    * then by their value.
    */
  def ascending[T: Ordering]: Ordering[Scored[T]] = new Ordering[Scored[T]] {
    def compare(x: Scored[T], y: Scored[T]): Int = {
      val scoreCmp = x.score.compare(y.score)
      if (scoreCmp != 0) scoreCmp
      else implicitly[Ordering[T]].compare(x.value, y.value)
    }
  }

  /**
    * Enumerates the best combinations of a range of given sizes.
    */
  def bestCombinations[T: Ordering](sizes: Range)(options: Seq[Scored[T]]): Stream[Vector[Scored[T]]] = {
    val sensibleSizes = Math.max(sizes.start, 0) to Math.min(sizes.end, options.size)
    val ord = new Ordering[Vector[Scored[T]]] {
      def reduceScored(x: Vector[Scored[T]]): Scored[Vector[T]] = x.foldLeft(Scored(Vector.empty[T], 0.0)) {
        case (Scored(vs, ss), Scored(v, s)) => Scored(vs :+ v, ss + s)
      }

      def compare(a: Vector[Scored[T]], b: Vector[Scored[T]]): Int = {
        descending[Vector[T]](VectorUtils.lexicographicOrdering[T]).compare(reduceScored(a), reduceScored(b))
      }
    }

    StreamUtils.mergeSortedStreams(sensibleSizes.toVector.map(s => bestCombinationsOfN(s)(options)))(ord)
  }

  /**
    * Enumerates the best combinations of `count` of the scored options.
    *
    * If `count` is greater than the number of options, behaves as though `count` was equal to
    * the number of options.
    */
  def bestCombinationsOfN[T: Ordering](count: Int)(options: Seq[Scored[T]]): Stream[Vector[Scored[T]]] = {

    val alg = new BestCombinationsAlgorithm(options)

    val sortedOptions = options.toVector.sorted(descending)

    val firstIndices = (0 until count).toVector.take(options.size)

    alg.bestFrom(firstIndices).map { is => is.map { i => options(i) } }
  }

  class BestCombinationsAlgorithm[T: Ordering](val options: Seq[Scored[T]]) {

    def indexSetScore(indices: Vector[Int]): Double = indices.map(i => options(i).score).sum

    /**
      * Generates the set of sets of indices that results in shifting one of the indices down by one.
      * (These are the candidates for the next-best set of indices.)
      */
    def nextCandidates(indices: Vector[Int]): Stream[Vector[Int]] = {
      indices.indices.toStream flatMap { i =>
        if (i + 1 < indices.size && indices(i + 1) <= indices(i) + 1)
          Stream.empty // cannot increase to the same or a higher index than the right neighbour
        else if (indices(i) + 1 >= options.size)
          Stream.empty // cannot increase to a nonexistent index
        else
          Stream(indices.updated(i, indices(i) + 1))
      }
    }

    def bestFrom(indices: Vector[Int]): Stream[Vector[Int]] = {
      var agenda = Set(indices)
      var history = Set.empty[Vector[Int]]

      def iterate: Stream[Vector[Int]] = {
        if (agenda.isEmpty) Stream.Empty
        else {
          val next = agenda.maxBy(indexSetScore)
          history = history + next
          agenda = agenda - next ++ nextCandidates(next).filter(!history.contains(_))
          next #:: iterate
        }
      }

      iterate
    }


  }

}

