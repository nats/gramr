package gramr.graph
package visit

import scala.collection.mutable

import Graph.ops._

/** A visitor which checks whether a graph is connected.
 *
 * The visitor is designed to be used with `DepthFirstSearch`. After the search, `isConnected` indicates
 * whether the graph is connected.
 */
class ConnectedVisitor[MR: Graph](val graph: MR) extends Visitor {
  private val unvisited = mutable.Set.empty[Int]
  unvisited ++= graph.nodes.map(_.pointer)

  override def visitNode(nodeIndex: Int): Unit = {
    unvisited.remove(nodeIndex)
  }

  def isConnected: Boolean = unvisited.isEmpty
}
