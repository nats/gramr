package gramr.jobs

import gramr.util.progressbar.ProgressBar
import monix.execution.Scheduler
import monix.reactive.Observable

import scala.concurrent.duration._

class AnyOps[A, B](val job: Job[A, B]) {

  implicit val scheduler: Scheduler = Scheduler.io("progressBar")

  def progressBar: Job[A, B] = {
    val tag = s"${job.name} "
    val displayType = ProgressBar.OneLineUpdatingDisplay()
    val bar = new ProgressBar(job.size, tag, displayType)

    var isRunning: Boolean = false
    def doTick(): Unit = {
      if(isRunning) {
        bar.display()
      }
    }

    val tick = Observable.timerRepeated(1.second, 1.second, ()).foreach(_ => doTick())

    job.onBeginIteration { () =>
      isRunning = true
    }

    job.listenToItems { _ =>
      bar.increment()
      bar.display()
    }

    job.onFinishIteration { () =>
      tick.cancel()
      isRunning = false
      bar.finish()
    }
  }

}
