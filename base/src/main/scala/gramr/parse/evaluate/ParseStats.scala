package gramr.parse.evaluate

import gramr.util.SimpleRetrievalStats

case class ParseStats(
  successful: Boolean,
  bestScoredParse: SimpleRetrievalStats,
  bestFoundParse: SimpleRetrievalStats
)
