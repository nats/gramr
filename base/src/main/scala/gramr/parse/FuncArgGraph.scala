package gramr.parse

import gramr.graph._
import gramr.construction.SyntacticContext

import Graph.ops._

/** Handles the process of combining a function and argument graph.
  */
class FuncArgGraph[MR](
  val func: MR,
  val arg: MR
)(implicit val meaningGraphLike: Graph[MR]) {

  import meaningGraphLike._

  lazy val argProcessed: MR = shiftBindingGroups(func, shiftExternalReferences(func, arg.preventCollisions(func)))

  lazy val union: MR = func union argProcessed

  case class Binding(placeholder: Int, referent: Int) {
    def applyTo(g: MR): Option[MR] = {
      assert(g.node(placeholder) match { case Indexed(ExternalNode(_, _), _) => true; case _ => false })

      val bound = for {
      // Move all edges adjacent to node2 over to node1.
        merged <- (g mapNonDeterministic { iel =>
          if(iel.element.isEdge) {
            transformEdge(iel.asInstanceOf[Indexed[Edge]]).toStream
          }
          else {
            Stream(iel)
          }
        }).headOption

        // Copy over the “root” attribute to the referent
        // The graph will be focused on the root by OnionBinding
        rerooted = if (g.node(placeholder).element.attributes.contains("root")) {
          merged.mapNodeAttributes {
            case Indexed(n@IsNode(), i) =>
              if (i == referent) {
                val old =
                  if (n.attributes.contains("root")) n.attributes.get[DefaultAttribute[Boolean]]("root").value
                  else false
                val `new` = g.node(placeholder).element.attributes.get[DefaultAttribute[Boolean]]("root").value
                n.attributes + ("root" -> DefaultAttribute(old || `new`))
              }
              else n.attributes
          }
        }
        else merged
      } yield rerooted remove placeholder

      bound
    }

    def transformEdge(e: Indexed[Edge]): Option[Indexed[Edge]] = e match {
      case Indexed(Edge(label, source, target, attr), i) =>
        val newSource = if (source == placeholder) referent else source
        val newTarget = if (target == placeholder) referent else target
        if (newSource == newTarget) None // Binding created a loop
        else Some(Indexed(Edge(label, newSource, newTarget, attr), i))
    }

    def insertDeferredBinding(g: MR, bindingGroup: Int): Option[MR] = {
      if (g.degree(placeholder) != 1) {
        //throw new Error(s"Deferred bindings can only be used with true half-edge graphs, where every external node has degree 1. The following graph violates this rule:\n$g")
        None
      }
      else {

        // Move the root attribute away from the placeholder
        val placeholderNode = g.node(placeholder)
        val rerooted =
          if (placeholderNode.element.attributes.contains(
            "root"
          ) && placeholderNode.element.attributes.get[DefaultAttribute[Boolean]]("root").value) {
            val placeholderNeighbour =
              g.edges.find(_.element.source == placeholder).map(_.element.target).getOrElse(
                g.edges.find(_.element.target == placeholder).map(_.element.source).get
              )
            g.mapNodeAttributes {
              case Indexed(node@IsNode(), pointer) =>
                if (pointer == placeholderNeighbour)
                  node.attributes + ("root" -> DefaultAttribute(true))
                else
                  node.attributes
            }
          }
          else {
            g
          }

        // Copy, transform and insert the (single) adjacent edge to the placeholder
        val halfEdge = (rerooted.edges find {
          case Indexed(edge@IsEdge(), _) =>
            edge.source == placeholder || edge.target == placeholder
        }).get
        for {
          Indexed(bindingEdge@IsEdge(), _) <- transformEdge(halfEdge)
          labelled = bindingEdge.setAttribute("deferredBindingGroup", DefaultAttribute(bindingGroup))
          edgeAdded = rerooted.addEdge(labelled)._1
        } yield edgeAdded
      }
    }

  }


  // Calculating bindings

  def funcArgBindings(
    placeholderFilter: Indexed[ExternalNode] => Boolean,
    referentFilter: Indexed[Node] => Boolean
  ): Seq[Binding] = {
    val placeholders = func.externalNodes.filter(placeholderFilter).map(_.pointer)

    for {
      placeholder <- placeholders.toSeq
      referent <- argProcessed.nodes.filter(referentFilter)
    } yield Binding(placeholder, referent.pointer)
  }

  def outermostShellPlaceholders: Indexed[ExternalNode] => Boolean = {
    val outermostShell: Int = {
      val refs = func.externalNodes.map {
        _.element.reference.index
      }
      if (refs.isEmpty) -1 else refs.max
    }

    def isPlaceholder(node: Indexed[ExternalNode]): Boolean =
      node.element.reference.index == outermostShell

    isPlaceholder
  }

  def anyNode: Indexed[Node] => Boolean = _ => true

  def noExternalNodes: Indexed[Node] => Boolean = {
    case Indexed(n@IsNode(), _) => !n.isExternalNode
    case _ => true
  }


  // Selectively apply some bindings

  def applyBindings(bindings: Seq[Binding]): Option[MR] = {
    if (func.isEmpty) {
      Some(argProcessed)
    }
    else {
      if (bindings.isEmpty) {
        None
      }
      else {
        for {
        // Apply all the bindings
          applied <- bindings.foldLeft(Some(union): Option[MR]) { (maybeMR, binding) =>
            maybeMR match {
              case Some(mr) => binding.applyTo(mr)
              case None => None
            }
          }
        } yield {
          val result = {
            val bldr = builder(applied)
            bldr.setRoot(newRoot(bindings))
            bldr.build.normalise
          }
          result
        }
      }
    }
  }

  def applyBindingDecisions(decisions: Seq[BindingDecision[MR]]): Option[MR] = {
    val bindings = decisions.map { decision =>
      Binding(decision.placeholder, decision.referent)
    }
    applyBindings(bindings)
  }

  def createDeferredBindings(bindings: Seq[Binding]): Option[MR] = {
    import meaningGraphLike._
    if (func.isEmpty) {
      Some(arg)
    }
    else {
      if (bindings.isEmpty || !Predicates.isTrueHalfEdgeGraph(func)) {
        Some(func)
      }
      else {
        val firstBindingGroup = Seq(
          maxBindingGroup(func),
          maxBindingGroup(argProcessed),
          -1
        ).max + 1

        // Mark all the placeholders with an attributes so we can remove them later.
        val placeholders = bindings.map(_.placeholder).toSet
        val markedUnion = this.union.mapNodeAttributes {
          case Indexed(n@IsNode(), pointer) =>
            if (placeholders.contains(pointer)) {
              n.attributes + ("isPlaceholder" -> DefaultAttribute(true))
            }
            else {
              n.attributes
            }
        }

        def isPlaceholder(nodePointer: Int, mr: MR): Boolean =
          mr.node(nodePointer).element.attributes.contains("isPlaceholder")

        // Assign a binding group to each placeholder
        val bindingGroups = (placeholders zip Stream.from(firstBindingGroup)).toMap

        val bindingsInserted = bindings.foldLeft(markedUnion) { (mr, binding) =>
          // If a deferred binding cannot be inserted, simply remove it.
          binding.insertDeferredBinding(mr, bindingGroups(binding.placeholder)) match {
            case Some(newMR) => newMR
            case None => mr
          }
        }

        // Inserting the bindings does not remove the placeholders, so we need to do that.
        val placeholdersRemoved = bindingsInserted filter {
          case Indexed(IsNode(), nodePointer) => !isPlaceholder(nodePointer, bindingsInserted)
          case Indexed(Edge(_, source, target, _), _) =>
            !isPlaceholder(source, bindingsInserted) && !isPlaceholder(target, bindingsInserted)
        }

        val result = {
          val bldr = builder(placeholdersRemoved)
          bldr.setRoot(newRoot(bindings))
          bldr.build
        }
        Some(result)
      }
    }
  }


  /** Given a set of bindings, returns a node that is not used as a placeholder (in the given bindings)
    * and that should become the root after applying those bindings.
    */
  private def newRoot(bindings: Seq[Binding]): Int = {
    val placeholders = bindings.map(_.placeholder).toSet
    if (!placeholders.contains(func.root)) {
      func.root
    }
    else {
      argProcessed.root
    }
  }

  private def shiftExternalReferences(l: MR, r: MR): MR = {
    // Shift external references (dependent layers are moved outside of head layers)
    val headOutermost = (Iterable(-1) ++ l.externalNodes.flatMap { n =>
      n.element.reference match {
        case ExternalReference(index) => Iterable(index)
        case _ => Iterable()
      }
    }).max

    r.mapNodes {
      case Indexed(ExternalNode(ExternalReference(index), attrs), i) =>
        Indexed(ExternalNode(ExternalReference(index + headOutermost + 1), attrs), i)
      case n => n
    }
  }

  private def shiftBindingGroups(head: MR, dependent: MR): MR = {
    val headMax = maxBindingGroup(head)
    dependent.mapEdgeAttributes {
      case Indexed(edge@IsEdge(), _) =>
        edge.attributes.getOption[DefaultAttribute[Int]]("deferredBindingGroup") match {
          case Some(bindingGroup) => edge.attributes + ("deferredBindingGroup" -> DefaultAttribute(
            bindingGroup.value + headMax + 1
          ))
          case None => edge.attributes
        }
    }
  }

  private def maxBindingGroup(mr: MR): Int = {
    val groups = mr.edges map { e =>
      e.element.attributes.getOption[DefaultAttribute[Int]](
        "deferredBindingGroup"
      )
    } collect { case Some(attr) => attr.value }
    (Iterable(-1) ++ groups).max
  }

}

object FuncArgGraph {

  def from[MR : Graph](i: (SyntacticContext, MR, MR)): FuncArgGraph[MR] = {
    val (context, left, right) = i
    val (func, arg) =
      if (context.combinator.headIndex == 0) (left, right)
      else (right, left)
    new FuncArgGraph(func, arg)
  }

}
