package gramr.util

import org.scalatest._

class DateTimeSpec extends FreeSpec {

  import DateTime._

  "The numeric date parser" - {
    "should parse a fully specified date" in {
      val expected = Some(Date(Some(2016), Some(5), Some(13)))
      val parsed = parseNumericDate("160513")
      assert(parsed == expected)
    }

    "should parse a date in 2000" in {
      val expected = Some(Date(Some(2000), Some(5), Some(13)))
      val parsed = parseNumericDate("000513")
      assert(parsed == expected)
    }

    "should parse a date without day" in {
      val expected = Some(Date(Some(2016), Some(5), None))
      val parsed = parseNumericDate("160500")
      assert(parsed == expected)
    }

    "should parse a date without month and day" in {
      val expected = Some(Date(Some(2016), None, None))
      val parsed = parseNumericDate("160000")
      assert(parsed == expected)
    }
  }

  "The dash date parser" - {
    "should parse a dash date" in {
      val expected = Some(Date(Some(2016), Some(5), Some(13)))
      val parsed = parseDashDate("2016-05-13")
      assert(parsed == expected)
    }
  }

  "The American slash date parser" - {
    "should parse a slash date" in {
      val expected = Some(Date(Some(2016), Some(5), Some(13)))
      val parsed = parseAmericanSlashDate("05/13/2016")
      assert(expected == parsed)
    }
  }

  "The text date parser" - {
    "should parse a variety of standard dates" in {

      val expectedY = Some(Date(Some(2016), None, None))

      assert(expectedY == parseTextDate(Seq("2016")))

      val expectedYM = Some(Date(Some(2016), Some(9), None))

      assert(expectedYM == parseTextDate(Seq("September", "2016")))
      assert(expectedYM == parseTextDate(Seq("September", "of", "2016")))
      assert(expectedYM == parseTextDate(Seq("September", ",", "2016")))
      assert(expectedYM == parseTextDate(Seq("Sept.", "2016")))
      assert(expectedYM == parseTextDate(Seq("Sept", "2016")))

      val expectedMD = Some(Date(None, Some(9), Some(21)))

      assert(expectedMD == parseTextDate(Seq("September", "21")))
      assert(expectedMD == parseTextDate(Seq("September", "21st")))
      assert(expectedMD == parseTextDate(Seq("September", "21nd")))
      assert(expectedMD == parseTextDate(Seq("September", "21rd")))
      assert(expectedMD == parseTextDate(Seq("September", "21th")))
      assert(expectedMD == parseTextDate(Seq("Sept.", "21st")))
      assert(expectedMD == parseTextDate(Seq("Sept", "21st")))
      assert(expectedMD == parseTextDate(Seq("September", ",", "21st")))
      assert(expectedMD == parseTextDate(Seq("21st", "of", "September")))

      val expectedMD2 = Some(Date(None, Some(9), Some(1)))

      assert(expectedMD2 == parseTextDate(Seq("September", "1st")))

      val expectedD = Some(Date(None, None, Some(1)))

      assert(expectedD == parseTextDate(Seq("1st")))

      assert(parseTextDate(Seq("1")).isEmpty)
    }
  }

}
