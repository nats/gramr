package gramr

import gramr.parse.Parser
import gramr.parse.chart.Chart
import gramr.parse.evaluate.ParseStats

import scala.collection.immutable.ListMap

package object jobs {

  type NamedParses[MR, FV] = ListMap[String, Chart[MR, FV]]
  type NamedParsers[MR, FV, E] = ListMap[String, Parser[MR, FV, E]]
  type NamedEvals = ListMap[String, ParseStats]

}
