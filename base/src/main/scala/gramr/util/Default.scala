package gramr.util

trait Default[T] {
  def default: T
}