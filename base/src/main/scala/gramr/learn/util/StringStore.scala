package gramr.learn.util

/** Handles conversion between feature names and feature IDs.
  *
  * Generally speaking, there should always be one global string store. Between sessions, this string store
  * can be serialised as part of the model.
  */
@SerialVersionUID(2L)
class StringStore extends FeatureIndex with Serializable {
  private val store = collection.mutable.HashMap.empty[String, Int]
  private var reverseStore = collection.immutable.Vector.empty[String]
  private var nextIndex = 0

  /** Translate a string to a feature ID, inventing one if necessary.
    *
    * @param s a feature name
    * @return a feature ID
    */
  def apply(s: String): Int =
    synchronized {
      store.get(s) match {
        case None =>
          val index = nextIndex
          store.put(s, index)
          reverseStore = reverseStore :+ s
          nextIndex += 1
          index
        case Some(index) =>
          index
      }
    }

  /** Return the feature name corresponding to a given feature ID.
    *
    * @param i a feature ID
    * @return a feature name
    * @throws NoSuchElementException if there is no such feature ID
    */
  def reverse(i: Int): String = reverseStore(i)

  /** Return the number of features mapped by the StringStore.
    *
    * @return the size
    */
  def size: Int = nextIndex
}
