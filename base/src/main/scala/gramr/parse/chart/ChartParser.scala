package gramr.parse.chart

import gramr.parse.{ActionPredicate, Decision, Parser}
import gramr.text.{Span, Token}

/** Base trait for parsers that use the ChartParsing functionality.
  *
  * @tparam MR type of meaning representation
  * @tparam F type of feature vector
  * @tparam L type of decision label
  * @tparam C type of example context
  */
trait ChartParser[MR, F, L, C] extends Parser[MR, F, C] with ChartParsing[MR, F, L] {

  def parse(sentence: Vector[Token], context: C): Chart

  def label(decision: Decision[MR], span: Span, context: C): L

  def actionFilter: ActionPredicate[MR, F, C]
}
