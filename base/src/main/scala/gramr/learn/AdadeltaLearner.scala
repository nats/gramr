package gramr.learn

import com.typesafe.scalalogging.LazyLogging
import gramr.learn.FeatureVector.Syntax._
import gramr.text.Example


/** Implements the Adadelta update.
  *
  * @param initialModel initial parameter configuration
  * @tparam MR type of meaning representation
  * @tparam FV type of feature vector
  * @tparam E type of example
  */
class AdadeltaLearner[MR, FV: FeatureVector, E: Example](
  initialModel: LinearModel,
  val rho: Double = 0.95,
  val epsilon: Double = 1e-6
) extends LinearLearner[MR, FV, E] with LazyLogging {

  private var model: LinearModel = initialModel

  private var gradientAccumulator = implicitly[FeatureVector[FV]].empty
  private var deltaAccumulator = implicitly[FeatureVector[FV]].empty

  /**
    * The model for use in training.
    */
  def trainModel: LinearModel = model

  /**
    * The model for use in testing – with adadelta, the same as the train model.
    */
  def testModel: LinearModel = model

  /** Updates the parameters directly by adding a feature delta.
    *
    * The learning rate is taken into account
    *
    * This action is counted as one single update for learning rate / decay / averaging purposes.
    *
    * @param gradients the parameter deltas to apply
    */
  def updateGradients(gradients: Iterable[FV]): Unit = {
    // Sum the individual update gradients into a single gradient vector
    val gradient = gradients.foldLeft(implicitly[FeatureVector[FV]].empty)(_ + _)

    // Calculate the Adadelta update and accumulators
    gradientAccumulator = weightedSum(rho, gradientAccumulator, square(gradient))
    val delta = elementWiseProduct(
      elementWiseRatio(rms(deltaAccumulator), rms(gradientAccumulator)),
      gradient
    ) * -1.0
    deltaAccumulator = weightedSum(rho, deltaAccumulator, square(delta))

    // Update the model
    model = model.add(delta)
  }

  def rms(fv: FV): FV = fv.mapValues(x => Math.sqrt(x + epsilon))

  def square(fv: FV): FV = fv.elementWise(_ * _)(fv)

  def elementWiseProduct(fv1: FV, fv2: FV): FV = fv1.elementWise(_ * _)(fv2)

  def elementWiseRatio(fv1: FV, fv2: FV): FV = fv1.elementWise(_ / _)(fv2)

  def weightedSum(weight: Double, fv1: FV, fv2: FV): FV = (fv1 * weight) + (fv2 * (1.0 - weight))

}
