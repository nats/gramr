package gramr.util

import java.io.File
import java.nio.file.attribute.BasicFileAttributes
import scala.collection.mutable.ArrayBuffer
import java.nio.file._

object Glob {

  def resolve(glob: String): List[File] = {
    val f = new File(glob)
    val dir = f.getParent
    val pattern = f.getName
    val fs = FileSystems.getDefault
    val matcher = fs.getPathMatcher(s"glob:$glob")

    var files = ArrayBuffer.empty[Path]

    Files.walkFileTree(
      FileSystems.getDefault.getPath(dir), new SimpleFileVisitor[Path] {
        override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
          if (matcher.matches(file)) {
            files += file
          }
          FileVisitResult.CONTINUE
        }
      }
    )

    files.toList.map(_.toFile())
  }
}