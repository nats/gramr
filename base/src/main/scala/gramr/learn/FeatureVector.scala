package gramr.learn


trait FeatureVector[T] extends Serializable with BaseVector[T] {

  type K = String
  type V = Double

  def from(entries: Iterable[(K, V)]): T

  def empty: T

  def emptyParams: T

  def toValue(d: Double): V

  def toDouble(v: V): Double

  def toOps(fv: T): FeatureVectorOps[T]
}

object FeatureVector {

  /**
    * Provides easy access to the methods of FeatureVector’s type classes.
    */
  object Syntax {

    import scala.language.implicitConversions

    implicit def toValue[F](d: Double)(implicit fv: FeatureVector[F]): Double =
      fv.toValue(d)

    implicit def toDouble[F](v: Double)(implicit fv: FeatureVector[F]): Double =
      fv.toDouble(v)

    implicit def toOps[F](vector: F)(implicit fv: FeatureVector[F]): FeatureVectorOps[F] =
      fv.toOps(vector)

  }

}

trait FeatureVectorOps[T] {

  type K = String
  type V = Double

  /**
    * Element-wise addition.
    */
  def +(other: T): T

  /**
    * Element-wise subtraction.
    */
  def -(other: T): T

  /** Broadcast any element-wise operation.
    *
    * @param f element-wise operator
    */
  def elementWise(f: (V, V) => V)(other: T): T

  /**
    * Scalar multiplication.
    */
  def *(v: V): T

  def get(k: K): V

  def entries: Iterable[(K, V)]

  def indexedEntries: Iterable[(Int, Double)]

  def size: Int

  def mapValues(f: V => V): T

  def magnitudeSquared: V
}
