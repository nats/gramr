package gramr.learn

import gramr.parse.chart.State
import FeatureVector.Syntax._


case class StatePairUpdate[MR, FV: FeatureVector](
  positive: State[MR, FV],
  negative: State[MR, FV]
) extends Update[FV] {

  val gradient: FV = negative.features - positive.features

}
