package gramr.parse.cky

import com.typesafe.scalalogging.LazyLogging
import gramr.ccg.{CombinatorSystem, DerivationNode, SyntacticCategory}
import gramr.construction.SyntacticContext
import gramr.learn.FeatureVector
import gramr.parse._
import gramr.parse.chart._
import gramr.parse.scorer.DecisionScorer
import gramr.text.{Span, Token}
import gramr.util.Scored

class CkyParser[MR, F : FeatureVector, L: ChartLabel: Ordering, C](
  val lexicalChoice: LexicalChoice[C, MR],
  val binaryChoice: BinaryChoice[MR],
  val combinatorSystem: CombinatorSystem,
  val decisionScorer: DecisionScorer[MR, C, InheritableLabel[L]],
  val actionFilter: ActionPredicate[MR, F, C],
  val leafCategories: (C, Span) => Iterable[SyntacticCategory],
  val dynamicBeamSize: Span => Int,
  val branchingFactor: Option[Double],
  val limitLexicalActions: Boolean = true,
  val limitUnaryActions: Boolean = true
)(implicit lHasFeatures: HasFeatures[F, L]) extends ChartParser[MR, F, InheritableLabel[L], C] with LazyLogging {

  override def label(decision: Decision[MR], span: Span, context: C): InheritableLabel[L] =
    decisionScorer.label(decision, span, context)

  override def featureVector: FeatureVector[F] = implicitly[FeatureVector[F]]
  override def chartLabel: ChartLabel[InheritableLabel[L]] = InheritableLabel.chartLabel
  override def hasFeatures: HasFeatures[F, InheritableLabel[L]] = InheritableLabel.hasFeatures[F, L]
  override def labelOrdering: Ordering[InheritableLabel[L]] = InheritableLabel.ordering[L]

  override def parse(sentence: Vector[Token], context: C): Chart = {
    val chart = Chart.empty(sentence)
    val filter = actionFilter(context)

    // Search over all spans in the sentence, starting with the smallest spans
    for {
      spanSize <- 1 to sentence.size
    } {
      (0 to (sentence.size - spanSize)).par.foreach { start =>
        val end = start + spanSize - 1
        val span = Span(start, end)
        parseSpan(sentence, context, chart, filter, span)
      }
    }

    // Mark all states in the root cell as output states
    for(state <- chart.root.states) {
      chart.outputStates.append(state)
    }

    chart
  }

  def parseSpan(sentence: Vector[Token], context: C, chart: Chart, filter: Action => Boolean, span: Span): Unit = {
    val Span(start, end) = span
    val beamSize = dynamicBeamSize(span)

    def insertBinaryDecisions(): Unit = {
      // Insert binary decisions
      for {
        split <- start + 1 to end
      } {
        val lstates = chart.states(Span(start, split - 1)).toVector
        val rstates = chart.states(Span(split, end)).toVector

        // Sort lstate/rstate combinations by the sums of the states' scores.
        // The idea is to apply the branching limitation: we want to score only a constant number of actions.
        // Therefore we sort the predecessors by how promising they are and later keep the first n resulting actions.
        val combinations =
          (for (lstate <- lstates; rstate <- rstates) yield Scored((lstate, rstate), lstate.score + rstate.score)).
            sortBy(-_.score).map(_.value)

        // First build unlabelled actions so we can filter them without having to extract features
        val unlabelledActions: Iterator[Action] = for {
          // For each of the best combinations
          (lstate, rstate) <- combinations.iterator
          lsyn = lstate.syntax.synCat
          rsyn = rstate.syntax.synCat

          // For each applicable combinator
          decision <- binaryChoice.decisionSpace(
            DerivationNode(sentence, lstate.span, lsyn, lstate.mr),
            DerivationNode(sentence, rstate.span, rsyn, rstate.mr)
          )
          syntax = SyntaxInformation(sentence, Span(lstate.span.start, rstate.span.end), decision.context.synCat)

          // Generate an unlabelled action (without extracted features)
          unlabelledAction = Action.unlabelled(decision, syntax, Vector(lstate, rstate)) if filter(unlabelledAction)
        } yield unlabelledAction

        // Apply branching limitation: only select the first n actions (that passed the filter)
        val branchingLimitedActions = branchingFactor match {
          case Some(factor) => unlabelledActions.take((beamSize * factor).ceil.toInt)
          case None => unlabelledActions
        }

        // Now compute features and scores for any action that wasn't symbolically excluded
        val labelledActions = branchingLimitedActions.map(action => Action.label(action, label(action.decision, action.span, context)))

        // Enter the labelled actions into the chart
        labelledActions.foreach(chart.putAction)
        chart.cell(span).enforce(beamSize)
      }
    }

    def insertLexicalDecisions(): Unit = {
      // Insert lexical decisions
      val leafCats = leafCategories(context, span).toSet
      val lexicalChoiceInstance = lexicalChoice(context)
      for {
        synCat <- leafCats
        syntax = SyntaxInformation(sentence, span, synCat)
        decision <- lexicalChoiceInstance(syntax)
        // Note that we're looping over a set here, so each decision is kept only once.
      } {
        // First perform filtering on an unlabelled action, to save on feature extraction
        val unlabelledAction = Action.unlabelled(decision, decision.syntax, Vector.empty)
        if (filter(unlabelledAction)) {
          val action = Action.label(unlabelledAction, label(decision, unlabelledAction.span, context))
          chart.putAction(action)
        }
      }

      if(limitLexicalActions) {
        chart.cell(span).enforce(beamSize)
      }
      else {
        // Make sure the states are sorted so the first state is guaranteed to be the best one.
        chart.cell(span).sortStates()
      }
    }

    def insertUnaryDecisions(): Unit = {
      // Insert unary conversions of syncats
      val currentStates = chart.states(span)
      for(state <- currentStates) {
        val synCat = state.syntax.synCat
        for {
          (cmb, converted) <- combinatorSystem.unaryConversions(synCat) if converted != synCat
        } {
          val decision = UnaryDecision(
            result = state.mr,
            context = SyntacticContext(cmb, converted, List(synCat))
          )
          val action = new Action(
            decision,
            label(decision, state.syntax.span, context),
            state.syntax.copy(synCat = converted),
            Vector(state)
          )
          if(filter(action)) {
            chart.putAction(action)
          }
        }
      }

      if(limitUnaryActions) {
        chart.cell(span).enforce(beamSize)
      }
      else {
        // Make sure the states are sorted so the first state is guaranteed to be the best one.
        chart.cell(span).sortStates()
      }
    }

    insertBinaryDecisions()
    insertLexicalDecisions()
    insertUnaryDecisions()
  }

}
