package gramr.graph

import gramr.amr._
import Graph.ops._
import com.typesafe.scalalogging.LazyLogging
import gramr.graph.AmrTreeConversion.NodeName
import gramr.graph.GraphFromAmrTree.ConversionException

object AmrTreeConversion {

  import scala.language.implicitConversions

  implicit def meaningGraphToAmrTree[MR : Graph](
    graph: MR
  ): ToAmrTreeConvertible[MR] = new ToAmrTreeConvertible(graph)

  // Node names

  case class NodeName(name: String, index: Int) {
    override def toString: String =
      if (index == 1)
        name
      else
        name ++ index.toString
  }

  object NodeName {

    def parse(l: String): Option[NodeName] = {
      val re = """([a-zA-Z]+)([0-9]*)""".r
      l match {
        case re(n, i) =>
          val index =
            if (i.isEmpty) 1
            else Integer.parseInt(i)
          Some(NodeName(n, index))
        case _ => None
      }
    }

  }

}


trait FromAmrTree[T] {

  def apply(amr: AmrTree): T

}


class GraphFromAmrTree[MR : Graph] extends FromAmrTree[MR] with LazyLogging {

  override def apply(amr: AmrTree): MR = {
    val fixedAmr = makeNamesUnique(amr)

    val builder = implicitly[Graph[MR]].builder

    // First pass, create referent nodes
    val referentMapping = collection.mutable.Map.empty[String, Int]

    def getExternalNode(index: Int): Int = {
      referentMapping.get(s"<$index>") match {
        case Some(p) => p
        case None =>
          val p = builder.addExternalNode(ExternalReference(index))
          referentMapping.put(s"<$index>", p)
          p
      }
    }

    def buildReferentNodes(amr: AmrTree): Option[Int] = amr match {
      case InnerNode(label, links, _) =>
        val pointer: Option[Int] = label match {
          case extRe(indexStr) => Some(getExternalNode(indexStr.toInt))
          case _ =>
            // Inner nodes are always ReferentNodes, thus should always have instance links
            links.find(_.label == "instance") match {
              case None =>
                throw new ConversionException("Missing instance edge")
              case Some(instanceEdge) =>
                val concept = instanceEdge.subTree.asInstanceOf[Leaf].label
                NodeName.parse(label) match {
                  case Some(parsedLabel) =>
                    // This code assumes that the labels are unique in the AMR. Not always given, but if anything
                    // is to be done about it, this should be done in a pre-processing step.
                    val pointer: Int = builder.addReferentNode(concept, Attributes(amr.attributes).extend("node_name", DefaultAttribute(label)))
                    referentMapping.put(label, pointer)
                    Some(pointer)
                  case None => None
                }
            }
        }
        links foreach { link => buildReferentNodes(link.subTree) }
        pointer
      case Leaf(label, _) => label match {
        case extRe(indexStr) => Some(getExternalNode(indexStr.toInt))
        case _ => None
      }
      case _ => None
    }

    val rootNodePointer: Option[Int] = buildReferentNodes(fixedAmr)

    // Second pass, build edges and constant nodes
    // buildGraph is responsible for building all of a node's outgoing edges,
    // but assumes the source node itself has already been built.
    def buildGraph(amr: AmrTree, currentNode: Int): Unit = amr match {
      case InnerNode(_, links, _) =>
        links.filter(_.label != "instance") foreach { link =>
          link.subTree match {
            case InnerNode(targetLabel, _, _) =>
              // Inner nodes are always referent nodes
              val targetNode = referentMapping(targetLabel)
              builder.addEdge(Edge(link.label, currentNode, targetNode, Attributes(link.attributes)))
              buildGraph(link.subTree, targetNode)
            case Leaf(targetLabel, _) =>
              // Choose whether to use an existing referent node or create a constant node
              val targetNode =
                if (referentMapping.contains(targetLabel))
                  referentMapping(targetLabel)
                else {
                  builder.addConstantNode(targetLabel, Attributes(link.subTree.attributes))
                }
              builder.addEdge(Edge(link.label, currentNode, targetNode, Attributes(link.attributes)))
            case EmptyAmrTree(attributes) =>
              throw new Exception("Unexpectedly encountered EmptyAmrTree during parsing")
          }
        }
      case Leaf(_, _) => ()
      case _ => ()
    }

    // If rootNodePointer is undefined, that means the entire graph is empty or comprised of one constant or coref node.
    if (rootNodePointer.isDefined) {
      buildGraph(fixedAmr, rootNodePointer.get)
      builder.setRoot(rootNodePointer.get)
    }
    else {
      amr match {
        case Leaf(label, attributes) =>
          builder.addConstantNode(amr.asInstanceOf[Leaf].label, Attributes(attributes))
        case EmptyAmrTree(attributes) =>
          ()
        case InnerNode(label, children, attributes) =>
          throw new Exception("Unexpectedly encountered InnerNode")
      }
    }

    try {
      val mr = builder.build

      if (!mr.isSaneAMR) {
        mr.isSaneAMR
        throw new ConversionException("AMR meaning graph is not sane!")
      }

      mr
    }
    catch {
      case error: ConversionException =>
        logger.error(s"ConversionError: ${error.message}")

        val builder = implicitly[Graph[MR]].builder
        builder.addReferentNode("invalid")
        builder.build
    }

  }

  private val extRe = """<(\d+)>""".r

}


object GraphFromAmrTree {

  class ConversionException(val message: String) extends Exception(message)

}


class ToAmrTreeConvertible[MR : Graph](unannotatedGraph: MR) {

  import ToAmrTreeConvertible._
  import AmrTreeConversion._

  val graph: MR = annotateNodeNames(unannotatedGraph)

  def toAmrTree: AmrTree = {
    def parseNode(walkTree: VisitNode): AmrTree = {
      val node = graph.node(walkTree.nodePointer).element
      if(walkTree.firstTime) {
        val links = walkTree.subTrees.toList.map(parseLink)
        val tree = node match {
          case rn: ReferentNode =>
            InnerNode(nodeName(walkTree.nodePointer).toString, Link("instance", Leaf(rn.concept)) :: links)
          case cn: ConstantNode =>
            Leaf(cn.name)
          case en: ExternalNode =>
            if(links.isEmpty) {
              Leaf(en.reference.toString)
            }
            else {
              InnerNode(en.reference.toString, links)
            }
        }
        tree
      }
      else {
        val tree = node match {
          case rn: ReferentNode => Leaf(nodeName(walkTree.nodePointer).toString)
          case cn: ConstantNode => Leaf(cn.name)
          case en: ExternalNode => Leaf(en.reference.toString)
        }
        tree
      }
    }

    def parseLink(walkTree: VisitEdge): Link = {
      val edge = graph.edge(walkTree.edgePointer).element
      val tree = parseNode(walkTree.subTree)
      val label = if(walkTree.subTree.nodePointer == edge.target) edge.label else edge.flip.label
      val link = Link(label, tree, Map.empty)
      link
    }

    if(!graph.isEmpty) {
      parseNode(graph.walkTree(graph.root))
    }
    else {
      InnerNode("e", Link("instance", Leaf("empty")) :: Nil)
    }
  }

  def nodeName(pointer: Int): NodeName = {
    graph.getNodeAttributes[DefaultAttribute[NodeName]](pointer, "nodeName").value
  }

}


object ToAmrTreeConvertible {

  import AmrTreeConversion._

  private val startsWithLetter = """([a-z]).*""".r

  def annotateNodeNames[MR : Graph](graph: MR): MR = {
    var nameIndices = Map.empty[String, Int].withDefaultValue(1)
    var nodeNames = Map.empty[Int, NodeName]

    graph.nodes foreach {
      case Indexed(ReferentNode(concept, _), i) =>
        if (!nodeNames.contains(i)) {
          val ch = concept.substring(0, 1) match {
            case startsWithLetter(l) => l
            case _ => "x"
          }

          val index = nameIndices(ch)
          nodeNames = nodeNames + (i -> NodeName(ch, index))
          nameIndices = nameIndices + (ch -> (index + 1))
        }
      case _ => ()
    }

    graph mapAttributes {
      case Indexed(ReferentNode(_, attr), i) => attr + ("nodeName" -> DefaultAttribute(nodeNames(i)))
      case Indexed(n@IsNode(), _) => n.attributes
      case Indexed(e@IsEdge(), _) => e.attributes
    }
  }

}
