package gramr
package ccg

import scala.util.matching.Regex

/** Implements the set of combinators used by EasyCCG, as described in [1].
  *
  * [1] A* CCG Parsing with a Supertag-factored Model, Lewis and Steedman, EMNLP 2014.
  */
class EasyccgCombinatorSystem extends CombinatorSystem {

  import EasyccgCombinatorSystem._

  import scala.language.implicitConversions

  implicit def parseSynCat(string: String): SyntacticCategory =
    SyntacticCategory.parse(string).get

  val combinators: Iterable[ccg.Combinator] = List(
    // Standard combinators
    FA, BA, FC, BXC, FC2, BXC2, Conj,
    RPunct, LPunct,

    // Unary rules

    // Rule used to allow nouns to become noun-phrases without needed a determiner.
    UnaryRule("""N""", """NP""", "lex_n_np"),
    // Relativization, as in "the boy playing tennis"
    UnaryRule("""S[pss]\NP""", """NP\NP""", "lex_pss"),
    UnaryRule("""S[ng]\NP""", """NP\NP""", "lex_ng"),
    UnaryRule("""S[adj]\NP""", """NP\NP""", "lex_adj"),
    UnaryRule("""S[to]\NP""", """NP\NP""", "lex_to_np"),
    UnaryRule("""S[to]\NP""", """N\N""", "lex_to_n"),
    UnaryRule("""S[dcl]/NP""", """NP\NP""", "lex_dcl"),
    // Rules that let verb-phrases modify sentences, as in "Born in Hawaii, Obama is the 44th president."
    UnaryRule("""S[pss]\NP""", """S/S""", "lex_pss_s"),
    UnaryRule("""S[ng]\NP""", """S/S""", "lex_ng_s"),
    UnaryRule("""S[to]\NP""", """S/S""", "lex_to_s"),
    // Type raising
    UnaryRule("""NP""", """S[X]/(S[X]\NP)""", "tr_np1"),
    UnaryRule("""NP""", """(S[X]\NP)\((S[X]\NP)/NP)""", "tr_np2"),
    UnaryRule("""PP""", """(S[X]\NP)\((S[X]\NP)/PP)""", "tr_pp"),
    // Extensions
    UnaryRule("""S[dcl]/NP""", """N\N""", "lex_dcl_n"),
    UnaryRule("""PP""", """NP\NP""", "lex_pp_fn_np"), // pp as function over noun phrases
    UnaryRule("""PP""", """(S[X]\NP)\(S[X]\NP)""", "lex_pp_fn_vp"), // pp as function over verb phrases

    // Binary type-changing rules
    TcRel, // Converts sentence-modifying relative clauses to NP modifiers
    TcNmod, // Composes noun modifiers while specifying they are still incomplete to the left
  )

  def synApply(combinator: ccg.Combinator, arguments: SyntacticCategory*): Option[SyntacticCategory] =
    combinator.synApply(arguments: _*)

}

/** A simple combinator system derived from EasyCCG, but not using features on the syntactic categories.
  */
class SimpleCombinatorSystem extends CombinatorSystem {

  import EasyccgCombinatorSystem._

  import scala.language.implicitConversions

  implicit def parseSynCat(string: String): SyntacticCategory =
    SyntacticCategory.parse(string).get

  val combinators: Iterable[ccg.Combinator] = List(
    FA, BA, FC, BXC, FC2, BXC2, Conj,
    RPunct, LPunct,
    // Rule used to allow nouns to become noun-phrases without needed a determiner.
    UnaryRule("""N""", """NP""", "lex_n_np"),
    // Relativization, as in "the boy playing tennis"
    UnaryRule("""S\NP""", """NP\NP""", "lex_s_rel_left"),
    UnaryRule("""S\NP""", """N\N""", "lex_s_rel_left_n"),
    UnaryRule("""S/NP""", """NP\NP""", "lex_s_rel_right"),
    // Rules that let verb-phrases modify sentences, as in "Born in Hawaii, Obama is the 44th president."
    UnaryRule("""S\NP""", """S/S""", "lex_mod"),
    // Type raising
    UnaryRule("""NP""", """S/(S\NP)""", "tr_np1"),
    UnaryRule("""NP""", """(S\NP)\((S\NP)/NP)""", "tr_np2"),
    UnaryRule("""PP""", """(S\NP)\((S\NP)/PP)""", "tr_pp"),
    // Extensions
    UnaryRule("""S/NP""", """N\N""", "lex_dcl_n"),
    UnaryRule("""PP""", """NP\NP""", "lex_pp_fn_np"), // pp as function over noun phrases
    UnaryRule("""PP""", """(S\NP)\(S\NP)""", "lex_pp_fn_vp"), // pp as function over verb phrases

    // Binary type-changing rules
    TcRel, // Converts sentence-modifying relative clauses to NP modifiers
    TcNmod, // Composes noun modifiers while specifying they are still incomplete to the left

  )

  def synApply(combinator: ccg.Combinator, arguments: SyntacticCategory*): Option[SyntacticCategory] =
    combinator.synApply(arguments: _*)

}

object EasyccgCombinatorSystem {

  sealed trait Combinator extends ccg.Combinator {

    import scalatags.Text.all._

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory]

    def display: StringFrag = toString

  }

  trait Forward {
    def headIndex = 0
  }

  trait Backward {
    def headIndex = 1
  }

  trait Binary {
    def arity = 2
  }

  trait Unary {
    def arity = 1

    def headIndex = 0
  }

  case object FA extends Combinator with Binary with Forward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(ForwardSlash(l, r), y) if r matches y =>
          Some(l)
        case _ => None
      }
    }

    override def toString = "fa"

  }

  case object BA extends Combinator with Binary with Backward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(y1, BackwardSlash(x, y2)) if y1 matches y2 =>
          Some(x)
        case _ => None
      }
    }

    override def toString = "ba"

  }

  case object FC extends Combinator with Binary with Forward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(ForwardSlash(x, y1), ForwardSlash(y2, z)) if y1 matches y2 =>
          Some(ForwardSlash(x, z))
        case _ => None
      }
    }

    override def toString = "fc"

  }

  case object BXC extends Combinator with Binary with Backward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(ForwardSlash(y1, z), BackwardSlash(x, y2)) if (y1 matches y2) && !isNominal(y1) =>
          Some(ForwardSlash(x, z))
        case _ => None
      }
    }

    override def toString = "bx"

  }

  case object FC2 extends Combinator with Binary with Forward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(ForwardSlash(x, y1), ForwardSlash(ForwardSlash(y2, z1), z2)) if y1 matches y2 =>
          Some(ForwardSlash(ForwardSlash(x, z1), z2))
        case _ => None
      }
    }

    // Note: EasyCCG defines applicability slightly differently (see below).
    // It ignores the outer slash in the right argument.
    // However, the version presented here seems more consistent with Steedman’s TSP.

    // if (left.isFunctor() && right.isFunctor() && right.getLeft().isFunctor()) {
    //   Category rightLeft = right.getLeft();
    //   return  left.getRight().matches(rightLeft.getLeft()) && left.getSlash() == leftSlash && rightLeft.getSlash() == rightSlash;
    // } else {
    //   return false;
    // }

    override def toString = "gfc"

  }

  case object BXC2 extends Combinator with Binary with Backward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(ForwardSlash(ForwardSlash(y1, z1), z2), BackwardSlash(x, y2)) if (y1 matches y2) && !isNominal(y1) =>
          Some(ForwardSlash(ForwardSlash(x, z1), z2))
        case _ => None
      }
    }

    // The above note on applicability applies here, as well.

    override def toString = "gbx"

  }

  case object Conj extends Combinator with Binary with Forward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(y, x) if isConjunction(y) && !isPunctuation(x) =>
          // Note: the condition for this case used to include !isTypeRaised(x),
          // but was removed since it was violated by EasyCCG output.
          Some(BackwardSlash(x, x))
        case _ => None
      }
    }

    override def toString = "conj"

  }

  case object RPunct extends Combinator with Binary with Backward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(y, x) if isPunctuation(x) && y != Atom("N") =>
          Some(y)
        case _ => None
      }
    }

    override def toString = "rp"

  }

  case object LPunct extends Combinator with Binary with Forward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(Atom("LRB"), y) => Some(y)
        case Seq(Atom("LQU"), y) => Some(y)
        case _ => None
      }
    }

    override def toString = "lp"

  }

  case object Lex extends Combinator with Unary {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = None

    override def toString = "lex"

  }

  case class UnaryRule(
    source: SyntacticCategory,
    result: SyntacticCategory,
    displayName: String
  ) extends Combinator with Unary {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(x) if x matches source => Some(result)
        case _ => None
      }
    }

    override def toString: String = displayName

  }


  case object TcNmod extends Combinator with Binary with Forward {

    def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      // N/N  (N/N)\(N/N)  =>  (N/N)\(N/N)
      // S/S  (S/S)\(S/S)  =>  (S/S)\(S/S)
      // (N/N)/(N/N)  ((N/N)/(N/N))\((N/N)/(N/N))  =>  ((N/N)/(N/N))\((N/N)/(N/N))
      arguments match {
        case Seq(ForwardSlash(x1, x2), BackwardSlash(ForwardSlash(x3, x4), ForwardSlash(x5, x6)))
          if x1 == x2 && x2 == x3 && x3 == x4 && x4 == x5 && x5 == x6 => Some(BackwardSlash(ForwardSlash(x1, x1), ForwardSlash(x1, x1)))
        case _ => None
      }
    }

    override def toString = s"tc-nmod"

  }


  case object TcRel extends Combinator with Binary with Forward {

    val left = Atom(",")
    // The right operand may match any of these categories.
    val right = List(
      BackwardSlash(Atom("""S[to]"""), Atom("""NP""")),
      BackwardSlash(Atom("""S[adj]"""), Atom("""NP""")),
      BackwardSlash(Atom("""S[pss]"""), Atom("""NP""")),
      BackwardSlash(Atom("""S[ng]"""), Atom("""NP"""))
    )

    override def synApply(arguments: SyntacticCategory*): Option[SyntacticCategory] = {
      arguments match {
        case Seq(l, r) if (l matches left) && right.exists(r2 => r matches r2) => Some(BackwardSlash(Atom("NP"), Atom("NP")))
        case _ => None
      }
    }

    override def toString = "tc-rel"

  }


  // These predicates follow EasyCCG’s implementation.

  def isNominal(y: SyntacticCategory): Boolean = y.eraseFeatures match {
    case Atom("N") => true
    case Atom("NP") => true
    case _ => false
  }

  def isConjunction(y: SyntacticCategory): Boolean = y.eraseFeatures match {
    case Atom("conj") => true
    case Atom(",") => true
    case Atom(";") => true
    case _ => false
  }

  val punctuationCategories = Set("LRB", "RRB", "LQU", "RQU", ".", ",", ";")
  val nonPunctuationRegex: Regex = """[A-Za-z]+""".r

  def isPunctuation(y: SyntacticCategory): Boolean = y.eraseFeatures match {
    case Atom(x) if punctuationCategories.contains(x) => true
    case Atom(nonPunctuationRegex()) => false
    case Atom(_) => true
    case _ => false
  }

  def isTypeRaised(y: SyntacticCategory): Boolean = y.eraseFeatures match {
    // X/(X\Y)
    case ForwardSlash(x1, BackwardSlash(x2, _)) if x1 == x2 => true
    // X\(X/Y)
    case BackwardSlash(x1, ForwardSlash(x2, _)) if x1 == x2 => true
    case _ => false
  }


}
