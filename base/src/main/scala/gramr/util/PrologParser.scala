package gramr.util

import scala.util.parsing.combinator._
import scala.util.parsing.input.CharSequenceReader

/**
  * Parses prolog terms.
  */
object PrologParser extends RegexParsers {

  def output: Parser[List[Term]] = term.*

  def term: Parser[Term] =
    expr ~ "." ^^ { case t ~ _ => t }

  def expr: Parser[Term] =
    compound | atom

  def atom: Parser[Atom] =
    ("""\w+""".r ^^ Atom) |
      // Quoted atoms can contain \' escapes, so the regex makes sure the closing ' isn’t escaped.
      // It permits only an even number of backslashes before the closing quote (so that they
      // represent only escaped backslashes, no escaped quotes).
      ("""'(.*?)(?<!\\)(\\\\)?'""".r ^^ { s => Atom(s.drop(1).take(s.length - 2)) })

  def compound: Parser[CompoundTerm] =
    atom ~ "(" ~ arguments ~ ")" ^^ { case h ~ _ ~ args ~ _ => CompoundTerm(h, args.toVector) }

  def arguments: Parser[List[Term]] =
    (expr ~ ("," ~ expr).*).? ^^ {
      case Some(first ~ rest) => first :: rest.map { case _ ~ e => e }
      case None => Nil
    }

  sealed trait Term {

    def asAtom: Atom = try {
      asInstanceOf[Atom]
    }
    catch {
      case e: ClassCastException => throw CastException(s"Term is not an atom: $this")
    }

    def asCompoundTerm: CompoundTerm = try {
      asInstanceOf[CompoundTerm]
    }
    catch {
      case e: ClassCastException => throw CastException(s"Term is not a compound term: $this")
    }

    def asInt: Int = Integer.parseInt(asAtom.label)

    def asString: String = asAtom.label

    case class CastException(msg: String) extends Exception(msg)

  }

  case class Atom(label: String) extends Term

  case class CompoundTerm(functor: Atom, arguments: Vector[Term]) extends Term

  def parseString(s: String): List[Term] = {
    phrase(output)(new CharSequenceReader(s)) match {
      case Success(terms, _) => terms
      case NoSuccess(msg, next) =>
        val unconsumed = next.source.subSequence(next.offset, next.source.length).toString
        val markedExpression = s.stripSuffix(unconsumed) + "<ERROR>" + unconsumed
        throw ParseException(s"Error parsing prolog expression: $msg in $markedExpression")
    }
  }

  case class ParseException(msg: String) extends Exception(msg)

}