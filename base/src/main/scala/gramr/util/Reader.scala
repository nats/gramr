package gramr.util

/**
  * A very simplistic implementation of a reader monad.
  */
case class Reader[A, B](f: A => B) {

  def run(a: A): B = f(a)

  def flatMap[C](g: (B => Reader[A, C])): Reader[A, C] = Reader { a =>
    val b = f(a)
    g(b).f(a)
  }

  def map[C](g: B => C): Reader[A, C] = Reader {
    g.compose(f)
  }

}

object Reader {

  def ask[A]: Reader[A, A] = Reader(a => a)

}