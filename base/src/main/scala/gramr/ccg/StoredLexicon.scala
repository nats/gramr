package gramr.ccg

import java.io._

import com.typesafe.scalalogging.LazyLogging
import gramr.amr.AmrCorpusParser
import gramr.text.{Span, Token}
import gramr.util.MultiMap

import scala.collection.concurrent.TrieMap
import scala.util.hashing.MurmurHash3
import gramr.graph.Graph.ops._
import gramr.util.html.ToHtml

/**
  * A lexicon based on MultiMap.
  */
@SerialVersionUID(2L)
final class StoredLexicon[MR] protected(
  val itemsById: Map[Int, IndexedLexicalItem[MR]],
  val itemsMap: MultiMap[StoredLexicon.Key, Int],
  val maxIndex: Int,
  val keyExtractor: StoredLexicon.KeyExtractor = StoredLexicon.defaultKeyExtractor
) extends Lexicon[MR] with Serializable {

  import StoredLexicon._

  override def get(key: LexiconKey): Iterable[IndexedLexicalItem[MR]] = itemsMap.get(keyExtractor(key)).map(itemsById)

  def items: Iterable[IndexedLexicalItem[MR]] = itemsMap.values.map(itemsById)

  def storedItems: Iterable[(Key, Set[IndexedLexicalItem[MR]])] = for {
    k <- itemsMap.keySet
  } yield (k, itemsMap.get(k).toSet.map(itemsById))

  def storedKeys: Set[Key] = itemsMap.keySet

  def +(item: PlainLexicalItem[MR]): StoredLexicon[MR] = {
    val indexedItem = IndexedLexicalItem(item.tokens, item.synCat, item.meaning, maxIndex + 1)
    new StoredLexicon[MR](
      itemsById + (maxIndex + 1 -> indexedItem),
      itemsMap + (keyExtractor(indexedItem.lexiconKey) -> (maxIndex + 1)),
      maxIndex + 1,
      keyExtractor
    )
  }

  def keyCount: Int = itemsMap.keySet.size

  lazy val itemCount: Int = itemsMap.keySet.toVector.map { k => itemsMap.get(k).size }.sum

  def size: Int = itemCount

  /**
    * Filters stored items by a predicate over items and item counts.
    */
  def filter(p: IndexedLexicalItem[MR] => Boolean): StoredLexicon[MR] = {
    val filteredItemsById = itemsById.filter { case (_, v) => p(v) }
    new StoredLexicon[MR](
      filteredItemsById,
      itemsMap.filter { (_, v) => filteredItemsById.keySet.contains(v) },
      maxIndex,
      keyExtractor
    )
  }

  /**
    * Removes all items that are contained in the other lexicon.
    */
  def --(other: Lexicon[MR]): StoredLexicon[MR] = filter { item =>
    !other.get(item.lexiconKey).toSet.contains(item)
  }

  def reIndex(keyExtractor: KeyExtractor): StoredLexicon[MR] =
    builder(keyExtractor).build

  def builder(keyExtractor: KeyExtractor): Builder[MR] = new StoredLexicon.Builder[MR](
    keyExtractor,
    TrieMap(storedItems.view.flatMap(_._2).map { indexed =>
      (indexed.toPlainLexicalItem, Builder.Entry(indexed.index))
    }.toSeq: _*),
    maxIndex
  )

  def saveToTextFile(file: File): Unit = {
    val writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))
    val keys = storedKeys.toVector.sortBy(_.tokens.mkString(" "))
    for(item <- itemsById.values) {
      writer.println(s"# ::index ${item.index}")
      writer.println(s"# ::tokens ${item.tokens.map(_.lowerCased).mkString(" ")}")
      writer.println(s"# ::lemmas ${item.tokens.map(_.lemma).mkString(" ")}")
      writer.println(s"# ::syncat ${item.synCat}")
      writer.println(item.meaning.toString)
      writer.println()
    }
    writer.close()
  }

  def dumpToHtml(file: File)(implicit ev: ToHtml[MR]): Unit = {
    import scalatags.Text.all._
    import java.io.{BufferedWriter, FileWriter}

    // Write HTML dump of the lexicon
    val pres = new gramr.ccg.presentation.StoredLexiconToHtml
    val markup = html(body(pres.toHtml(this)))

    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(markup.toString)
    bw.close()
  }

}

object StoredLexicon extends LazyLogging {

  import gramr.util.MultiMap
  import gramr.split._
  import gramr.graph._

  def empty[MR](keyExtractor: KeyExtractor = defaultKeyExtractor): StoredLexicon[MR] =
    new StoredLexicon[MR](Map.empty, MultiMap.empty, 0, keyExtractor)

  def fromItems[MR](
    items: => Iterable[PlainLexicalItem[MR]], keyExtractor: KeyExtractor = defaultKeyExtractor
  ): StoredLexicon[MR] =
    items.foldLeft(empty[MR](keyExtractor)) { case (lexicon, item) => lexicon + item }

  def splitChartItems[MR : Graph](splitChart: SplitChart[MR]): Iterable[SplitLexicalItem[MR]] = {
    for {
      item <- splitChart.items
      lexicalItem <- PartialMR.lexicalItems(item, splitChart.sentence)
    } yield lexicalItem
  }

  def loadFromTextFile[MR : Graph](file: File, keyExtractor: KeyExtractor = defaultKeyExtractor): StoredLexicon[MR] = {
    val builder = new Builder[MR](keyExtractor)
    AmrCorpusParser.parseFile(file) foreach {
      case Left(error) =>
        logger.error(s"Skipping a lexical entry due to an error: $error")
      case Right(item) =>
        val mr = implicitly[Graph[MR]].fromAmrTree(item.amr).normalise.clearAttributes
        val synCat = SyntacticCategory(item.getSingleAttribute("syncat"))
        val tokens = (item.getSingleAttribute("tokens").split(" ") zip item.getSingleAttribute("lemmas").split(" ")).toVector.map { case (t, l) => Token(t, l) }
        val index = item.getSingleAttribute("index").toInt
        builder.addWithIndex(PlainLexicalItem(tokens, synCat, mr), index)
    }
    builder.build
  }

  case class SplitLexicalItem[MR](
    tokens: Vector[Token],
    synCat: SyntacticCategory,
    meaning: MR,
    span: Span
  ) extends LexicalItem[MR]

  class Builder[MR](
    val keyExtractor: KeyExtractor = defaultKeyExtractor,
    var items: TrieMap[PlainLexicalItem[MR], Builder.Entry] = TrieMap.empty[PlainLexicalItem[MR], Builder.Entry],
    var maxIndex: Int = 0
  ) {

    def add(item: PlainLexicalItem[MR]): Int = {
      items.get(item) match {
        case Some(existing) =>
          existing.index
        case None =>
          maxIndex = maxIndex + 1
          items.put(item, Builder.Entry(maxIndex))
          maxIndex
      }
    }

    def addWithIndex(item: PlainLexicalItem[MR], index: Int): Unit = {
      items.put(item, Builder.Entry(index))
      maxIndex = Integer.max(maxIndex, index)
    }

    def build: StoredLexicon[MR] = {
      new StoredLexicon(
        items.iterator.map {
          case (item, entry) =>
            entry.index -> IndexedLexicalItem(item.tokens, item.synCat, item.meaning, entry.index)
        }.toMap,
        MultiMap.from(items.iterator.map { case (item, entry) => (keyExtractor(item.lexiconKey), entry.index) }.toSeq),
        maxIndex,
        keyExtractor
      )
    }

  }

  object Builder {

    case class Entry(
      index: Int
    )

  }

  case class Key(
    tokens: Vector[String]
  ) extends Ordered[Key] {

    def compare(other: Key): Int = {
      gramr.util.StreamUtils.lexicographicOrdering[String].compare(
        tokens.toStream, other.tokens.toStream
      )
    }

    override lazy val hashCode: Int =
      MurmurHash3.productHash(this)

  }

  @SerialVersionUID(1L)
  class KeyExtractor(f: LexiconKey => Key) extends Serializable {
    def apply(lexKey: LexiconKey): Key = f(lexKey)
  }

  val defaultKeyExtractor = new KeyExtractor(lexKey => Key(lexKey.tokens.map(_.lowerCased)))
  val lemmaKeyExtractor = new KeyExtractor(lexKey => Key(lexKey.tokens.map(_.lemma)))

}
