package gramr.graph

import scala.util.matching.Regex

import Graph.ops._

object Predicates {

  /** A true half-edge graph is one where every external node has degree 1.
    */
  def isTrueHalfEdgeGraph[MR](mr: MR)(implicit ev: Graph[MR]): Boolean = {
    mr.nodes.forall { case Indexed(node@IsNode(), pointer) =>
      node match {
        case ExternalNode(_, _) => mr.degree(pointer) == 1
        case _ => true
      }
    }
  }

  def isValidAmr[MR : Graph](mr: MR): Boolean =
    checkPolarityEdges(mr) && checkConstantDegrees(mr) && checkArgEdges(mr) && checkNumberedEdgesUnique(mr) &&
      checkInstanceConcepts(mr)

  /** Edges labelled :polarity must point to a constant node labelled -, or to a placeholder.
    */
  def checkPolarityEdges[MR : Graph](mr: MR): Boolean = {
    val relevantEdges = mr.edges.map(_.element.unflip).filter(_.label == "polarity")
    relevantEdges.forall { edge =>
      mr.node(edge.target).element match {
        case ConstantNode(name, _) => name == "-"
        case ReferentNode(concept, _) if concept.startsWith("<") => true
        case ExternalNode(_, _) => true
        case _ => false
      }
    }
  }

  /** Constant nodes must have a maximum in-degree of 1 (we permit individual constant nodes not connected to anything)
    * and an out-degree of 0.
    */
  def checkConstantDegrees[MR : Graph](mr: MR): Boolean = {
    val relevantNodes = mr.nodes collect { case in@Indexed(ConstantNode(_, _), _) => in }
    relevantNodes.forall { n => mr.inDegree(n.pointer) <= 1 && mr.outDegree(n.pointer) == 0 }
  }

  val argRE: Regex = """ARG\d+""".r
  val opRE: Regex = """op\d+""".r
  val numberedRE: Regex = """.*\d+""".r
  val senseTagRE: Regex = """\w+-\d\d""".r

  /** Disallows ARG edges to originate anywhere but at event nodes (those with a sense tag, e.g. open-01) and
    * placeholder nodes (external / coref nodes).
    */
  def checkArgEdges[MR : Graph](mr: MR): Boolean = {
    val relevantEdges = mr.edges.map(_.element.unflip) collect { case e@Edge(argRE(), _, _, _) => e }
    relevantEdges.forall { edge =>
      mr.node(edge.source).element match {
        case ReferentNode(senseTagRE(), _) => true
        case ReferentNode(concept, _) if concept.startsWith("<") => true  // <coref>
        case ExternalNode(_, _) => true
        case _ => false
      }
    }
  }

  /** Forces the outgoing edges of each node to have a unique numbering.
    *
    * Embodies the assumption that a node will only ever have a single set of numbered edges
    * (e.g. either ARG or op edges, but not both).
    */
  def checkNumberedEdgesUnique[MR](mr: MR)(implicit ev: Graph[MR]): Boolean = {
    mr.nodes.forall { node =>
      val numberedEdges = mr.outEdges(node.pointer).map(_.element.label).filter(numberedRE.anchored.findFirstIn(_).isDefined).toVector
      numberedEdges.size == numberedEdges.distinct.size
    }
  }

  /** Forces concepts in referent nodes to start with lowercase characters.
    */
  def checkInstanceConcepts[MR : Graph](mr: MR): Boolean = {
    mr.nodes.forall { node =>
      node.element match {
        case ReferentNode(concept, _) if concept.nonEmpty && concept.head.isLower => true
        case ReferentNode(concept, _) if concept.startsWith("<") => true
        case ReferentNode(_, _) => false
        case _ => true
      }
    }
  }

}
