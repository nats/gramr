package gramr
package split
package labelling

import com.typesafe.scalalogging.LazyLogging
import gramr.graph.{Indexed, _}
import ccg._
import Graph.ops._
import gramr.graph.edit.{SplitDirection, SplitLeft, SplitRight, SplitUnconstrained}

/** Computes the set of possible completions of a partial node labelling. Each “unconstrained” node is in turn
  * labelled as SplitLeft or SplitRight, and the full set of combinations is returned.
  */
case class CompleteNodeAlignments[MR : Graph](
  /** If there are more than this number of unlabelled nodes, no labellings will be returned at all.
    */
  maxUnlabelledNodes: Option[Int] = None
) extends LabellingFunction[MR] with LazyLogging {

  def label(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): Stream[PartialMR[MR]] = {
    val PartialMR(mr, span, synCat) = partialMR

    val unlabelled = mr.nodes.filter { case Indexed(n, _) =>
      n.attributes.get[SplitDirection]("splitDirection") == SplitUnconstrained
    }
    maxUnlabelledNodes match {
      case Some(count) if unlabelled.size > count =>
        logger.trace(s"Skipping splitting (${unlabelled.size} unaligned nodes)")
        Stream.empty
      case _ =>
        val newMRs = mr.mapNodeAttributesNonDeterministic { case Indexed(element, _) =>
          element.attributes.get[SplitDirection]("splitDirection") match {
            case SplitUnconstrained => Stream(
              element.attributes + ("splitDirection" -> SplitLeft),
              element.attributes + ("splitDirection" -> SplitRight)
            )
            case SplitLeft | SplitRight => Stream(element.attributes)
          }
        }
        newMRs.map { newMR => PartialMR(newMR, span, synCat) }
    }
  }
}
