package gramr.experiments

import gramr.amr._
import gramr.ccg._
import gramr.parse.TagDistribution
import gramr.text.{DependencyForest, HasMR, NamedEntitySpan, Token}

case class Example[MR](
  item: AmrCorpusItem,
  reference: MR
)(implicit val combinatorSystem: CombinatorSystem) {
  def id: String = item.id

  lazy val sentence: Vector[Token] = item.sentenceTokens
  lazy val indexedDerivations: Vector[(String, Derivation[Unit])] = item.ccgDerivations.toVector
  lazy val namedEntitySpans: List[NamedEntitySpan] = item.namedEntitySpans()
  lazy val dependencyTree: DependencyForest = item.dependencyTree()
  lazy val ccgTags: Vector[Vector[(SyntacticCategory, Double)]] = item.ccgTags()

  def supertags(attributeName: String = "supertags-0"): Seq[TagDistribution] = {
    try {
      TagDistribution.fromAttributeString(item.getSingleAttribute(attributeName))
    }
    catch {
      case _: Exception => Seq.empty
    }
  }

  def derivations: Vector[Derivation[Unit]] = indexedDerivations.map(_._2)

  def ccgDerivation(derivationId: String): Option[Derivation[Unit]] =
    item.ccgDerivation(derivationId)
}

object Example {

  object ops {
    implicit def exampleInstance[MR]: gramr.text.Example[gramr.experiments.Example[MR]] = new gramr.text.Example[gramr.experiments.Example[MR]] {
      override def id(a: Example[MR]): String = a.id

      override def sentence(a: Example[MR]): Vector[Token] = a.sentence

      override def indexedDerivations(a: Example[MR]): Vector[(String, Derivation[Unit])] = a.indexedDerivations
    }

    implicit def hasMRInstance[MR]: HasMR[Example[MR], MR] = new HasMR[gramr.experiments.Example[MR], MR] {
      override def mr(a: Example[MR]): MR = a.reference
    }

  }

}
