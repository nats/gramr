package gramr.learn

import com.typesafe.scalalogging.LazyLogging
import gramr.parse.chart.{Chart, State}
import gramr.text.Example
import gramr.text.Example.ops._

/** Implements an early update strategy.
 *
 * This implementation only updates towards a single oracle derivation. This is suboptimal because many different
 * derivations could lead to the oracle-best parse. Therefore, this implementation can be considered outdated.
 */
class EarlyUpdateStrategy[MR, FV: FeatureVector, E: Example](
  val freeChartName: String,
  val oracleChartName: String
) extends UpdateStrategy[MR, FV, E] with LazyLogging {

  override def compute(example: E, charts: Map[String, Chart[MR, FV]]): Iterable[StatePairUpdate[MR, FV]] = {
    val freeChart = charts(freeChartName)
    val correctChart = charts(oracleChartName)

    /** Given a correct state, returns an iterable of violating state pairs, where the first element is a predecessor
      * of the input state and the second element is its counterpart in the free chart.
      *
      * @param state a state from the correct chart
      * @return an iterable of state pairs
      */
    def violatingStatePairs(state: State[MR, FV]): Iterable[(State[MR, FV], State[MR, FV])] = {
      violatingState(state) match {
        case Some(counterpart) => Iterable((state, counterpart))
        case None =>
          // This considers only the highest-scoring action for the state, which is possibly too strict.
          val action = state.actions.maxBy(_.score)
          action.predecessors.flatMap { predecessor =>
            violatingStatePairs(predecessor)
          }
      }
    }

    /** Return the highest-scored violating state for a given forced-parse state
      */
    def violatingState(state: State[MR, FV]): Option[State[MR, FV]] = {
      // The states from the free parse corresponding to the input state
      val freeStates = freeChart.states(state.span)
      // Which of the states represent correct results?
      val (correctStates, wrongStates) = freeStates.partition(_.mr == state.mr)

      // The state is violating if the correct action has dropped off the beam
      if(correctStates.isEmpty && wrongStates.nonEmpty) {
        Some(wrongStates.maxBy(_.score))
      }
      else {
        None
      }
    }

    /** Returns a lowermost sequence of violating state pairs.
      *
      * @param state a state from the correctChart
      * @return an iterable of pairs of a correct state and a violating state
      */
    def earliestViolatingStates(state: State[MR, FV]): Iterable[(State[MR, FV], State[MR, FV])] = {
      violatingState(state) match {
        case None => Iterable()
        case Some(violating) =>
          // Try to decide if we need to go further down in the actions tree
          val action = state.actions.maxBy(_.score)
          val violatingPredecessors = action.predecessors.flatMap(earliestViolatingStates)
          if (violatingPredecessors.nonEmpty) {
            violatingPredecessors
          }
          else {
            Iterable((state, violating))
          }
      }
    }

    if (correctChart.outputStates.nonEmpty) {
      val latestViolatingStates = violatingStatePairs(correctChart.outputStates.head)
      val violatingStates = latestViolatingStates.flatMap { case (state, _) => earliestViolatingStates(state) }.toVector

      if (violatingStates.nonEmpty) {
        val spans = violatingStates.map(_._1.span)
        logger.trace(s"${example.id}: Updating spans ${spans.mkString(", ")}" +
          s"(${spans.map(_.in(example.sentence).mkString(" ")).mkString("[", "] [", "]")})"
        )

        val updates = for((p, n) <- violatingStates) yield StatePairUpdate(p, n)
        updates
      }
      else {
        // No violation found
        logger.trace(s"${example.id}: No update (no violation)")
        Iterable.empty
      }
    }
    else {
      // No correct parses
      logger.trace(s"${example.id}: No update (no correct parses)")
      Iterable.empty
    }
  }

}
