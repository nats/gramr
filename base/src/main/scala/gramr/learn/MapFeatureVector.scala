package gramr.learn

import gramr.learn.util.FeatureIndex

import scala.collection.immutable.HashMap

/**
  * A feature vector backed by a map.
  */
@SerialVersionUID(1L)
class MapFeatureVector(
  private val vector: HashMap[Int, Double],
  private val featureIndex: FeatureIndex
) extends Serializable {

  type Key = Int
  type Value = Double
  type K = Key
  type V = Value

  def numeric: Numeric[Double] = implicitly[Numeric[Value]]

  def entries: Iterable[(String, V)] = vector.map { case (i, v) => (featureIndex.reverse(i), v) }

  def size: Int = vector.size

  def get(k: Key): Value = vector.getOrElse(k, 0)

  def get(s: String): Value = get(featureIndex(s))

  def mapValues(f: Value => Value): MapFeatureVector =
    new MapFeatureVector(vector.map { case (k, v) => (k, f(v)) }, featureIndex)

  def elementWise(f: (Value, Value) => Value)(other: MapFeatureVector): MapFeatureVector = {
    val keys: Set[K] = vector.keySet union other.vector.keySet
    val newVector: HashMap[K, V] = keys.foldLeft(HashMap.empty[K, V]) { (map, k) =>
      map + (k -> f(this.get(k), other.get(k)))
    }
    new MapFeatureVector(newVector, featureIndex)
  }

  def add(other: MapFeatureVector): MapFeatureVector = {
    // This implementation is somewhat optimised, making use of the fact that non-existent elements are equivalent
    // to zero.
    if(vector.size < other.vector.size) {
      other.add(this)
    }
    else {
      val newVector: HashMap[K, V] = other.vector.foldLeft(vector) { case (vec, (k, v)) =>
        vec.updated(k, v + vec.getOrElse(k, 0.0))
      }
      new MapFeatureVector(newVector, featureIndex)
    }
  }

  def addIndexedEntry(entry: (Key, Value)): MapFeatureVector =
    new MapFeatureVector(vector.updated(entry._1, get(entry._1) + entry._2), featureIndex)

  def subtract(other: MapFeatureVector): MapFeatureVector = {
    elementWise(_ - _)(other)
  }

  def +(entry: (String, Value)): MapFeatureVector =
    addIndexedEntry(featureIndex(entry._1) -> entry._2)

  def scale(n: Value): MapFeatureVector = mapValues(_ * n)

  def dot(other: MapFeatureVector): Value = {
    val sharedKeys = vector.keySet intersect other.vector.keySet
    sharedKeys.foldLeft(0.0) { case (sum, k) => sum + (get(k) * other.get(k)) }
  }

  def magnitudeSquared: Value = vector.values.map(x => x * x).sum

  override def toString: String =
    entries.map { case (k, v) => f"$k => $v" }.mkString(", ")

}

object MapFeatureVector {

  object ops {

    /**
      * A FeatureVectorLike instance for Double-values MapFeatureVectors.
      */
    implicit def featureVectorInstance(implicit featureIndex: FeatureIndex): FeatureVector[MapFeatureVector] =
      new FeatureVector[MapFeatureVector] {
        def from(entries: Iterable[(String, Double)]): MapFeatureVector =
          entries.foldLeft(empty) { (fv, entry) => fv + entry }

        def empty: MapFeatureVector = new MapFeatureVector(HashMap.empty, featureIndex)

        def emptyParams: MapFeatureVector = empty

        def toValue(d: Double): Double = d

        def toDouble(v: Double): Double = v

        override def entries(v: MapFeatureVector): Iterable[(Int, V)] = v.vector

        def toOps(self: MapFeatureVector): FeatureVectorOps[MapFeatureVector] =
          new FeatureVectorOps[MapFeatureVector] {
            override def elementWise(f: (V, V) => V)(other: MapFeatureVector): MapFeatureVector = self.elementWise(f)(other)

            /**
              * Element-wise addition.
              */
            override def +(other: MapFeatureVector): MapFeatureVector = self.add(other)

            /**
              * Element-wise subtraction.
              */
            override def -(other: MapFeatureVector): MapFeatureVector = self.subtract(other)

            /**
              * Scalar multiplication.
              */
            override def *(v: Double): MapFeatureVector = self.scale(v)

            override def get(k: K): Double = self.get(k)

            override def entries: Iterable[(K, Double)] = self.entries

            override def indexedEntries: Iterable[(Int, Double)] = self.vector

            override def size: Int = self.size

            override def mapValues(f: Double => Double): MapFeatureVector = self.mapValues(f)

            override def magnitudeSquared: Double = self.magnitudeSquared
          }
      }
  }

  def featureVectorLike(implicit featureIndex: FeatureIndex): FeatureVector[MapFeatureVector] = ops.featureVectorInstance

}
