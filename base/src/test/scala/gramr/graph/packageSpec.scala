package gramr.graph

import org.scalatest.FlatSpec

class packageSpec extends FlatSpec {

  type MR = AdjacencyMeaningGraph

  implicit val graph: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance
  import graph.Building._

  "Graph levels" should "be computed for a non-branching graph" in {
    val (g, (a, b, c)) = graph.buildReturning {
      for {
        a <- mkReferentNode("a", Attributes.empty)
        b <- mkReferentNode("b", Attributes.empty)
        c <- mkReferentNode("c", Attributes.empty)
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        _ <- normalise(false)
      } yield (a, b, c)
    }

    assert(levels(g).toList == List(Set(a), Set(b), Set(c)))
    assert(levelsFrom(g, a).toList == List(Set(a), Set(b), Set(c)))
    assert(levelsFrom(g, b).toList == List(Set(b), Set(a, c)))
    assert(levelsFrom(g, c).toList == List(Set(c), Set(b), Set(a)))
  }

  it should "be computed for a binary-branching graph" in {
    val (g, (a, b, c)) = graph.buildReturning {
      for {
        a <- mkReferentNode("a", Attributes.empty)
        b <- mkReferentNode("b", Attributes.empty)
        c <- mkReferentNode("c", Attributes.empty)
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("ac", a, c)
        _ <- normalise(false)
      } yield (a, b, c)
    }

    assert(levels(g).toList == List(Set(a), Set(b, c)))
    assert(levelsFrom(g, a).toList == List(Set(a), Set(b, c)))
    assert(levelsFrom(g, b).toList == List(Set(b), Set(a), Set(c)))
    assert(levelsFrom(g, c).toList == List(Set(c), Set(a), Set(b)))
  }

  it should "be computed in a forward-referencing graph" in {
    val (g, (a, b, c, d)) = graph.buildReturning {
      for {
        a <- mkReferentNode("a", Attributes.empty)
        b <- mkReferentNode("b", Attributes.empty)
        c <- mkReferentNode("c", Attributes.empty)
        d <- mkReferentNode("d", Attributes.empty)
        _ <- mkEdge("ab", a, b)
        _ <- mkEdge("bc", b, c)
        _ <- mkEdge("cd", c, d)
        _ <- mkEdge("ad", a, d)
        _ <- normalise(false)
      } yield (a, b, c, d)
    }

    assert(levels(g).toList == List(Set(a), Set(b, d), Set(c)))
    assert(levelsFrom(g, a).toList == List(Set(a), Set(b, d), Set(c)))
    assert(levelsFrom(g, b).toList == List(Set(b), Set(a, c), Set(d)))
    assert(levelsFrom(g, c).toList == List(Set(c), Set(b, d), Set(a)))
    assert(levelsFrom(g, d).toList == List(Set(d), Set(a, c), Set(b)))
  }

}
