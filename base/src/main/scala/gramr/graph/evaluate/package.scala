package gramr.graph

import gramr.text.{Example, HasMR}
import gramr.util.SimpleRetrievalStats
import gramr.graph.Graph.ops._
import gramr.text.Example.ops._

package object evaluate {

  def smatch[MR: Graph, E: Example](smart: Int, random: Int)(mr: MR, example: E)(implicit hasMR: HasMR[E, MR]): SimpleRetrievalStats = {
    val smatch = new Smatch().smartAndRandomInit(smart, random, mr, example.reference)
    smatch._2
  }

  def kernelSmatch[MR: Graph, E: Example](smart: Int, random: Int)(mr: MR, example: E)(implicit hasMR: HasMR[E, MR]): SimpleRetrievalStats = {
    smatch(smart, random)(mr.kernel, example)
  }

}
