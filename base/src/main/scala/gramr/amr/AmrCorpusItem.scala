package gramr.amr

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import gramr.ccg._
import gramr.text.{DependencyForest, NamedEntitySpan, Token}
import gramr.util.MultiMap

import scala.util.{Failure, Success}
import scala.util.matching.Regex

class AmrCorpusItem(
  val amr: AmrTree,
  val attributes: Iterable[(String, String)]
) extends LazyLogging {
  val attributeMap: MultiMap[String, String] = MultiMap.from(attributes)

  def hasAttribute(key: String): Boolean = getAttribute(key).nonEmpty

  /** Returns all occurrences of the given attribute.
    *
    * Note that AMR allows the same key to be used multiple times for a single corpus item.
    */
  def getAttribute(key: String): Iterable[String] = attributeMap.get(key)

  /** Get attributes that match a given regular expression.
    *
    * @param regex a regular expression to match the attribute name against
    * @return all key-value pairs where the key matches the regex
    */
  def getMatchingAttributes(regex: util.matching.Regex): Iterable[(String, String)] = attributes.flatMap { attr =>
    attr._1 match {
      case regex(_*) => List((attr._1, attr._2))
      case _ => Nil
    }
  }

  /** Returns the single occurrence of the given attribute.
    *
    * @throws Exception if the attribute key does not exist or occurs more than once.
    */
  def getSingleAttribute(key: String): String = getAttribute(key).toList match {
    case attr :: Nil => attr
    case Nil =>
      val idval = if (key == "id") "AMR corpus item" else id  // Avoid infinite recursion if id attribute is not unique
      throw new Exception(s"Attribute $key not found in $idval")
    case _ =>
      throw new Exception(s"More than one attribute $key found in $id")
  }

  def extendAttributes(entries: Iterable[(String, String)]): AmrCorpusItem = new AmrCorpusItem(amr, attributes ++ entries)

  /**
    * Returns the value of the “id” attribute. This attribute is present in AMR by default; it is central
    * enough to get its own convenience accessor.
    *
    * @throws Exception if there is no (or more than one) “id” attribute.
    */
  def id: String = getSingleAttribute("id")

  /**
    * Returns the contents of the “tok” attribute, split by whitespace. This attribute
    * can be created by processing the corpus file with the JAMR aligner.
    *
    * @throws Exception if there is no (or more than one) “tok” attribute.
    */
  lazy val sentenceTokens: Vector[Token] = {
    val toks = getSingleAttribute("tok").split(" ").toVector
    val lemmas = getSingleAttribute("lemmas").split(" ").toVector
    val posCoarse: Vector[Option[String]] =
      if(hasAttribute("pos-coarse"))
        getSingleAttribute("pos-coarse").split(" ").toVector.map(Some(_))
      else
        toks.map(_ => None)
    (toks zip lemmas zip posCoarse) map { case ((t, l), pc) => Token(t, l, coarsePos = pc) }
  }

  lazy val derivationAttributeRe: Regex = """ccg-derivation-(.*)""".r

  /** Returns the CCG derivations encoded in the “ccg-derivation-*” attributes.
    *
    * @return pairs of derivation IDs and parsed derivations
    */
  def ccgDerivations(implicit combinatorSystem: CombinatorSystem): Iterable[(String, Derivation[Unit])] = {
    import scala.util.Failure
    for {
      (name, value) <- getMatchingAttributes(derivationAttributeRe).toVector
      key = name match {
        case derivationAttributeRe(k) => k
        case _ => throw new Error(s"Invalid derivation attribute: $name")  // should never happen
      }
      parser = new CcgbankParser(value, combinatorSystem)
      result = {
        val r = parser.derivation.run()
        r match {
          case Failure(e) => logger.warn(s"In $id attribute $name: $e")
          case _ => ()
        }
        r.toOption
      }
      der <- result
      fixed = der.fixSpans
    } yield (key, fixed)
  }

  def ccgDerivation(derivationId: String)(implicit combinatorSystem: CombinatorSystem): Option[Derivation[Unit]] = {
    val attributeName = s"ccg-derivation-$derivationId"
    val attributeString = getSingleAttribute(attributeName)
    CcgbankParser.parseAttributeString(attributeString) match {
      case Success(derivation) => Some(derivation)
      case Failure(e) =>
        logger.warn(s"CCG derivation parse error in example $id attribute $attributeName: $e")
        None
    }
  }

  /** Removes all but the first n CCG derivations.
    *
    * The first `count` derivations are kept according to their numerical index.
    *
    * @param count the number of derivations to keep
    * @return a copy of the corpus item with attributes filtered accordingly
    */
  def limitCcgDerivations(count: Int)(implicit combinatorSystem: CombinatorSystem): AmrCorpusItem = {
    // Get all CCG derivation attribute names
    val derivationKeys = getMatchingAttributes(derivationAttributeRe).map(_._1)
    // Extract the numerical derivation IDs to sort them
    val derivationIds = derivationKeys.map { case key@derivationAttributeRe(id) => (id.toInt, key) }.toSeq.sortBy(_._1)
    // Compute derivation keys to be removed
    val toRemove = derivationIds.drop(count).map(_._2)
    // Remove the extra derivations
    val filteredAttributes = attributeMap.filter {
      case (k, _) => !toRemove.contains(k)
    }.entries

    new AmrCorpusItem(
      amr = amr,
      attributes = filteredAttributes)
  }

  /** Filters attributes by name.
   *
   * @param predicate decides which CCG derivations are kept
   */
  def filterAttributes(predicate: String => Boolean): AmrCorpusItem = {
    val filteredAttributes = attributeMap.filter((k, v) => predicate(k))
    new AmrCorpusItem(
      amr = amr,
      attributes = filteredAttributes.entries
    )
  }

  def namedEntitySpans(attributeName: String = "ent"): List[NamedEntitySpan] = {
    NamedEntitySpan.fromAttribute(getSingleAttribute(attributeName))
  }

  def dependencyTree(attributeName: String = "dep"): DependencyForest = {
    DependencyForest.fromAttributeString(getSingleAttribute(attributeName))
  }

  def ccgTags(attributeName: String = "supertags"): Vector[Vector[(SyntacticCategory, Double)]] = {
    val entries = getSingleAttribute(attributeName).split("\\|").map { entryStr =>
      val entryFields = entryStr.trim.split(" ")
      (entryFields.head.toInt, SyntacticCategory(entryFields(1)), entryFields(2).toDouble)
    }
    val entryGroups = entries.groupBy(_._1).toVector.sortBy(_._1).map(_._2).map(_.map(e => (e._2, e._3)).toVector)
    entryGroups
  }

  /**
    * Returns the JAMR alignment encoded in the “alignments” attribute. This attribute can be
    * created by processing the corpus with the JAMR aligner.
    *
    * @throws Error if there is no (or more than one) “alignments” attribute.
    */
  def jamrAlignment(attributeName: String = "alignments"): JamrAlignment =
    JamrAlignment.parse(getSingleAttribute(attributeName))

  /**
    * Annotates the JAMR alignment encoded in the “alignments” attribute onto this corpus item’s AMR tree.
    * Each node’s aligned tokens are stored as a Set[Int] under the “alignments” key in the node’s attributes.
    */
  def withJamrAlignment(sourceAttributeName: String, targetAttributeName: String): AmrCorpusItem =
    jamrAlignment(sourceAttributeName).annotate(this, targetAttributeName)

  def isiAlignment(attributeName: String): StandoffAlignment =
    StandoffAlignment.parse(id, getSingleAttribute(attributeName))

  def withIsiAlignment(sourceAttributeName: String, targetAttributeName: String): AmrCorpusItem =
    isiAlignment(sourceAttributeName).annotate(this, targetAttributeName)


  /**
    * Converts the item to a string representation in AMR corpus format.
    */
  def format: String = {
    val attrLines = attributes.map { case (n, v) => s"# ::$n $v" }
    attrLines.mkString("\n") + "\n" + AmrTree.format(amr)
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[AmrCorpusItem]

  override def equals(other: Any): Boolean = other match {
    case that: AmrCorpusItem =>
      (that canEqual this) &&
        amr == that.amr &&
        attributes == that.attributes
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(amr, attributes)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  override def toString = s"AmrCorpusItem($id)"
}

object AmrCorpus {

  import java.io.PrintWriter

  def save(c: Stream[AmrCorpusItem], f: File): Unit = {
    val writer = new PrintWriter(f)
    try {
      c foreach { i =>
        writer.append(i.format)
        writer.append("\n\n")
      }
    } finally {
      writer.close()
    }
  }

  /**
    * The set of all roles used in the AMR corpus.
    */
  val roleSet: Set[String] =
    Set(
      "ARG0", "ARG1", "ARG2", "ARG3", "ARG4", "ARG5", "accompanier", "age",
      "beneficiary", "calendar", "century", "compared-to", "concession", "condition",
      "conj-as-if", "consist-of", "day", "dayperiod", "decade", "degree",
      "destination", "direction", "domain", "duration", "era", "example", "extent",
      "frequency", "instrument", "li", "location", "manner", "medium", "mod", "mode",
      "month", "name", "op1", "op10", "op11", "op2", "op3", "op4", "op5", "op6",
      "op7", "op8", "op9", "ord", "part", "path", "polarity", "polite", "poss",
      "prep-against", "prep-along-with", "prep-amid", "prep-among", "prep-as",
      "prep-at", "prep-by", "prep-except", "prep-following", "prep-for", "prep-from",
      "prep-in", "prep-in-addition-to", "prep-instead-of", "prep-into", "prep-on",
      "prep-on-behalf-of", "prep-through", "prep-to", "prep-toward", "prep-under",
      "prep-with", "prep-within", "prep-without", "purpose", "quant", "quarter",
      "range", "scale", "season", "snt1", "snt2", "snt3", "snt4", "snt5", "snt6",
      "source", "subevent", "subset-of", "time", "timezone", "topic", "unit",
      "value", "weekday", "year"
    )


}
