package gramr
package split

import scala.collection.immutable.HashMap
import text._
import ccg._
import gramr.parse.SyntaxInformation

class SplitChart[MR](
  val sentence: Vector[Token],
  val derivations: Vector[Derivation[Unit]],
  val chart: Array[Array[HashMap[PartialMR[MR], SplitChart.Entry[MR]]]]
) {

  import SplitChart._

  private var itemCounter = 0
  private var nextId = 0
  private var splitSet = Set.empty[SplitChart.Split]

  def addRoot(partialMR: PartialMR[MR]): Option[Int] =
    add(partialMR, None).ifNew

  def addSplit(parentId: Int, partialMRs: List[PartialMR[MR]]): Iterable[(Option[Int], PartialMR[MR])] = {
    val ids = partialMRs.map { partialMR =>
      (add(partialMR, Some(parentId)), partialMR)
    }
    splitSet = splitSet + Split(parentId, ids.map(_._1.v))
    ids.map({ case (id, partialMR) => (id.ifNew, partialMR) })
  }

  def contains(partialMR: PartialMR[MR]): Boolean = {
    val Span(start, end) = partialMR.span
    chart(start)(end).contains(partialMR)
  }

  /** If a new entry was added, returns its ID. If the entry already existed, returns None.
    */
  private def add(partialMR: PartialMR[MR], parentId: Option[Int]): NewOrOld[Int] = {
    val Span(start, end) = partialMR.span
    val oldMap = chart(start)(end)
    if (oldMap.contains(partialMR)) {
      val entry = oldMap(partialMR)
      Old(entry.id)
    }
    else {
      val entry = Entry(inventId(), parentId, partialMR, sentence)
      val newMap = oldMap + (partialMR -> entry)
      chart(start)(end) = newMap
      itemCounter = itemCounter + 1
      New(entry.id)
    }
  }

  def get(span: Span): Iterable[Entry[MR]] = {
    if (span.start >= chart.length || span.end >= chart(span.start).length) {
      throw new Exception("Index out of bounds!")
    }
    chart(span.start)(span.end).values
  }

  def getMRs(span: Span): Set[PartialMR[MR]] = {
    if (span.start >= chart.length || span.end >= chart(span.start).length) {
      throw new Exception("Index out of bounds!")
    }
    chart(span.start)(span.end).keySet
  }

  def items: Iterable[PartialMR[MR]] =
    entries.map(_.partialMR)

  def entries: Iterable[Entry[MR]] = for {
    spanLength <- 1 to sentence.size
    start <- 0 to (sentence.size - spanLength)
    end = start + spanLength - 1
    entry <- chart(start)(end).values
  } yield entry

  def splits: Iterable[SplitChart.Split] = splitSet

  def itemCount: Int = itemCounter

  /** The spans contained in the chart in an unspecified order.
    */
  def spans: Iterable[Span] = for {
    start <- sentence.indices
    end <- start until sentence.size
  } yield Span(start, end)

  def filter(f: SplitChartFilter[MR]): Unit = {
    val instance = f.instantiate(this)
    spans foreach { span =>
      val oldMap = chart(span.start)(span.end)
      val newMap = oldMap.filter { e => instance(e._1) }
      chart(span.start)(span.end) = newMap
    }
  }

  override def clone: SplitChart[MR] = {
    val newChart = Array.tabulate(sentence.size, sentence.size) { (start, end) =>
      chart(start)(end)
    }
    new SplitChart(sentence, derivations, newChart)
  }

  /** Computes the lowest (i.e. closest to the leaves) cross-section of the derivation
    * of which every parent node is represented in the split chart by at least one partial MR.
    */
  def lowestCoveredCrossSection(derivation: Derivation[Unit]): Vector[DerivationStep[Set[PartialMR[MR]]]] = {
    // Finds the lowest cross-section below this step
    def expand(step: DerivationStep[Unit]): Vector[DerivationStep[Set[PartialMR[MR]]]] =
      if (step.subSteps.nonEmpty && step.subSteps.forall(stepIsRepresented))
        step.subSteps.toVector.flatMap(expand)
      else
        Vector(annotateStep(step))

    expand(derivation.root)
  }

  // Decides whether the step is represented in the chart.
  def annotateStep(step: DerivationStep[Unit]): DerivationStep[Set[PartialMR[MR]]] = {
    step.map { _ => getMRs(step.span).filter { entry => entry matchesSynCat step.synCat } }
  }

  def stepIsRepresented(step: DerivationStep[Unit]): Boolean = {
    getMRs(step.span).exists { item => item matchesSynCat step.synCat }
  }

  def splitQuality: Double =
    if (derivations.isEmpty)
      0.0
    else
      derivations.map(splitQuality).max

  /** The proportion of derivation steps that could be annotated.
    *
    * This can be interpreted as a 0-to-1 measure of the success of splitting.
    */
  def splitQuality(derivation: Derivation[Unit]): Double = {
    derivation.steps.map(step => if (stepIsRepresented(step)) 1 else 0).sum.toDouble / derivation.steps.size.toDouble
  }

  def tokenLevelCoverage: Double = {
    val spans = sentence.indices map { i => Span(i, i) }
    spans.count { span => getMRs(span).nonEmpty }.toDouble / spans.size.toDouble
  }

  def annotateDerivation(derivation: ccg.Derivation[Unit]): ccg.Derivation[Set[MR]] = {
    derivation.mapSteps { step =>
      getMRs(step.span).filter(_.matchesSynCat(step.synCat)).map(_.mr)
    }
  }

  private def inventId(): Int = {
    val result = nextId
    nextId = nextId + 1
    result
  }

}

object SplitChart {

  def empty[MR](sentence: Vector[Token], derivations: Vector[Derivation[Unit]]): SplitChart[MR] =
    new SplitChart(
      sentence,
      derivations,
      Array.fill(sentence.size, sentence.size)(HashMap.empty)
    )

  case class Entry[MR](
    id: Int,
    parentId: Option[Int],
    partialMR: PartialMR[MR],
    sentence: Vector[Token]
  ) {

    def span: Span = partialMR.span

    def syntax: Set[SyntaxInformation] = partialMR.synCats.map { synCat => gramr.parse.SyntaxInformation(sentence, span, synCat) }

    override def equals(other: Any): Boolean = other match {
      case e: Entry[MR] => partialMR.equals(e.partialMR)
      case _ => false
    }

    override def hashCode: Int = partialMR.hashCode

  }

  case class Split(
    parentId: Int,
    childIds: List[Int]
  )

  sealed trait NewOrOld[A] {
    def v: A

    def ifNew: Option[A]
  }

  case class New[A](v: A) extends NewOrOld[A] {
    def ifNew = Some(v)
  }

  case class Old[A](v: A) extends NewOrOld[A] {
    def ifNew = None
  }

}
