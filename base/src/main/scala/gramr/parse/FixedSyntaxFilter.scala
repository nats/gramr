package gramr.parse

import gramr.ccg.Derivation

/** A filter that only allows parses that match one of a set of given syntactic derivations.
  *
  * Every action must create a node which matches (with span and syntactic category) a node
  * in one of the derivations.
  *
  * Note that there is no check whether all nodes occur in the same derivation, but this is
  * not expected to create too much freedom.
  */
object FixedSyntaxFilter {
  def apply[MR, F, C](implicit ev: C => Seq[Derivation[Unit]]): ActionPredicate[MR, F, C] = context => {
    val derivations: Seq[Derivation[Unit]] = ev(context)

    action => {
      derivations.exists { derivation =>
        derivation.steps.exists { step =>
          (step.synCat matches action.syntax.synCat) && step.span == action.syntax.span
        }
      }
    }
  }

}
