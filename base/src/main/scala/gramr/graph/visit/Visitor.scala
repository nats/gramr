package gramr.graph
package visit

trait Visitor {
  def visitNode(nodeIndex: Int): Unit = {}
  def visitEdge(edgeIndex: Int): Unit = {}
}

object Visitor {

  /** Combine several visitors so they are all notified within a single pass.
    *
    * @param visitors list of visitors to combine
    */
  def combine(visitors: Visitor*): Visitor = new Visitor {
    override def visitNode(nodeIndex: Int): Unit = visitors.foreach(_.visitNode(nodeIndex))
    override def visitEdge(edgeIndex: Int): Unit = visitors.foreach(_.visitEdge(edgeIndex))
  }

}
