package gramr.util.html

import scalatags.Text.all._

/**
  * Type class for things that can be converted to HTML
  */
trait ToHtml[-A] {
  def toHtml(a: A): Frag
}

object ToHtml {

  object Implicits {

    import scala.language.implicitConversions

    implicit def convert[A](a: A)(implicit ev: ToHtml[A]): Frag = ev.toHtml(a)

  }

}
