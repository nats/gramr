package gramr
package parse
package chart

import gramr.ccg._
import cats.instances.vector._, cats.syntax.traverse._

case class ExtractDerivation[MR, F](
  combinatorSystem: CombinatorSystem,
  internalSyntax: Int => Option[DerivationStep[Unit]] = _ => None
) {
  /** Extracts the top-scored CCG derivation from a parse chart.
    */
  def fromChart(chart: Chart[MR, F]): Option[Derivation[State[MR, F]]] = {
    if(chart.outputStates.nonEmpty) {
      val root = chart.outputStates.maxBy(_.score)
      val rootStep = fromState(root, root.syntax.synCat)
      if(rootStep.isEmpty) {
        throw new Exception("Cannot extract derivation")
      }
      Some(Derivation(rootStep.head).fixSpans)
    }
    else {
      None
    }
  }

  def fromState(state: State[MR, F], synCat: SyntacticCategory): Vector[DerivationStep[State[MR, F]]] = {
    if(state.actions.isEmpty) {
      throw new Exception("state without actions")
    }
    else {
      val precedingAction = state.actions.maxBy(_.score)
      val predecessors = precedingAction.predecessors
      if(predecessors.isEmpty) {
        precedingAction.decision match {
          case LexicalDecision(item, _, _) =>
            item match {
              case stored: IndexedLexicalItem[MR] =>
                val inner = internalSyntax(stored.index).
                  getOrElse(LexicalStep(state.syntax.span.in(state.syntax.sentence), state.syntax.synCat, state.syntax.span, state)).
                  map(_ => state)
                Vector(inner)
              case generated: GeneratedLexicalItem[MR] =>
                Vector(LexicalStep(state.syntax.span.in(state.syntax.sentence), state.syntax.synCat, state.syntax.span, state))
              case delexed: DelexedLexicalItem[MR] =>
                Vector(LexicalStep(state.syntax.span.in(state.syntax.sentence), state.syntax.synCat, state.syntax.span, state))
              case _ =>
                throw new Exception("Lexical item is not stored or generated")
            }
          case _ =>
            throw new Exception("Leaf decision is not lexical")
        }
      }
      else if(predecessors.size == 1 && predecessors.head.syntax.synCat == state.syntax.synCat) {
        fromState(predecessors.head, predecessors.head.syntax.synCat)
      }
      else {
        val predecessorSteps: Vector[CombinatoryStep[State[MR, F]]] = for {
          argCats <- predecessors.toVector.traverse(s => Vector(s.syntax.synCat))
          c <- combinatorSystem.combinators if c.canApply(argCats: _*)
          resultCat <- c.synApply(argCats: _*).toIterable if state.syntax.synCat == resultCat
          subSteps <- predecessors.toVector.zip(argCats).traverse { case (pred, cat) => fromState(pred, cat) }
          cmbStep = CombinatoryStep(
            c,
            subSteps.toList,
            resultCat,
            state.syntax.span,
            state
          )
        } yield cmbStep

        if(predecessorSteps.isEmpty) {
          println(s"$synCat failed for predecessors ${predecessors.map(_.syntax.synCat.toString).mkString("; ")}")
        }

        predecessorSteps
      }
    }
  }
}
