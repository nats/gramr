package gramr
package split

import gramr.graph._, ccg._

/** A function that labels some or all elements of a graph with SplitDirection labels.
  *
  * Labelling functions can be sequenced using andNext.
  *
  * Each labelling function has its own pre- and post-conditions, and it is up to the user
  * to make sure that a complete labelling is returned in the end.
  */
trait LabellingFunction[MR] {
  self =>

  def label(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): Stream[PartialMR[MR]]

  def andThen(next: LabellingFunction[MR]): LabellingFunction[MR] = new LabellingFunction[MR] {

    def label(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): Stream[PartialMR[MR]] = for {
      fstResult <- self.label(partialMR, step)
      sndResult <- next.label(fstResult, step)
    } yield sndResult

  }

}
