package gramr.learn

trait Update[FV] {

  def gradient: FV

}
