package gramr.util.html

import java.io.{BufferedWriter, File, FileWriter}

import gramr.graph._
import gramr.learn.{FeatureVector, LinearModel}
import gramr.parse.chart.{Chart, State}
import gramr.text.Example.ops._
import gramr.text.{Example, HasMR, Span}
import scalatags.Text.TypedTag
import scalatags.Text.all._

/** Writes a parse chart to an HTML file for debugging.
  */
class HtmlChartWriter[MR : Graph : ToHtml, FV : FeatureVector, E: Example](
  val chart: Chart[MR, FV],
  val example: E,
  val model: LinearModel,
  val processor: MR => MR = (x: MR) => x
)(implicit hasMR: HasMR[E, MR]) {

  import ToHtml.Implicits._

  def writeToFile(file: File): Unit = {

    val markup = html(
      head(),
      body(
        div(
          example.id, br,
          b(example.sentence.map(_.text).mkString(" ")), br,
          example.reference
        ),
        chart.spans.toList.sortBy(span => -span.size).map { span => displaySpan(span) }
      )
    )

    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(markup.toString)
    bw.close()

  }

  def displaySpan(span: Span): Frag = {
    val states = chart.states(span).toList
    List(
      h3(a(name := spanReference(span), s"${span.start} - ${span.end} (${states.size} states)")),
      chartTable(span),
      table(
        stateTableHeader,
        states.map(displayState)
      )
    )
  }

  def chartTable(span: Span): Frag = {
    val indices = (span.start to span.end).toList

    table(
      tr(th(), indices.map { i => th(chart.sentence(i).text) }),
      indices.map { startIndex =>
        tr(
          th(chart.sentence(startIndex).text),
          indices.map { endIndex =>
            val span = Span(startIndex, endIndex)
            val states = chart.states(Span(startIndex, endIndex))

            if (states.isEmpty)
              td()
            else
              td(a(href := s"#${spanReference(span)}", s"${states.size}"))
          }
        )
      }
    )
  }

  def stateTableHeader: TypedTag[String] = tr(th("syncat"), th("MR"), th("score"), th("features"))

  def displayState(state: State[MR, FV]): Frag = {
    tr(
      td(state.syntax.synCat.toString),
      td(processor(state.mr)),
      td(state.score),
      td(displayFeatures(state))
    )
  }

  def displayFeatures(state: State[MR, FV]): Frag = HtmlChartWriter.displayFeatures(state, model)

  def spanReference(span: Span): String = s"span_${span.start}_${span.end}"

}

object HtmlChartWriter {
  def displayFeatures[MR, FV : FeatureVector](
    state: State[MR, FV],
    model: LinearModel
  ): Frag = {
    import FeatureVector.Syntax._

    def keyDisplay(k: String): Frag = {
      val lexPattern = """lex (\d+)""".r
      k match {
        case lexPattern(idx) => a(href := s"../lexicon.html#$idx", k)
        case _ => k
      }
    }

    val features = state.features

    def contribution(entry: (String, Double)): (Double, Double, Double) = {
      val (k, v) = entry
      val featureValue: Double = v
      val paramValue: Double = model.get(model.featureIndex(k))
      (featureValue, paramValue, featureValue * paramValue)
    }

    table(
      fontSize := "10px",
      features.entries.toVector.sortBy(e => -Math.abs(contribution(e)._3)) map { entry =>
        val (featureValue, paramValue, contrib) = contribution(entry)
        tr(
          td(keyDisplay(entry._1)),
          td(
            style := {
              if (contrib > 0) "color: green"
              else if (contrib == 0) "color: black"
              else "color: red"
            },
            f"$featureValue%.2g * $paramValue%.2g = $contrib%.2g"
          )
        )
      }
    )
  }

}

