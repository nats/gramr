package gramr.graph.edit

import gramr.graph._
import Graph.ops._

sealed trait SplitDirection extends Attribute {

  def opposite: SplitDirection

  def label(attributes: Attributes): Attributes =
    attributes + ("splitDirection" -> this)

}

object SplitDirection {

  def of[MR : Graph](indexed: Indexed[Element]): SplitDirection =
    query(indexed).get

  def query[MR : Graph](indexed: Indexed[Element]): Option[SplitDirection] =
    indexed.element.attributes.getOption[SplitDirection]("splitDirection")

  def erase(attributes: Attributes): Attributes = attributes - "splitDirection"

  def erase[MR : Graph](mr: MR): MR = mr.mapAttributes { indexed => erase(indexed.element.attributes) }

}

case object SplitLeft extends SplitDirection {

  def opposite: SplitDirection = SplitRight

}

case object SplitRight extends SplitDirection {

  def opposite: SplitDirection = SplitLeft

}

case object SplitUnconstrained extends SplitDirection {

  def opposite: SplitDirection = SplitUnconstrained

}