package gramr.util

object OptionUtils {

  def ordering[T: Ordering] = new Ordering[Option[T]] {

    def compare(a: Option[T], b: Option[T]): Int = (a, b) match {
      case (Some(xa), Some(xb)) => implicitly[Ordering[T]].compare(xa, xb)
      case (Some(xa), None) => 1
      case (None, Some(xb)) => -1
      case (None, None) => 0
    }

  }

}