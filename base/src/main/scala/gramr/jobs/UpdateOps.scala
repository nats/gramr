package gramr.jobs

import com.typesafe.scalalogging.LazyLogging
import gramr.learn.{LinearLearner, Update}

class UpdateOps[MR, FV, E](job: Job[E, (E, Iterable[Update[FV]])]) extends LazyLogging {

  def applyTo[L](learner: LinearLearner[MR, FV, E]): Job[E, (E, Iterable[Update[FV]])] = {
    job.listenToBatch { items =>
      val updates = items.flatMap(_._2)
      learner.updateBatch(updates)
      logger.trace(s"Processed ${updates.size} updates for ${job.name}")
    }
  }

}
