package gramr.ccg

import scalatags.Text.all._

trait Combinator {
  def arity: Int

  def headIndex: Int

  def display: Frag

  def synApply(args: SyntacticCategory*): Option[SyntacticCategory]

  def canApply(args: SyntacticCategory*): Boolean = synApply(args: _*).isDefined
}
