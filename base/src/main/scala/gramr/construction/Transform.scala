package gramr.construction

trait Transform[MR] {

  def name: String

  def transform(mr: MR): Iterable[MR]

}
