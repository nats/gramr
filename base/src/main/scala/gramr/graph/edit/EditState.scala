package gramr.graph
package edit

import gramr.graph.mark.Marker

/** The state for the Edit monad
  */
case class EditState[MR : Graph](
  mr: MR,
  root: Option[Marker] = None,
  nextMarker: Int = 0
) {

}

object EditState {

  def initial[MR : Graph](mr: MR): EditState[MR] =
    EditState(mr)

}
