package gramr.amr

import gramr.graph.{AlignmentsAttribute, Attribute}

/** Represents alignments created by the amr_ud aligner.
 *
 * amr_ud aligns sub-graphs of the AMR to sub-trees of a dependency parse.
 * These alignments are hierarchical, i.e. there are links on a node-to-node basis, but also links between
 * larger sub-graphs and sub-trees.
 *
 * Since gramr is not equipped to handle these kinds of alignments, we represent only the alignments of individual
 * AMR nodes.
 */
case class AmrUdAlignment(
  elements: Set[AmrUdAlignment.Element]
) {

  /** Transfer the alignment to the AMR elements in the AMR tree.
   *
   * @param amrTree The AMR tree to annotate.
   * @param attributeName The attribute name to annotate under.
   */
  def annotate(amrTree: AmrTree, attributeName: String = "alignments"): AmrTree = {
    elements.foldLeft(amrTree) {
      case (amr, element) =>
        element.annotate(amr, attributeName)
    }
  }
}


object AmrUdAlignment {

  def parse(lines: Seq[String]): AmrUdAlignment = {
    val elements = lines.flatMap(Element.parse).toSet
    AmrUdAlignment(elements)
  }

  case class Element(
    nodeName: String,
    tokenIds: Set[Int]
  ) {
    /** Transfer the alignment to the AMR element in the AMR tree.
     *
     * @param amrTree The AMR tree to annotate.
     * @param attributeName The attribute name to annotate under.
     */
    def annotate(amrTree: AmrTree, attributeName: String = "alignments"): AmrTree = {
      // amr_ud alignments start at token index 1 – we need to shift them
      val adjustedTokenIds = tokenIds.map(_ - 1)

      def currentAlignments(attributes: Map[String, Attribute]): Set[Int] = {
        attributes.get(attributeName).map(_.asInstanceOf[AlignmentsAttribute].edges).getOrElse(Set.empty)
      }

      def recurse(subtree: AmrTree): AmrTree = {
        subtree match {
          case InnerNode(label, _, _) if label == nodeName =>
            val alignedTokens = currentAlignments(subtree.attributes)
            subtree.setAttribute(attributeName, AlignmentsAttribute(alignedTokens ++ adjustedTokenIds))
          case InnerNode(label, children, attributes) =>
            val mappedChildren = children.map {
              case Link(label, subTree, attributes) =>
                Link(label, recurse(subTree), attributes)
            }
            InnerNode(label, mappedChildren, attributes)

          // Leaves are ignored (they don't have a variable name)
          case _ => subtree
        }
      }

      recurse(amrTree)
    }
  }

  object Element {

    /** Read a single alignment from an attribute value.
     *
     * Parses alignments that are for single AMR nodes. Returns None for AMR subgraph alignments.
     *
     * @return the parsed alignment, or None if the line contains a multi-node alignment
     */
    def parse(alignmentLine: String): Option[Element] = {
      val Seq(left, right) = alignmentLine.split("    #    ").toSeq
      if(left.contains(":")) {
        // Disregard multi-node alignments, which are identified by :edges.
        None
      }
      else {
        val nodeName = left.split("/").head
        val tokens = right.split(""" :[^\s]+ """)
        val tokenIds = tokens.map(_.split("/").head.toInt).toSet
        Some(Element(nodeName, tokenIds))
      }
    }

  }

}
