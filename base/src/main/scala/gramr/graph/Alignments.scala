package gramr.graph

import gramr.util.{RetrievalStats, SimpleRetrievalStats}

import Graph.ops._

case class Alignments[MR : Graph](
  edges: Map[Int, Set[Int]]
) {

  def +(e: (Int, Set[Int])): Alignments[MR] = {
    val oldIndices = edges.getOrElse(e._1, Set.empty)
    copy(edges = edges + (e._1 -> (oldIndices ++ e._2)))
  }

  def coversToken(i: Int): Boolean = {
    edges.values.exists {
      _.contains(i)
    }
  }

  def evaluateAgainst(goldAlignments: Alignments[MR]): RetrievalStats = {
    def toTuples(edgeMap: Map[Int, Set[Int]]): Set[(Int, Int)] =
      edgeMap.toSeq.flatMap { case (p, es) => es.map { e => (p, e) } }.toSet

    val testTuples = toTuples(edges)
    val goldTuples = toTuples(goldAlignments.edges)
    val correctTuples = testTuples & goldTuples
    val incorrectTuples = testTuples -- goldTuples
    SimpleRetrievalStats(goldTuples.size, correctTuples.size, incorrectTuples.size)
  }

}

object Alignments {

  def empty[MR : Graph]: Alignments[MR] = Alignments(Map.empty)

  def of[MR](
    mr: MR,
    attributeName: String = "alignments"
  )(implicit mgl: Graph[MR]): Alignments[MR] = {

    def edgesOf(e: Indexed[Element]): Set[Int] =
      e.element.attributes.getOption[AlignmentsAttribute](attributeName).map(_.edges).getOrElse(Set.empty[Int])

    val attrs = mr.elements.map { el => (el.pointer, edgesOf(el)) }
    Alignments(attrs.toMap)
  }

}
