package gramr.parse.chart

import gramr.parse.SyntaxInformation
import gramr.text.Span

trait State[MR, FV] {

  def mr: MR

  def syntax: SyntaxInformation

  def span: Span = syntax.span

  def score: Double

  def features: FV

  def actions: Iterable[Action[MR, FV]]

}
