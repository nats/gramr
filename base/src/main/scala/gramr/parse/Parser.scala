package gramr
package parse

import text._
import chart._

trait Parser[MR, F, C] {

  def parse(sentence: Vector[Token], context: C): Chart[MR, F]

}
