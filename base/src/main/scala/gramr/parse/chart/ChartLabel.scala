package gramr.parse.chart

trait ChartLabel[L] {

  def score(l: L): Double

  def zero: L

  def add(l: L, r: L): L

  def subtract(l: L, r: L): L

  def mul(l: L, r: L): L

  def sum(labels: Iterable[L]): L = {
    labels.foldLeft(zero) { (l, r) => add(l, r) }
  }

  def product(labels: Iterable[L]): L = {
    labels.tail.fold(labels.head) { case (l, r) => mul(l, r) }
  }

  def inheritablePortion(l: L): L

}

object ChartLabel {

  def score[L: ChartLabel](l: L): Double = implicitly[ChartLabel[L]].score(l)

  def zero[L: ChartLabel]: L = implicitly[ChartLabel[L]].zero

  def add[L: ChartLabel](l: L, r: L): L = implicitly[ChartLabel[L]].add(l, r)

  def subtract[L: ChartLabel](l: L, r: L): L = implicitly[ChartLabel[L]].subtract(l, r)

  def mul[L: ChartLabel](l: L, r: L): L = implicitly[ChartLabel[L]].mul(l, r)

  def sum[L: ChartLabel](labels: Iterable[L]): L = implicitly[ChartLabel[L]].sum(labels)

  def product[L: ChartLabel](labels: Iterable[L]): L = implicitly[ChartLabel[L]].product(labels)

  def inheritablePortion[L: ChartLabel](l: L): L = implicitly[ChartLabel[L]].inheritablePortion(l)

}
