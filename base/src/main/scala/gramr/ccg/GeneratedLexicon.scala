package gramr.ccg

import gramr.parse.{LexicalDecision, SyntaxInformation}

/**
  * A collection of lexical generators. This class provides a lexical choice that sums up all the generators.
  *
  * @param generators a list of generators, each paired with an identifying name
  * @tparam MR the meaning representation
  */
class GeneratedLexicon[MR](
  val generators: (String, LexicalGenerator[MR])*
) {

  def lexicalChoice[E]: E => SyntaxInformation => Iterator[LexicalDecision[MR]] = (e: E) => (i: SyntaxInformation) => {
    val items = for {
      (name, gen) <- generators.toIterator
      item <- gen.generate(i)
    } yield LexicalDecision(
      GeneratedLexicalItem(i.tokens, item.synCat, item.meaning, name),
      i.sentence,
      i.span
    )
    items
  }

}
