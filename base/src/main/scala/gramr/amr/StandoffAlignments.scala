package gramr.amr

import gramr.util.{SeqUtil, ListUtil}
import gramr.graph.{Attribute, AlignmentsAttribute}

sealed trait AlignmentEdge {
  def token: Int
}

case class AlignedNode(token: Int, path: List[Int]) extends AlignmentEdge

case class AlignedRole(token: Int, path: List[Int]) extends AlignmentEdge

case class StandoffAlignment(id: String, alignments: List[AlignmentEdge]) {

  def annotate(item: AmrCorpusItem, attributeName: String = "alignments"): AmrCorpusItem =
    new AmrCorpusItem(
      amr = StandoffAlignment.annotateAmrTree(item.amr, this, attributeName),
      attributes = item.attributes)

}

object StandoffAlignment {

  import scala.language.implicitConversions

  private implicit def mkAlignmentsAttribute(tokens: Set[Int]): AlignmentsAttribute =
    AlignmentsAttribute(tokens)

  private val edgeRe = """(\d+)-(\d(?:(?:\.\d+)*)(?:\.r)?)""".r

  def parse(id: String, line: String): StandoffAlignment = {
    val edges = line.split(" ").toList map { case edgeRe(tok, str) =>
      val tokenIndex = Integer.parseInt(tok)
      val segments = str.split("\\.").toList
      if (segments.last == "r")
        AlignedRole(tokenIndex, segments.init.map(Integer.parseInt))
      else
        AlignedNode(tokenIndex, segments.map(Integer.parseInt))
    }
    StandoffAlignment(id, edges)
  }

  def annotateAmrTree(
    amrTree: AmrTree, standoffAlignment: StandoffAlignment, attributeName: String = "alignments"
  ): AmrTree = {

    def currentAlignments(attributes: Map[String, Attribute]): Set[Int] = {
      attributes.lift(attributeName).map(_.asInstanceOf[AlignmentsAttribute].edges).getOrElse(Set.empty)
    }

    def mapNthChild(n: Int, children: List[Link])(f: Link => Link): List[Link] = {
      val indexed: List[(Link, Option[Int])] = ListUtil.indexSome(children) {
        _.label != "instance"
      }
      val mapped = SeqUtil.mapWhere(indexed, { x: (Link, Option[Int]) => x._2.contains(n) }) { x => (f(x._1), x._2) }
      mapped.map(_._1).toList
    }

    def annotateNode(token: Int, amr: AmrTree, path: List[Int]): AmrTree = path match {
      case Nil =>
        // Annotate this node
        val alignedTokens = currentAlignments(amr.attributes)
        amr.setAttribute(attributeName, alignedTokens + token)
      case index :: rest =>
        // Descend further to find the node
        amr match {
          case InnerNode(label, children, attributes) =>
            val changedChildren = mapNthChild(index, children) { link =>
              link.copy(subTree = annotateNode(token, link.subTree, path.tail))
            }
            InnerNode(label, changedChildren, attributes)
          case Leaf(label, attributes) =>
            throw new Error("unresolvable path")
          case other => other
        }
    }

    def annotateEdge(token: Int, amr: AmrTree, path: List[Int]): AmrTree = path match {
      case index :: Nil =>
        // Annotate the nth outgoing edge from this node
        amr match {
          case InnerNode(label, children, attributes) =>
            val changedChildren = mapNthChild(index, children) { link =>
              val alignedTokens = currentAlignments(link.attributes)
              link.setAttribute(attributeName, alignedTokens + token)
            }
            InnerNode(label, changedChildren, attributes)
          case Leaf(_, _) =>
            throw new Error("unresolvable path")
          case other => other
        }
      case index :: _ =>
        // Descend further to find the edge
        amr match {
          case InnerNode(label, children, attributes) =>
            val changedChildren = mapNthChild(index, children) { link =>
              link.copy(subTree = annotateEdge(token, link.subTree, path.tail))
            }
            InnerNode(label, changedChildren, attributes)
          case Leaf(_, _) =>
            throw new Error("unresolvable path")
          case other => other
        }
      case Nil =>
        throw new Error("cannot annotate an edge with an empty path")
    }

    standoffAlignment.alignments.foldLeft(amrTree) { (amr, alignmentEdge) =>
      alignmentEdge match {
        case AlignedNode(token, path) => annotateNode(token, amr, path.map(_ - 1).tail)
        case AlignedRole(token, path) => annotateEdge(token, amr, path.map(_ - 1).tail)
      }
    }
  }

}
