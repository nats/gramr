package gramr.amr

import gramr.text._
import gramr.ccg._
import gramr.graph._
import gramr.parse.SyntaxInformation

/**
  * A collection of lexical generators for AMR corpora.
  */
object LexicalGenerators {

  import gramr.util.DateTime._
  import scala.language.implicitConversions

  implicit def parseSynCat(str: String): SyntacticCategory =
    SyntacticCategory.parse(str).get

  /** Processes a lemma so that it doesn’t contain AMR special characters.
    */
  def processLemma(lemma: String): String = {
    lemma.replaceAll("""[:/()]""", "_")
  }

  // Filling in vocab gaps with dummies

  def empty[MR : Graph](
    synCats: Set[SyntacticCategory] = Set("""NP""")
  ): TokenBasedLexicalGenerator[MR] =
    (tokens: Vector[Token]) => {
      val mr = implicitly[Graph[MR]].empty
      synCats.map { cat =>
        GeneratedLexicalItem(tokens, cat, mr, "")
      }
    }

  def identity[MR : Graph]: LexicalGenerator[MR] = (syntax: SyntaxInformation) => {
    val tokens = syntax.tokens
    val synCat = syntax.synCat
    if(synCat.arity > 0) {
      val builder = implicitly[Graph[MR]].builder
      builder.addExternalNode(ExternalReference(0))
      val mr = builder.build
      Set(GeneratedLexicalItem(tokens, synCat, mr, ""))
    }
    else {
      Set.empty
    }
  }

  def singleNode[MR](
    concept: String,
    synCats: Set[SyntacticCategory]
  )(implicit mgl: Graph[MR]): TokenBasedLexicalGenerator[MR] = (tokens: Vector[Token]) => {
    val builder = mgl.builder
    builder.addReferentNode(concept)
    //builder.setNormalise(false)
    val mr = builder.build
    synCats.map { cat =>
      GeneratedLexicalItem(tokens, cat, mr, "")
    }
  }

  // Basic parts of speech

  def noun[MR : Graph]: TokenBasedLexicalGenerator[MR] = new TokenBasedLexicalGenerator[MR] {
    private val synCats: Set[SyntacticCategory] = Set("""N""", """N/PP""")

    override def generate(tokens: Vector[Token]): Set[GeneratedLexicalItem[MR]] = {
      val mgl = implicitly[Graph[MR]]
      import mgl.Building._
      if (tokens.size != 1)
        Set.empty
      else {
        val meaning = mgl.build {
          for {
            _ <- mkReferentNode(processLemma(tokens.head.lemma))
            //_ <- normalise(false)
          } yield ()
        }
        synCats.map { synCat =>
          GeneratedLexicalItem(tokens, synCat, meaning, "")
        }
      }
    }
  }

  def intransitiveVerb[MR : Graph]: TokenBasedLexicalGenerator[MR] = new TokenBasedLexicalGenerator[MR] {
    private val synCat: SyntacticCategory = """S\NP"""

    override def generate(tokens: Vector[Token]): Set[GeneratedLexicalItem[MR]] = {
      val mgl = implicitly[Graph[MR]]
      import mgl.Building._

      if (tokens.size != 1)
        Set.empty
      else {
        val meaning = mgl.build {
          for {
            n <- mkReferentNode(processLemma(tokens.head.lemma) + "-01")
            e0 <- mkExternalNode(ExternalReference(0))
            _ <- mkEdge("ARG0", n, e0)
            //_ <- normalise(false)
          } yield ()
        }
        Set(GeneratedLexicalItem(tokens, synCat, meaning, ""))
      }
    }
  }

  def transitiveVerb[MR : Graph]: TokenBasedLexicalGenerator[MR] = new TokenBasedLexicalGenerator[MR] {
    private val synCats: Set[SyntacticCategory] = Set("""(S\NP)/NP""", """(S\NP)/PP""")

    override def generate(tokens: Vector[Token]): Set[GeneratedLexicalItem[MR]] = {
      val mgl = implicitly[Graph[MR]]
      import mgl.Building._

      if (tokens.size != 1)
        Set.empty
      else {
        val meaning = mgl.build {
          for {
            n <- mkReferentNode(processLemma(tokens.head.lemma) + "-01")
            e0 <- mkExternalNode(ExternalReference(0))
            e1 <- mkExternalNode(ExternalReference(1))
            _ <- mkEdge("ARG0", n, e0)
            _ <- mkEdge("ARG1", n, e1)
            //_ <- normalise(false)
          } yield ()
        }
        synCats.map { synCat =>
          GeneratedLexicalItem(tokens, synCat, meaning, "")
        }
      }
    }
  }


  val adjAdvSynCats: Set[SyntacticCategory] = Set(
    """N/N""",
    """(S\NP)/(S\NP)""",
    """(S\NP)\(S\NP)"""
  )

  def modifier[MR](
    edgeLabel: String,
    synCats: Set[SyntacticCategory] = adjAdvSynCats
  )(implicit mgl: Graph[MR]): TokenBasedLexicalGenerator[MR] = (tokens: Vector[Token]) => {
    if (tokens.size == 1) {
      val token = tokens.head
      val builder = mgl.builder
      val ext = builder.addExternalNode(ExternalReference(0))
      val n = builder.addReferentNode(processLemma(token.lemma))
      val edge = builder.addEdge(edgeLabel, ext, n)
      val mr = builder.build
      //builder.setNormalise(false)
      synCats.map { cat =>
        GeneratedLexicalItem(tokens, cat, mr, "")
      }
    }
    else {
      Set.empty
    }
  }

  // Named entities

  def namedOrganisation[MR : Graph]: TokenBasedLexicalGenerator[MR] = namedEntity("organization")

  def namedPerson[MR : Graph]: TokenBasedLexicalGenerator[MR] = namedEntity("person")

  /**
    * Generates named entity representations for spans that have a minimum rate of capitalised words.
    */
  def namedEntity[MR : Graph](
    entityConcept: String, minCapsRate: Double = 0.5
  ): TokenBasedLexicalGenerator[MR] = new TokenBasedLexicalGenerator[MR] {
    private val synCat: SyntacticCategory = """N"""

    override def generate(tokens: Vector[Token]): Set[GeneratedLexicalItem[MR]] = {
      import scalaz._, Scalaz._
      val mgl = implicitly[Graph[MR]]
      import mgl.Building._

      val capsTokens = tokens.count { t => t.text.exists { c: Char => Character.isUpperCase(c) } }
      val capsRate = capsTokens.toDouble / tokens.size.toDouble

      if (capsRate < minCapsRate)
        Set.empty
      else {
        // Adds a name token to the name node
        def addToken(name: Int)(ti: (Token, Int)): mgl.Building[Unit] = for {
          n <- mkConstantNode("\"" + ti._1.text + "\"")
          _ <- mkEdge(s"op${ti._2 + 1}", name, n)
        } yield ()

        // Constructs the meaning graph:
        // (o / organisation :name (n / name :op1 "tok1" :op2 "tok2" …))
        val meaning = mgl.build(
          for {
            org <- mkReferentNode(entityConcept)
            name <- mkReferentNode("name")
            _ <- mkEdge("name", org, name)
            _ <- tokens.zipWithIndex.traverse(addToken(name))
            //_ <- normalise(false)
          } yield ()
        )
        Set(GeneratedLexicalItem(tokens, synCat, meaning, ""))
      }
    }
  }

  /** Generates only a name without any governing entity. Might be useful for phrases like “Felix the cat.”
    */
  def name[MR : Graph](
    minCapsRate: Double
  ): TokenBasedLexicalGenerator[MR] = (tokens: Vector[Token]) => {
    import scalaz._, Scalaz._
    val mgl = implicitly[Graph[MR]]
    import mgl.Building._

    val capsTokens = tokens.count { t => t.text.exists { c: Char => Character.isUpperCase(c) } }
    val capsRate = capsTokens.toDouble / tokens.size.toDouble

    if (capsRate < minCapsRate)
      Set.empty
    else {
      // Adds a name token to the name node
      def addToken(name: Int)(ti: (Token, Int)): mgl.Building[Unit] = for {
        n <- mkConstantNode("\"" + ti._1.text + "\"")
        _ <- mkEdge(s"op${ti._2 + 1}", name, n)
      } yield ()

      // Constructs the meaning graph:
      // (o / organisation :name (n / name :op1 "tok1" :op2 "tok2" …))
      val meaning = mgl.build(
        for {
          name <- mkReferentNode("name")
          _ <- tokens.zipWithIndex.traverse(addToken(name))
          //_ <- normalise(false)
        } yield ()
      )
      Set(GeneratedLexicalItem(tokens, """NP""", meaning, ""))
    }
  }

  def quoted[MR : Graph]: TokenBasedLexicalGenerator[MR] =
    (tokens: Vector[Token]) => {
      val mgl = implicitly[Graph[MR]]
      import mgl.Building._

      if (tokens.length > 1)
        Set.empty
      else {
        val t = tokens.head
        val meaning = mgl.build(
          for {
            e <- mkExternalNode(ExternalReference(0))
            n <- mkConstantNode("\"" + t.text + "\"")
            _ <- mkEdge("op1", e, n)
            //_ <- normalise(false)
          } yield ()
        )
        Set(GeneratedLexicalItem(tokens, """N""", meaning, ""))
      }
    }

  // Dates

  private def mkDate[MR](date: Date)(implicit mgl: Graph[MR]): MR = {
    val builder = mgl.builder
    val ent = builder.addReferentNode("date-entity")
    date.year foreach { yyear =>
      val y = builder.addConstantNode(yyear.toString)
      builder.addEdge("year", ent, y)
    }
    date.month foreach { mmonth =>
      val m = builder.addConstantNode(mmonth.toString)
      builder.addEdge("month", ent, m)
    }
    date.day foreach { dday =>
      val d = builder.addConstantNode(dday.toString)
      builder.addEdge("day", ent, d)
    }
    //builder.setNormalise(false)
    builder.build
  }

  def numericDate[MR](implicit mgl: Graph[MR]): TokenBasedLexicalGenerator[MR] =
    (tokens: Vector[Token]) => {
      tokens.map(_.text) match {
        case Vector(numericDateMatcher(date)) =>
          Set(GeneratedLexicalItem(tokens, """N""", mkDate(date), ""))
        case _ => Set.empty
      }
    }

  def dashDate[MR](implicit mgl: Graph[MR]): TokenBasedLexicalGenerator[MR] =
    (tokens: Vector[Token]) => {
      tokens.map(_.text) match {
        case Vector(dashDateMatcher(date)) =>
          Set(GeneratedLexicalItem(tokens, """N""", mkDate(date), ""))
        case _ => Set.empty
      }
    }

  def americanSlashDate[MR](implicit mgl: Graph[MR]): TokenBasedLexicalGenerator[MR] =
    (tokens: Vector[Token]) => {
      tokens.map(_.text) match {
        case Vector(americanSlashDateMatcher(date)) =>
          Set(GeneratedLexicalItem(tokens, """N""", mkDate(date), ""))
        case _ => Set.empty
      }
    }

  def textDate[MR](implicit mgl: Graph[MR]): TokenBasedLexicalGenerator[MR] =
    (tokens: Vector[Token]) => {
      val tokenTexts = tokens.map(_.text.toLowerCase)
      val itemOpt = for {
        date <- parseTextDate(tokenTexts)
      } yield GeneratedLexicalItem(tokens, """N""", mkDate(date), "")
      itemOpt.toSet
    }

}
