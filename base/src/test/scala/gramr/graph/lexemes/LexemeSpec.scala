package gramr.graph.lexemes

import org.scalatest.FreeSpec
import gramr.graph.{AdjacencyMeaningGraph, Graph}
import gramr.util.Parsing

class LexemeSpec extends FreeSpec {

  type T = AdjacencyMeaningGraph

  implicit def meaningGraphLike: Graph[T] = AdjacencyMeaningGraph.graphInstance

  val mgl: Graph[T] = AdjacencyMeaningGraph.graphInstance

  "Lex node detection" in {
    val amrStr0 =
      """(a / a
        |  :ab (b / B
        |          :bc (c / C)))""".stripMargin
    val amr0 = Parsing.parseAmr(amrStr0)

    assert(Lexeme.lexConceptCount(amr0) == 0)
    assert(Lexeme.lexConstantCount(amr0) == 0)
    assert(Lexeme.lexEdgeCount(amr0) == 0)

    val amrStr1 =
      """(a / a
        |  :ab (b / B
        |          :bc (x / <lex-0>)))""".stripMargin
    val amr1 = Parsing.parseAmr(amrStr1)

    assert(Lexeme.lexConceptCount(amr1) == 1)
    assert(Lexeme.lexConstantCount(amr1) == 0)
    assert(Lexeme.lexEdgeCount(amr1) == 0)

    val amrStr2 =
      """(a / a
        |  :ab (b / <lex-1>
        |          :bc (x / <lex-0>)))""".stripMargin
    val amr2 = Parsing.parseAmr(amrStr2)

    assert(Lexeme.lexConceptCount(amr2) == 2)
    assert(Lexeme.lexConstantCount(amr2) == 0)
    assert(Lexeme.lexEdgeCount(amr2) == 0)
  }

  "Lexicalization" in {

    val mrStr =
      """(a / a
        |  :ab (b / B
        |          :bc (c / C)))""".stripMargin
    val mr = Parsing.parseAmr(mrStr)

    val amrStr1 =
      """(a / a
        |  :ab (b / B
        |          :bc (x / <lex-0>)))""".stripMargin
    val amr1 = Parsing.parseAmr(amrStr1)
    val lexeme1 = Lexeme(Vector("t"), Vector("C"), Vector.empty, Vector.empty)
    val lex1 = lexeme1.lexicalize(amr1).get

    assert(lex1 ~= mr)

    val amrStr2 =
      """(a / a
        |  :ab (b / <lex-0>
        |          :bc (x / <lex-1>)))""".stripMargin
    val amr2 = Parsing.parseAmr(amrStr2)

    assert(Lexeme(Vector("t"), Vector("B", "C"), Vector.empty, Vector.empty).lexicalize(amr2).get ~= mr)

    val constStr =
      """(a / a
        |  :ab (b / B
        |          :bc <lex-0>))""".stripMargin
    val const = Parsing.parseAmr(constStr)
    val constStrExp =
      """(a / a
        |  :ab (b / B
        |          :bc "C"))""".stripMargin
    val constExp = Parsing.parseAmr(constStrExp)
    val lexemeConst = Lexeme(Vector("t"), Vector.empty, Vector("\"C\""), Vector.empty)
    val lexConst = lexemeConst.lexicalize(const).get

    assert(lexConst ~= constExp)

  }

  "Delexicalization" - {

    val tokens = Vector("A")
    val mrStr1 =
      """(a / a)""".stripMargin
    val mr1 = Parsing.parseAmr(mrStr1)

    val expAStr1 =
      """(a / <lex-0>)""".stripMargin
    val expA1 = Parsing.parseAmr(expAStr1)


    val mrStr2 =
      """(a / a :ab (b / b))""".stripMargin
    val mr2 = Parsing.parseAmr(mrStr2)

    val expAStr2 =
      """(a / <lex-0> :ab (b / b))""".stripMargin
    val expA2 = Parsing.parseAmr(expAStr2)

    val expBStr2 =
      """(a / a :ab (b / <lex-0>))""".stripMargin
    val expB2 = Parsing.parseAmr(expBStr2)

    val expABStr2 =
      """(a / <lex-0> :ab (b / <lex-1>))""".stripMargin
    val expAB2 = Parsing.parseAmr(expABStr2)


    val mrConstantStr =
      """(n / name :op1 "constant")"""
    val mrConstant = Parsing.parseAmr(mrConstantStr)

    val expConstantStr =
      """(n / name :op1 <lex-0>)"""
    val expConstant = Parsing.parseAmr(expConstantStr)


    val mrEdgeStr =
      """(<0> :mod <1>)"""
    val mrEdge = Parsing.parseAmr(mrEdgeStr)

    val mrEdgeDelexStr =
      """(<0> :<lex-0> <1>)"""
    val mrEdgeDelex = Parsing.parseAmr(mrEdgeDelexStr)


    "with maxNodes==0 should return the input" in {
      val delex = Lexeme.delexicalize(0)(tokens, mr1)
      assert(delex.size == 1)
      assert(delex.head._2 ~= mr1)
    }

    "with maxNodes==1" in {
      val delex = Lexeme.delexicalize(1)(tokens, mr1).toVector
      assert(delex.size == 2)
      assert(delex.head._2 ~= mr1)
      assert(delex(1)._2 ~= expA1)
    }

    "with maxNodes==2 on graph of size 1" in {
      val delex = Lexeme.delexicalize(2)(tokens, mr1).toVector
      assert(delex.size == 2)
      assert(delex.head._2 ~= mr1)
      assert(delex(1)._2 ~= expA1)
    }

    "with maxNodes==2 on graph of size 2" in {
      val delex = Lexeme.delexicalize(2, delexicalizeEdges = false)(tokens, mr2).toVector
      assert(delex.size == 4)
      assert(delex.exists(_._2 ~= mr2))
      assert(delex.exists(_._2 ~= expA2))
      assert(delex.exists(_._2 ~= expB2))
      assert(delex.exists(_._2 ~= expAB2))
    }

    "delexicalizing a constant" in {
      val delex = Lexeme.delexicalize(2, delexicalizeEdges = false)(tokens, mrConstant).toVector
      assert(delex.size == 4)
      assert(delex.exists(_._2 ~= mrConstant))
      assert(delex.exists(_._2 ~= expConstant))
    }

    "delexicalizing an edge" in {
      val delex = Lexeme.delexicalize(1)(tokens, mrEdge).toVector
      assert(delex.size == 2)
      assert(delex.exists(_._2 ~= mrEdge))
      assert(delex.exists(_._2 ~= mrEdgeDelex))
    }
  }

}
