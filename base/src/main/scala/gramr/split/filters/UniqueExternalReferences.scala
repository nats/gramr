package gramr
package split
package filters

import gramr.graph.Graph
import gramr.text.Token

import Graph.ops._

case class UniqueExternalReferences[MR : Graph]() extends SplitFilter[MR] {

  def apply(split: Split[MR], sentence: Vector[Token]): Boolean = {
    pred(split.left.mr) && pred(split.right.mr)
  }

  def pred(mr: MR): Boolean = {
    mr.externalLayers.forall(_._2.size == 1)
  }

}