package gramr.graph.edit

import cats.syntax.traverse._
import cats.instances.vector._
import gramr.graph._
import gramr.graph.mark.Marker
import Edit._

import Graph.ops._

object Transform {

  private val conjConcepts = Set("and", "or", "and-or")

  def flattenConjunctions[MR : Graph]: Edit[MR, Unit] = for {
    mr <- getMR[MR]
    conjs = mr.nodes.filter(in => in.element.isInstanceOf[ReferentNode] && conjConcepts.contains(in.element.content)).toVector
    conjMarkers <- conjs.traverse(in => mark[MR](in.pointer))
    _ <- conjMarkers.traverse(flattenNestedConj[MR])
  } yield ()

  def flattenNestedConj[MR : Graph](conjMarker: Marker): Edit[MR, Unit] = for {
    mr <- getMR[MR]
    conjP <- resolve[MR](conjMarker)
    conj = mr.node(conjP)
    nested <- findNestedConjOperand(conj)
    _ <- nested match {
      case None =>
        noop[MR]
      case Some(opEdge) => for {
        operandMarker <- mark(opEdge.element.target)
        operandIndex = opEdgeIndex(opEdge)

        subOperandEdges: Vector[Indexed[Edge]] = mr.outEdges(opEdge.element.target).filter(isOpEdge[MR]).toVector

        // Shift all the op edges after the sub-operand by the number of inserted sub-operands.
        opEdgesAfter = mr.outEdges(conjP).filter(isOpEdge[MR]).filter(opEdgeIndex(_) > operandIndex).toVector
        _ <- opEdgesAfter.traverse { ie =>
          val newIndex = opEdgeIndex(ie) + subOperandEdges.size - 1
          relabelEdge(ie.pointer, s"op$newIndex")
        }

        // Shift all the sub-operand's op edges by the sub-operand's own op index. This assumes that they were
        // consecutive and starting with one in the first place.
        _ <- subOperandEdges.traverse { ie =>
          val newIndex = opEdgeIndex(ie) + operandIndex - 1
          relabelEdge(ie.pointer, s"op$newIndex")
        }

        _ <- build { builder: GraphBuilder[MR] =>
          builder.remove(opEdge.pointer)
        }
        _ <- mergeNodes(conjMarker, operandMarker)
        _ <- flattenNestedConj(conjMarker)
      } yield ()
    }
  } yield ()

  def findNestedConjOperand[MR : Graph](
    conj: Indexed[Node]
  ): Edit[MR, Option[Indexed[Edge]]] = for {
    mr <- getMR[MR]
  } yield {
    if(!conj.element.isInstanceOf[ReferentNode]) {
      throw new Exception("Invalid graph: conjunction node is not a referent node")
    }
    val label = conj.element.asInstanceOf[ReferentNode].concept
    (for {
      opEdge <- mr.outEdges(conj.pointer) if isOpEdge(opEdge)
      opNode = mr.node(opEdge.element.target) if opNode.element.content == label
    } yield opEdge).headOption
  }

  private val opEdgeRE = """op(\d+)""".r

  def isOpEdge[MR : Graph](edge: Indexed[Edge]): Boolean = {
    edge.element.label match {
      case opEdgeRE(_) => true
      case _ => false
    }
  }

  def opEdgeIndex[MR : Graph](edge: Indexed[Edge]): Int = {
    edge.element.label match {
      case opEdgeRE(idx) => Integer.parseInt(idx)
    }
  }

}
