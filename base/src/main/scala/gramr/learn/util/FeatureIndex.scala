package gramr.learn.util

/** Maps feature names to feature IDs.
  *
  */
trait FeatureIndex {

  /** Translate a feature to its ID.
    *
    * @param featureName a feature name
    * @return a numeric, non-negative feature ID
    */
  def apply(featureName: String): Int

  /** Translate a feature ID into a feature name.
    *
    * @param featureId a numeric feature ID
    * @return a string giving the name of the feature, if possible; otherwise another identifying string
    */
  def reverse(featureId: Int): String

}
