package gramr.split.em

import gramr.split.SplitChart

trait EMModel[T, MR, P] {

  def ops(model: T): EMModelOps[T, MR, P]

  /** Produces a count object for a given split chart entry.
    *
    * @param model the model to use for counting
    * @param chart the split chart
    * @param count fractional count weight assigned to the entry
    * @param entry the entry itself
    * @return a count object
    */
  def count(model: T, chart: SplitChart[MR])(count: Double, entry: SplitChart.Entry[MR]): P

  /** Updates the model parameters given a count object.
    *
    * @param counts the counts to add to the model
    * @return the updated model
    */
  def update(model: T, counts: P): T

  def emptyCounts: P

  def addCounts(c1: P, c2: P): P

  def sumCounts(cs: Iterable[P]): P = cs.par.fold(emptyCounts)(addCounts)

}


object EMModel {

  import scala.language.implicitConversions

  implicit def toOps[T, MR, P](model: T)(implicit emModel: EMModel[T, MR, P]): EMModelOps[T, MR, P] = emModel.ops(model)

}


trait EMModelOps[T, MR, Counts] {

  def score(entry: SplitChart.Entry[MR], chart: SplitChart[MR]): Double

  def update(counts: Counts): T

}

