package gramr
package split
package chartFilters

case class ExcludeBusySpans[MR](maxItemCount: Int) extends SplitChartFilter[MR] {
  override def instantiate(splitChart: SplitChart[MR]) = new SplitChartFilter.ItemFilter[MR] {
    private val filteredSpans = splitChart.spans.filter { span => splitChart.get(span).size > maxItemCount }.toSet

    override def apply(item: PartialMR[MR]): Boolean = !filteredSpans.contains(item.span)
  }
}
