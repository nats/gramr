package gramr.construction
package sgraph

import gramr.ccg.CombinatoryStep
import gramr.graph._
import gramr.graph.edit.EditException
import gramr.graph.edit.sgraph.SemanticOperator
import gramr.graph.edit.sgraph.Combine._
import gramr.graph.edit.sgraph.Split.performSplit
import gramr.split.{PartialMR, Split}

class SGraphRule[MR](
  val name: String,
  val appliesToFn: SyntacticContext => Boolean,
  val operator: SemanticOperator[MR],
  val backward: Boolean
)(implicit val mgl: Graph[MR]) extends Rule[MR] {

  override def meaningGraphLike: Graph[MR] = mgl

  override def appliesTo(context: SyntacticContext): Boolean = appliesToFn(context)

  override def combine(left: MR, right: MR, context: SyntacticContext): Iterator[MR] =
    try {
      Iterator(performSemanticOperator(operator.combine)(left, right, backward)).flatten
    } catch {
      case _: EditException => Iterator()
    }

  override def split(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): Iterator[Split[MR]] =
    performSplit(operator.split)(partialMR.mr, backward).map { mrs => gramr.split.Split.fromMRs(mrs, step)}.iterator
}
