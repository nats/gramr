package gramr.text

import org.scalatest._

class DependencyForestSpec extends FlatSpec {

  "parsing an attribute string" should "yield the expected tree" in {

    // Is the article too intense, is the United States so good?
    val string = "0,0,ROOT 2,1,det 0,2,nsubj 4,3,advmod 0,4,acomp 0,5,punct 0,6,conj 9,7,det 9,8,compound 6,9,nsubj 11,10,advmod 6,11,acomp 6,12,punct"

    val depTree = DependencyForest.fromAttributeString(string)
    assert(depTree.roots == Set(0))
    assert(depTree.headEdge(1) == (2, "det"))
    assert(depTree.children(2) == Set(1))
    assert(depTree.children(0) == Set(0, 2, 4, 5, 6))

  }

  "finding the path between two nodes" should "find the correct path" in {

    import DependencyForest.{UpStep, DownStep}

    // Is the article too intense, is the United States so good?
    val string = "0,0,ROOT 2,1,det 0,2,nsubj 4,3,advmod 0,4,acomp 0,5,punct 0,6,conj 9,7,det 9,8,compound 6,9,nsubj 11,10,advmod 6,11,acomp 6,12,punct"
    val depTree = DependencyForest.fromAttributeString(string)

    assert(depTree.path(1, 2) == Vector(UpStep(2, "det")))
    assert(depTree.path(3, 0) == Vector(UpStep(4, "advmod"), UpStep(0, "acomp")))
    assert(depTree.path(3, 2) == Vector(UpStep(4, "advmod"), UpStep(0, "acomp"), DownStep(2, "nsubj")))
    assert(depTree.path(8, 7) == Vector(UpStep(9, "compound"), DownStep(7, "det")))
  }

}