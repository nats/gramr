package gramr
package parse

import text._, ccg._

/** Collects information about the syntactic properties of a given parse forest node.
  */
case class SyntaxInformation(
  sentence: Vector[Token],
  span: Span,
  synCat: SyntacticCategory
) {

  lazy val tokens: Vector[Token] = span.in(sentence)

}

object SyntaxInformation {

  def fromStep(sentence: Vector[Token], step: DerivationStep[Unit]): SyntaxInformation =
    SyntaxInformation(sentence, step.span, step.synCat)

}
