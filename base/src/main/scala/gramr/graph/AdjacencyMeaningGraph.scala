package gramr.graph

import gramr.graph
import monocle.Lens
import AdjacencyMeaningGraph._
import gramr.graph.visit.{ConnectedVisitor, DepthFirstSearch, ElementOrderVisitor}

import scala.language.higherKinds


/**
  * A MeaningGraph that keeps an array of elements as well as adjacency lists.
  */
@SerialVersionUID(3L)
sealed class AdjacencyMeaningGraph private(
  val elementVector: Vector[Option[AdjacencyMeaningGraph.Entry]],
  val rootOption: Option[Int]
) extends Serializable {

  override def toString: String = AdjacencyMeaningGraph.graphInstance.graphToString(this)

  def isEmpty: Boolean = elements.isEmpty

  def elements: Iterable[Indexed[Element]] =
    elementVector.view.collect { case Some(entry) => entry.indexedElement }

  def nodes: Iterable[Indexed[Node]] =
    elements.filter(_.element.isNode).map(_.asInstanceOf[Indexed[Node]])

  def edges: Iterable[Indexed[Edge]] =
    elements.filter(_.element.isEdge).map(_.asInstanceOf[Indexed[Edge]])

  def getOption(index: Int): Option[Indexed[Element]] =
    elementVector.lift(index).flatten.map(_.indexedElement)

  def get(index: Int): Indexed[Element] = getOption(index).get

  def nodeOption(index: Int): Option[Indexed[Node]] = getOption(index) match {
    case Some(in@Indexed(IsNode(), _)) => Some(in.asInstanceOf[Indexed[Node]])
    case _ => None
  }

  def node(index: Int): Indexed[Node] = nodeOption(index).get

  def edgeOption(index: Int): Option[Indexed[Edge]] = getOption(index) match {
    case Some(ie@Indexed(IsEdge(), _)) => Some(ie.asInstanceOf[Indexed[Edge]])
    case _ => None
  }

  def edge(index: Int): Indexed[Edge] = edgeOption(index).get

  def root: Int = rootOption.getOrElse(nodes.head.pointer)

  def outEdges(n: Int): Iterable[Indexed[Edge]] = elementVector(n) match {
    case Some(NodeEntry(_, outEdges, _)) => outEdges.view.map(edge)
    case _ => Vector.empty
  }

  def inEdges(n: Int): Iterable[Indexed[Edge]] = elementVector(n) match {
    case Some(NodeEntry(_, _, inEdges)) => inEdges.view.map(edge)
    case _ => Vector.empty
  }

  def ~=(other: AdjacencyMeaningGraph): Boolean =
    elements.size == other.elements.size &&
      ((elements zip other.elements) forall { case (e1, e2) => e1.element ~= e2.element })

  def addNode(n: Node): (T, Int) = {
    val nextIndex = elementVector.size
    val pointer = nextIndex
    val entry = NodeEntry(Indexed[Node](n, pointer), Vector.empty, Vector.empty)
    (new AdjacencyMeaningGraph(elementVector :+ Some(entry), rootOption), pointer)
  }

  def addEdge(e: Edge): (T, Int) = {
    val unflipped = e.unflip
    val nextIndex = elementVector.size
    val pointer = nextIndex
    val entry = EdgeEntry(Indexed[Edge](unflipped, pointer))
    val updatedElementVector = elementVector
      .updated(unflipped.source, elementVector(unflipped.source).map(_.asInstanceOf[NodeEntry].addOutEdge(pointer)))
      .updated(unflipped.target, elementVector(unflipped.target).map(_.asInstanceOf[NodeEntry].addInEdge(pointer)))
    (new AdjacencyMeaningGraph(updatedElementVector :+ Some(entry), rootOption), pointer)
  }

  def remove(p: Int): T = {
    val updatedElementVector = elementVector.updated(p, None).map(_.map(_.removeElement(p)))
    new AdjacencyMeaningGraph(updatedElementVector, rootOption)
  }

  /** Insert a node in the place of another.
    *
    * @return the modified meaning representation
    */
  def replaceElement(p: Int, replacement: Element): T = {
    val updatedElementVector = elementVector.updated(p, elementVector(p).map(_.setIndexedElement(Indexed(replacement, p))))
    new AdjacencyMeaningGraph(updatedElementVector, rootOption)
  }

  def union(other: T): T = {
    if(this.isEmpty) {
      other
    }
    else if(other.isEmpty) {
      this
    }
    else {
      val thisIndices = this.elements.map(_.pointer).toSet
      val otherIndices = other.elements.map(_.pointer).toSet

      if(thisIndices.intersect(otherIndices).nonEmpty) {
        throw NotDisjointException(this, other)
      }

      val maxIndex = Integer.max(thisIndices.max, otherIndices.max)
      val newElementVector = (0 to maxIndex).toVector.map { index: Int =>
        if(thisIndices.contains(index)) {
          this.elementVector(index)
        }
        else if(otherIndices.contains(index)) {
          other.elementVector(index)
        }
        else {
          None
        }
      }

      new AdjacencyMeaningGraph(newElementVector, rootOption)
    }
  }

  def map(f: Indexed[Element] => Indexed[Element]): T = fromIndexedElements(elements.map(x => f(x)), rootOption)

  def mapElementContents(f: Indexed[Element] => Element): T = {
    val newElementVector = elementVector.map(_.map( entry =>
      (Entry._indexedElement composeLens Indexed._element).set(f(entry.indexedElement))(entry)
    ))
    new AdjacencyMeaningGraph(newElementVector, rootOption)
  }

  def traverse[M[_]](f: Indexed[Element] => M[Indexed[Element]])
    (implicit app: scalaz.Applicative[M]): M[T] = {
    import scalaz._, Scalaz._
    elements.toStream.traverse(f).map(els => AdjacencyMeaningGraph.fromIndexedElements(els, rootOption))
  }

  def filter(pred: Indexed[Element] => Boolean): T = {
    val predicate = elements.map { element => (element.pointer, pred(element)) }.toMap
    val newElementVector = elementVector.map {
      case Some(EdgeEntry(Indexed(edge, pointer))) if predicate(pointer) =>
        Some(EdgeEntry(Indexed(edge, pointer)))
      case Some(NodeEntry(Indexed(node, pointer), outEdges, inEdges)) if predicate(pointer) =>
        Some(NodeEntry(Indexed(node, pointer), outEdges.filter(predicate), inEdges.filter(predicate)))
      case _ => None
    }
    val newRoot = rootOption.flatMap(rt => if(pred(node(rt))) Some(rt) else None)
    new AdjacencyMeaningGraph(newElementVector, newRoot)
  }





  def adjacentEdges(n: Int): Iterable[Indexed[Edge]] = for {
    entry <- elementVector(n).map(_.asInstanceOf[NodeEntry]).view
    edgePointer <- entry.outEdges.view ++ entry.inEdges.view
    edgeEntry <- elementVector(edgePointer).map(_.asInstanceOf[EdgeEntry])
  } yield Indexed[Edge](edgeEntry.edge, edgePointer)

  def isRooted: Boolean = rootOption.isDefined

  def elementAttributes(e: Int): Attributes =
    get(e).element.attributes


  def getNodeAttributes[X](n: Int, attr: String): X =
    elementAttributes(n).get(attr)


  // Builder method implementations

  def toBuilder: AdjacencyMeaningGraph.Builder = new AdjacencyMeaningGraph.Builder(this)

  def setRoot(p: Int): T = {
    new AdjacencyMeaningGraph(elementVector, Some(p))
  }

  /** Attach an edge to a new source and/or target node.
    *
    * @return the modified meaning representation
    */
  def moveEdge(p: Int, newSource: Int, newTarget: Int): T = {
    val e: Edge = edge(p).element
    val movedSource =
      if(newSource == e.source) {
        elementVector
      }
      else {
        elementVector.
          updated(e.source, elementVector(e.source).map(_.removeElement(p))).
          updated(newSource, elementVector(newSource).asInstanceOf[Option[NodeEntry]].map(_.addOutEdge(p)))
      }
    val movedTarget =
      if(newTarget == e.target) {
        movedSource
      }
      else {
        movedSource.
          updated(e.target, elementVector(e.target).map(_.removeElement(p))).
          updated(newTarget, elementVector(newTarget).asInstanceOf[Option[NodeEntry]].map(_.addInEdge(p)))
      }
    val updatedElementVector = movedTarget.
      updated(p, Some(EdgeEntry(Indexed[Element](e.copy(source = newSource, target = newTarget), p))))
    new AdjacencyMeaningGraph(updatedElementVector, rootOption)
  }


  // Mapping

  def mapAttributes(f: Indexed[Element] => Attributes): T = {
    val newElements = elementVector.map(_.map { entry =>
      (Entry._indexedElement composeLens Indexed._element).modify(
        _.setAttributes(f(entry.indexedElement))
      )(entry)
    })
    new AdjacencyMeaningGraph(newElements, rootOption)
  }



  // Equality

  override def equals(other: Any): Boolean = other match {
    case that: AdjacencyMeaningGraph =>
      hashCode == other.hashCode &&
        elements == that.elements &&
        rootOption == that.rootOption
    case _ => false
  }

  @transient
  override lazy val hashCode: Int = {
    var code = elements.hashCode
    code = code * 41 + rootOption.hashCode
    code
  }

  def refocus(pointer: Int): T = setRoot(pointer)

  /**
    * Cleans up the graph so that all element pointers are contiguous.
    */
  def compact: T = {
    val remainingEntries = elementVector.view.collect { case Some(entry) => entry }

    // Build a map from old to new pointers
    val renumberedEntries = remainingEntries.view.zip(remainingEntries.indices)
    val pointerMap: Map[Int, Int] =
      renumberedEntries.view.collect({ case (entry, i) => (entry.pointer, i) }).toMap

    val mappedEntries = remainingEntries map { entry => Some(entry.reIndex(pointerMap)) }
    val newRoot = rootOption.map { r => pointerMap(r) }
    new AdjacencyMeaningGraph(mappedEntries.toVector, newRoot)
  }

  def walkTree(start: Int): VisitNode = {
    val visited = collection.mutable.Set.empty[Int]
    def walkFrom(n: Int): VisitNode = {
      val firstTime = !visited.contains(n)

      val subTrees = if (firstTime) {
        visited += n

        val edgesToFollow = adjacentEdges(n).toVector.sortBy(_.element.label).view

        edgesToFollow.map {
          case Indexed(Edge(_, source, target, _), e) =>
            if(!visited.contains(e)) {
              visited += e
              if (n == source)
                Some(VisitEdge(e, walkFrom(target)))
              else
                Some(VisitEdge(e, walkFrom(source)))
            }
            else {
              None
            }
        }.collect { case Some(v) => v }
      }
      else {
        Vector.empty
      }
      VisitNode(n, firstTime, subTrees.toVector)
    }

    walkFrom(start)
  }

  def dfsComponent(start: Int = root): Iterable[Indexed[Element]] = {
    def fromVisitedNode(n: VisitNode): Vector[Indexed[Element]] = {
      val tail = n.subTrees.flatMap { e => edge(e.edgePointer) +: fromVisitedNode(e.subTree) }
      if(n.firstTime)
        node(n.nodePointer) +: tail
      else
        tail
    }
    fromVisitedNode(walkTree(start))
  }

  def dfsComponents: Vector[Iterable[Indexed[Element]]] = {
    var unvisitedNodes = nodes.map(_.pointer).toSet
    var components = Vector.empty[Vector[Indexed[Element]]]

    def addComponent(start: Int): Unit = {
      val component = dfsComponent(start).toVector
      unvisitedNodes = unvisitedNodes -- component.filter(_.element.isNode).map(_.pointer)
      components = components :+ component
    }

    if(!isEmpty) {
      addComponent(root)
      while (unvisitedNodes.nonEmpty) {
        addComponent(unvisitedNodes.min)
      }

      components
    }
    else {
      Vector(Vector.empty)
    }
  }

  def toDfsOrder: T = {
    implicit val graphInstance: Graph[AdjacencyMeaningGraph] = gramr.graph.AdjacencyMeaningGraph.graphInstance
    if (isEmpty)
      this
    else {
      val visitor = new ElementOrderVisitor
      DepthFirstSearch.labelOrdered(this, visitor, restart = true)
      val indices = visitor.elementOrder
      val orderedElements = indices.iterator.map(get).toSeq
      reorderElements(orderedElements, rootOption)
    }
  }

  /**
    * Refocuses the graph on the root, puts all elements into DFS order and compacts the indices.
    */
  def normalise: T = {
    if(!isEmpty) {
      // We set the root in case it was None before
      setRoot(root).toDfsOrder
    }
    else {
      this
    }
  }

  /**
    * Splits the graph into its connected components.
    *
    * If the graph is empty, it will be returned in a single-element iterable. Otherwise, it will be split
    * into all of its non-empty components, i.e. the resulting stream will not contain an empty graph.
    */
  def components: Iterable[T] = {
    dfsComponents.map { component =>
      AdjacencyMeaningGraph.fromIndexedElements(component)
    }
  }

  // Dealing with external references

  /**
    * Renames all element pointers so they don’t conflict with those used by wit.
    * This is done by shifting all pointers’ indices to after the end of the largest index used by wit.
    */
  def preventCollisions(wit: T): T = {
    val minIndex = wit.elementVector.length

    def adjustPointer(p: Int): Int = p + minIndex

    val newElementVector = Vector.fill(wit.elementVector.length)(None) ++ elementVector.map(_.map(entry => entry.reIndex(adjustPointer)))
    val newRoot = rootOption.map(pointer => pointer + minIndex)
    new AdjacencyMeaningGraph(newElementVector, newRoot)
  }

}

object AdjacencyMeaningGraph {

  type T = AdjacencyMeaningGraph
  val T: AdjacencyMeaningGraph.type = AdjacencyMeaningGraph

  def apply(els: Indexed[Element]*): T =
    fromIndexedElements(els.toIterable)

  def reorderElements(els: Iterable[Indexed[Element]], root: Option[Int]): T = {
    val indexMap: Map[Int, Int] = (els.map(_.pointer).zipWithIndex map { case (oldPointer, newIndex) =>
      (oldPointer, newIndex)
    }).toMap
    val newElements = els.map { case Indexed(el, pointer) =>
      val newPointer = indexMap(pointer)
      val newElement = el match {
        case n: Node => n
        case e: Edge => e.setSource(indexMap(e.source)).setTarget(
          indexMap(e.target)
        )
      }
      graph.Indexed[Element](newElement, newPointer)
    }
    fromIndexedElements(newElements, root.map(indexMap))
  }

  def fromIndexedElements(
    elements: Iterable[Indexed[Element]], root: Option[Int] = None
  ): T = {
    val elementsCopy = elements.toVector
    val edges = elementsCopy.filter(_.element.isEdge).map(_.asInstanceOf[Indexed[Edge]])
    val outEdges = edges.map { case Indexed(e, p) => (e.unflip.source, p) }.groupBy(_._1).mapValues(_.map(_._2)).withDefault(_ => Vector.empty)
    val inEdges = edges.map { case Indexed(e, p) => (e.unflip.target, p) }.groupBy(_._1).mapValues(_.map(_._2)).withDefault(_ => Vector.empty)
    val numbered = elementsCopy.map {
      case in@Indexed(e, p) if e.isNode =>
        (p, NodeEntry(in, outEdges(p), inEdges(p)))
      case ie@Indexed(e, p) if e.isEdge =>
        (p, EdgeEntry(ie))
    }.toMap
    val maxIndex: Int = if (elementsCopy.isEmpty) -1 else elementsCopy.view.map(_.pointer).max
    val elementVector: Vector[Option[Entry]] = ((0 to maxIndex) map { i => numbered.get(i) }).toVector
    new AdjacencyMeaningGraph(elementVector, root)
  }

  def build(f: Graph[AdjacencyMeaningGraph]#Building[Unit]): AdjacencyMeaningGraph = graphInstance.build(f)

  def buildReturning[A](f: Graph[AdjacencyMeaningGraph]#Building[A]): (AdjacencyMeaningGraph, A) = graphInstance.buildReturning(f)

  lazy val graphInstance: Graph[AdjacencyMeaningGraph] = ops.graphInstance

  sealed trait Entry {
    def indexedElement: Indexed[Element]

    def setIndexedElement(ie: Indexed[Element]): Entry

    def pointer: Int = indexedElement.pointer

    def element: Element = indexedElement.element

    def reIndex(mapping: Int => Int): Entry

    def removeElement(p: Int): Entry
  }

  object Entry {

    val _indexedElement: Lens[Entry, Indexed[Element]] =
      Lens[Entry, Indexed[Element]](_.indexedElement)(ie => entry => entry.setIndexedElement(ie))

  }

  case class NodeEntry(
    indexedElement: Indexed[Element],
    outEdges: Vector[Int],
    inEdges: Vector[Int]
  ) extends Entry {

    def setIndexedElement(ie: Indexed[Element]): NodeEntry = copy(indexedElement = ie)

    def reIndex(mapping: Int => Int): Entry = {
      def edgePointerMapping(ep: Int): Int = mapping(ep)

      NodeEntry(
        indexedElement.copy(pointer = mapping(indexedElement.pointer)),
        outEdges.map(edgePointerMapping),
        inEdges.map(edgePointerMapping)
      )
    }

    def addOutEdge(p: Int): NodeEntry = copy(outEdges = outEdges :+ p)

    def addInEdge(p: Int): NodeEntry = copy(inEdges = inEdges :+ p)

    def removeElement(p: Int): NodeEntry = {
      copy(outEdges = outEdges.filter(_ != p), inEdges = inEdges.filter(_ != p))
    }

  }

  case class EdgeEntry(
    indexedElement: Indexed[Element]
  ) extends Entry {

    private implicit def mgl: Graph[T] = graphInstance

    def setIndexedElement(ie: Indexed[Element]): EdgeEntry = copy(indexedElement = ie)

    def edge: Edge = indexedElement.element.asInstanceOf[Edge]

    def reIndex(mapping: Int => Int): Entry = {
      EdgeEntry(
        Indexed[Edge](
          graph.Edge(
            edge.label,
            mapping(edge.source),
            mapping(edge.target),
            edge.attributes
          ),
          mapping(pointer)
        )
      )
    }

    def removeElement(p: Int): EdgeEntry = this
  }


  class Builder(
    private var mr: T,
    private var normaliseFlag: Boolean = true
  ) extends GraphBuilder[AdjacencyMeaningGraph] {
    implicit val mgl: Graph[T] = graphInstance

    import scala.language.existentials

    override def build: T = if(normaliseFlag) mr.normalise else mr

    override def addNode(n: Node): Int = {
      val (newMR, p): (T, Int) = mr.addNode(n)
      val withRoot = if(newMR.isRooted) newMR else newMR.setRoot(p)
      mr = withRoot
      p
    }

    override def addEdge(e: Edge): Int = {
      val (newMR, p): (T, Int) = mr.addEdge(e)
      mr = newMR
      p
    }

    override def remove(pointer: Int): Unit = {
      val newMR = mr.remove(pointer)
      mr = newMR
    }

    override def setRoot(pointer: Int): Unit = {
      val newMR = mr.setRoot(pointer)
      mr = newMR
    }

    override def moveEdge(p: Int, newSource: Int, newTarget: Int): Unit = {
      val newMR = mr.moveEdge(p, newSource, newTarget)
      mr = newMR
    }

    override def replaceNode(p: Int, n: Node): Unit = {
      val newMR = mr.replaceElement(p, n)
      mr = newMR
    }

    override def relabelEdge(p: Int, label: String): Unit = {
      val modifiedEdge: Edge = mr.edge(p).element.copy(label = label)
      mr = mr.replaceElement(p, modifiedEdge)
    }

    override def replaceAttributes(p: Int, attr: Attributes): Unit = {
      mr = mr.replaceElement(p, mr.get(p).element.setAttributes(attr))
    }

    /**
      * This specialty method is needed for some unit tests and switches off
      * focusing the graph on the root.
      */
    def setNormalise(value: Boolean): Unit = normaliseFlag = value
  }

  object ops {

    implicit def graphInstance: Graph[AdjacencyMeaningGraph] =
      new Graph[AdjacencyMeaningGraph] {

        import scala.language.higherKinds

        override def isEmpty(graph: T): Boolean = graph.isEmpty

        override def getOption(graph: T, index: Int): Option[Indexed[Element]] = graph.getOption(index)

        override def elements(graph: T): Iterable[Indexed[Element]] = graph.elements

        override def root(graph: T): Int = graph.root

        override def rootOption(graph: T): Option[Int] = graph.rootOption

        override def outEdges(graph: T, n: Int): Iterable[Indexed[Edge]] = graph.outEdges(n)

        override def inEdges(graph: T, n: Int): Iterable[Indexed[Edge]] = graph.inEdges(n)

        override def ~=(graph1: T, graph2: T): Boolean = graph1 ~= graph2

        override def addNode(graph: T, n: Node): (T, Int) = graph.addNode(n)

        override def addEdge(graph: T, e: Edge): (T, Int) = graph.addEdge(e)

        override def remove(graph: T, index: Int): T = graph.remove(index)

        override def replaceElement(graph: T, p: Int, replacement: Element): T = graph.replaceElement(p, replacement)

        override def union(graph1: T, graph2: T): T = graph1.union(graph2)

        override def map(graph: T)(f: Indexed[Element] => Indexed[Element]): T = graph.map(f)

        override def mapElementContents(graph: T)(f: Indexed[Element] => Element): T = graph.mapElementContents(f)

        override def traverse[M[_]](graph: T)(f: Indexed[Element] => M[Indexed[Element]])(implicit app: scalaz.Applicative[M]): M[T] = graph.traverse(f)

        override def filter(graph: T)(pred: Indexed[Element] => Boolean): T = graph.filter(pred)

        override def components(graph: T): Iterable[T] = graph.components

        override def walkTree(graph: T, source: Int): VisitNode = graph.walkTree(source)

        override def refocus(graph: T, p: Int): T = graph.refocus(p)

        override def normalise(graph: T): T = graph.normalise

        override def preventCollisions(graph: T, wit: T): T = graph.preventCollisions(wit)



        override def empty: T = new AdjacencyMeaningGraph(Vector.empty, None)

        override def fromIndexedElements(
          elements: scala.collection.Iterable[Indexed[Element]], root: Option[Int] = None
        ): T =
          T.fromIndexedElements(elements, root)

        override def builder: Builder = new Builder(empty)

        override def builder(graph: T, normalise: Boolean = true): Builder = new Builder(graph, normalise)

      }

  }

}
