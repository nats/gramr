package gramr.graph
package presentation

import Graph.ops._

case class HighlightAttribute(highlights: Set[Int]) extends Attribute {

  import HighlightAttribute._
  import scalatags.Text.all._, scalatags.Text.TypedTag

  private def bgColour = colours(highlights.head)

  override def mangleEdgeHtml(html: TypedTag[String]): TypedTag[String] =
    if (highlights.isEmpty)
      html
    else
      span(backgroundColor := bgColour, html)

  override def mangleNodeHtml(html: TypedTag[String]): TypedTag[String] =
    if (highlights.isEmpty)
      html
    else
      span(backgroundColor := bgColour, html)

}

object HighlightAttribute {

  val colours: Stream[String] = {
    import java.awt.Color
    val hueStep = 11.0f / 37.0f
    val satStep = 1.0f / 7.0f

    def gen(i: Int): Stream[java.awt.Color] = {
      val hue = hueStep * i % 1.0f
      val sat = 0.1f + (satStep * i % 1.0f) * 0.3f
      //val sat = 0.1f
      val lum = 1.0f
      Color.getHSBColor(hue, sat, lum) #:: gen(i + 1)
    }

    gen(0).map(col => "#%02x%02x%02x".format(col.getRed, col.getGreen, col.getBlue))
  }

  def annotate[G](g: G)(implicit meaningGraphLike: Graph[G]): G = {
    def mkAttr(a: Attributes): Option[Attribute] =
      for (al <- a.getOption[AlignmentsAttribute]("alignments")) yield HighlightAttribute(al.edges)

    g.mapAttributes(
      { case Indexed(el, _) =>
        mkAttr(el.attributes) match {
          case Some(attr) => el.attributes + ("highlight" -> attr)
          case None => el.attributes
        }
      }
    )
  }

}