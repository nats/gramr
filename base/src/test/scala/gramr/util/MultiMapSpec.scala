package gramr.util

import org.scalatest._

class MultiMapSpec extends FlatSpec {

  "Empty MultiMap" should "be empty" in {
    val empty = MultiMap.empty[String, String]
    assert(empty.isEmpty)
    assert(!empty.nonEmpty)
    assert(empty.keySet.isEmpty)
    assert(empty.values.isEmpty)
    assert(empty.entries.isEmpty)
  }

  "MultiMap with added item" should "contain entries" in {
    val empty = MultiMap.empty[String, String]
    val nonEmpty = empty + ("a" -> "A")
    assert(nonEmpty.nonEmpty)
    assert(nonEmpty.keySet == Set("a"))
    assert(nonEmpty.values.toSeq == Seq("A"))
    assert(nonEmpty.entries.toSeq == Seq(("a", "A")))
  }

}