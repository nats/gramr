package gramr.ccg.presentation

import scalatags.Text.all._
import scalatags.Text.TypedTag
import gramr.ccg.{Lexicon, StoredLexicon}
import gramr.util.html.ToHtml

class StoredLexiconToHtml[MR: ToHtml] extends ToHtml[StoredLexicon[MR]] {

  def toHtml(lexicon: StoredLexicon[MR]): TypedTag[String] = div(
    lexicon.storedItems.toVector.sortBy(_._1) flatMap { case (key, items) => Seq(
      h3(s"${key.tokens.mkString(" ")}"),
      table(
        items.toSeq.map { item =>
          tr(
            td(fontSize := "10px", verticalAlign := "top", a(name := s"${item.index}", item.index)),
            td(item.synCat.toString),
            td(implicitly[ToHtml[MR]].toHtml(item.meaning))
          )
        }
      )
    )
    }
  )

}
