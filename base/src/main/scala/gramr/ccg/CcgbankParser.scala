package gramr
package ccg

import com.typesafe.scalalogging.LazyLogging
import text._
import org.parboiled2._

import scala.util.{Failure, Success, Try}

/** A class that parses CCG derivations in CCGbank’s AUTO format.
  */
class CcgbankParser(val inputString: String, combinatorSystem: CombinatorSystem) extends Parser {

  override val input: ParserInput = inputString

  /** Parses a single derivation in AUTO format.
    */
  def derivation: Rule1[Derivation[Unit]] = rule {
    tree ~ EOI ~> { (tree: DerivationStep[Unit]) => Derivation(tree) }
  }

  def tree: Rule1[DerivationStep[Unit]] = rule {
    (&("(<T") ~ innerNode) | (&("(<L") ~ leaf)
  }

  def innerNode: Rule1[CombinatoryStep[Unit]] = rule {
    openNode ~ innerNodeDesc ~ zeroOrMore(tree) ~ closeNode ~> { (
      synCat: SyntacticCategory, subTrees: Seq[DerivationStep[Unit]]
    ) =>
      val matchingCombinators = combinatorSystem.matchingCombinators(synCat, subTrees.toList.map(_.synCat))
      val span = Span(0, 0)
      val step = for (combinator <- matchingCombinators.headOption) yield CombinatoryStep(
        combinator, subTrees.toList, synCat, span, ()
      )
      if (step.isEmpty)
        throw new Exception(
          s"Derivation contains unknown combinator with result $synCat from arguments ${
            subTrees.map(_.synCat).mkString(", ")
          }"
        )
      step.get
    }
  }

  def leaf: Rule1[DerivationStep[Unit]] = rule {
    openNode ~ leafDesc ~ closeNode
  }

  def innerNodeDesc: Rule1[SyntacticCategory] = rule {
    openInnerNodeDesc ~ synCat ~ int ~ int ~ closeDesc ~> { (synCat: SyntacticCategory) =>
      synCat
    }
  }

  def leafDesc: Rule1[LexicalStep[Unit]] = rule {
    openLeafDesc ~ synCat ~ tag ~ tag ~ string ~ synCat ~ closeDesc ~> { (
      synCat: SyntacticCategory, token: String, _: SyntacticCategory
    ) =>
      val tokens = Vector(Token(token, token))
      val span = Span(0, 0)
      LexicalStep(tokens, synCat, span, ())
    }
  }

  def openInnerNodeDesc: Rule0 = rule {
    "<T" ~ space
  }

  def openLeafDesc: Rule0 = rule {
    "<L" ~ space
  }

  def closeDesc: Rule0 = rule {
    ">" ~ space.?
  }

  def openNode: Rule0 = rule {
    "(" ~ space.?
  }

  def closeNode: Rule0 = rule {
    ")" ~ space.?
  }

  // This is just a very rough rule as we don’t actually want to parse the syncats here.
  def synCat: Rule1[SyntacticCategory] = rule {
    capture(oneOrMore(!space ~ (CharPredicate.AlphaNum ++ """()[]_/\,.:;"""))) ~ space.? ~> { s: String =>
      SyntacticCategory.parse(s).get
    }
  }

  // This parser doesn’t use POS tags, so the rule doesn’t even return them.
  def tag: Rule0 = rule {
    oneOrMore(CharPredicate.Alpha ++ """$,.:;""") ~ space.?
  }

  def string: Rule1[String] = rule {
    capture(oneOrMore(!space ~ ANY)) ~ space.?
  }

  // This parser doesn’t use the head index and daughter count fields, so the rule doesn’t return them.
  def int: Rule0 = rule {
    oneOrMore(CharPredicate.Digit) ~ space.?
  }

  def space: Rule0 = rule {
    oneOrMore(" ")
  }

}


object CcgbankParser extends LazyLogging {

  def parseAttributeString(attributeString: String)(implicit combinatorSystem: CombinatorSystem): Try[Derivation[Unit]] = {
    val parser = new CcgbankParser(attributeString, combinatorSystem)
    parser.derivation.run() match {
      case f@Failure(e) => f
      case Success(derivation) =>
        val fixed = derivation.fixSpans
        Success(fixed)
    }
  }

}
