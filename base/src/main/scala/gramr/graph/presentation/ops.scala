package gramr.graph.presentation

import gramr.graph.Graph
import gramr.util.html.ToHtml

object ops {

  import scala.language.implicitConversions

  implicit def graphToHtml[MR: Graph]: ToHtml[MR] = new GraphToHtml[MR]

}
