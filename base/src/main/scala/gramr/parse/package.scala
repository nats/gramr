package gramr

import gramr.parse.chart.{Action, Chart}
import gramr.text.Example
import Example.ops._

import scala.language.implicitConversions

package object parse {

  type LexicalChoice[E, MR] = E => SyntaxInformation => Iterator[LexicalDecision[MR]]

  implicit def lexicalChoiceOps[E, MR](choice: LexicalChoice[E, MR]): LexicalChoiceOps[E, MR] =
    new LexicalChoiceOps[E, MR](choice)

  class LexicalChoiceOps[E, MR](choice: LexicalChoice[E, MR]) {

    def union(other: LexicalChoice[E, MR]): LexicalChoice[E, MR] = (e: E) => {
      val leftInstance = choice(e)
      val rightInstance = other(e)
      i: SyntaxInformation => leftInstance(i) ++ rightInstance(i)
    }

    def without(other: LexicalChoice[E, MR]): LexicalChoice[E, MR] = (e: E) => {
      val leftInstance = choice(e)
      val rightInstance = other(e)
      i: SyntaxInformation => {
        val toSubtract = rightInstance(i).toSet
        leftInstance(i).filter(!toSubtract.contains(_))
      }
    }

  }

  /** Filter predicate on parser actions.
    *
    * ActionPredicates are employed by parsing algorithms to limit the search space of the parser.
    * Every action predicate evaluates whether a given unlabelled parser action is valid in a given context.
    *
    * For efficiency, action predicates are instantiated in two steps. Since a predicate is called many times within
    * the same context (i.e. for the same example), it is first instantiated within the context and then called for
    * every tentative action.
    *
    * @tparam MR type of meaning representation
    * @tparam FV type of feature vector
    * @tparam C type of context (e.g. example)
    */
  type ActionPredicate[MR, FV, C] = C => Action[MR, FV] => Boolean

  /** Conjunction of several action predicates. */
  def and[MR, FV, C](ps: ActionPredicate[MR, FV, C]*): ActionPredicate[MR, FV, C] = context => {
    val instances = ps.map(_(context))
    action => {
      instances.forall(_(action))
    }
  }


  /** Parse a bunch of examples in batch, collecting the parse chart for each example.
    *
    * Note that charts tend to get large and so you either should only do this with a small set of examples,
    * or use a post-processed parser that produces trimmed-down charts (e.g. containing only the single best parse).
    *
    * @param parser a parser
    * @param examples the examples to parse
    * @tparam MR type of meaning representation
    * @tparam FV type of feature vector
    * @tparam E type of example
    * @return a map from strings to parse charts
    */
  def batchParse[MR, FV, E: Example](parser: Parser[MR, FV, E], examples: Iterable[E]): Map[String, Chart[MR, FV]] = {
    examples.par.map { example =>
      val chart = parser.parse(example.sentence, example)
      (example.id, chart)
    }.seq.toMap
  }

}
