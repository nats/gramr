package gramr.parse

import gramr.ccg.Combinator
import gramr.construction.SyntacticContext

trait CombinatoryDecision[MR] extends Decision[MR] {

  def context: SyntacticContext

  def combinator: Combinator = context.combinator

}

trait CombinatoryChoice[MR] extends Choice[(SyntacticContext, MR, MR), MR]
