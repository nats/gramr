package gramr.graph

import org.scalatest._
import gramr.amr._
import gramr.util.Parsing._

class JamrAlignmentsSpec extends FlatSpec {

  "JAMR alignment annotations" should "be parsed correctly" in {

    val str1 = "0-1|0+0.0+0.1+0.2"
    val al1 = JamrAlignment(
      Vector(
        (0 until 1, Set(Vector(0), Vector(0, 0), Vector(0, 1), Vector(0, 2)))
      )
    )
    assert(JamrAlignment.parse(str1) == al1)

  }

  "Corpus items with alignments" should "lead to correctly annotated meaning graphs" in {

    implicit val mgl: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance
    import mgl.Building._

    val item =
      """
# ::alignments 0-1|0+0.0+0.1+0.2
(d / date-entity
  :year 2002
  :month 1
  :day 5)
"""

    def al(s: Set[Int]) = Attributes(Map("alignments" -> AlignmentsAttribute(s)))

    val graph = mgl.build {
      for {
        n0 <- mkReferentNode("date-entity", al(Set(0)))
        n00 <- mkConstantNode("2002", al(Set(0)))
        n01 <- mkConstantNode("1", al(Set(0)))
        n02 <- mkConstantNode("5", al(Set(0)))
        _ <- mkEdge("year", n0, n00)
        _ <- mkEdge("month", n0, n01)
        _ <- mkEdge("day", n0, n02)
      } yield ()
    }

    assert(parseAlignedAmr[AdjacencyMeaningGraph](item) ~= graph)
  }

}
