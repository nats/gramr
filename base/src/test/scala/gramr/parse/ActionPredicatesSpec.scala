package gramr.parse

import scala.language.existentials
import org.scalatest._
import gramr.graph.{AdjacencyMeaningGraph, Graph}

class ActionPredicatesSpec extends FreeSpec {

  implicit val meaningGraphLike: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance

  import meaningGraphLike.Building._

  "Edge precision rating" - {
    "should correctly handle differently focused graphs" in {
      val (g, innovate) = meaningGraphLike.buildReturning {
        for {
          establish <- mkReferentNode("establish-01")
          model <- mkReferentNode("model")
          innovate <- mkReferentNode("innovate-01")
          industry <- mkReferentNode("industry")
          _ <- mkEdge("ARG1", establish, model)
          _ <- mkEdge("mod", model, innovate)
          _ <- mkEdge("ARG1", innovate, industry)
          _ <- normalise(false)
        } yield innovate
      }

      val grb = meaningGraphLike.builder(g)
      grb.setRoot(innovate)
      val gr = grb.build

      assert(ActionPredicates.EdgeTriplePrecision.precision(gr.normalise, g.normalise) == 1.0)
    }
  }

}
