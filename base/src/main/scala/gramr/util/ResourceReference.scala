package gramr.util

class ResourceReference[A](var resource: Option[A]) {

  def define(a: A): Unit = resource = Some(a)

  def access: A = resource.get

}

object ResourceReference {

  def empty[A]: ResourceReference[A] = new ResourceReference[A](None)

  def of[A](a: A): ResourceReference[A] = new ResourceReference[A](Some(a))

}
