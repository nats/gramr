package gramr.graph.edit.sgraph

import gramr.amr.AmrTreeParser
import gramr.ccg.EasyccgCombinatorSystem.{BA, FA}
import gramr.ccg.{CombinatoryStep, Derivation, LexicalStep, SyntacticCategory}
import gramr.graph._
import gramr.split.labelling.{AttachConstantNodes, CompleteNodeAlignments, LabelAlignments}
import gramr.split.{PatternBasedSplitGenerator, RecursiveSplitting, SplitChart}
import gramr.text.{Example, HasMR, Span, Token}
import org.scalatest.FreeSpec
import Graph.ops._
import gramr.util.Parsing

class InductionTests extends FreeSpec {

  import TestGrammar.patterns

  import scala.language.implicitConversions

  type MR = AdjacencyMeaningGraph

  case class TestExample(
    sentence: Vector[Token],
    reference: MR,
    derivation: Derivation[Unit],
    id: String = "id"
  ) {

    def indexedDerivations: Vector[(String, Derivation[Unit])] = Vector(("der", derivation))

  }

  implicit val exampleInstance: Example[TestExample] = new Example[TestExample] {
    override def id(a: TestExample) = a.id

    override def sentence(a: TestExample) = a.sentence

    override def indexedDerivations(a: TestExample) = a.indexedDerivations
  }

  implicit val hasMRInstance: HasMR[TestExample, MR] = new HasMR[TestExample, MR] {
    override def mr(a: TestExample) = a.reference
  }

  implicit val mgl: Graph[MR] = AdjacencyMeaningGraph.graphInstance
  import mgl.Building._

  def split(input: TestExample): SplitChart[MR] = {
    val splitGenerator = new PatternBasedSplitGenerator[MR](
      LabelAlignments[MR]() andThen AttachConstantNodes[MR]() andThen CompleteNodeAlignments[MR](),
      patterns
    )
    val algorithm = new RecursiveSplitting[MR, TestExample](
      (e: TestExample) => splitGenerator,
      splitFilter = gramr.split.filters.Connected() and
        gramr.split.filters.MatchSyntacticArity.lessOrEqual and
        gramr.split.filters.NoSingleConstants()
    )
    algorithm.split(input, input.derivation, "0").right.get
  }

  "anna married manny" in {
    val tokens = Vector("anna", "married", "manny").map(s => Token(s, s))
    val alignments = Map(
      "m" -> Set(1),
      "p1" -> Set(0),
      "n1" -> Set(0),
      "p2" -> Set(2),
      "n2" -> Set(2)
    )
    val mr = Parsing.parseAmr(
      """
        |(m / marry
        |  :ARG0 (p1 / person
        |    :name (n1 / name :op1 "anna"))
        |  :ARG1 (p2 / person
        |    :name (n2 / name :op1 "manny")))
      """.stripMargin).mapNodes {
        case Indexed(node, p) =>
          node.attributes.getOption[DefaultAttribute[String]]("node_name") match {
            case Some(DefaultAttribute(name)) => Indexed(node.setAttributes(node.attributes.extend("alignments", AlignmentsAttribute(alignments(name)))), p)
            case _ => Indexed(node, p)
          }
      }


    val example = TestExample(
      tokens,
      mr,
      Derivation(
        CombinatoryStep(BA, List(
          LexicalStep(tokens.slice(0, 1), SyntacticCategory("NP"), Span(0, 0), ()),
          CombinatoryStep(FA, List(
            LexicalStep(tokens.slice(1, 2), SyntacticCategory("""(S\NP)/NP"""), Span(1, 1), ()),
            LexicalStep(tokens.slice(2, 3), SyntacticCategory("""NP"""), Span(2, 2), ())
          ), SyntacticCategory("""S\NP"""), Span(1, 2), ())
        ), SyntacticCategory("S"), Span(0, 2), ())
      )
    )

    val expectedAnna = Parsing.parseAmr("""(p / person :name (n / name :op1 "anna"))""")
    val expectedManny = Parsing.parseAmr("""(p / person :name (n / name :op1 "manny"))""")
    val expectedMarry = mgl.build {
      for {
        m <- mkReferentNode("marry")
        a0 <- mkExternalNode(ExternalReference(0))
        a1 <- mkExternalNode(ExternalReference(1))
        _ <- mkEdge("ARG0", m, a0)
        _ <- mkEdge("ARG1", m, a1)
      } yield ()
    }

    val splitChart = split(example)

    assert(splitChart.items.exists(_.mr ~= expectedAnna))
    assert(splitChart.items.exists(_.mr ~= expectedManny))
    assert(splitChart.items.exists(_.mr ~= expectedMarry))
  }

  "opium farmers" in {
    val tokens = Vector("opium", "farmers").map(s => Token(s, s))
    val alignments = Map(
      "p" -> Set(1),
      "f" -> Set(1),
      "o" -> Set(0),
    )
    val mr = Parsing.parseAmr(
      """
        |(p / person
        |  :ARG0-of (f / farm
        |    :ARG1 (o / opium)))
      """.stripMargin).mapNodes {
      case Indexed(node, p) =>
        node.attributes.getOption[DefaultAttribute[String]]("node_name") match {
          case Some(DefaultAttribute(name)) => Indexed(node.setAttributes(node.attributes.extend("alignments", AlignmentsAttribute(alignments(name)))), p)
          case _ => Indexed(node, p)
        }
    }

    val example = TestExample(
      tokens,
      mr,
      Derivation(
        CombinatoryStep(FA, List(
          LexicalStep(tokens.slice(0, 1), SyntacticCategory("""N/N"""), Span(0, 0), ()),
          LexicalStep(tokens.slice(1, 2), SyntacticCategory("""N"""), Span(1, 1), ())
        ), SyntacticCategory("""N"""), Span(0, 1), ())
      )
    )

    val expectedOpium = Parsing.parseAmr("""(<0> :ARG1 (o / opium))""")
    val expectedFarmers = Parsing.parseAmr("""(p / person :ARG0-of (f / farm))""")

    val splitChart = split(example)

    assert(splitChart.items.exists(_.mr ~= expectedOpium))
    assert(splitChart.items.exists(_.mr ~= expectedFarmers))
  }

}