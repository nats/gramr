package gramr
package split
package labelling

import gramr.graph._
import ccg._
import Graph.ops._
import gramr.graph.edit.{SplitDirection, SplitLeft, SplitRight, SplitUnconstrained}

/** Labels edges as follows:
  *
  * - If an edge is already labelled left or right, it keeps that label
  * - If it’s unconstrained and both adjacent nodes have the same label, the edge gets that label
  * - If the nodes have different labels, the edge is assigned to the syntactic functional side
  */
case class LabelFuncArgEdges[MR : Graph]() extends DeterministicLabellingFunction[MR] {

  def labelDeterministic(partialMR: PartialMR[MR], step: CombinatoryStep[Unit]): PartialMR[MR] = {
    import gramr.graph.Indexed
    val PartialMR(mr, span, synCat) = partialMR
    val headIsLeft = step.combinator.headIndex == 0
    val labelledMR = mr mapEdgeAttributes { case Indexed(edge, pointer) =>
      val direction = edge.attributes.get[SplitDirection]("splitDirection") match {
        case SplitLeft => SplitLeft
        case SplitRight => SplitRight
        case SplitUnconstrained =>
          val sourceDirection = mr.node(edge.source).element.attributes.get[SplitDirection]("splitDirection")
          val targetDirection = mr.node(edge.target).element.attributes.get[SplitDirection]("splitDirection")
          if (sourceDirection == targetDirection)
            sourceDirection
          else if (headIsLeft) SplitLeft else SplitRight
      }
      edge.attributes + ("splitDirection" -> direction)
    }
    PartialMR(labelledMR, span, synCat)
  }
}
