package gramr.parse.chart

import gramr.graph.AdjacencyMeaningGraph
import gramr.learn.util.StringStore
import gramr.learn.{FeatureVector, MapFeatureVector}
import gramr.text.{Span, Token}
import org.scalatest.FlatSpec

class ChartParsingSpec extends FlatSpec {

  val stringStore = new StringStore

  class MyParsing extends ChartParsing[AdjacencyMeaningGraph, MapFeatureVector, Unit] {
    override implicit def featureVector: FeatureVector[MapFeatureVector] = MapFeatureVector.featureVectorLike(stringStore)

    override implicit def chartLabel: ChartLabel[Unit] = ???

    override implicit def hasFeatures: HasFeatures[MapFeatureVector, Unit] = ???

    override implicit def labelOrdering: Ordering[Unit] = ???
  }

  val parsing = new MyParsing

  val snt5 = Vector(Token("the", "the"), Token("cat", "cat"), Token("sat", "sit"),
    Token("on", "on"), Token("the", "the"), Token("mat", "mat"))

  val spans5 = Vector(
    Span(0, 0), Span(1, 1), Span(2, 2), Span(3, 3), Span(4, 4), Span(5, 5),
    Span(0, 1), Span(1, 2), Span(2, 3), Span(3, 4), Span(4, 5),
    Span(0, 2), Span(1, 3), Span(2, 4), Span(3, 5),
    Span(0, 3), Span(1, 4), Span(2, 5),
    Span(0, 4), Span(1, 5),
    Span(0, 5)
  )

  "spans" should "emit all expected spans" in {
    val chart = parsing.Chart.empty(snt5)
    assert(chart.spans.toSet == spans5.toSet)
  }

  "spansShortToLong" should "emit all expected spans in the right order" in {
    val chart = parsing.Chart.empty(snt5)
    assert(chart.spansShortToLong.toVector == spans5)
  }

}
