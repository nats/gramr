package gramr.graph

import Graph.ops._

class SubGraphOps[T](g: T)(implicit meaningGraphLike: Graph[T]) {

  import SubGraphOps._

  def containsGraph(g2: T): Boolean = contains(g, g2)

  def isEquivalentTo(g2: T): Boolean = equivalent(g, g2)

  def mappingTo(g2: T): Option[SubGraphAlgorithms.Mapping[T]] =
    new SubGraphAlgorithms(g2, g).validMappings.headOption

}

object SubGraphOps {

  object Implicits {

    import scala.language.implicitConversions

    implicit def toSubGraphOps[T : Graph](g: T): SubGraphOps[T] = new SubGraphOps(g)

  }

  /**
    * Returns true if sub is properly contained within sup (modulo attributes and path renamings).
    */
  def contains[G : Graph](sup: G, sub: G): Boolean =
    new SubGraphAlgorithms(sup, sub).contains

  def equivalent[G : Graph](a: G, b: G): Boolean =
    contains(a, b) && contains(b, a)

  /**
    * Determines the hull of sub with respect to sup: Assuming that sub is a sub-graph of sup,
    * limits sup to those nodes and the edges between those nodes.
    */
  def hull[G](sup: G, sub: G)(implicit meaningGraphLike: Graph[G]): Option[G] = {
    for {
      mapping <- new SubGraphAlgorithms(sup, sub).validMappings.headOption
    } yield {
      val elements: Set[Int] = mapping.mapping.values.toSet
      sup.filter {
        case Indexed(IsNode(), pointer) => elements.contains(pointer)
        case Indexed(Edge(_, source, target, _), _) => elements.contains(source) && elements.contains(target)
      }
    }
  }

}

/** Implements the algorithm that computes the sub graph property.
  *
  * TODO: There is huge performance improvement potential here
  */
class SubGraphAlgorithms[G](sup: G, sub: G)(implicit meaningGraphLike: Graph[G]) {

  import SubGraphAlgorithms._

  case class MyMapping(mapping: Map[Int, Int]) extends Mapping[G] {
    def addNodeMapping(a: Int, b: Int): Option[MyMapping] = {
      val valid =
        (sub.node(a).element, sup.node(b).element) match {
          case (ra@ReferentNode(_, _), rb@ReferentNode(_, _)) =>
            ra.concept == "<coref>" || ra.concept == rb.concept
          case (ca@ConstantNode(_, _), cb@ConstantNode(_, _)) =>
            ca.name == cb.name
          case (_@ExternalNode(_, _), _) =>
            // External nodes can map to anything as they change identity later in the process.
            true
          case _ => false
        }
      if (valid) Some(MyMapping(mapping + (a -> b))) else None
    }

    def addEdgeMapping(a: Indexed[Edge], b: Indexed[Edge]): Option[MyMapping] = {
      val (an, bn) = (a.element.unflip, b.element.unflip)
      val valid =
        mapping.contains(an.source) && mapping(an.source) == bn.source &&
          mapping.contains(an.target) && mapping(an.target) == bn.target &&
          an.label == bn.label
      if (valid) Some(MyMapping(mapping + (a.pointer -> b.pointer))) else None
    }
  }

  def validMappings: Stream[Mapping[G]] = {
    def nodeCompletions(
      m: MyMapping,
      unpairedSubNodes: Stream[Int],
      unpairedSupNodes: Set[Int]
    )
    : Stream[MyMapping] = {

      def availableSupNodes(subNode: Int): Stream[Int] = {
        val node = sub.node(subNode).element
        val isMarker = node match {
          case ReferentNode(concept, _) => concept.startsWith("<")
          case _ => false
        }
        if (isMarker)
          sup.nodes.map(_.pointer).toStream
        else
          unpairedSupNodes.toStream
      }

      unpairedSubNodes match {
        case subNode #:: subNodes => for {
          supNode <- availableSupNodes(subNode)
          nodeMapped <- m.addNodeMapping(subNode, supNode).toStream
          completed <- nodeCompletions(nodeMapped, subNodes, unpairedSupNodes - supNode)
        } yield completed
        case Stream.Empty => Stream(m)
      }
    }

    def edgeCompletions(
      m: MyMapping,
      unpairedSubEdges: Stream[Int],
      unpairedSupEdges: Set[Int]
    )
    : Stream[MyMapping] = unpairedSubEdges match {
      case subEdge #:: subEdges => for {
        supEdge <- unpairedSupEdges.toStream
        edgeMapped <- m.addEdgeMapping(sub.edge(subEdge), sup.edge(supEdge)).toStream
        completed <- edgeCompletions(edgeMapped, subEdges, unpairedSupEdges - supEdge)
      } yield completed
      case Stream.Empty => Stream(m)
    }

    for {
      nodeMapping <- nodeCompletions(
        MyMapping(Map.empty),
        sub.nodes.toStream.map(_.pointer),
        sup.nodes.map(_.pointer).toSet
      )
      edgeMapping <- edgeCompletions(
        nodeMapping,
        sub.edges.toStream.map(_.pointer),
        sup.edges.map(_.pointer).toSet
      )
    } yield edgeMapping
  }

  /**
    * Returns true if sub is properly contained within sup (modulo attributes and path renamings).
    */
  def contains: Boolean = validMappings.nonEmpty

}


object SubGraphAlgorithms {

  trait Mapping[G] {
    def mapping: Map[Int, Int]
  }

}
