package gramr.construction
package sgraph

import gramr.graph._

import Graph.ops._

/** Optionally converts a graph to a modifier by making the 0-th placeholder the root of the graph.
  *
  * This is needed for relative clause constructions such as "eine banane, *die auf dem tisch liegt*".
  *
  * @param name
  * @tparam MR
  */
class ToModifier[MR : Graph](
  val name: String
) extends Transform[MR] {

  override def transform(mr: MR): Iterable[MR] = {
    if(mr.externalLayerCount > 0) {
      val transformed = mr.refocus(mr.externalLayers(0).head.pointer)
      Iterable(mr, transformed)
    }
    else {
      Iterable(mr)
    }
  }

}
