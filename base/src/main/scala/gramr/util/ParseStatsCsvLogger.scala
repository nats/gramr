package gramr.util

import java.io.{BufferedWriter, File, FileWriter}

import gramr.parse.evaluate.{BatchParseStats, ParseStats}

class ParseStatsCsvLogger(fileName: File) {

  import org.apache.commons.csv.CSVFormat

  val file: BufferedWriter = new BufferedWriter(new FileWriter(fileName))
  val printer = CSVFormat.DEFAULT.withHeader(
    "iterationId",
    "parserId",
    "exampleId",
    "total",
    "correct",
    "incorrect",
    "precision",
    "recall",
    "f1"
  ).print(file)

  def logExampleStats(iterationId: String, parserId: String, exampleId: String, stats: ParseStats): Unit = synchronized {
    printer.printRecord(iterationId, parserId, exampleId,
      stats.bestScoredParse.total.toString, stats.bestScoredParse.correct.toString, stats.bestScoredParse.incorrect.toString,
      stats.bestScoredParse.precision.toString, stats.bestScoredParse.recall.toString, stats.bestScoredParse.f1.toString)
    flush()
  }

  def logIterationStats(iterationId: String, parserId: String, stats: BatchParseStats): Unit = synchronized {
    printer.printRecord(iterationId, parserId, "all",
      stats.bestScoredParses.total.toString, stats.bestScoredParses.correct.toString, stats.bestScoredParses.incorrect.toString,
      stats.bestScoredParses.precision.toString, stats.bestScoredParses.recall.toString, stats.bestScoredParses.f1.toString)
    flush()
  }

  def flush(): Unit = synchronized {
    printer.flush()
  }

  def close(): Unit = {
    printer.close()
  }

}
