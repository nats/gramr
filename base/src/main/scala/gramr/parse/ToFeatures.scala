package gramr.parse

/**
  * A type class for things that features can be extracted from.
  */
trait ToFeatures[A, F] {

  def features(a: A): F

  def apply(a: A): F = features(a)

}
