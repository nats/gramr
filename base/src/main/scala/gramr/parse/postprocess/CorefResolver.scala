package gramr.parse.postprocess

import gramr.graph.Graph.ops._
import gramr.graph._
import gramr.learn.FeatureVector
import gramr.parse.chart.ChartParser

/** Adds coref-resolving actions to a chart.
  */
class CorefResolver[MR, F, C, L](
  val cp: ChartParser[MR, F, L, C]
)(implicit mgl: Graph[MR], fv: FeatureVector[F]) {

  import cp.{Action, Chart, State, actionFilter, label}

  def apply(chart: gramr.parse.chart.Chart[MR, F], context: C): gramr.parse.chart.Chart[MR, F] = {
    val concreteChart = chart.asInstanceOf[Chart]
    val processedChart = concreteChart.copy
    processedChart.outputStates.clear()
    for {
      state <- concreteChart.outputStates
      createdState <- resolve(processedChart, state, context)
    } {
      processedChart.outputStates.append(createdState)
    }
    processedChart
  }

  def resolve(chart: Chart, state: State, context: C): Option[State] = {
    val actionFilterInstance = actionFilter(context)
    val corefs = state.mr.nodes.filter(isCoref).toVector
    val emptyDecision: Option[CorefResolver.Decision[MR]] = Some(CorefResolver.Decision(state.mr, Vector.empty))
    val decision = corefs.foldLeft(emptyDecision) { (d, coref) =>
      d.flatMap { intermediate =>
        val bindings = intermediate.result.nodes.filter(!isCoref(_)).map(_.pointer).map((coref.pointer, _)).toVector
        if (bindings.isEmpty) {
          Some(intermediate)
        }
        else {
          def toAction(binding: (Int, Int)): Action = {
            val decision = CorefResolver.Decision(intermediate.unresolved, intermediate.bindings :+ binding)
            new Action(decision, label(decision, state.syntax.span, context), state.syntax,Vector(state))
          }

          val decisions = bindings.map(toAction).filter(actionFilterInstance)
          if(decisions.nonEmpty) {
            val bestDecision = decisions.maxBy(_.score).decision.asInstanceOf[CorefResolver.Decision[MR]]
            Some(bestDecision)
          }
          else {
            None
          }
        }
      }
    }

    for(d <- decision) yield {
      if(d.bindings.nonEmpty) {
        val action = new Action(d, label(d, state.syntax.span, context), state.syntax, Vector(state))
        chart.putAction(action)
      }
      else {
        state
      }
    }
  }

  def isCoref(indexed: Indexed[Node]): Boolean = {
    indexed.element.content == "<coref>"
  }
}


object CorefResolver {

  case class Decision[MR: Graph](
    unresolved: MR,
    bindings: Vector[(Int, Int)]
  ) extends gramr.parse.Decision[MR] {

    override lazy val result: MR = {
      bindings.foldLeft(unresolved) { case (mr, (coref, target)) =>
        val builder = implicitly[Graph[MR]].builder(mr)
        builder.setNormalise(false)
        for(edge <- mr.adjacentEdges(coref)) {
          builder.remove(edge.pointer)
          if(edge.element.source == coref) {
            builder.addEdge(edge.element.label, target, edge.element.target)
          }
          else if(edge.element.target == coref) {
            builder.addEdge(edge.element.label, edge.element.source, target)
          }
        }
        if(mr.root == coref) {
          builder.setRoot(target)
        }
        builder.remove(coref)
        builder.build
      }
    }

  }

}
