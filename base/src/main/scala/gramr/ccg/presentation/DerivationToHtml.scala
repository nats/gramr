package gramr.ccg.presentation

import gramr.ccg._
import gramr.util.html.ToHtml
import scalatags.Text.all._

class DerivationToHtml[L: ToHtml] extends ToHtml[Derivation[L]] {
  /**
    * Generates an HTML table representing the derivation.
    *
    * Warning: The current implementation is pretty untidy.
    */
  def toHtml(derivation: Derivation[L]): Frag = {
    import scalaz._, Scalaz._
    import scalatags.Text.all._

    // Assemble all the derivation steps into a list and order them into what will become the table’s rows.
    def enumSteps(step: DerivationStep[L]): List[DerivationStep[L]] = step :: step.subSteps.flatMap(enumSteps)

    val rowMap = enumSteps(derivation.root).groupBy(_.height)
    val rows = (0 to derivation.root.height).map { h => rowMap.getOrElse(h, Nil) } // traverse the row map in order

    // Utilities to generate individual table cells
    def mkPaddingCell(width: Int) = td(borderWidth := "0px", colspan := width)

    def mkContentCell(step: DerivationStep[L]) = {
      (step match {
        case s: CombinatoryStep[L] =>
          td(
            borderWidth := "0px", padding := 0,

            // This table makes up the separating line and the combinator display. 
            table(
              margin := 0, border := 0, marginTop := "-6px", width := "100%", tr(
                border := 0,
                td(
                  border := 0, padding := 0, width := "100%",
                  div(height := "1px", width := "100%", backgroundColor := "black")
                ), // the line
                td(
                  border := 0, padding := 0, paddingLeft := "2px", div(
                    font := "10px sans-serif",
                    s.combinator.display
                  )
                )
              )
            ),

            // The syncat, located below the line.
            div(
              width := "100%",
              marginTop := "-4px",
              textAlign := "center",
              s.synCat.toString, br,
              implicitly[ToHtml[L]].toHtml(s.label)
            ) // label

          )

        case s: LexicalStep[L] =>
          td(
            borderWidth := "0px", textAlign := "center",
            b(s.tokens.map(_.text).intersperse(" ").concatenate), br,
            s.synCat.toString, br,
            implicitly[ToHtml[L]].toHtml(s.label)
          ) // label
      }) (
        // common attributes for all kinds of steps
        colspan := step.span.size,
        paddingBottom := "6px" // for some space between the syncat and the line
      )
    }

    // Generates the table cells for a whole row (or its remainder; it’s recursive).
    // index is the current token index.
    def rowTds(row: List[DerivationStep[L]], index: Int): List[Frag] = row match {
      case step :: rest =>
        // Optionally generate padding, then the content cell
        val padding =
          if (step.span.start == index) Nil
          else List(mkPaddingCell(step.span.start - index))
        val entry = List(mkContentCell(step))
        padding ++ entry ++ rowTds(rest, step.span.end + 1)
      case Nil => // Generate a last padding cell if needed
        if (index > derivation.root.span.end) Nil
        else List(mkPaddingCell(derivation.root.span.end - index + 1))
    }

    table(
      borderWidth := "0px",
      rows map { row =>
        tr(
          borderWidth := "0px",
          rowTds(row, 0)
        )
      }
    )
  }

}
