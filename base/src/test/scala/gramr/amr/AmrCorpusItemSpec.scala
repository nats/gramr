package gramr.amr

import gramr.ccg.{CombinatorSystem, EasyccgCombinatorSystem}
import org.scalatest.FlatSpec

class AmrCorpusItemSpec extends FlatSpec {

  val dummyAmr: AmrTree =
    InnerNode("w", List(
      Link(":instance", Leaf("world")),
      Link(":mod", InnerNode(
        "h", List(
          Link(":instance", Leaf("hello"))
        )
      ))
    ))

  val dummyDerivation: String = "(<T NP 0 2> (<T NP 0 1> (<T N 1 2> (<L N/N POS POS Establishing N/N>) (<L N POS POS Models N>) ) ) (<T NP\\NP 0 2> (<L (NP\\NP)/NP POS POS in (NP\\NP)/NP>) (<T NP 0 1> (<T N 1 2> (<L N/N POS POS Industrial N/N>) (<L N POS POS Innovation N>) ) ) ) )"

  "Limiting CCG derivations" should "remove the last extra derivations" in {
    implicit val cs: CombinatorSystem = new EasyccgCombinatorSystem()
    val attributeStrings = Vector(
      "ccg-derivation-0" -> dummyDerivation,
      "ccg-derivation-1" -> dummyDerivation,
      "ccg-derivation-2" -> dummyDerivation,
      "ccg-derivation-3" -> dummyDerivation,
      "ccg-derivation-4" -> dummyDerivation,
      "ccg-derivation-5" -> dummyDerivation,
      "ccg-derivation-6" -> dummyDerivation,
      "ccg-derivation-7" -> dummyDerivation,
      "ccg-derivation-8" -> dummyDerivation,
      "ccg-derivation-9" -> dummyDerivation,
      "ccg-derivation-10" -> dummyDerivation,
      "ccg-derivation-11" -> dummyDerivation,
    )
    val unlimited = new AmrCorpusItem(dummyAmr, attributeStrings)
    assert(unlimited.ccgDerivations.size == 12)

    val limited = unlimited.limitCcgDerivations(3)
    assert(limited.ccgDerivations.size == 3)

    assert(limited.ccgDerivations.map(_._1).toSet == Set("0", "1", "2"))
  }

  "Limiting CCG derivations" should "remove the numerically lowest derivations" in {
    implicit val cs: CombinatorSystem = new EasyccgCombinatorSystem()
    val attributeStrings = Vector(
      "ccg-derivation-5" -> dummyDerivation,
      "ccg-derivation-0" -> dummyDerivation,
      "ccg-derivation-11" -> dummyDerivation,
      "ccg-derivation-4" -> dummyDerivation,
      "ccg-derivation-9" -> dummyDerivation,
      "ccg-derivation-2" -> dummyDerivation,
      "ccg-derivation-10" -> dummyDerivation,
      "ccg-derivation-1" -> dummyDerivation,
      "ccg-derivation-8" -> dummyDerivation,
      "ccg-derivation-3" -> dummyDerivation,
      "ccg-derivation-6" -> dummyDerivation,
      "ccg-derivation-7" -> dummyDerivation,
    )
    val unlimited = new AmrCorpusItem(dummyAmr, attributeStrings)
    assert(unlimited.ccgDerivations.size == 12)

    val limited = unlimited.limitCcgDerivations(3)
    assert(limited.ccgDerivations.size == 3)

    assert(limited.ccgDerivations.map(_._1).toSet == Set("0", "1", "2"))
  }

}