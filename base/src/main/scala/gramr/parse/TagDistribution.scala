package gramr.parse

case class TagDistribution(
  tags: Array[String],
  confidences: Array[Double]
)

object TagDistribution {

  def fromAttributeString(attributeString: String): Seq[TagDistribution] = {
    val tokenDistStrings = attributeString.split(" ")
    val tokenDists = tokenDistStrings.map { s =>
      val entries = s.split(",").map { es =>
        val components = es.split("""\|""")
        (components(0), components(1).toDouble)
      }
      val (tags, confidences) = entries.unzip
      TagDistribution(tags, confidences)
    }
    tokenDists
  }

}
