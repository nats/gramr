package gramr.parse
import gramr.parse.chart.Chart
import gramr.text.Token

class FallbackParser[MR, FV, C](
  val parsers: Iterable[Parser[MR, FV, C]]
) extends Parser[MR, FV, C] {
  override def parse(sentence: Vector[Token], context: C): Chart[MR, FV] = {
    parseWith(parsers.head, parsers.tail, sentence, context)
  }

  def parseWith(parser: Parser[MR, FV, C], fallbacks: Iterable[Parser[MR, FV, C]], sentence: Vector[Token], context: C): Chart[MR, FV] = {
    val chart = parser.parse(sentence, context)
    // Should & can we fall back?
    if(chart.outputStates.isEmpty && fallbacks.nonEmpty) {
      parseWith(fallbacks.head, fallbacks.tail, sentence, context)
    }
    else {
      chart
    }
  }
}
