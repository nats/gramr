package gramr.split.em

import gramr.ccg.{LexiconKey, StoredLexicon}
import gramr.graph.Graph
import gramr.split.SplitChart

import scala.util.Random
import Graph.ops._
import gramr.split.em.LexicalizedEstimator.Parameters

case class LexicalizedModel[MR : Graph](
  parameters: Parameters,
  lexicon: StoredLexicon[MR]
) {

  import LexicalizedModel._

  def score(entry: SplitChart.Entry[MR], chart: SplitChart[MR]): Double = parameters(lexEntryId(chart, lexicon)(entry))

  def update(counts: Parameters): LexicalizedModel[MR] = LexicalizedModel[MR](counts.normalize, lexicon)

}

object LexicalizedModel {

  def empty[MR : Graph](lexicon: StoredLexicon[MR]): LexicalizedModel[MR] = LexicalizedModel(Parameters(Map.empty), lexicon)

  def randomUniform[MR : Graph](lexicon: StoredLexicon[MR]): LexicalizedModel[MR] = {
    val rnd = new Random()

    val parametersMap = lexicon.items.map(item => (item.index, rnd.nextDouble())).toMap
    val parametersSum = parametersMap.values.sum
    val parameters = Parameters(parametersMap.mapValues(v => v / parametersSum).withDefault(_ => 0.0))
    LexicalizedModel(parameters, lexicon)
  }

  implicit def emModel[MR : Graph]: EMModel[LexicalizedModel[MR], MR, Parameters] = new EMModel[LexicalizedModel[MR], MR, Parameters] {

    override def ops(model: LexicalizedModel[MR]): EMModelOps[LexicalizedModel[MR], MR, Parameters] = emModelOps[MR](model)

    override def count(model: LexicalizedModel[MR], chart: SplitChart[MR])(count: Double, entry: SplitChart.Entry[MR]): Parameters = {
      val lexId = lexEntryId(chart, model.lexicon)(entry)
      Parameters(Map(lexId -> count))
    }

    override def update(model: LexicalizedModel[MR], counts: Parameters): LexicalizedModel[MR] = {
      model.update(counts)
    }

    override def emptyCounts: Parameters = Parameters.empty

    override def addCounts(c1: Parameters, c2: Parameters): Parameters =
      Parameters.add(c1, c2)

  }

  def emModelOps[M : Graph](model: LexicalizedModel[M]): EMModelOps[LexicalizedModel[M], M, Parameters] = new EMModelOps[LexicalizedModel[M], M, Parameters] {

    def score(entry: SplitChart.Entry[M], chart: SplitChart[M]): Double = model.score(entry, chart)

    def update(counts: Parameters): LexicalizedModel[M] = model.update(counts)

  }

  /** Look up the lexicon entry ID for a given split chart entry */
  def lexEntryId[MR : Graph](chart: SplitChart[MR], lexicon: StoredLexicon[MR])(entry: SplitChart.Entry[MR]): Int = {
    val tokenEntries = lexicon.get(LexiconKey(entry.partialMR.span.in(chart.sentence)))
    val matchingTokenEntries = tokenEntries.filter { lexEntry =>
      entry.partialMR.synCats.exists(_ matches lexEntry.synCat) && (lexEntry.meaning ~= entry.partialMR.mr)
    }
    matchingTokenEntries.headOption match {
      case Some(h) => h.index
      case None => -1 // Use -1 as a catch-all bucket
    }
  }

}
