package gramr.parse

import gramr.ccg.{CombinatorSystem, SyntacticCategory}
import gramr.learn.FeatureVector
import gramr.parse.chart.ScoredFeatures
import gramr.parse.cky.CkyParser
import gramr.parse.scorer.{MRScorer, ModelScorer}
import gramr.text.Span

package object cky {

  def learned[MR, FV: FeatureVector, C](
    lexicalChoice: LexicalChoice[C, MR],
    binaryChoice: BinaryChoice[MR],
    combinatorSystem: CombinatorSystem,
    decisionFeatures: (Decision[MR], C) => FV,
    stateFeatures: (MR, C) => FV,
    modelScore: FV => Double,
    actionFilter: ActionPredicate[MR, FV, C],
    leafCategories: (C, Span) => Iterable[SyntacticCategory],
    dynamicBeamSize: Span => Int,
    branchingFactor: Option[Double],
    limitLexicalActions: Boolean = true,
    limitUnaryActions: Boolean = true
  ): CkyParser[MR, FV, ScoredFeatures[FV], C] = {

    val scorer = new ModelScorer[MR, FV, C](decisionFeatures, stateFeatures, modelScore)

    new CkyParser[MR, FV, ScoredFeatures[FV], C](
      lexicalChoice,
      binaryChoice,
      combinatorSystem,
      scorer,
      actionFilter,
      leafCategories,
      dynamicBeamSize,
      branchingFactor,
      limitLexicalActions,
      limitUnaryActions
    )
  }

  def heuristic[MR, FV: FeatureVector, C](
    lexicalChoice: LexicalChoice[C, MR],
    binaryChoice: BinaryChoice[MR],
    combinatorSystem: CombinatorSystem,
    decisionFeatures: (Decision[MR], C) => FV,
    stateFeatures: (MR, C) => FV,
    score: C => (MR, Span) => Double,
    actionFilter: ActionPredicate[MR, FV, C],
    leafCategories: (C, Span) => Iterable[SyntacticCategory],
    dynamicBeamSize: Span => Int,
    branchingFactor: Option[Double],
    limitLexicalActions: Boolean = true,
    limitUnaryActions: Boolean = true
  ): CkyParser[MR, FV, ScoredFeatures[FV], C] = {

    val scorer = new MRScorer[MR, FV, C](decisionFeatures, stateFeatures, score)

    new CkyParser[MR, FV, ScoredFeatures[FV], C](
      lexicalChoice,
      binaryChoice,
      combinatorSystem,
      scorer,
      actionFilter,
      leafCategories,
      dynamicBeamSize,
      branchingFactor,
      limitLexicalActions,
      limitUnaryActions
    )
  }

}
