package gramr.util

/** Encapsulates a cached value.
  */
class Cache[V](val calculate: () => V) {

  var cachedValue: Option[V] = None

  def value: V = synchronized {
    cachedValue match {
      case Some(v) => v
      case None =>
        val v = calculate()
        cachedValue = Some(v)
        v
    }
  }

  def invalidate(): Unit = synchronized {
    cachedValue = None
  }

  /** Lazily map the cache's value.
    */
  def map[W](f: V => W): Cache[W] = {
    val current = this
    new Cache(() => f(current.value))
  }

}

object Cache {

  def apply[V](calculate: () => V) = new Cache(calculate)

}
